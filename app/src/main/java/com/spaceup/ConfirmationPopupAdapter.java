package com.spaceup;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spaceup.uninstall.activities.AppInfo;
import com.spaceup.uninstall.activities.DataTransferInterface;

import java.util.List;

/**
 * Created by shashanktiwari on 08/03/17.
 */



/**
 * Created by Sajal Jain on 08-11-2016.
 */

public class ConfirmationPopupAdapter extends RecyclerView.Adapter<ConfirmationPopupAdapter.ViewHolder> {

    public Context context;
    DataTransferInterface dtInterface;
    private List<AppInfo> ignoreList;

    public ConfirmationPopupAdapter(Context context, List<AppInfo> ignoreList, DataTransferInterface dtInterface) {
        this.context = context;
        this.ignoreList = ignoreList;
        this.dtInterface = dtInterface;
    }

    @Override
    public ConfirmationPopupAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.confirmation_popup_item, parent, false);
        return new ConfirmationPopupAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ConfirmationPopupAdapter.ViewHolder holder, final int position) {

        Object arr[] = new Object[2];

        final AppInfo app = ignoreList.get(position);
        arr[0] = app;
        arr[1] = holder;

        new ConfirmationPopupAdapter.ImageLoader().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, arr);

        holder.checkbox.setClickable(false);
        holder.appName.setText(app.getAppName());

        if (app.isChecked()) {
            holder.checkbox.setChecked(true);
        } else {
            holder.checkbox.setChecked(false);
        }

        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("adapt", "onClick: " + holder.checkbox.isChecked());
                if (holder.checkbox.isChecked()) {
                    Log.d("adapt", "onClick: " + "I am here 1");
                    app.setChecked(false);
                    holder.checkbox.setChecked(false);
                    dtInterface.setValues(app, 5, position);
                } else {
                    Log.d("adapt", "onClick: " + "I am here 2");
                    app.setChecked(true);
                    holder.checkbox.setChecked(true);
                    dtInterface.setValues(app, 6, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return ignoreList.size();
    }

    String getSizeinMB(long size) {
        if (size > 1024) {
            size = size / 1024;
            if (size > 1024) {
                size = size / 1024;
                if (size > 1024) {
                    size = size / 1024;
                    return size + "GB";
                }
                return (size) + " MB";
            }
            return size + " KB";
        }
        return size + " Bytes";
    }

    public List<AppInfo> getList() {
        return ignoreList;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView appIcon;
        public TextView appName;
        public RelativeLayout row;
        public CheckBox checkbox;

        public ViewHolder(View view) {
            super(view);
            appIcon = (ImageView) view.findViewById(R.id.icon);
            appName = (TextView) view.findViewById(R.id.app_name);
            checkbox = (CheckBox) view.findViewById(R.id.check_box);
            row = (RelativeLayout) view.findViewById(R.id.row);
        }
    }

    class ImageLoader extends AsyncTask<Object, Void, Drawable> {
        ConfirmationPopupAdapter.ViewHolder viewHolder;

        @Override
        protected void onPostExecute(Drawable drawable) {
            super.onPostExecute(drawable);
            viewHolder.appIcon.setImageDrawable(drawable);
        }

        @Override
        protected Drawable doInBackground(Object... params) {
            Log.d("opt", "called in a queue");
            AppInfo app = (AppInfo) params[0];
            ConfirmationPopupAdapter.ViewHolder viewHolder = (ConfirmationPopupAdapter.ViewHolder) params[1];
            this.viewHolder = viewHolder;
            Drawable icon = null;
            try {
                icon = context.getPackageManager().getApplicationIcon(app.getPkgName());
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            return icon;
        }
    }


}
