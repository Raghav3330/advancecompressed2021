package com.spaceup.apkgenerator.constants;

import android.os.Environment;

import com.spaceup.R;
import com.spaceup.RemoteConfig;

/**
 * @brief Constants related project directories, preferences and keys
 * <p/>
 * Created by Dhruv on 16/11/16.
 */
public class Constants {

    public static final String WHITE_LIST_FILE_ASSET_NAME = "financeApps.txt";

    public static final String WHITE_LIST_FILE_PATH = "/db/whitelist.stash";

    public final static String STASH = "/.Stash/";

    public final static String UNZIP = Environment.getExternalStorageDirectory().getPath() + STASH + "unzipped/";

    public final static String APK_DB = STASH + "db/";

    public final static String STASH_DB_FILE = "residual";

    public final static String GOOGLE_PLAY_APP_URL = "Hey, hi! you know i just tried out an app that lets you *Install 3 Times More Apps*. _It actually Increases your Phone's Storage Space_ 😎 Try it! I think you'll find it useful\nhttps://goo.gl/Y5kXC8";

    public final static String MIME_TYPE_TEXT = "text/plain";

    public final static String RECYCLE_BIN_FOLDER = Environment.getExternalStorageDirectory().getPath() + STASH + "WhatsappDump/";

    public final static String WHATSAPP_MEDIA_PATH = Environment.getExternalStorageDirectory().toString() + RemoteConfig.getString(R.string.whatsapp_path);


    //0 theme is blue
    //1 theme is red
    public final static String THEME = "theme";
    //path of residual file
    public final static String RESIDUAL_FILE = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + "/" + Constants.STASH_DB_FILE + ".json";


    // notification type depending on alarm event
    public static final String NOTIFICATION_TYPE = "type";
    public static final String REMOVE_UNCOMPRESS_NOTIFICATION = "remove_uncompress";

    public static final String NOT_COMPRESS_WITHIN_FIVE_NOTIFICATION = "not_compress_within_five";
    public static final int UNCOMPRESSION_NOTIFICATION_ID = 69;

    //public static final String UNINSTALL_PROGRESS = "uninstall_progress";
    public static final String JSON_VERSION = "version_code";
    public static final int NETWORK_ERROR = 404;
    public static final String WHATSAPP_PKG = "com.whatsapp";
    public static final String TITLE_PARAMETER = "title_parameter";
    public static final String URL_PARAMETER = "url_parameter";
    public static final String INCOME_FROM_ONBOARDING = "income_from_onboarding";
    public static final String PACKAGE_NAME = "package_name";
    public static final int LOAD_ADVERTISEMENT = 120;


    //allow reading "Tap on the top right hand toggle to enable SpaceUp" while taking accessibility permission
    public static boolean REQUEST_TURN_ON = false;

    public static final int WARNING_THEME_DEFAULT = 1;
    public static final int WARNING_THEME_RED = 2;

    //to differentiate between dummy apps and original apps
    public static final String DUMMY_APP_VERSION_NAME = "1.stash";
    public static final String CURRENT_PKG_NAME = "com.spaceup";

    //---App Table--- COMPRESS STATUS STATES
    public static final int COMPRESSED = 1;
    public static final int UNCOMPRESSED = -1;
    public static final int QUICK_COMPRESSED = 0;
    public static final int SYSTEM_APP = -2;
    public static final int APP_UNINSTALLED = -4;


    //---Images table--- RESPONSE STATUS : column values
    public static final int SCANNED = 1;
    public static final int NOT_SCANNED = 2; //[DEFAULT]
    public static final int TRASH = 3;
    public static final int RECOVERED = 4;
    public static final int JUNK_NOT_TRASH = 8;

    //---Images table--- ACTION STATUS : column values
    public static final int JUNK = 5;
    public static final int NOT_JUNK = 6; // [DEFAULT]
    public static final int IGNORED = 7;
    public static final int NOT_JUNK_NOT_TRASH = 9;


    //Apk extension
    public static final String APK_EXTENSION = ".stash";

    //JSON FILE KEYS
    public static final String LAST_MODIFIED_DATE = "last_modified_date";
    public static final String PKGNAME = "pkgname";
    public static final String SIZE = "size";
    public static final String STATUS = "status";

    //TAG's
    public static final String JSON_TAG = "jsonfile";

    public static final String WHATSAPP_IMAGES_PATH = Environment.getExternalStorageDirectory().getPath() + "/WhatsApp/Media/WhatsApp Images";


    public class Receiver {
        public static final String DUMMY_INSTALL_RECEIVER = "com.times.eventDummyAppRemovedNew";
        public static final String APP_UNINSTALLED_BROADCAST = "app_uninstalled_broadcast";
        public static final String APP_INSTALLED_BROADCAST = "app_installed_broadcast";

    }

    public class CUSTOM_CRASH {
        public static final String UNINSTALL_RETRY_OCCUR = "uninstall_retry_occur";
        public static final String INSTALL_RETRY_OCCUR = "install_retry_occur";
        public static final String INSTALL_FILE_NOT_FOUND = "install_file_not_found";


    }
}
