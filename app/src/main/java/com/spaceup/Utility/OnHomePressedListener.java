package com.spaceup.Utility;

/**
 * Created by dhurv on 04-12-2017.
 */

public interface OnHomePressedListener {
    public void onHomePressed();

    public void onHomeLongPressed();
}