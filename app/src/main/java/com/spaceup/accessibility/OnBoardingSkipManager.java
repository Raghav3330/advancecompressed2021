package com.spaceup.accessibility;

import android.accessibilityservice.AccessibilityService;
import androidx.annotation.UiThread;
import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;

import com.spaceup.Utility.UtilityMethods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.view.accessibility.AccessibilityNodeInfo.ACTION_CLICK;
import static android.view.accessibility.AccessibilityNodeInfo.ACTION_SCROLL_FORWARD;

/**
 * Created by Ananda.Kumar on 18-03-2017.
 */

public class OnBoardingSkipManager {

    private static OnBoardingSkipManager sInstance;
    private final String TAG = "OnBoardingSkipManager";
    private final String TEXT_JUNK = "Junk";
    private final String TEXT_GRANT = "Grant";
    private final String TEXT_SHOP = "Shop";
    private final String TEXT_LATER = "Later";
    private final String TEXT_ALLOW = "Allow";
    private final String TEXT_SKIP = "Skip";
    private final String TEXT_NEXT = "Next";
    private final String TEXT_CLOSE = "Close";
    private final String TEXT_SWIPE = "Swipe";
    private final String TEXT_CANCEL = "Cancel";
    private final String TEXT_ACCEPT = "Accept";
    private final String TEXT_CONTINUE = "Continue";
    private final String TEXT_START = "Start";
    private final String TEXT_OK = "Ok";
    private final String TEXT_BEGIN = "Begin";
    private final String TEXT_GO = "Go";
    private final String TEXT_SAVE = "Save";
    private final String TEXT_ENGLISH = "English";
    private final String TEXT_DONE = "Done";
    private final String TEXT_AGREE = "Agree";
    private final String TEXT_GOT_IT = "GOT IT";
    private final String BACK = "PRESS_BACK";
    private final String TEXT_NO = "No";
    private final String TEXT_USE_NOW = "Use Now";
    private final String TEXT_SCROLL = "Scroll";
    private final String TEXT_HOME = "Home";
    private List<String> mDefaultTextList = Arrays.asList(TEXT_SKIP, TEXT_NEXT, TEXT_CLOSE, TEXT_OK,
            TEXT_ACCEPT, TEXT_CONTINUE, TEXT_START, TEXT_CANCEL, TEXT_BEGIN, TEXT_GO, TEXT_ALLOW, TEXT_SCROLL);
    private Map<String, List<String>> mSkipListMap;

    private OnBoardingSkipManager() {
        initialize();
    }

    @UiThread
    public static OnBoardingSkipManager getInstance() {
        if (sInstance == null) {
            sInstance = new OnBoardingSkipManager();
        }
        return sInstance;
    }

    @UiThread
    public static void start() {
        getInstance();
    }

    public static void stop() {
        sInstance = null;
        Log.d("press", "Stopped");
    }

    private void initialize() {
        mSkipListMap = new HashMap<>();
        mSkipListMap.put("com.lenovo.anyshare.gps", Arrays.asList(TEXT_ALLOW, TEXT_START, TEXT_SAVE));
        mSkipListMap.put("com.mxtech.videoplayer.ad", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("com.jio.myjio", Arrays.asList(TEXT_ALLOW));
        mSkipListMap.put("cn.xender", Arrays.asList(TEXT_ALLOW, TEXT_SKIP));
        mSkipListMap.put("in.amazon.mShop.android.shopping", Arrays.asList(TEXT_ALLOW, TEXT_SKIP));
        mSkipListMap.put("com.jio.jioplay.tv", Arrays.asList(TEXT_ALLOW, TEXT_SKIP));
        mSkipListMap.put("com.flipkart.android", Arrays.asList(TEXT_ALLOW, TEXT_CANCEL, TEXT_CLOSE));
        mSkipListMap.put("in.startv.hotstar", Arrays.asList(TEXT_ALLOW, TEXT_SKIP));
        mSkipListMap.put("com.estrongs.android.pop", Arrays.asList(TEXT_ALLOW));
        mSkipListMap.put("com.myairtelapp", Arrays.asList(TEXT_ALLOW));
        mSkipListMap.put("com.jio.media.ondemand", Arrays.asList(TEXT_ALLOW, TEXT_SKIP));
        mSkipListMap.put("com.domobile.applock", Arrays.asList(TEXT_ALLOW));
        mSkipListMap.put("com.opera.mini.native", Arrays.asList(TEXT_ALLOW, TEXT_ENGLISH, TEXT_SKIP));
        mSkipListMap.put("com.cricbuzz.android", Arrays.asList(TEXT_ALLOW));
        mSkipListMap.put("cn.wps.moffice_eng", Arrays.asList(TEXT_ALLOW, TEXT_START));
        mSkipListMap.put("org.videolan.vlc", Arrays.asList(TEXT_ALLOW));
        mSkipListMap.put("com.jio.media.jiobeats", Arrays.asList(TEXT_ALLOW, TEXT_SKIP));
        mSkipListMap.put("com.supercell.clashofclans", Arrays.asList(TEXT_ALLOW));
        mSkipListMap.put("com.appstar.callrecorder", Arrays.asList(TEXT_ALLOW, TEXT_NEXT, TEXT_DONE));
        mSkipListMap.put("com.olx.southasia", Arrays.asList(TEXT_ALLOW, TEXT_NEXT, TEXT_START));
        mSkipListMap.put("com.atomic.apps.ringtone.cutter", Arrays.asList(TEXT_ALLOW));
        mSkipListMap.put("com.snapdeal.main", Arrays.asList(TEXT_ALLOW));
        mSkipListMap.put("com.lbe.parallel.intl", Arrays.asList(TEXT_ALLOW, TEXT_START));
        mSkipListMap.put("com.intsig.camscanner", Arrays.asList(TEXT_ALLOW, TEXT_USE_NOW));
        mSkipListMap.put("com.google.android.apps.translate", Arrays.asList(TEXT_ALLOW));
        mSkipListMap.put("com.bt.bms", Arrays.asList(TEXT_ALLOW, TEXT_START, TEXT_SKIP));
        mSkipListMap.put("com.mventus.selfcare.activity", Arrays.asList(TEXT_ALLOW));
        mSkipListMap.put("com.adobe.reader", Arrays.asList(TEXT_ALLOW, TEXT_SCROLL, TEXT_START));
        mSkipListMap.put("com.commsource.beautyplus", Arrays.asList(TEXT_ALLOW, TEXT_SCROLL, TEXT_HOME));
        mSkipListMap.put("com.cyberlink.youperfect", Arrays.asList(TEXT_ALLOW, TEXT_SCROLL, TEXT_START));

        mSkipListMap.put("com.venticake.retrica", Arrays.asList(TEXT_OK, TEXT_ALLOW, TEXT_ALLOW, TEXT_ALLOW, TEXT_ALLOW));
        mSkipListMap.put("com.fundevs.app.mediaconverter", Arrays.asList(TEXT_ALLOW, TEXT_OK, TEXT_ALLOW, TEXT_CLOSE, TEXT_SWIPE));
        mSkipListMap.put("free.mobile.internet.data.recharge", Arrays.asList(TEXT_OK, TEXT_ALLOW, TEXT_ALLOW, TEXT_ALLOW));
        mSkipListMap.put("com.linecorp.b612.android", Arrays.asList(TEXT_SKIP, TEXT_SKIP, TEXT_ALLOW, TEXT_ALLOW, TEXT_ALLOW, TEXT_AGREE));
        mSkipListMap.put("com.twitter.android", Arrays.asList(TEXT_ALLOW, TEXT_OK));   //TODO
        mSkipListMap.put("com.utorrent.client", Arrays.asList(TEXT_ALLOW, TEXT_ALLOW, TEXT_GOT_IT, BACK));
        mSkipListMap.put("com.eterno", Arrays.asList(TEXT_ENGLISH, TEXT_CONTINUE));
        mSkipListMap.put("com.joeware.android.gpulumera", Arrays.asList(TEXT_ALLOW, TEXT_ALLOW, TEXT_NO));
        mSkipListMap.put("com.mobikwik_new", Arrays.asList(TEXT_SKIP, TEXT_ALLOW, TEXT_SKIP, TEXT_ALLOW));
        mSkipListMap.put("com.sec.android.mimage.photoretouching", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("com.saavn.android", Arrays.asList(TEXT_DONE, TEXT_NEXT, TEXT_GOT_IT, BACK));
        mSkipListMap.put("com.cam001.selfie", Arrays.asList(TEXT_ALLOW));
        mSkipListMap.put("com.mxtech.videoplayer.pro", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("com.instagram.android", Arrays.asList(TEXT_CONTINUE));
        mSkipListMap.put("mobi.infolife.appbackup", new ArrayList<String>());
        mSkipListMap.put("com.android.vending.billing.InAppBillingService.LOCK", new ArrayList<String>());
        mSkipListMap.put("com.niksoftware.snapseed", new ArrayList<String>());
        mSkipListMap.put("com.don.offers", Arrays.asList(TEXT_SKIP, TEXT_ALLOW, TEXT_ALLOW, TEXT_ALLOW, TEXT_ALLOW, TEXT_SKIP, BACK, TEXT_ENGLISH));
        mSkipListMap.put("com.balancehero.truebalance", Arrays.asList(TEXT_OK)); // TODO
        mSkipListMap.put("com.shopclues", new ArrayList<String>());

        mSkipListMap.put("com.king.candycrushsaga", new ArrayList<String>());
        mSkipListMap.put("company.fortytwo.slide.app", Arrays.asList(TEXT_START));
        mSkipListMap.put("com.thinkyeah.galleryvault", Arrays.asList(TEXT_START));
        mSkipListMap.put("cris.org.in.prs.ima", Arrays.asList(TEXT_ALLOW, TEXT_ALLOW));
        mSkipListMap.put("sun.way2sms.hyd.com", Arrays.asList(TEXT_SCROLL, TEXT_SCROLL, TEXT_SCROLL, TEXT_SCROLL, TEXT_ENGLISH, TEXT_OK));
        mSkipListMap.put("com.myntra.android", Arrays.asList(TEXT_SHOP, TEXT_LATER));
        mSkipListMap.put("cn.xender", Arrays.asList(TEXT_ALLOW, TEXT_SCROLL, TEXT_SKIP, TEXT_ALLOW, TEXT_ALLOW, TEXT_ALLOW));
        mSkipListMap.put("com.cleanmaster.mguard", Arrays.asList(TEXT_START, TEXT_JUNK, TEXT_GRANT, TEXT_ALLOW));
        mSkipListMap.put("com.ubercab", Arrays.asList(TEXT_ALLOW, TEXT_OK));

        
        mSkipListMap.put("livio.pack.lang.en_US", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("mobi.infolife.appbackup", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("com.miniclip.eightballpool", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("com.mobond.mindicator", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("cris.icms.ntes", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("com.sec.android.easyMover", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("com.hecorat.screenrecorder.free", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("com.nextwave.wcc2", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("com.tester.wpswpatester", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("naukriApp.appModules.login", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("org.zwanoo.android.speedtes", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("com.UCMobile", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("com.meitu.beautyplusme", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("org.blackmart.market", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("com.sec.android.app.music", Arrays.asList(TEXT_ALLOW, TEXT_OK));
        mSkipListMap.put("com.picsart.studio", Arrays.asList(TEXT_ALLOW));


    }

    public void skip(AccessibilityNodeInfo pNodeInfo, String pPackageName, boolean isForM) {
        Log.d("TAG", "PACKAGE_ON_BOARDING" + pPackageName);
        List<String> textList = mSkipListMap.get(pPackageName);
        if (isForM) {
            if (textList != null) {
                if (!textList.isEmpty() && !skipForM(pNodeInfo, textList)) {
                    skipForM(pNodeInfo, mDefaultTextList);
                }
            } else {
                skipForM(pNodeInfo, mDefaultTextList);
            }
        } else {
            if (textList != null) {
                if (!textList.isEmpty() && !skip(pNodeInfo, textList)) {
                    skip(pNodeInfo, mDefaultTextList);
                }
            } else {
                skip(pNodeInfo, mDefaultTextList);
            }
        }
    }

    private boolean skipForM(AccessibilityNodeInfo pNodeInfo, List<String> pTextList) {
        List<AccessibilityNodeInfo> list;
        Log.d(TAG, "TO_Press " + pTextList.toString());

        for (String text : pTextList) {
            Log.d(TAG, "Pressed Searching" + text);
            if (TEXT_SCROLL.equals(text)) {
                if (scroll(pNodeInfo)) {
                    return true;
                } else {
                    continue;
                }
            }
            list = pNodeInfo.findAccessibilityNodeInfosByText(text);
            if (UtilityMethods.isNotEmptyCollection(list)) {

                for (AccessibilityNodeInfo node : list) {
                    Log.d(TAG, "Pressed " + text + " of uncompressed app");
                    AccessibilityAutomation.getSharedInstance().flick();
                    if (text.equals(BACK)) {
                        performEvent(node, AccessibilityService.GLOBAL_ACTION_BACK);
                    } else {
                        performEvent(node, ACTION_CLICK);
                    }

                }
            }
        }
        return false;
    }

    private boolean skip(AccessibilityNodeInfo pNodeInfo, List<String> pTextList) {
        List<AccessibilityNodeInfo> list;
        int size = pTextList.size();
        for (int count = 0; count < size; count++) {
            String text = pTextList.get(count);
            if (TEXT_SCROLL.equals(text)) {
                if (scroll(pNodeInfo)) {
                    return true;
                } else {
                    continue;
                }
            }
            list = pNodeInfo.findAccessibilityNodeInfosByText(text);
            if (UtilityMethods.isNotEmptyCollection(list)) {
                for (AccessibilityNodeInfo node : list) {
                    Log.d(TAG, "Pressed " + text + " of uncompressed app");
                    if (node.isEnabled() && node.isClickable()) {
                        node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void performEvent(AccessibilityNodeInfo node, int event) {

        while (node != null) {

            if (node.isClickable()) {
                node.performAction(event);
                Log.d("ACCESSIBILITY_SCROLL", "Pressed SCROLL");

                return;
            } else {
                Log.d("ACCESSIBILITY_SCROLL", "NOT SCROLLABLRE");

            }

            node = node.getParent();

        }
    }

    private boolean scroll(AccessibilityNodeInfo node) {

        AccessibilityNodeInfo tempNode = node;

        // Checking Childs of Current Node
        int idx = 0;
        while (tempNode != null) {
            if (tempNode.isScrollable() && tempNode.performAction(ACTION_SCROLL_FORWARD)) {
                AccessibilityAutomation.getSharedInstance().flick();
                return true;
            }

            if (tempNode.getChildCount() > idx)
                tempNode = tempNode.getChild(idx);
            else
                tempNode = null;
            idx++;
        }

        // Checking Parent of Current Node
        while (node != null) {
            if (node.isScrollable() && node.performAction(ACTION_SCROLL_FORWARD)) {
                AccessibilityAutomation.getSharedInstance().flick();
                return true;
            }
            node = node.getParent();
        }
        return false;
    }
}
