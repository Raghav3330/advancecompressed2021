package com.spaceup.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.Utility.PrefManager;

public class DayCountService extends Service {

    private final String TAG = "DayCountService";

    public DayCountService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        PrefManager prefManager = PrefManager.getInstance(this);
        String dayCount = prefManager.setDayCount() + "";
        Log.d(TAG, "Started Service day num : " + dayCount);
        //GA Analytics
        Bundle gaParams = new Bundle();
        gaParams.putString("category", AnalyticsConstants.Category.DAY_COUNT);
        gaParams.putString("action", dayCount);
        gaParams.putInt("cd", AnalyticsConstants.CustomDim.DAY_COUNT);
        gaParams.putString("cd_value", dayCount);
        new AnalyticsHandler().logGAEvent(gaParams);
        Log.d(AnalyticsConstants.GATAG, gaParams.toString());


        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }
}
