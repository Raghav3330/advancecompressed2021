package com.spaceup.services;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import com.spaceup.Utility.RecursiveFileObserver;

import static android.os.FileObserver.CLOSE_WRITE;
import static android.os.FileObserver.CREATE;
import static android.os.FileObserver.OPEN;


public class ObserverService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.d("ObserverService", "onCreate");
        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new RecursiveFileObserver(Environment.getExternalStorageDirectory().getPath()).startWatching();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d("ObserverService", "onDESTROY");
        Intent broadcastIntent = new Intent("ac.in.ActivityRecognition.RestartSensor");
        sendBroadcast(broadcastIntent);
        super.onDestroy();
    }
}
