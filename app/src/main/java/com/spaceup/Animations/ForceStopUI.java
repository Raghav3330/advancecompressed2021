package com.spaceup.Animations;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.spaceup.R;
import com.spaceup.accessibility.AccessibilityAutomation;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by dhurv on 24-01-2017.
 */
public class ForceStopUI {
    Context context;
    WindowManager wm1;
    View oView1;
    AccessibilityAutomation sSharedInstance;

    public ForceStopUI(Context context, AccessibilityAutomation sSharedInstance) {
        this.context = context;
        this.sSharedInstance = sSharedInstance;
    }

    public void startOverlay() {
        Log.d("TAG", "Force_stop overlay started");
        LayoutInflater inflater = (LayoutInflater) sSharedInstance.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            wm1.removeView(oView1);
        } catch (Exception e) {

        }
        oView1 = inflater.inflate(R.layout.stash_force_stop, null);

        ImageView cancel = (ImageView) oView1.findViewById(R.id.cross);
        TextView dont_uninstall = (TextView) oView1.findViewById(R.id.do_not_uninstall);
        dont_uninstall.setContentDescription("\u00A0");
        TextView dont_uninstall_outside = (TextView) oView1.findViewById(R.id.uninstall_stash);
        dont_uninstall_outside.setContentDescription("\u00A0");
        WindowManager.LayoutParams params1;

        try {
            params1 = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    PixelFormat.TRANSPARENT);
            params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

            wm1 = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
            try {
                wm1.addView(oView1, params1);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception j) {
            params1 = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSPARENT);
            params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
            wm1 = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
            try {
                wm1.addView(oView1, params1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dont_uninstall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    wm1.removeView(oView1);
                    //   need_benifits = 1 ;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dont_uninstall_outside.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void removeOverlay() {


    }
}
