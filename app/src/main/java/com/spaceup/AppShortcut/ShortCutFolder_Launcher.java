package com.spaceup.AppShortcut;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.spaceup.Activities.ManageApps;

/**
 * Created by dhurv on 11-01-2018.
 */

public class ShortCutFolder_Launcher extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(getApplicationContext(), ManageApps.class));
        finish();
    }
}
