package com.spaceup.AppShortcut;

/**
 * Created by dhurv on 25-01-2017.
 */


import android.graphics.Bitmap;

/**
 * Created by dhurv on 11-01-2017.
 */

public class ItemObject {

    private String name;
    private Bitmap photo;
    private String packageName;

    public ItemObject(String name, Bitmap photo, String packageName) {
        this.name = name;
        this.photo = photo;
        this.packageName = packageName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}