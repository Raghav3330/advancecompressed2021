package com.spaceup.uninstall.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spaceup.R;
import com.spaceup.Utility.PrefManager;
import com.spaceup.uninstall.activities.AppInfo;
import com.spaceup.uninstall.activities.DataTransferInterface;

import java.util.List;

/**
 * Created by Sajal Jain on 08-11-2016.
 */

public class IgnoreListAdapter extends RecyclerView.Adapter<IgnoreListAdapter.ViewHolder> {

    public Context context;
    DataTransferInterface dtInterface;
    private List<AppInfo> ignoreList;

    public IgnoreListAdapter(Context context, List<AppInfo> ignoreList, DataTransferInterface dtInterface) {
        this.context = context;
        this.ignoreList = ignoreList;
        this.dtInterface = dtInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.deactivate_app_row, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Object arr[] = new Object[2];

        final AppInfo app = ignoreList.get(position);
        arr[0] = app;
        arr[1] = holder;

        new ImageLoader().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, arr);

        holder.checkbox.setClickable(false);

        String appName = app.getAppName();
        if (appName.length() >= 18) {
            appName = appName.substring(0, 17);
            appName = appName + "...";
        }

        holder.appName.setText(appName);


        long size = app.getSize() + app.getCacheSize() + app.getCodeSize() + app.getApkSize();

        holder.size.setText(getSizeinMB(size));


        //uncomment this code if algorithms changes on priority index

        //        holder.usageCount.setText("low usage");
        //      int color = Color.parseColor("#ffb96a"); //The color YELLOW
        //    holder.labelcolor.setColorFilter(color);


       /* if (size < 5 * 1024 * 1024) {
            holder.usageCount.setText("low usage");
            int color = Color.parseColor("#23c795"); //The color GREEN
            holder.labelcolor.setColorFilter(color);
        } else if (size > 5 * 1024 * 1024 && size < 30 * 1024 * 1024) {
            holder.usageCount.setText("medium usage");
            int color = Color.parseColor("#ffb96a"); //The color YELLOW
            holder.labelcolor.setColorFilter(color);
        } else {*/


        holder.usageCount.setText("high usage");
        int color = Color.parseColor("#ff7a7a"); //The color RED
        holder.labelcolor.setColorFilter(color);
        //}






       /* holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dtInterface.setValues(app,3);


            }
        });
*/

        //holder.checkbox.setClickable(false);

        if (app.isChecked()) {
            holder.checkbox.setChecked(true);
        } else {
            holder.checkbox.setChecked(false);
        }

        if( context.getString(R.string.ignore_packages).contains(ignoreList.get(position).getPkgName()) ){
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);

            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            holder.appIcon.setColorFilter(filter);
            holder.row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showSensitivePopup();
                }
            });
        }
        else {
            holder.row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("adapt", "onClick: " + holder.checkbox.isChecked());
                    if (holder.checkbox.isChecked()) {
                        Log.d("adapt", "onClick: " + "I am here 1");
                        app.setChecked(false);
                        holder.checkbox.setChecked(false);
                        dtInterface.setValues(app, 3, position);
                    } else {
                        boolean shownPopup = PrefManager.getInstance(context).getBoolean(PrefManager.IS_WARNING_ACCEPTED_IN_SESSION);
                        if (!shownPopup) {
                            showHighUsagePopup(app, holder, dtInterface, position);
                        } else {
                            Log.d("adapt", "onClick: " + "I am here 2");
                            app.setChecked(true);
                            holder.checkbox.setChecked(true);
                            dtInterface.setValues(app, 4, position);
                        }

                    }
                }
            });
        }
    }

    private void showSensitivePopup() {
        final Dialog dialog = new Dialog((Activity) this.dtInterface, R.style.Dialog1);
        dialog.setContentView(R.layout.data_sensititve);
        final TextView cancelButton = (TextView) dialog.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    void showHighUsagePopup(final AppInfo app, final ViewHolder holder, final DataTransferInterface dtInterface, final int position) {
        final Dialog dialog = new Dialog((Activity) this.dtInterface, R.style.Dialog1);
        dialog.setContentView(R.layout.high_usage_warning);
        final TextView cancelButton = (TextView) dialog.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        final TextView selectButton = (TextView) dialog.findViewById(R.id.select_button);
        selectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("adapt", "onClick: " + "I am here 2");
                app.setChecked(true);
                holder.checkbox.setChecked(true);
                dtInterface.setValues(app, 4, position);
                dialog.dismiss();
                PrefManager.getInstance(context).putBoolean(PrefManager.IS_WARNING_ACCEPTED_IN_SESSION, true);
            }
        });
        CheckBox checkbox_agree = (CheckBox) dialog.findViewById(R.id.checkbox_agree);

        checkbox_agree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectButton.setEnabled(true);
                    selectButton.setTextColor(Color.parseColor("#ffffff"));
                    GradientDrawable drawable = (GradientDrawable) selectButton.getBackground();
                    drawable.setColor(Color.parseColor("#23c795"));


                } else {
                    selectButton.setEnabled(false);
                    selectButton.setTextColor(Color.parseColor("#9e9e9e"));
                    GradientDrawable drawable = (GradientDrawable) selectButton.getBackground();
                    drawable.setColor(Color.parseColor("#cecece"));

                }
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    @Override
    public int getItemCount() {
        return ignoreList.size();
    }

    String getSizeinMB(long size) {
        if (size > 1024) {
            size = size / 1024;
            if (size > 1024) {
                size = size / 1024;
                if (size > 1024) {
                    size = size / 1024;
                    return size + "GB";
                }
                return (size) + " MB";
            }
            return size + " KB";
        }
        return size + " Bytes";
    }

    public List<AppInfo> getList() {
        return ignoreList;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView appIcon, labelcolor;
        public TextView appName, usageCount, size;
        public RelativeLayout row;

        public CheckBox checkbox;

        public ViewHolder(View view) {
            super(view);
            appIcon = (ImageView) view.findViewById(R.id.icon);
            appName = (TextView) view.findViewById(R.id.app_name);
            usageCount = (TextView) view.findViewById(R.id.usage);
            size = (TextView) view.findViewById(R.id.size);
            checkbox = (CheckBox) view.findViewById(R.id.check_box);
            row = (RelativeLayout) view.findViewById(R.id.row);
            labelcolor = (ImageView) view.findViewById(R.id.labelcolor);

        }
    }

    class ImageLoader extends AsyncTask<Object, Void, Drawable> {
        ViewHolder viewHolder;

        @Override
        protected void onPostExecute(Drawable drawable) {
            super.onPostExecute(drawable);
            viewHolder.appIcon.setImageDrawable(drawable);
        }

        @Override
        protected Drawable doInBackground(Object... params) {
            Log.d("opt", "called in a queue");
            AppInfo app = (AppInfo) params[0];
            ViewHolder viewHolder = (ViewHolder) params[1];
            this.viewHolder = viewHolder;
            Drawable icon = null;
            try {
                icon = context.getPackageManager().getApplicationIcon(app.getPkgName());
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            return icon;
        }
    }


}
