package com.spaceup.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAd;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAdLoadCallback;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.App;
import com.spaceup.Asycs.TotalAppSizeAsync;
import com.spaceup.R;
import com.spaceup.RemoteConfig;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class OnBoarding extends AppCompatActivity implements View.OnClickListener {

    //make sure AnalysisScreen is started by only one of the startActivity() calls
    private boolean mAnalysisScreenStarted = false;
    public static int top, view_height;
    ProgressBar pb;


    PrefManager prefManager;
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    //private LinearLayout dotsLayout;
    //private TextView[] dots;
    private int[] layouts;
    private TextView btn, txt, initializing;
    private LinearLayout terms;
    private ImageView ambience, stars;
    private RewardedInterstitialAd mInterstitialAd;
    private InterstitialAd InterstitialAd;
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        private float mLastPositionOffset = 0f;

        @Override
        public void onPageSelected(int position) {

            switch (position) {
                case 0:
                    txt.setText("Never Uninstall Apps");
                    btn.setText("Next");
                    ambience.setVisibility(View.VISIBLE);
                    stars.setVisibility(View.VISIBLE);
                    initializing.setVisibility(View.GONE);
                    break;
                case 1:
                    txt.setText("Compress Apps\nAnd Save Storage");
                    btn.setText("Next");
                    ambience.setVisibility(View.VISIBLE);
                    stars.setVisibility(View.VISIBLE);
                    initializing.setVisibility(View.GONE);

                    break;
                case 2:
                    txt.setText("Open Compressed\nApps in 1 Tap");
                    ambience.setVisibility(View.VISIBLE);
                    //stars.setVisibility(View.GONE);
                    btn.setText("Get Started");

                    break;
                default:

                    break;
            }


            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.length - 1) {
                // last page. make button text to GOT IT
                //btnNext.setText(getString(R.string.start));
                //btnSkip.setVisibility(View.GONE);
            } else {
                // still pages are left
                //btnNext.setText(getString(R.string.next));
                //btnSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            if (positionOffset < mLastPositionOffset && positionOffset < 0.9) {
                viewPager.setCurrentItem(position);
            } else if (positionOffset > mLastPositionOffset && positionOffset > 0.1) {
                viewPager.setCurrentItem(position + 1);
            }
            mLastPositionOffset = positionOffset;
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };
    //	viewpager change listener
    private Bundle mGAParams;
    //private final StartAnalysisScreenReceiver startHomeScreenReceiver = new StartAnalysisScreenReceiver();
    private boolean get_started_clicked = false;
    BroadcastReceiver startHomeScreenReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equalsIgnoreCase(TotalAppSizeAsync.CUSTOM_INTENT)) {

                Log.d("ThreadErr", "broadcast received in OnBoarding");

                if (get_started_clicked && !mAnalysisScreenStarted) {
                    mAnalysisScreenStarted = true;
                    Intent i = new Intent(context, AnalysisScreen.class);
                    i.putExtra(Constants.INCOME_FROM_ONBOARDING, "true");

                    startActivity(i);
                    Log.d("ThreadErr", "Analysis starting from OnBoarding");
                    finish();
                }
                Log.d("ThreadErr", "receiver : UNregistered broadcast on OnBoarding");
                //LocalBroadcastManager.getInstance(context).unregisterReceiver(startHomeScreenReceiver);
            }


        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);
        // Checking for first time launch - before calling setContentView()

        prefManager = PrefManager.getInstance(App.getInstance());
        prefManager.putBoolean(PrefManager.IS_NEW_USER, true);
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.ONBOARD,
                AnalyticsConstants.Action.ONBOARDED_NEW,
                AnalyticsConstants.Label.FIRED,
                null, false, null);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        } else if (Build.VERSION.SDK_INT >= 19) {
            setStatusBarTranslucent(true);
        }


        init();


        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
                R.layout.welcome_slide2};

        // adding bottom dots
        //addBottomDots(0);

        // making notification bar transparent
        //changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        //new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.ONBOARDING);

        new AnalyticsHandler().logGAScreenCustomDim(AnalyticsConstants.Screens.ONBOARDING, AnalyticsConstants.CustomDim.CUSTOM_ATTRIBUTION, PrefManager.getInstance(this).getString(PrefManager.KEY_FB_CAMPAIGN_SOURCE));
        terms.setVisibility(View.VISIBLE);

        AudienceNetworkAds.initialize(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        Log.d("ThreadErr", "onResume : registered broadcast OnBoarding");
        IntentFilter startAnalysisScreenReceiverIntentFilter = new IntentFilter();
        startAnalysisScreenReceiverIntentFilter.addAction(TotalAppSizeAsync.CUSTOM_INTENT);
        LocalBroadcastManager.getInstance(this).registerReceiver(startHomeScreenReceiver,
                startAnalysisScreenReceiverIntentFilter);

        //after 2 second of starting OnBoarding screen check the if cond'n
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be more usefull when the user resumed from recent
                if (PrefManager.getInstance(OnBoarding.this).getTotalAsyncStatus() == 1 && !mAnalysisScreenStarted && get_started_clicked) {
                    //user pressed home button, we missed the broadcast and AnalysisScreen was not started and "get_started_clicked" was clicked
                    mAnalysisScreenStarted = true;
                    //user pressed home i.e missing the broadcast since in onPause() we unregistered receiver
                    Log.d("ThreadErr", "2 second wait, starting AnalysisScreen from resume method of OnBoarding, as missed the broadcast intent");
                    Intent analysisStart = new Intent(OnBoarding.this, AnalysisScreen.class);
                    analysisStart.putExtra(Constants.INCOME_FROM_ONBOARDING, "true");
                    startActivity(analysisStart);
                    finish();
                }
            }
        }, 2000);

        super.onResume();

    }

    @Override
    protected void onPause() {
        Log.d("ThreadErr", "onPause : UNregister broadcast OnBoarding ");
        //may be we have already unregistered Receiver from startHomeScreenReceiver declaration
        LocalBroadcastManager.getInstance(this).unregisterReceiver(startHomeScreenReceiver);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void init() {
        btn = (TextView) findViewById(R.id.btn);
        btn.setOnClickListener(this);
        txt = (TextView) findViewById(R.id.txt);
        txt.setText("Never Uninstall Apps");
        btn.setText("Get Started");
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        ambience = (ImageView) findViewById(R.id.ambience);
        stars = (ImageView) findViewById(R.id.stars);
        pb = (ProgressBar) findViewById(R.id.circular_progress_bar_background);

        initializing = (TextView) findViewById(R.id.initializing);
        terms = findViewById(R.id.terms);

    }

    public void openPrivacy(View v) {
        try {
            Intent privacyIntent = new Intent(OnBoarding.this, PrivacyPolicy.class);
            privacyIntent.putExtra(Constants.URL_PARAMETER, "http://spaceupapp.com/privacy.html");
            privacyIntent.putExtra(Constants.TITLE_PARAMETER, "Privacy Policy");

            startActivity(privacyIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void openTerms(View v) {
        try {

            Intent termsIntent = new Intent(OnBoarding.this, PrivacyPolicy.class);
            termsIntent.putExtra(Constants.URL_PARAMETER, "http://spaceupapp.com/tos.html");
            termsIntent.putExtra(Constants.TITLE_PARAMETER, "Terms of Service");
            startActivity(termsIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }


    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn:
//                if (RemoteConfig.getString(R.string.ad_network).equals("google")) {
//
//                MobileAds.initialize(this, new OnInitializationCompleteListener() {
//                    @Override
//                    public void onInitializationComplete(InitializationStatus initializationStatus) {
//                        loadAd();
//                    }
//                });
//                }else if(RemoteConfig.getString(R.string.ad_network).equals("facebook")) {
//                    AdSettings.addTestDevice("83c9c45d-4348-4ecf-aa2e-192901e4167e");
//                    loadFBAd();
//                }
                // checking for last page
                // if last page home screen will be launched

                new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.ONBOARD,
                        AnalyticsConstants.Action.ONBOARDED_NEW,
                        AnalyticsConstants.Label.CLICKED,
                        null, false, null);

                int current = getItem(+1);
                if (current < layouts.length) {
                    // move to next screen
                    viewPager.setCurrentItem(current);
                } else if (current == layouts.length && !get_started_clicked) {
                    prefManager.setFirstRun(false);

                    /***
                     *
                     *
                     * Start Alarm Service
                     *
                     */


                    pb.setVisibility(View.VISIBLE);
                    terms.setVisibility(View.GONE);
                    initializing.setVisibility(View.VISIBLE);
                    initializing.setText("Initializing");


                    btn.setVisibility(View.GONE);
                    get_started_clicked = true;
                    //Toast.makeText(this, "OKay", Toast.LENGTH_SHORT).show();

                    if (PrefManager.getInstance(OnBoarding.this).getTotalAsyncStatus() == 1) {
                        mAnalysisScreenStarted = true;
                        // Onboarding Complete
                        // ANALYTICS

                        Bundle paramBundle = new Bundle();
                        paramBundle.putString(AnalyticsConstants.Params.DUMMY, "dummy");
                        new AnalyticsHandler().logEvent(AnalyticsConstants.Event.ONBOARD, paramBundle);

                        Log.d(AnalyticsConstants.TAG, AnalyticsConstants.Event.ONBOARD + " " + paramBundle);


                        mGAParams = new Bundle();
                        mGAParams.putString("category", AnalyticsConstants.Category.ONBOARD);
                        mGAParams.putString("action", AnalyticsConstants.Action.ONBOARDED);
                        new AnalyticsHandler().logGAEvent(mGAParams);
                        Log.d(AnalyticsConstants.GATAG, AnalyticsConstants.Category.ONBOARD + " " + mGAParams);

                        // Starting Analysis Screen
                        Intent i = new Intent(this, AnalysisScreen.class);
                        i.putExtra(Constants.INCOME_FROM_ONBOARDING, "true");

                        finish();
                        startActivity(i);

                    }


                }
                break;
        }
    }
    private void loadAd() {
        RewardedInterstitialAd.load(OnBoarding.this, UtilityMethods.getInstance().getAdmobID(UtilityMethods.GET_STARTED),
                new AdRequest.Builder().build(),  new RewardedInterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(RewardedInterstitialAd ad) {
                        mInterstitialAd = ad;
                        Log.d("TAG", "onAdLoaded");
                        showAds();
                    }
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        Log.e("TAG", "onAdFailedToLoad");
                    }
                });
    }
    private void showAds() {
        mInterstitialAd.show(OnBoarding.this, new OnUserEarnedRewardListener() {
            @Override
            public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
            }
        });
    }

    private void loadFBAd() {
        InterstitialAd = new InterstitialAd(this, "600591120962050_601832407504588");
//        InterstitialAd = new InterstitialAd(this, "YOUR_PLACEMENT_ID");
        InterstitialAdListener interstitialAdListener = new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback
                Log.d( "ad displayed.","I am here");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                // Interstitial dismissed callback
                Log.d( "ad dismissed.","I am here");
            }
            @Override
            public void onError(Ad ad, com.facebook.ads.AdError adError) {
                Log.d("AD ERROR ",adError.getErrorMessage().toString());

            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Interstitial ad is loaded and ready to be displayed
                Log.d( "ad is loaded ","i am here");
                // Show the ad
                InterstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                Log.d( "ad clicked!","AD CLICKED");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d( "ad impression logged!","LOGGING IMPRESSION");
            }
        };

        // For auto play video ads, it's recommended to load the ad
        // at least 30 seconds before it is shown
        InterstitialAd.loadAd(
                InterstitialAd.buildLoadAdConfig()
                        .withAdListener(interstitialAdListener)
                        .build());
    }


    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }


}