package com.spaceup.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.util.Pair;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.R;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

/**
 * Created by shashanktiwari on 09/02/17.
 */


public class TransitionActivity extends Activity {

    RelativeLayout remove_overlay_screen, tick_container;
    TextView apps_compressed_text, apps_compressed_size_text;
    String x, size;
    boolean advanceCompression;
    ImageView image, inner, outer;
    Animation clockwise, anticlockwise;
    BroadcastReceiver finishUnInstaller = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                LocalBroadcastManager.getInstance(context).unregisterReceiver(finishUnInstaller);
                Intent new_Activity = new Intent(getApplication(), FinalResultScreen.class);
                new_Activity.putExtra("no_of_apps_compressed", "" + x);
                new_Activity.putExtra("size_saved", "" + size);
                new_Activity.putExtra("advance_compression", advanceCompression);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    Pair<View, String> pair1 = Pair.create((View) remove_overlay_screen, remove_overlay_screen.getTransitionName());
                    Pair<View, String> pair2 = Pair.create((View) tick_container, tick_container.getTransitionName());
                    Pair<View, String> pair3 = Pair.create((View) apps_compressed_text, apps_compressed_text.getTransitionName());
                    Pair<View, String> pair4 = Pair.create((View) apps_compressed_size_text, apps_compressed_size_text.getTransitionName());
                    //Pair<View, String> pair3 = Pair.create((View)tick_container, tick_container.getTransitionName());

                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(TransitionActivity.this, pair1, pair2, pair3, pair4);


                    ActivityCompat.startActivity(TransitionActivity.this, new_Activity, options.toBundle());


                } else
                    startActivity(new_Activity);

                Log.d("Transition", "FinalScreen");

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        TransitionActivity.this.finish();
                    }
                }, 0);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // remove title
        PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.COMPRESSED_TIME, System.currentTimeMillis());
        int progress_perc = PrefManager.getInstance(getApplicationContext()).getInt(PrefManager.PREVIOUS_PROGRESS_VAL);

        PrefManager.getInstance(getApplicationContext()).putInt(PrefManager.PREVIOUS_PROGRESS_VAL_FINAL, progress_perc);

        long compressionStartTime = PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.START_TIME_COMPRESSION);
        long timeTaken = System.currentTimeMillis() - compressionStartTime;
        long seconds = timeTaken / 1000;
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.COMPRESS,
                AnalyticsConstants.Action.TIME,
                null,
                String.valueOf(seconds),
                false,
                null);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.remove_overlay_trsition_layout);

        x = getIntent().getStringExtra("no_of_apps_compressed");
        size = getIntent().getStringExtra("size_saved");
        advanceCompression = getIntent().getBooleanExtra("advance_compression", false);

        remove_overlay_screen = (RelativeLayout) findViewById(R.id.remove_overlay_screen);
        tick_container = (RelativeLayout) findViewById(R.id.tick_container);
        image = (ImageView) findViewById(R.id.final_tick);
        inner = (ImageView) findViewById(R.id.stars_inner);
        outer = (ImageView) findViewById(R.id.stars_outer);
        apps_compressed_text = (TextView) findViewById(R.id.apps_compressed_text);
        apps_compressed_size_text = (TextView) findViewById(R.id.apps_compressed_size_text);

        apps_compressed_text.setText(x + " Apps Uninstalled");
        apps_compressed_size_text.setText(UtilityMethods.getInstance().getSizeinMB(Long.parseLong(size)) + " Saved");

        clockwise = AnimationUtils.loadAnimation(this, R.anim.rotate_image_clockwise);
        inner.startAnimation(clockwise);

        anticlockwise = AnimationUtils.loadAnimation(this, R.anim.rotate_image_anticlockwise);
        outer.startAnimation(anticlockwise);

        IntentFilter accessibilityReceiverIntentFilter = new IntentFilter();
        accessibilityReceiverIntentFilter.addAction("com.times.finishUnInstallerActivity");
        LocalBroadcastManager.getInstance(this).registerReceiver(finishUnInstaller, accessibilityReceiverIntentFilter);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // LocalBroadcastManager.getInstance(this).unregisterReceiver(finishUnInstaller);
    }
}
