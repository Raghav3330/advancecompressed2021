package com.spaceup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.apsalar.sdk.Apsalar;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.bolts.AppLinks;
import com.spaceup.Activities.AnalysisScreen;
import com.spaceup.Activities.OnBoarding;
import com.spaceup.Alarm.AlarmService_Controller;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.Asycs.TotalAppSizeAsync;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.services.DBService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.PatternSyntaxException;

//import bolts.AppLinks;

import static com.spaceup.apkgenerator.constants.Constants.WHITE_LIST_FILE_ASSET_NAME;
import static com.spaceup.apkgenerator.constants.Constants.WHITE_LIST_FILE_PATH;

// FB SDK


public class Splash_Screen extends AppCompatActivity implements Handler.Callback {

    private static final String TAG = Splash_Screen.class.getSimpleName();
    private static final String SPLIT_ARG_START = "START-";
    private static final String SPLIT_ARG_END = "-END";
    private final int TOTAL_APP_SIZE = 1;
    PrefManager prefManager;
    //AppDBHelper db;
    UtilityMethods methods;
    private Context mContext;
    //    DatabaseHandler db;
    private boolean mAnalysisScreenStarted = false;
    BroadcastReceiver startHomeScreenReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equalsIgnoreCase(TotalAppSizeAsync.CUSTOM_INTENT)) {
                Log.d("ThreadErr", "broadcast received on Splash_Screen");
                //LocalBroadcastManager.getInstance(context).unregisterReceiver(startHomeScreenReceiver);
                if (!prefManager.isFirstRun() && !mAnalysisScreenStarted) {
                    mAnalysisScreenStarted = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent analysisStart = new Intent(Splash_Screen.this, AnalysisScreen.class);
                            finish();
                            startActivity(analysisStart);
                            Log.d("ThreadErr", "Analysis started from Splash_Screen");
                        }
                    }, 2300);

                }

            }
        }
    };
    private Bundle mGAParams;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // AppDBHelper.getInstance(App.getInstance()).getLastModifiedDate("com.dev47apps.droidcam.stash");
        super.onCreate(savedInstanceState);
        if(BuildConfig.DEBUG){
//            FirebaseCrash.setCrashCollectionEnabled(false);
        }
        saveTimeWhenAppLaunchedFirstTime();
        //TODO : sajal remove this line from here
        init();
    /*
        mGAParams = new Bundle();
        mGAParams.putString("category", AnalyticsConstants.Category.EMAIL);
        mGAParams.putString("action", email);
        mGAParams.putInt("cd", AnalyticsConstants.CustomDim.EMAIL);
        mGAParams.putString("cd_value", email);
        new AnalyticsHandler().logGAEvent(mGAParams);
        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
        */
        if (!PrefManager.getInstance(mContext).getBoolean(PrefManager.dayCountFirst)) {
            PrefManager.getInstance(mContext).putBoolean(PrefManager.dayCountFirst, true);

            String mDayCount = "0";
            mGAParams = new Bundle();
            mGAParams.putString("category", AnalyticsConstants.Category.DAY_COUNT);
            mGAParams.putString("action", mDayCount);
            mGAParams.putInt("cd", AnalyticsConstants.CustomDim.DAY_COUNT);
            mGAParams.putString("cd_value", mDayCount);
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

            new AlarmService_Controller(this).startDayCountAlarm();
        }

     /*   // Custom Log [Email ID]
        if (email != null) {
            FirebaseCrash.log(email.replace("@", "_"));
        }*/


        new AnalyticsHandler().logApsalarEvent("test_splash");

        //new AnalyticsHandler().logApsalarEvent("splash_screen");

        Bundle targetUrl = AppLinks.getAppLinkData( getIntent());
        if (targetUrl != null) {
            String campaignLink = targetUrl.toString();
            String campaignSource = null;
            try {
                String[] firstSplit = campaignLink.split(SPLIT_ARG_START);
                if (firstSplit.length >= 2) {
                    String[] secondSplit = firstSplit[1].split(SPLIT_ARG_END);
                    if (secondSplit.length >= 1) {
                        campaignSource = secondSplit[0];
                    }
                }
            } catch (PatternSyntaxException e) {
                Log.d(TAG, e.getMessage(), e);
            }
            if (campaignSource != null) {
                PrefManager.getInstance(this).putString(PrefManager.KEY_FB_CAMPAIGN_SOURCE, campaignSource);
            } else {
                PrefManager.getInstance(this).putString(PrefManager.KEY_FB_CAMPAIGN_SOURCE, campaignLink);
            }
        }
        init_notification();
        setContentView(R.layout.activity_splash__screen);

        ImageView best_of_2017_illustration = (ImageView) findViewById(R.id.best_of_2017_illustration);
        boolean showIllustration = RemoteConfig.getBoolean(R.string.show_best_of_2017_illustration);
        if (showIllustration) {
            best_of_2017_illustration.setVisibility(View.VISIBLE);
        } else {
            best_of_2017_illustration.setVisibility(View.INVISIBLE);

        }
        methods = UtilityMethods.getInstance();
        prefManager = PrefManager.getInstance(App.getInstance());
        prefManager.setAppRunCount(prefManager.getAppRunCount() + 1);
        //reset reminder
        prefManager.setTooManyAppsReminder(true);
        //TODO : check again about the accessibility and unknown sources programmatically

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        } else if (Build.VERSION.SDK_INT >= 19) {
            setStatusBarTranslucent(true);
        }
        checkAppVersion();
        clean_project();
        extract_apk();
        copyWhiteListApps();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Log.d("ThreadErr", "3 sec wait over");
                if (prefManager.isFirstRun()) {
                    Log.d("ThreadErr", "was a first run starting OnBoarding");
                    Intent onBoard = new Intent(Splash_Screen.this, AnalysisScreen.class);
                    startActivity(onBoard);
                    finish();
                }
            }
        }, 4000);


        new AnalyticsHandler().logGAScreenCustomDim(AnalyticsConstants.Screens.SPLASH, AnalyticsConstants.CustomDim.RUN_COUNT, PrefManager.getInstance(getApplicationContext()).getAppRunCount() + "");
    }


    private void saveTimeWhenAppLaunchedFirstTime() {
        /**
         *         To be used by Analytics (Save System Millis when first time user land on SplashScreen on
         *         Preference PrefManager.FIRST_TIME_CONTROLLER
         */
        if (!PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.FIRST_TIME_CONTROLLER)) {
            PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.SYSTEM_MILLIES_WHEN_APP_LAUNCHED_FIRST_TIME, System.currentTimeMillis());
            PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.FIRST_TIME_CONTROLLER, true);
        }
    }

    private void init_notification() {
        /**
         *
         * Show notification if user has not compressed any app till date
         *
         */

        AlarmService_Controller alarmService_controller = new AlarmService_Controller(getApplicationContext());
        alarmService_controller.fiveMinAppNotCompressed();


        // Starting 4 days Alarm

        AlarmService_Controller am = new AlarmService_Controller(getApplicationContext());
        am.every4days();
        //repeating alaram service

    }

    private void clean_project() {
        File file = new File(Constants.UNZIP + "/hcjb1.apk");
        file.delete();
        file = new File(Constants.UNZIP + "/hcjb2.apk");
        file.delete();
        file = new File(Constants.UNZIP + "/hcjb3.apk");
        file.delete();
        file = new File(Constants.UNZIP + "/hcjb4.apk");
        file.delete();
        file = new File(Constants.UNZIP + "/hcjb1.zip");
        file.delete();
        file = new File(Constants.UNZIP + "/hcjb2.zip");
        file.delete();
        file = new File(Constants.UNZIP + "/hcjb3.zip");
        file.delete();
        file = new File(Constants.UNZIP + "/hcjb4.zip");
        file.delete();


    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    private void copyWhiteListApps() {
        AssetManager am = App.getInstance().getAssets();
        File output = new File(Environment.getExternalStorageDirectory().getPath() + Constants.STASH + WHITE_LIST_FILE_PATH);

        try {
            output.createNewFile();
            InputStream is = am.open(WHITE_LIST_FILE_ASSET_NAME);
            FileOutputStream fos = new FileOutputStream(output);
            copyFile(is, fos);
            fos.close();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    void extract_apk() {
        PackageManager packageManager = getPackageManager();
        try {
            File file = new File(packageManager.getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA).publicSourceDir);
            File output = new File(Environment.getExternalStorageDirectory().getPath() + Constants.STASH + "/db/spaceup.stash");
            try {
                output.createNewFile();
                FileOutputStream fos;
                try {

                    InputStream assetFile = new FileInputStream(file);
                    fos = new FileOutputStream(output);
                    copyFile(assetFile, fos);
                    fos.close();
                    assetFile.close();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void init() {

        Intent recommendationIntent = new Intent(Splash_Screen.this, DBService.class);
        startService(recommendationIntent);

        AlarmService_Controller alarm = new AlarmService_Controller(getApplicationContext());
        alarm.Alarmat2PM();

        AlarmService_Controller alarmDaily = new AlarmService_Controller(getApplicationContext());
        alarmDaily.AlarmDaily12PM();


        AlarmService_Controller alarmDaily2 = new AlarmService_Controller(getApplicationContext());
        alarmDaily2.AlarmDaily6PM();
    }

    @Override
    protected void onResume() {

        Log.d("ThreadErr", "Resume");

        if (!prefManager.isFirstRun()) {
            Log.d("ThreadErr", "registered broadcast on splash screen");
            IntentFilter startAnalysisScreenReceiverIntentFilter = new IntentFilter();
            startAnalysisScreenReceiverIntentFilter.addAction(TotalAppSizeAsync.CUSTOM_INTENT);
            LocalBroadcastManager.getInstance(this).registerReceiver(startHomeScreenReceiver,
                    startAnalysisScreenReceiverIntentFilter);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    // This method will be executed when the user resumed from recents
                    if (PrefManager.getInstance(Splash_Screen.this).getTotalAsyncStatus() == 1 && !mAnalysisScreenStarted) {
                        mAnalysisScreenStarted = true;
                        //user pressed home i.e missing the broadcast since in onPause() we unregistered receiver
                        Log.d("ThreadErr", "2 second wait, starting AnalysisScreen from resume method of Splash_Screen, as missed the broadcast intent");
                        Intent analysisStart = new Intent(Splash_Screen.this, AnalysisScreen.class);
                        finish();
                        startActivity(analysisStart);
                    }
                }
            }, 4000);
        }

        //Initializing APSALAR
        new AnalyticsHandler().apsalarInitialization(Splash_Screen.this);


        // FB Install Tracking
        AppEventsLogger.activateApp(this);

        super.onResume();
    }

    @Override
    protected void onPause() {

        Log.d("ThreadErr", "pause");
        super.onPause();
        Log.d("ThreadErr", "UNregistered broadcast on splash screen");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(startHomeScreenReceiver);
        Apsalar.unregisterApsalarReceiver();
    }

    @Override
    protected void onStop() {
        AppEventsLogger.deactivateApp(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case TOTAL_APP_SIZE:
                Bundle data = msg.getData();
                //total_app_size = data.getDouble("appsize", 0);
                //total_apps = data.getInt("totalapps", 0);
                //TODO:send app size and total apps
                //   startHomeScreen();
                break;
        }
        return false;
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    //updates version code and reset review status to false on new version
    private void checkAppVersion() {

        int currentAppVersionCode = getCurrentAppVersionCode();
        int oldAppVersion = PrefManager.getInstance(Splash_Screen.this).getVersionCode();
        Log.d("version", "checkAppVersion: currentAppVersionCode " + currentAppVersionCode);
        Log.d("version", "checkAppVersion: oldAppVersion " + oldAppVersion);
       /* if (oldAppVersion < currentAppVersionCode) {
            try {
                if (oldAppVersion > 0)
                    Log.d(TAG, "checkAppVersion: " + String.format("App updated from version %d", oldAppVersion));
                else
                    Log.d(TAG, "checkAppVersion: " + String.format("App started for the first time", oldAppVersion));
            } finally {
                PrefManager.getInstance(Splash_Screen.this).setVersionCode(currentAppVersionCode);
                PrefManager.getInstance(Splash_Screen.this).setReview(false);
            }
        }*/
    }


    private int getCurrentAppVersionCode() {
        int versionCode = -1;
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }


}
