package com.spaceup.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.spaceup.R;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityAutomation;
import com.spaceup.accessibility.AccessibilityCommunicator;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.models.DataModel;
import com.spaceup.models.StateRetrieveModel;

import java.util.ArrayList;

/**
 * Created by shashank.tiwari on 11/01/17.
 */

public class AfterCallActivity extends Activity {

    boolean callFlag, continue_flag;
    BroadcastReceiver finishUnInstaller = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            callFlag = true;
            AfterCallActivity.this.finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.compression_aborted_layout);

        Log.d("AccessibilityY", "After call activity");
        callFlag = false;
        continue_flag = false;


        TextView continue_process = (TextView) findViewById(R.id.continue_process);
        TextView remaining_uncompressed = (TextView) findViewById(R.id.remaining_uncompressed);
        TextView uncompressed_size_saved = (TextView) findViewById(R.id.uncompressed_size_saved);


        IntentFilter accessibilityReceiverIntentFilter = new IntentFilter();
        accessibilityReceiverIntentFilter.addAction("com.times.callInitiate");
        LocalBroadcastManager.getInstance(this).registerReceiver(finishUnInstaller, accessibilityReceiverIntentFilter);

        int size = 0;
        StateRetrieveModel stateRetrieveModel = AppDBHelper.getInstance(this).getUncompressedAppStateData();
        ArrayList<String> uncompressed_app_package_list = stateRetrieveModel.getUncompressedAppList();
        if (uncompressed_app_package_list != null) {
            for (String app_package : uncompressed_app_package_list) {
                DataModel data = AppDBHelper.getInstance(this).getDataSize(app_package);
                Log.d("AppDBHelper", "package -->" + app_package);
                Log.d("AppDBHelper", "getApkSize -->" + data.getApkSize());
                Log.d("AppDBHelper", "getCodeSize -->" + data.getCodeSize());
                Log.d("AppDBHelper", "getCacheSize -->" + data.getCacheSize());
                Log.d("AppDBHelper", "getDataSize -->" + data.getDataSize());
                size += data.getApkSize() + data.getCodeSize() + data.getCacheSize() + data.getDataSize();
            }
        }

        remaining_uncompressed.setText(uncompressed_app_package_list.size() + " Apps");
        uncompressed_size_saved.setText("Upto " + UtilityMethods.getInstance().getSizeinMB(size) + " can be saved");


        continue_process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(AfterCallActivity.this, AccessibilityCommunicator.class);
                intent.putExtra("continue_after_call", true);
                intent.putExtra("state", "multiple_app");
                AfterCallActivity.this.startService(intent);
                continue_flag = true;
                finish();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("AccessibilityY", "After call activity OnDestroy");
        if (!callFlag && !continue_flag) {
            if (AccessibilityAutomation.getSharedInstance() != null) {
                AppDBHelper.getInstance(AccessibilityAutomation.getSharedInstance()).deleteCompressedAppData();
            }
        }

        LocalBroadcastManager.getInstance(this).unregisterReceiver(finishUnInstaller);

    }
}
