package com.spaceup.Activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.spaceup.R;
import com.spaceup.RemoteConfig;

public class AppUpgradeActivity extends BaseActivity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.force_upgrade_layout);
        findViewById(R.id.upgrade).setOnClickListener(this);
        findViewById(R.id.cancel).setOnClickListener(this);
        ((TextView) findViewById(R.id.heading)).setText(RemoteConfig.getString(R.string.app_upgrade_heading));
        ((TextView) findViewById(R.id.subtext)).setText(RemoteConfig.getString(R.string.app_upgrade_sub_text));
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        } else if (Build.VERSION.SDK_INT >= 19) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.upgrade:
                Uri uri = Uri.parse(RemoteConfig.getString(R.string.market_app_url));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Log.d("AppUpgradeActivity", "play store redirection failed ");
                }
                finish();
                break;
            case R.id.cancel:
                finish();
                break;
            default:
                break;
        }
    }
}
