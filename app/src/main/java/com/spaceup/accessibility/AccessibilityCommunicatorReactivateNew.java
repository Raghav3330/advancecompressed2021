package com.spaceup.accessibility;

import static androidx.core.content.FileProvider.getUriForFile;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;

import java.io.File;
import java.util.List;

/**
 * Created by Dhruv kaushal on 10/12/16.
 */

public class AccessibilityCommunicatorReactivateNew extends Service {

    private String path;
    private String TAG = "Accessibility_REACTIVATE_NEW";
    // declaring list of uncompressed apps:- Used in sequentially uninstalling - installing apps
    BroadcastReceiver accessibilityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (Constants.Receiver.DUMMY_INSTALL_RECEIVER.equals(intent.getAction())) {
                if (intent.getExtras().getBoolean("get_packagename")) {
                    Log.d(TAG, "eventDummyAppRemovedNew 3");
                    String dummy_app_package = intent.getExtras().getString("package_removed");
                    if (dummy_app_package != null && isPackageExisted(dummy_app_package)) {
                        // Case of
                        Intent i1 = new Intent("com.times.accessibilityREACTIVATE1");
                        i1.putExtra("packageName_of_apps_uninstalled", dummy_app_package);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(i1);


                    } else {
                        /** Install Intents */
                        Log.d(TAG, "eventDummyAppRemovedNew 4");
                        File f = new File(path);
                        if (f.exists()) {
                            Log.d("Location", "path " + path);
//                            Uri path1 = Uri.fromFile(f);
                            Uri path1= getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".fileprovider",f);

                            UtilityMethods.getInstance().throwInstallPopup(path1, getApplicationContext());
                        }
                        else{
                            Log.d(TAG, " APK NOT FOUND");
                            Log.d(TAG, "eventDummyAppRemovedNew 6");

                        }
                        LocalBroadcastManager.getInstance(AccessibilityCommunicatorReactivateNew.this).unregisterReceiver(accessibilityReceiver);
                    }
                }
                LocalBroadcastManager.getInstance(AccessibilityCommunicatorReactivateNew.this).unregisterReceiver(accessibilityReceiver);

            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "Overlay started");
        try {
            AccessibilityAutomation.getSharedInstance().fromActivity(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (intent != null && intent.getBooleanExtra("non_sticky_intent", false)) {
            Log.d(TAG, "START_NOT_STICKY called");
            return START_NOT_STICKY;
        } else if (intent != null && intent.getBooleanExtra("decompresser_initialate", false)) {
            Log.d("REACTIVATE", "WE ARE IN REACTIVATOR APP");

            Bundle b1 = intent.getExtras();
            assert b1 != null;
            path = b1.getString("path");
            final String source = b1.getString("source");
            final String type = b1.getString("type");     // Shortcut or Dummy

            // Register a broadcast receiver to throw install event when uninstall broadcast is received
            IntentFilter accessibilityReceiverIntentFilter = new IntentFilter();

            /**
             *
             * SET A DUMMY INSTALL RECEIVER
             *
             */
            accessibilityReceiverIntentFilter.addAction(Constants.Receiver.DUMMY_INSTALL_RECEIVER);
            LocalBroadcastManager.getInstance(this).registerReceiver(accessibilityReceiver,
                    accessibilityReceiverIntentFilter);

            if (AccessibilityAutomation.getSharedInstance() != null) {

                AccessibilityAutomation.getSharedInstance().overlayDecompression(source);
                AccessibilityAutomation.getSharedInstance().allowReading(true);
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (type.equals("dummy")) {
                        /** Uninstall Intents*/
                        Log.d("TAG", "CALL THIS WHEN DUMMY" + source);
                        Uri packageUri = Uri.parse("package:" + source);
                        Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageUri);
                        uninstallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(uninstallIntent);

                    } else if (type.equals("shortcut")) {
                        //Install Intent
                        File f = new File(path);
                        if (f.exists()) {
                            Log.d("Location", "path " + path);
                            Uri path1 = Uri.fromFile(f);
                            UtilityMethods.getInstance().throwInstallPopup(path1, getApplicationContext());
                        }
                    }
                }
            }, 1500);


            return START_NOT_STICKY;
        }
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Accessibility", "Accessibility Reactivate on destroy");
        LocalBroadcastManager.getInstance(AccessibilityCommunicatorReactivateNew.this).unregisterReceiver(accessibilityReceiver);
        if (AccessibilityAutomation.getSharedInstance() != null) {
            AccessibilityAutomation.getSharedInstance().fromActivity(false);
        }
    }

    public boolean isPackageExisted(String targetPackage) {
        List<ApplicationInfo> packages;
        PackageManager pm;

        pm = getPackageManager();
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            if (packageInfo.packageName.equals(targetPackage))
                return true;
        }
        return false;
    }
}
