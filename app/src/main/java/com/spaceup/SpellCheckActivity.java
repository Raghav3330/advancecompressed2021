package com.spaceup;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.ViewGroup;
import android.view.textservice.SpellCheckerSession;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.googlecode.tesseract.android.TessBaseAPI;

import java.util.ArrayList;

public class SpellCheckActivity extends Activity {
    private static final String TAG = SpellCheckActivity.class.getSimpleName();
    private static final int NOT_A_LENGTH = -1;
    private TextView mMainView;
    private SpellCheckerSession mScs;

    /**
     * Called when the activity is first created.
     */

    Bitmap[] imageIDs;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spell_check);
        ArrayList<String> useless = getIntent().getExtras().getStringArrayList("useless");
        imageIDs = new Bitmap[useless.size()];
        for(int i=0;i<useless.size() ; i ++){
            imageIDs[i] = BitmapFactory.decodeFile(useless.get(i));
        }

        GridView gridView = (GridView) findViewById(R.id.gridview);
        gridView.setAdapter(new ImageAdapter(this));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id)
            {
                Toast.makeText(getBaseContext(),
                        "pic" + (position + 1) + " selected",
                        Toast.LENGTH_SHORT).show();
            }
        });

    }
    public class ImageAdapter extends BaseAdapter
    {
        private Context context;

        public ImageAdapter(Context c)
        {
            context = c;
        }

        //---returns the number of images---
        public int getCount() {
            return imageIDs.length;
        }

        //---returns the ID of an item---
        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        //---returns an ImageView view---
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ImageView imageView;
            if (convertView == null) {
                imageView = new ImageView(context);
                imageView.setLayoutParams(new GridView.LayoutParams(185, 185));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(5, 5, 5, 5);
            } else {
                imageView = (ImageView) convertView;
            }
            Bitmap bmp  = imageIDs[position];
            try {
                bmp = Bitmap.createScaledBitmap(imageIDs[position], 700, 700, false); //scale image
            } catch (Exception e) {
                e.printStackTrace();
            }
            imageView.setImageBitmap(bmp);
            return imageView;
        }
    }
}