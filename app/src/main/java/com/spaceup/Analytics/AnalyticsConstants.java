package com.spaceup.Analytics;

/**
 * Created by Sajal Jain on 27-01-2017.
 */


public class AnalyticsConstants {

    public static final String APSALARTAG = "Apsalar";
    public static String TAG = "Analytics";
    public static String GATAG = "tryaa";

    public static class Event {
        public final static String ONBOARD = "onboarding_complete";
        public final static String SCAN = "scan";
        public final static String APPS_DETAILS = "apps_details";
        public final static String SCAN_BACK_PRESSED = "scan_back_pressed";
        public final static String COMPRESS_PRESSED = "compress_pressed";
        public final static String COMPRESSING = "compressing";
        public final static String RESULT = "result";
        public static final String UNCOMPRESS = "uncompress";
        public static final String DO_IN_BKG = "do_in_bkg";
        public static final String DRAG_COMPRESS = "drag_compress";
        public static final String FEEDBACK = "feedback";
        public static final String INIT_FEEDBACK = "init_feedback";
        public static final String UNCOMPRESS_FINISHED = "uncompress_finished";


//        public final static String STASH = "/.Stash/";
//        public final static String STASH = "/.Stash/";
//        public final static String STASH = "/.Stash/";

    }


    public static class Params {
        //SCAN
        public final static String PERCENTAGE = "storage_percentage";

        //APPS_DETAILS
        public final static String TOTAL_APPS_COMPRESSED = "total_apps_compressed";
        public final static String TOTAL_APPS = "total_apps";
        public static final String DUMMY = "dummy";

        //COMPRESS_PRESSED
        public static final String PERMISSION = "permission";
        public static final String RUN_COUNT = "run_count";

        //COMPRESSING
        public final static String APPS_SELECTED = "apps_selected";
        public final static String RECOM_APPS_UNSELECTED = "recom_apps_unselected"; //checked by default
        public final static String SUGGESTED_APPS_SELECTED = "suggested_apps_selected"; //suugested list 10 in number as of now
        //RESULT
        public final static String APP_COMPRESSED = "apps_compressed";
        public final static String STOP = "stop";

        public static final String PACKAGE_NAME = "pkg_name";

        //DRAG_COMPRESS
        public static final String CANCEL_TYPE = "cancel_type";
        public static final String STATUS = "status";


        public static final String DIFFICULT = "difficult";
        public static final String APP_CRASHED = "app_crashed";
        public static final String DATA_LOST = "data_lost";
        public static final String SLOW_PROCESS = "slow_process";
        public static final String SEND = "send";
        public static final String REVIEW = "review";
        public static final String PLAY_STORE = "play_store";
        public static final String TIME = "time";
        public static final String DO_IN_BKG = "do_in_bkg";

        // Campaign Source
        public static final String CAMPAIGN_SOURCE = "campaign_source";
    }

    public static class Category {
        public final static String ONBOARD = "onboarding";
        public final static String SCAN = "scan";
        public final static String COMPRESS = "compress";
        public final static String FEEDBACK = "feedback";
        public final static String UNCOMPRESS = "uncompress";
        public final static String DRAG_COMPRESS = "drag_compress";
        public final static String PERMISSION = "permission";
        public final static String NOTIFICATION = "notification";
        public final static String EMAIL = "email";
        public final static String DAY_COUNT = "day_count";
        public final static String APPUSAGE_FAIL = "appusage_fail";
        public final static String HOMESCREEN = "homescreen";
        public final static String REVIEW = "review";
        public static final String RESULT = "result";
        public static final String UNINSTALL = "uninstall";
        public static final String STASH_REF = "stash_ref";
        public static final String APP_USAGE_STATS = "app_usage_stats";
        public static final String FILE_USAGE_STATS = "file_usage_stats";

        public static final String ERROR = "error";
    }

    public static class Action {
        //ONBOARD
        public final static String ONBOARDED = "onboarded";
        public final static String ONBOARD_DELAY = "onboard_delay";
        //MANAGE_APPS and UNCOMPRESS
        public final static String INAPP = "inapp";
        public final static String SHORTCUT = "SHORTCUT";
        public final static String MENU_LAUNCHER = "menu_launcher";
        public final static String RESULT_SCREEN = "result_screen";
        public final static String SRC = "src";
        //SCAN
        public final static String INIT_DETAILS = "init_details";
        //COMPRESS
        public final static String COMPRESS_TAP = "COMPRESS_TAP";
        public final static String COMPRESS_DETAILS = "compress_details";
        public final static String RESULT = "result";
        public final static String COMPRESSED_PKG = "compressed_pkg";
        public final static String PROCEED = "proceed";
        //FEEDBACK
        public final static String POSITIVE = "positive";
        public static final String NEGATIVE = "negative";
        public static final String NEGATIVE_TEXT = "negative_text";
        public final static String SHARE = "share";
        //UNCOMPRESS
        public final static String MENU = "menu";
        public static final String UNCOMPRESSING = "uncompressing";
        public static final String TIME = "time";
        //DRAG_COMPRESS
        public final static String COMPRESS = "compress";
        public final static String UNINSTALL = "uninstall";
        public final static String NOTACTIVE = "notactive";
        public final static String CANCEL = "cancel";
        //PERMISSION
        public final static String TYPE = "type";
        public static final String ACCESSIBILITY = "accessibility";
        public static final String UNKNOWN_SOURCES = "unknown_sources";
        public static final String UNKNOWN_SOURCES_AUTO = "unknown_sources_auto";

        public static final String APP_LOST_NOTIFICATION = "app_lost_notification";
        public static final String BOTTOM_NOTIFICATION = "bottom_notification";
        public static final String PLAY_STORE_NOTIFICATION = "play_store_notification";

        public static final String TOP_NOTIFICATION = "top_notification";
        public static final String ACC_GREENIFY_POPUP = "acc_greenify_popup";
        public static final String APPS_STORAGE = "apps_storage";
        public static final String PHONE_STORAGE = "phone_storage";
        public static final String PERCENTAGE_STORAGE = "percentage_storage";
        public static final String CONVERSION_TO_SCAN_RED = "conversion_to_scan_red";
        public static final String CONVERSION_TO_SCAN_BLUE = "conversion_to_scan_blue";
        public static final String TOTAL_COMPRESSED = "total_compressed";
        public static final String SPACE_SAVED = "space_saved";
        public static final String ANIMATION_STATS = "animation_stats";
        public static final String APP_POSITION = "app_position";
        public static final String APPS_SELECTED = "apps_selected";
        public static final String APPS_COMPRESSED = "apps_compressed";
        public static final String APPS_DESELECTED = "apps_deselected";
        public static final String DROP_AFTER_PROCEED = "drop_after_proceed";
        public static final String NORMAL_DROP = "normal_drop";
        public static final String VIEW_COMP_APPS = "view_comp_apps";
        public static final String FIRED_RESULT = "landing";
        public static final String RESULT_BACK = "result_back";
        public static final String LESS_THAN_30 = "less_than_30";
        public static final String ONE_DAY = "one_day";
        public static final String THREE_DAYS = "three_days";
        public static final String SEVEN_DAY = "seven_day";
        public static final String MORE_THAN_SEVEN = "more_than_seven";
        public static final String COMPRESSED = "compressed";
        public static final String APP_PCKG = "app_pckg";
        public static final String POPUP_WHATSAPP = "whatsapp";
        public static final String POPUP_WHATSAPP_OLD = "whatsapp_old";
        public static final String POPUP_WHATSAPP_NEW = "whatsapp_new";

        public static final String POPUP_MANAGEAPPS = "manageapps";
        public static final String POPUP_RESULT_SCREEN = "result_screen";
        public static final String POPUP_RESULT_SCREEN_OLD = "popup_result_screen_old";
        public static final String POPUP_RESULT_SCREEN_NEW = "popup_result_screen_new";

        public static final String APP_INSTALLED = "app_installed";


        public static final String BUBBLE = "bubble";
        public static final String APPS_USED_PER_DAY = "apps_used_per_day";
        public static final String TIME_6AM_TO_12PM = "time_6am_to_12pm";
        public static final String TIME_12PM_TO_6PM = "time_12pm_to_6pm";
        public static final String TIME_6PM_TO_12AM = "time_6pm_to_12am";
        public static final String TIME_12AM_TO_6AM = "time_12am_to_6am";
        public static final String APP_INSTALL_DAYS = "app_install_days";
        public static final String ONBOARDED_NEW = "onboarded_new";
        public static final String TIME_UNCOMP = "time_uncomp";
        public static final String UNCOMP_PCKG = "uncomp_pckg";
        public static final String UNCOMPRESS = "uncompress";
        public static final String TOTAL_COMP_1ST = "total_comp_1st";
        public static final String POPUP_1TAPLAUNCHER = "popup_1taplauncher";
        public static final String APP_USAGE = "app_usage";
        public static final String SYSTEM_STORAGE = "system_storage";
        public static final String FILE_RECEIVED = "file_received";
        public static final String FILE_CREATED = "file_created";
        public static final String MEDIA_RECIEVED = "media_recieved";
        public static final String FILE_CREATED_SIZE = "file_created_size";
        public static final String MEDIA_RECIEVED_SIZE = "media_recieved_size";
        public static final String FILE_RECEIVED_SIZE = "file_received_size";
        public static final String MEDIA_CLICKED_SIZE = "media_clicked_size";
        public static final String UNCOMPRESSION_ERROR = "uncompression_error";


        public static String SCAN_START = "scan_start";
        public static String UNCOMPRESSED_PKG = "uncompressed_pkg";
        public static String USAGE_STATS = "usage_stats";
        //NOTIFICATION
        public static String TWO_APP_INSTALLED = "two_app_installed";
        public final static String PLAYSTORE_UPDATE = "playstore_update";
        public static final String TOTAL_APPS = "total_apps";
        public final static String TOTAL_COMP = "total_comp";

        public static final String APP_STORAGE_DATA = "apps_strg_data";
        public static final String APP_STORAGE_INCREASED = "apps_strg_inc";
        public static final String NO_OF_APP_STORAGE_INCREASED = "no_of_apps_strg_inc";

        public static final String MEDIA_CLICKED = "media_clicked";

    }

    //label are related to action
    public static class Label {
        //ONBOARDED
        public final static String ONBOARD_TIME = "time";
        //MANAGE APPS
        public final static String CONVERT = "convert_to_adv";
        public final static String CREATE_SHORTCUT = "create_shortcut";
        public final static String INAPP = "inapp";
        public final static String SHORTCUT = "SHORTCUT";
        public final static String MENU_LAUNCHER = "menu_launcher";
        public final static String RESULT_SCREEN = "result_screen";
        //INIT DETAILS
        public final static String PERCENTAGE = "percentage";
        public final static String TOTAL_UNCOM = "total_uncom";
        public final static String TOTAL_STORAGE = "total_storage";
        public static final String TOTAL_APPS = "total_apps";
        //COMPRESS TAP
        //0 quick and 1 advanced
        public final static String ADV_COM_FLAG = "adv_com_flag";
        public final static String QUICK_COM_FLAG = "quick_com_flag";
        //Compress Details
        public final static String APPS_SELECTED = "apps_selected";
        public final static String APPS_FROM_PRE_SELECTED = "apps_from_pre_selected";
        public final static String APPS_FROM_SUGGESTED = "apps_from_suggested";
        public final static String REVIEW_UNSELECT = "review_unselect";

        //RESULT
        public final static String APPS_COMPRESSED = "apps_compressed";
        public final static String STOP = "stop";
        public final static String CALL = "call";
        public final static String SHORTCUT_BTN = "shortcut_btn";
        public final static String BACK = "back";
        public final static String RESULT_BACK = "result_back";

        //POSITIVE
        public final static String PLAY_STORE = "play_store";
        //uncompress
        public final static String DUMMY = "dummy";
        public final static String DO_IN_BKG = "do_in_bkg";
        public final static String OPEN = "open";
        public final static String CANCEL = "cancel";
        public final static String TIME = "time";
        //FEEDBACK
        public static final String DIFFICULT_CLICKED = "difficult";
        public static final String APP_CRASHED_CLICKED = "app_crashed";
        public static final String DATA_LOST_CLICKED = "data_lost";
        public static final String APP_CONSUMING_BATTERY_CLICKED = "app_consuming_battery_clicked";
        public static final String APP_SPACE_NOT_SAVED_CLICKED = "app_space_not_saved_clicked";
        public static final String SLOW_PROCESS_CLICKED = "slow_process";
        //PERMISSIONS
        public final static String SRC = "src";
        public final static String NOT_GIVEN = "NOT_GIVEN";
        public final static String GIVEN = "GIVEN";
        public static final String REVIEW_SCREEN = "review_screen";
        public final static String UNCOMPRESS_SCREEN = "uncompress_screen";
        public final static String EXIST = "exist";
        public static final String POPUP = "popup"; // Usage Stats Popup
        public static final String STORAGE_4GB = "4GB";
        public static final String STORAGE_8GB = "8GB";
        public static final String STORAGE_16GB = "16GB";
        public static final String STORAGE_32GB = "32GB";
        public static final String STORAGE_64GB = "64GB";
        public static final String STORAGE_128GB = "128GB";
        public static final String HOME_LANDING = "home_landing";
        public static final String SCAN_CLICKED = "scan_clicked";
        public static final String LONE = "1";
        public static final String LTWO = "2";
        public static final String LTHREE = "3";
        public static final String LFOUR = "4";
        public static final String LFIVE = "5";
        public static final String FIRED = "fired";
        public static final String CLOSED = "closed";
        public static final String FIRST_UNCOMPRESS = "first_uncompress";
        public static final String SECOND_UNCOMPRESS = "second_uncompress";
        public static final String REVIEW_SCREEN_FIRST = "review_screen_first";
        public static final String UNCOMPRESS_SCREEN_FIRST = "uncompress_screen_first";
        public static final String SCANNING_SCREEN = "scanning_screen";
        public static final String SCANNING_SCREEN_1ST = "scanning_screen_1st";
        public static final String TAKEN = "taken";


        public static String EIGHT = "8GB";
        public static String FOUR = "4GB";
        public static String SIXTEEN = "16GB";
        public static String THIRTY_TWO = "32GB";
        public static String SIXTY_FOUR = "64GB";
        public static String DRAG_SCREEN_NOTIFICATION = "drag_screen_notification";
        public static String HOME_SCREEN = "home_screen";
        public static String BOTTOM_NOTIFICATION = "bottom_notification";

        // Notification
        public static String CLICKED = "clicked";
    }

    public static class Screens {
        public final static String ONBOARDING = "onboarding";
        public final static String SPLASH = "splash";
        public final static String HOME = "home";
        public final static String SCANNING = "scanning";
        public final static String REVIEW = "review";
        public final static String COMPRESSION = "compression";
        public final static String RESULT = "result";
        public final static String UNCOMPRESSION = "uncompression";
        public final static String FEEDBACK_HOME = "feedback_home";
        public final static String FEEDBACK_RESULT = "feedback_result";
        public final static String SHORTCUT = "shortcut";
        public final static String MANAGE_APPS = "manage_apps";
        public final static String UNCOMPRESS = "uncompress";

        public final static String PERMISSION_UNKNOWN_SOURCE = "permission_unknown_source";
        public final static String PERMISSION_ACCESSIBILITY = "permission_accessibility";
        public final static String PERMISSION_USAGE_ACCESS = "permission_usage_access";

        public final static String LAUNCHER = "launcher";
        public final static String COMPRESSION_REVIEW = "compression_review";
    }

    public static class CustomDim {
        public final static int UNCOMPRESSION_COUNT = 1;
        public final static int TOTAL_STORAGE = 2;
        public final static int COMPRESSION_TYPE = 3;
        public final static int RUN_COUNT = 4;
        public final static int CUSTOM_ATTRIBUTION = 5;
        public final static int STORAGE_LEFT = 6;
        public final static int DAY_COUNT = 7;
        public final static int COMPRESSION_COUNT = 8;
        public final static int EMAIL = 9;
    }

    public static class CustomDimValue {
        //TOTAL_STORAGE
        public final static String FOUR_GB = "4_GB";
        public final static String EIGHT_GB = "8_GB";
        public final static String SIXTEEN_GB = "16_GB";
        public final static String THIRTY_TWO_GB = "32_GB";
        public final static String SIXTY_FOUR_GB = "64_GB";
        //COMPRESSION_TYPE
        public final static String ADV = "adv_compression";
        public final static String QUICK = "quick_compression";


    }


    //key for apsalar analytics
    public static class Key {
        public final static String COMPRESSION_COMPLETE = "compression_complete";
        public static final String UNCOMPRESS_FINISHED = "uncompress_finished";
        public static final String NOTIFICATION = "notification";
        public static final String REVIEW_TEST = "review_test";
    }

    public static class CustomMetric {
        public final static int APPS_COM = 1;
        public final static int APPS_UNCOM = 2;

    }

}
