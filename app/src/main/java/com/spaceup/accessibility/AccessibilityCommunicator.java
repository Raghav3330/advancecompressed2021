package com.spaceup.accessibility;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.spaceup.APK_GENERATOR;
import com.spaceup.Activities.AfterCallActivity;
import com.spaceup.Activities.FinalResultScreen;
import com.spaceup.App;
import com.spaceup.Asycs.ApkBackupAndUnInstallGenerator;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.cache.LogCleaner.Cache_Clean.RamCleaner;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.models.AppsCompressedModel;
import com.spaceup.models.DataModel;
import com.spaceup.models.StateRetrieveModel;
import com.spaceup.uninstall.activities.AppInfo;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.spaceup.AppShortcut.ShortCutFolder.MOVE_TO_MENU;


/**
 * Created by shashank.tiwari on 10/12/16.
 */

public class AccessibilityCommunicator extends Service {

    private int count;
    private String state;
    private String TAG = "Accessibility";
    private int apkInstalled = 0;
    private HashMap<String, String> dummyApkPathList;
    private ThreadPoolExecutor executor;
    // declaring list of uncompressed apps:- Used in sequentially uninstalling - installing apps
    private ArrayList<String> unCompressedAppList;
    private ArrayList<String> toCompressAppList, toCompressAppName;
    private BroadcastReceiver accessibilityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("com.times.accessibilityReceiver")) {
                if (intent.getExtras().getBoolean("icon_edit_finished")) {
                    Log.d("Accessibility", "Icon Editing FINISHED");

                    AccessibilityAutomation.getSharedInstance().allowReading(true);

                    if (dummyApkPathList != null)
                        dummyApkPathList.clear();
                    // Apk's for first five apps generator call
                    initialApkGeneratorCall();


                } else if (intent.getExtras().getBoolean("single_apk_generation_task_finished")) {

                    String path = intent.getExtras().getString("apk_path");
                    String pkgName = intent.getExtras().getString("pkg_name");

                    Log.d(TAG, "apk generated & apk path pushed to stack " + pkgName);

                    if (dummyApkPathList == null)
                        dummyApkPathList = new HashMap<>();
                    dummyApkPathList.put(pkgName, path);

                    // Check whether a single Batch of apk's are generated or not
                    if (dummyApkPathList.size() == unCompressedAppList.size() || dummyApkPathList.size() == 5) {
                        compressSingleApp();
                    }

                } else if (intent.getExtras().getBoolean("cleaning")) {
                    String cleanPackage = intent.getExtras().getString("clean_package") + ".stash";

                    Uri packageUri = Uri.parse("package:" + cleanPackage);
                    Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageUri);
                    uninstallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(uninstallIntent);

                    if (AccessibilityAutomation.getSharedInstance() != null) {
                        AccessibilityAutomation.getSharedInstance().setCurrentInstallation(cleanPackage);
                        AccessibilityAutomation.getSharedInstance().activateCleaningFlag();
                    }


                } else if (intent.getExtras().getBoolean("compress_single_app")) {

                    if (!AccessibilityAutomation.getSharedInstance().isAbortFlag() && !AccessibilityAutomation.getSharedInstance().isCallFlag()) {

                        if (AccessibilityAutomation.getAllowed()) {

                            if (state != null && !state.equals("single_compress"))
                                AccessibilityAutomation.getSharedInstance().updateAnimation();

                            if (dummyApkPathList != null && dummyApkPathList.size() == 0)
                                initialApkGeneratorCall();
                            else
                                compressSingleApp();
                        }
                    } else {

                        if (unCompressedAppList != null && unCompressedAppList.size() != 0) {
                            Intent i = new Intent("com.times.accessibilityReceiver");
                            i.putExtra("unregister_broadcast", true);
                            LocalBroadcastManager.getInstance(context).sendBroadcast(i);


                            if (executor != null && !executor.isShutdown())
                                executor.shutdown();

                            Log.d("Accessibility_", "Unregister Broadcast");
                            LocalBroadcastManager.getInstance(context).unregisterReceiver(accessibilityReceiver);


                            Handler handle = new Handler();
                            handle.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d("Accessibility_", "StopSelf Called");
                                    AccessibilityCommunicator.this.stopSelf();
                                }

                            }, 4000);


                            if (AccessibilityAutomation.getSharedInstance().isAbortFlag()) {
                                long size = 0;
                                StateRetrieveModel stateRetrieveModel = AppDBHelper.getInstance(context).getUncompressedAppStateData();
                                ArrayList<String> compressed_app_package_list = stateRetrieveModel.getCompressedAppList();
                                if (compressed_app_package_list != null && !compressed_app_package_list.isEmpty()) {
                                    for (String app_package : compressed_app_package_list) {
                                        DataModel data = AppDBHelper.getInstance(context).getDataSize(app_package);
                                        Log.d("AppDBHelper", "package -->" + app_package);
                                        Log.d("AppDBHelper", "getApkSize -->" + data.getApkSize());
                                        Log.d("AppDBHelper", "getCodeSize -->" + data.getCodeSize());
                                        Log.d("AppDBHelper", "getCacheSize -->" + data.getCacheSize());
                                        Log.d("AppDBHelper", "getDataSize -->" + data.getDataSize());
                                        size += data.getApkSize() + data.getCodeSize() + data.getCacheSize() + data.getDataSize();

                                    }

                                   /* ArrayList<String> uncompressed_app_package_list = stateRetrieveModel.getUncompressedAppList();
                                    if (uncompressed_app_package_list != null) {
                                        for (String app_package : compressed_app_package_list) {
                                            String app_dummy_path = AppDBHelper.getInstance(context).getDummyPath(app_package);
                                            if (app_dummy_path != null) {
                                                File file = new File(AppDBHelper.getInstance(context).getDummyPath(app_dummy_path));
                                                if (file.exists())
                                                    file.delete();
                                            }
                                            else
                                                Log.d("app_dummy_path",app_package+"dummy path null");
                                        }
                                    }*/

                                    if (MOVE_TO_MENU == 0) {
                                        AppDBHelper.getInstance(context).deleteCompressedAppData();
                                        Intent new_Activity = new Intent(getApplication(), FinalResultScreen.class);
                                        new_Activity.putExtra("no_of_apps_compressed", "" + compressed_app_package_list.size());
                                        new_Activity.putExtra("size_saved", "" + size);
                                        new_Activity.putExtra("process_abort", true);
                                        new_Activity.putExtra("advance_compression", true);
                                        new_Activity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(new_Activity);
                                    }


                                }

                                Intent finish_uninstaller_intent = new Intent("com.times.finishUnInstallerActivity");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(finish_uninstaller_intent);


                            }
                            AccessibilityAutomation.getSharedInstance().removeOverLay(true, true);

                            count = 0;
                            if (toCompressAppList != null)
                                toCompressAppList.clear();
                            if (toCompressAppName != null)
                                toCompressAppName.clear();
                            if (unCompressedAppList != null)
                                unCompressedAppList.clear();
                            if (dummyApkPathList != null)
                                dummyApkPathList.clear();

                            toCompressAppList = null;
                            toCompressAppName = null;
                            unCompressedAppList = null;
                            dummyApkPathList = null;


                        }

                    }
                } else if (intent.getExtras().getBoolean("unregister_broadcast")) {

                    Intent i = new Intent(context, AccessibilityCommunicator.class);
                    i.putExtra("non_sticky_intent", true);
                    startService(i);
                } else if (intent.getExtras().getBoolean("afterCall")) {

                    afterCallProcess();

                }
                /*else if (intent.getExtras().getBoolean("afterCallProcessCancelled")) {

                    afterCallProcessFinish();

                }*/
                else if (intent.getExtras().getBoolean("install_fail_event_timer")) {


                    try {
                        AccessibilityAutomation.getSharedInstance().getActionFailHandler().refreshUninstallCounter();
                        AccessibilityAutomation.getSharedInstance().getActionFailHandler().setUninstall_handler(true, intent.getExtras().getString("package"), false);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } else if (intent.getAction().equals("com.times.doneApkGenerator")) {

                count++;
                if (toCompressAppList != null)
                    Log.d("AccessibilityY", (toCompressAppList.size()) + " Done_apk_denerator " + count);

                if (toCompressAppList != null && count == toCompressAppList.size()) {
                    Log.d("DHRUV_CHECK", "OVERLAY COMPRESSING DONE");
                    count = 0;
                    AppDBHelper.getInstance(context).deleteCompressedAppData();

                    if (toCompressAppList != null)
                        toCompressAppList.clear();
                    if (toCompressAppName != null)
                        toCompressAppName.clear();
                    if (unCompressedAppList != null)
                        unCompressedAppList.clear();
                    if (dummyApkPathList != null)
                        dummyApkPathList.clear();

                    toCompressAppList = null;
                    toCompressAppName = null;
                    unCompressedAppList = null;
                    dummyApkPathList = null;

                    // worst case to remove all previous icons

                    Intent i = new Intent("com.times.accessibilityReceiver");
                    i.putExtra("unregister_broadcast", true);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(i);


                    if (executor != null && !executor.isShutdown())
                        executor.shutdown();

                    Log.d("Accessibility_", "Unregister Broadcast");
                    LocalBroadcastManager.getInstance(context).unregisterReceiver(accessibilityReceiver);
                    if (state.equals("single_compress")) {
                        AccessibilityAutomation.getSharedInstance().removeSingleAppUI();
                    } else if (state.equals("multi_outer")) {
                        AccessibilityAutomation.getSharedInstance().removeOverLayOuter();
                    } else {

                        AccessibilityAutomation.getSharedInstance().removeOverLay(false, true); //remove overlay with animation
                    }


                    Handler handle = new Handler();
                    handle.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("Accessibility_", "StopSelf Called");
                            AccessibilityCommunicator.this.stopSelf();
                        }

                    }, 4000);


                    // CLEAN PROJECT BY DELETING ZIP AND APK FILES
                    clean_project();


                }


            }
        }
    };
    private int number_of_apps_uninstalled = 0;
    private int pending_flag = 0, index = 0, size = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Communicator onStartCalled");
        Log.d(TAG, "is ON");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (AccessibilityAutomation.getSharedInstance() != null)
            AccessibilityAutomation.getSharedInstance().fromActivity(true);

        if (intent != null && intent.getBooleanExtra("non_sticky_intent", false)) {
            Log.d(TAG, "START_NOT_STICKY called");
            return START_NOT_STICKY;
        } else if (intent != null && intent.getStringArrayListExtra("compressList") != null && intent.getStringArrayListExtra("compressList").size() > 0) {

            if (AccessibilityAutomation.isCleanRam()) {
                // cleanRam();    Commented because of lag user were experiencing

            }

            toCompressAppList = intent.getStringArrayListExtra("compressList");
            toCompressAppName = intent.getStringArrayListExtra("compressName");

            PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.START_TIME_COMPRESSION, System.currentTimeMillis());
            // increment=true;
            if (toCompressAppList != null && toCompressAppList.size() > 0) {
                unCompressedAppList = (ArrayList<String>) toCompressAppList.clone();
                if (AccessibilityAutomation.getSharedInstance() != null) {
                    AccessibilityAutomation.getSharedInstance().initializeAppsToCompress((ArrayList<String>) toCompressAppList.clone(), (ArrayList<String>) toCompressAppName.clone());
                    apkInstalled = 0;
                    count = 0;
                    initializeCompressedAppDB(toCompressAppList);
                    IntentFilter accessibilityReceiverIntentFilter = new IntentFilter();
                    accessibilityReceiverIntentFilter.addAction("com.times.accessibilityReceiver");
                    accessibilityReceiverIntentFilter.addAction("com.times.doneApkGenerator");
                    LocalBroadcastManager.getInstance(this).registerReceiver(accessibilityReceiver,
                            accessibilityReceiverIntentFilter);

                    state = intent.getExtras().getString("state");
                    if (state.equals("single_compress")) {


                        // initiate overlay when single app compress
                        Log.d(TAG, "State Single App Compression");
                        if (AccessibilityAutomation.getSharedInstance() != null)
                            AccessibilityAutomation.getSharedInstance().overlay_singleApp();
                    } else {
                        Log.d(TAG, "Multiple App Compression");
                        if (AccessibilityAutomation.getSharedInstance() != null)
                            AccessibilityAutomation.getSharedInstance().overlay(true, false);
                    }
                }
            }
            return START_STICKY;
        } else if (intent != null && intent.getBooleanExtra("callEnded", false)) {


            StateRetrieveModel stateRetrieveModel = AppDBHelper.getInstance(this).getUncompressedAppStateData();
            if ((stateRetrieveModel.getUncompressedAppList() == null || stateRetrieveModel.getUncompressedAppList().size() == 0) && stateRetrieveModel.getUninstalRemainingPackage() == null) {
                Log.d(TAG, "Nothing to do after Call");
            } else {
                Intent afterCallIntent = new Intent(this, AfterCallActivity.class);
                afterCallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(afterCallIntent);

            }


            return START_STICKY;
        } else if (intent != null && intent.getBooleanExtra("continue_after_call", false)) {



               /* Intent afterCallIntent = new Intent(this, AfterCallActivity.class);
                afterCallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(afterCallIntent);*/

            IntentFilter accessibilityReceiverIntentFilter = new IntentFilter();
            accessibilityReceiverIntentFilter.addAction("com.times.accessibilityReceiver");
            accessibilityReceiverIntentFilter.addAction("com.times.doneApkGenerator");
            LocalBroadcastManager.getInstance(this).registerReceiver(accessibilityReceiver,
                    accessibilityReceiverIntentFilter);


            Intent apk_generation_done_event = new Intent("com.times.accessibilityReceiver");
            apk_generation_done_event.putExtra("afterCall", true);
            LocalBroadcastManager.getInstance(this).sendBroadcast(apk_generation_done_event);


            return START_STICKY;
        } else {
            return START_NOT_STICKY;
        }
    }

    void afterCallProcess() {
        StateRetrieveModel stateRetrieveModel = AppDBHelper.getInstance(this).getUncompressedAppStateData();

        Log.d("AccessibilityY", stateRetrieveModel.getUncompressedAppList().size() + "");
        if (stateRetrieveModel.getUninstalRemainingPackage() != null && AccessibilityAutomation.getSharedInstance() != null) {
            Log.d("AccessibilityY", stateRetrieveModel.getUninstalRemainingPackage() + " UnInstall Remaining");
            AccessibilityAutomation.getSharedInstance().allowReading(true);
            unCompressedAppList = stateRetrieveModel.getUncompressedAppList();
            toCompressAppName = stateRetrieveModel.getUncompressedAppNameList();
            dummyApkPathList = stateRetrieveModel.getDummyApkPathList();

            ArrayList<String> animation_list = (ArrayList<String>) unCompressedAppList.clone();
            animation_list.add(0, stateRetrieveModel.getUninstalRemainingPackage());
            ArrayList<String> animation_name_list = (ArrayList<String>) toCompressAppName.clone();
            animation_name_list.add(0, stateRetrieveModel.getUninstalRemainingPackage());

            toCompressAppList = (ArrayList<String>) animation_list.clone();
            count = 0;

            AccessibilityAutomation.getSharedInstance().initializeAppsToCompress(animation_list, animation_name_list);

            AccessibilityAutomation.getSharedInstance().overlay(false, false);

            AccessibilityAutomation.getSharedInstance().getActionFailHandler().setUninstall_handler(true, stateRetrieveModel.getUninstalRemainingPackage(), false);
            AccessibilityAutomation.getSharedInstance().getActionFailHandler().refreshUninstallCounter();
            Uri packageUri = Uri.parse("package:" + stateRetrieveModel.getUninstalRemainingPackage());
            Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageUri);
            uninstallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            App.getInstance().startActivity(uninstallIntent);

            showListPackage(unCompressedAppList);

        } else if (stateRetrieveModel.getUncompressedAppList() != null && stateRetrieveModel.getUncompressedAppList().size() > 0) {
            AccessibilityAutomation.getSharedInstance().allowReading(true);

            unCompressedAppList = stateRetrieveModel.getUncompressedAppList();
            toCompressAppName = stateRetrieveModel.getUncompressedAppNameList();
            dummyApkPathList = stateRetrieveModel.getDummyApkPathList();


            ArrayList<String> animation_list = (ArrayList<String>) unCompressedAppList.clone();
            // animation_list.add(0,stateRetrieveModel.getUninstalRemainingPackage());
            ArrayList<String> animation_name_list = (ArrayList<String>) toCompressAppName.clone();
            // animation_name_list.add(0,stateRetrieveModel.getUninstalRemainingPackage());

            toCompressAppList = (ArrayList<String>) animation_list.clone();
            count = 0;
            state = "multiple_app";

            AccessibilityAutomation.getSharedInstance().initializeAppsToCompress(animation_list, animation_name_list);
            AccessibilityAutomation.getSharedInstance().overlay(false, false);

            showListPackage(unCompressedAppList);


           /* if (state != null && !state.equals("single_compress"))
                AccessibilityAutomation.getSharedInstance().updateAnimation();*/

            if (dummyApkPathList != null && dummyApkPathList.size() == 0)
                initialApkGeneratorCall();
            else
                compressSingleApp();

           /* Intent uninstallIntent = new Intent("com.times.accessibilityReceiver");
            uninstallIntent.putExtra("compress_single_app", true);
            LocalBroadcastManager.getInstance(context).sendBroadcast(uninstallIntent);*/

        }

    }

    private void cleanRam() {
        List<AppInfo> appList;
        ArrayList<String> temp_packages;
        temp_packages = new ArrayList<>();
        appList = AppDBHelper.getInstance(this).getAppDetails();
        for (AppInfo applist : appList) {
            temp_packages.add(applist.getPkgName());
        }

        RamCleaner ramCleaner = new RamCleaner(this, temp_packages);
        ramCleaner.execute();
    }

    private void showListPackage(ArrayList<String> toCompressAppList) {
        for (String s : toCompressAppList)
            Log.d("AccessibilityY", s);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void clean_project() {
        File file = new File(Constants.UNZIP + "/hcjb1.apk");
        file.delete();
        file = new File(Constants.UNZIP + "/hcjb2.apk");
        file.delete();
        file = new File(Constants.UNZIP + "/hcjb3.apk");
        file.delete();
        file = new File(Constants.UNZIP + "/hcjb4.apk");
        file.delete();
        file = new File(Constants.UNZIP + "/hcjb1.zip");
        file.delete();
        file = new File(Constants.UNZIP + "/hcjb2.zip");
        file.delete();
        file = new File(Constants.UNZIP + "/hcjb3.zip");
        file.delete();
        file = new File(Constants.UNZIP + "/hcjb4.zip");
        file.delete();
        file = new File(Constants.UNZIP + "/babystash.zip");
        file.delete();
        file = new File(Constants.UNZIP + "/babystash.apk");
        file.delete();


    }

    private void initialApkGeneratorCall() {

        // send list to Accessibility Service


        // Extract data included in the Intent
        /**
         *
         *   APK EXTRACTED FROM PACKAGE MANAGER
         *
         */
        /**
         * INSTALL BABY STASH
         */

        if (executor == null)
            executor = new ThreadPoolExecutor(1, 10, 10, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

        index = 0;
        apk_generator(index);

        //  Toast.makeText(getApplicationContext(), "BABY STASH INSTALL STARTED", Toast.LENGTH_SHORT).show();

        Log.d("Accessibility", "BABY STASH INSTALL INTENT");

    }

    void apk_generator(int index) {


        if (unCompressedAppList.size() > 5)
            size = 5;
        else
            size = unCompressedAppList.size();


        for (int j = 0; j < size; j++) {
            Log.d(TAG, "DUMMY APP GENERATOR " + j);

            APK_GENERATOR apk_generator = new APK_GENERATOR(getApplicationContext(), j, unCompressedAppList.get(j), toCompressAppName.get(j), apkInstalled);

            try {
                executor.execute(apk_generator);
                //Check here for crash
            } catch (Exception e) {
                e.printStackTrace();
            }
            apkInstalled++;

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Accessibility", "Communicator onDestroy called");

        if (AccessibilityAutomation.getSharedInstance() != null) {
            AccessibilityAutomation.getSharedInstance().fromActivity(false);
        }
    }

    // This will be an atomic fucntion to handle cancle or call during the process
    private synchronized void compressSingleApp() {


        if (dummyApkPathList != null && dummyApkPathList.size() > 0 && UtilityMethods.isNotEmptyCollection(unCompressedAppList)) {

            // Throwing Uninstall event of app;.
            // THIS ARRAY IS created only because ApkBackupAndUnInstallGenerator takes input an array
            // Therefore it will only have element
            ArrayList<String> unCompressedApp = new ArrayList<>();
            try {
                unCompressedApp.add(unCompressedAppList.remove(0));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (toCompressAppName != null && toCompressAppName.size() > 0)
                toCompressAppName.remove(0);

            // APK path of Dummy App
            String dummyApkPath = dummyApkPathList.get(unCompressedApp.get(0));
            dummyApkPathList.remove(unCompressedApp.get(0));


            ApkBackupAndUnInstallGenerator apkBackupAndUnInstallGenerator = new ApkBackupAndUnInstallGenerator(AccessibilityCommunicator.this, unCompressedApp, dummyApkPath);
            apkBackupAndUnInstallGenerator.start();

        }


    }

    private void initializeCompressedAppDB(ArrayList<String> compressedAppArrayList) {
        AppDBHelper.getInstance(this).deleteCompressedAppData();

        int x = 0;
        for (String app_package : compressedAppArrayList) {
            AppsCompressedModel acm = new AppsCompressedModel();
            acm.setAppName(toCompressAppName.get(x));
            acm.setFolder("");
            acm.setPackageName(app_package);
            acm.setInstall_status("0");
            acm.setUninstall_status("0");
            AppDBHelper.getInstance(this).addCompressedApp(acm);
            x++;
        }
    }
}