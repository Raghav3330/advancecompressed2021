package com.spaceup;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.provider.Settings;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.Asycs.TotalAppSizeAsync;
import com.spaceup.Runnable.AccRunnable;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.data.version_one.AppDBHelper;

import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan;
import io.github.inflationx.calligraphy3.TypefaceUtils;


public class TakeAccessibilityfromNoti extends AppCompatActivity {
    public static boolean requestTurnOn = false;
    View oView1;
    WindowManager wm1;
    private Bundle mGAParams;
    Thread accCheckThread;
    AccRunnable accRunnable;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 55:
                if (null != accRunnable) {
                    Log.d("backdebug", "stopping acc runnable");
                    accRunnable.stop();
                }
                Log.d("backdebug", "inside finish 55 ");

                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {

                }


                if (UtilityMethods.getInstance().isAccessibilityServiceRunning(TakeAccessibilityfromNoti.this)) {
                    // Analytics Start
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
                    mGAParams.putString("action", AnalyticsConstants.Action.ACCESSIBILITY);

                    mGAParams.putString("label", AnalyticsConstants.Label.DRAG_SCREEN_NOTIFICATION);
                    mGAParams.putLong("value", 1);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

                    Intent intent1 = new Intent(TakeAccessibilityfromNoti.this, YouJustLostAnApp_POPUP_gained.class);
                    startActivity(intent1);


                } else {
                    // Analytics Start
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
                    mGAParams.putString("action", AnalyticsConstants.Action.ACCESSIBILITY);
                    mGAParams.putString("label", AnalyticsConstants.Label.DRAG_SCREEN_NOTIFICATION);
                    mGAParams.putLong("value", 0);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                    // Analytics end

                }
                break;

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppDBHelper.getInstance(getApplicationContext()).getAppDetails().size() == 0) {
            //Initialize DBS
            new TotalAppSizeAsync(getApplicationContext()).execute();
        }
        PrefManager.getInstance(getApplicationContext()).set_Alternate_LOSTAPP("noti");
        if (!UtilityMethods.getInstance().isAccessibilityServiceRunning(TakeAccessibilityfromNoti.this)) {
            openAccessibilityHelper();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (UtilityMethods.getInstance().isAccessibilityServiceRunning1(getApplicationContext())) {
            finish();
            Toast.makeText(getApplicationContext(), "Cool! Next Time we will compress apps for you!", Toast.LENGTH_LONG).show();
        }
    }

    public void openAccessibilityHelper() {
        try {
            wm1.removeView(oView1);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        startActivityForResult(intent, 55);


        LayoutInflater accessibilityHelper = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        oView1 = accessibilityHelper.inflate(R.layout.accessibility_permission, null);
        WindowManager.LayoutParams params1;
        TextView span_text_view = (TextView) oView1.findViewById(R.id.span_text_view);
        TextView permission_helper_text = (TextView) oView1.findViewById(R.id.permission_helper_text);
        String first = "";
        String second = "\uD83C\uDF1F SpaceUp \uD83C\uDF1F";
        final String totalString = first + second;
        Spannable spanText = new SpannableString(totalString);
        //set color to white
        spanText.setSpan(new ForegroundColorSpan(getResources()
                .getColor(R.color.spaceup_black)), first.length(), first.length() + second.length(), 0);
        //set text to bradon medium
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon_medium.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spanText.setSpan(typefaceSpan, first.length(), first.length() + second.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        span_text_view.setText(spanText, TextView.BufferType.SPANNABLE);

        String f = "Give permission to ";
        String s = "save upto 1.2 GB";
        final String t = f + s;
        Spannable spannableText = new SpannableString(t);
        //set color to white
        spannableText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(TakeAccessibilityfromNoti.this, R.color.text_color)), f.length(), f.length() + s.length(), 0);
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spannableText.setSpan(typefaceSpan, f.length(), f.length() + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        permission_helper_text.setText(spannableText, TextView.BufferType.SPANNABLE);

        ImageView cancel = (ImageView) oView1.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        params1 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);
        //params1.gravity = Gravity.TOP;
        params1.gravity = Gravity.BOTTOM;
        params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;


        wm1 = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        try {
            wm1.addView(oView1, params1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        accRunnable = new AccRunnable(wm1, oView1, this);
        accCheckThread = new Thread(accRunnable, "acc");
        accCheckThread.start();

        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.PERMISSION_ACCESSIBILITY);
    }
}
