package com.spaceup.services;

import android.animation.ValueAnimator;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.spaceup.Activities.StashFeedPopup;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.R;
import com.spaceup.RemoteConfig;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;

import static java.lang.Math.abs;

/**
 * Created by dhurv on 21-11-2017.
 */

public class StashBubbleService extends Service {

    public StashBubbleService() {
        super();
    }

    private static final String TAG = "PostBubbleService";

    private WindowManager.LayoutParams params, closeChatHeadParams, backdropParams, tagParams;
    private WindowManager windowManager;
    private int SENSITIVITY = 10;
    private View closeChatHead;
    private View backdrop, chatHead, tag_layout;
    private boolean tagHidden;

    private int displayHeight, bubbleLimit;
    private ImageView launcherBottom;
    private LayoutInflater inflater;

    @Override
    public void onCreate() {
        super.onCreate();



        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        displayHeight = size.y;
        bubbleLimit = displayHeight - (int) UtilityMethods.convertDpToPixel(64.0f, this);

        inflateBubble();
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.STASH_REF,
                AnalyticsConstants.Action.BUBBLE,
                AnalyticsConstants.Label.FIRED,
                null,
                false,
                null);

    }


    private void inflateBubble() {

        chatHead = inflater.inflate(R.layout.sticker_bubble_post, null);
        backdrop = inflater.inflate(R.layout.backdrop_layout, null);
        tag_layout = inflater.inflate(R.layout.post_bubble_tag, null);

        closeChatHead = inflater.inflate(R.layout.sticker_close, null);
        launcherBottom = (ImageView) closeChatHead.findViewById(R.id.launcher_bottom);

        backdropParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                PixelFormat.TRANSLUCENT);


        closeChatHeadParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        tagParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                (int) UtilityMethods.convertDpToPixel(64.0f, this),
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.LEFT;

        tagParams.gravity = Gravity.TOP | Gravity.LEFT;

        params.x = windowManager.getDefaultDisplay().getWidth() - (int) UtilityMethods.convertDpToPixel(60.0f, this);
        params.y = (windowManager.getDefaultDisplay().getHeight() / 2) - 300;

        tagParams.x = windowManager.getDefaultDisplay().getWidth() - (int) UtilityMethods.convertDpToPixel(260.0f, this);
        tagParams.y = (windowManager.getDefaultDisplay().getHeight() / 2) - 300;


        chatHead.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        closeChatHead.setVisibility(View.VISIBLE);
                        backdrop.setVisibility(View.VISIBLE);

                        if (!tagHidden)
                            hideTag();

                        return true;
                    case MotionEvent.ACTION_UP:

                        int count = 0;

                        if (((initialX - params.x) >= 0 && (initialX - params.x) <= SENSITIVITY) || (initialX - params.x) <= 0 && (initialX - params.x) >= -SENSITIVITY) {
                            count++;
                        }
                        if (((initialY - params.y) >= 0 && (initialY - params.y) <= SENSITIVITY) || (initialY - params.y) <= 0 && (initialY - params.y) >= -SENSITIVITY) {
                            count++;
                        }

                        backdrop.setVisibility(View.GONE);
                        if (count == 2) {

                            openPopUp();
                            closeChatHead.setVisibility(View.INVISIBLE);
                        } else if (isViewOverlapping(chatHead, closeChatHead)) {
                            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.STASH_REF,
                                    AnalyticsConstants.Action.BUBBLE,
                                    AnalyticsConstants.Label.CLOSED,
                                    null,
                                    false,
                                    null);
                            windowManager.removeViewImmediate(chatHead);
                            windowManager.removeViewImmediate(closeChatHead);
                            int installCount = PrefManager.getInstance(getApplicationContext()).getInt(PrefManager.PRESSED_STASH_INSTALL);
                            installCount++;
                            PrefManager.getInstance(getApplicationContext()).putInt(PrefManager.PRESSED_STASH_INSTALL, installCount);
                            if (installCount > RemoteConfig.getInt(R.string.threshold_drag_close_bubble)) {
                                PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.GLOBAL_TURNOFF_BUBBLE_PERMANENT, true);
                            }
                            stopSelf();
                        } else {
                            closeChatHead.setVisibility(View.INVISIBLE);
                            int moveToRight = windowManager.getDefaultDisplay().getWidth() - (int) UtilityMethods.convertDpToPixel(60.0f, StashBubbleService.this);
                            overlayAnimation(chatHead, params.x, moveToRight);
                        }

                        return true;
                    case MotionEvent.ACTION_MOVE:
                        params.x = initialX
                                + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY
                                + (int) (event.getRawY() - initialTouchY);

                        if (params.y < bubbleLimit)
                            windowManager.updateViewLayout(chatHead, params);

                        if (isViewOverlapping(chatHead, closeChatHead)) {
                            System.out.println("overlap");
                            launcherBottom.setVisibility(View.VISIBLE);
                            chatHead.setVisibility(View.INVISIBLE);
                        } else {
                            closeChatHead.setVisibility(View.VISIBLE);
                            launcherBottom.setVisibility(View.INVISIBLE);
                            chatHead.setVisibility(View.VISIBLE);

                        }

                        return true;
                }
                return false;
            }

        });

        closeChatHeadParams.gravity = Gravity.BOTTOM | Gravity.CENTER;

        closeChatHead.setVisibility(View.INVISIBLE);

        backdrop.setVisibility(View.GONE);

        try {
            windowManager.addView(backdrop, backdropParams);
            windowManager.addView(closeChatHead, closeChatHeadParams);
            windowManager.addView(chatHead, params);
            windowManager.addView(tag_layout, tagParams);
        } catch (Exception e) {
            e.printStackTrace();
            stopSelf();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (!tagHidden)
                    hideTag();

            }
        }, 10000);

    }

    private void openPopUp() {
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.STASH_REF,
                AnalyticsConstants.Action.BUBBLE,
                AnalyticsConstants.Label.CLICKED,
                null,
                false,
                null);
        Intent i = new Intent(this, StashFeedPopup.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        stopSelf();
    }

    private void hideTag() {
        try {
            tagHidden = true;
            windowManager.removeViewImmediate(tag_layout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isViewOverlapping(View firstView, View secondView) {
        int[] firstPosition = new int[2];
        int[] secondPosition = new int[2];

        firstView.measure(View.MeasureSpec.EXACTLY,
                View.MeasureSpec.EXACTLY);
        firstView.getLocationOnScreen(firstPosition);
        secondView.getLocationOnScreen(secondPosition);

        int r = firstView.getMeasuredWidth() + firstPosition[1];
        int l = secondPosition[1];

        System.out.println(r + " : r + l : " + l);

        return abs(r - l) < 100 && (r != 0 && l != 0);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (chatHead != null)
            try {
                windowManager.removeView(backdrop);
                windowManager.removeView(chatHead);
                windowManager.removeView(closeChatHead);
                if (!tagHidden)
                    hideTag();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }



    public void overlayAnimation(final View view2animate, int viewX, final int endX) {
        ValueAnimator translateLeft = ValueAnimator.ofInt(viewX, endX);
        translateLeft.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                updateViewLayout(view2animate, val, null, null, null);
            }
        });
        translateLeft.setInterpolator(new DecelerateInterpolator());
        translateLeft.setDuration(100);
        translateLeft.start();
    }

    public void updateViewLayout(View view, Integer x, Integer y, Integer w, Integer h) {
        try {
            if (view != null) {
                WindowManager.LayoutParams lp = (WindowManager.LayoutParams) view.getLayoutParams();

                if (x != null) lp.x = x;
                if (y != null) lp.y = y;
                if (w != null && w > 0) lp.width = w;
                if (h != null && h > 0) lp.height = h;

                windowManager.updateViewLayout(view, lp);
            }
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}