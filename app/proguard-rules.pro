# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\Sajal\AppData\Local\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

#-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient
-keep class com.apsalar.sdk.** { *; }
-keep class org.apache.**{
    *;
}
-keep class javax.** { *; }
-keep class pxb.android.** {
    *;
}

-keep class org.json.simple.** {
    *;
}
-keep class kellinwood.logging.** {
    *;
}
-keep class kellinwood.security.zipsigner.** {
    *;
}
-keep class org.spongycastle.** {
   *;
}
-keep class kellinwood.logging.android.** {
    *;
}
-keep class org.spongycastle.** {
    *;
}
-dontwarn javax.naming.**

-keep class android.content.pm.** {
    *;
}

-keep class com.google.api.client.googleapis.**{
    *;
}
-keep class com.google.api.client.googleapis.extensions.android.**{
    *;
}
-keep class com.google.api.services.vision.v1.**{
*;
}

-keep class com.google.api.client.**{
    *;
}

-keep class com.google.api.client.extensions.android.**{
    *;
}
-keep class com.google.api.client.json.gson.**{
*;
}
-keep class com.google.api.client.json.jackson2.**{
*;
}


-keep class com.google.api.client.auth.**{
*;
}

-keep class com.google.common.**{
*;
}
-dontwarn com.squareup.okhttp.**
-keep class com.google.android.gms.** { *; }
-dontwarn com.google.android.gms.**

-keep class com.google.**
-dontwarn com.google.**