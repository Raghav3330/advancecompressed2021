package com.spaceup.accessibility.permission;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityAutomation;

import java.util.List;

import static android.view.accessibility.AccessibilityNodeInfo.ACTION_CLEAR_FOCUS;
import static android.view.accessibility.AccessibilityNodeInfo.ACTION_CLICK;
import static android.view.accessibility.AccessibilityNodeInfo.ACTION_SCROLL_BACKWARD;
import static android.view.accessibility.AccessibilityNodeInfo.ACTION_SCROLL_FORWARD;

/**
 * Created by dhurv on 20-03-2017.
 */

public class UnknownSourcePermission {
    private static UnknownSourcePermission methods = null;
    private boolean PERMISSION_GRANTED = false;
    private boolean UNKNOWN_SOURCES_CLICKED = false;
    String TAG = "UnknownSourcePermission";
    private Context context;

    public static UnknownSourcePermission getInstance() {
        if (methods == null) {
            methods = new UnknownSourcePermission();
        }
        return methods;
    }

    public void launchIntent(Context context, Intent intentActivty) {
        if (AccessibilityAutomation.getSharedInstance() == null) {
            return;
        }
        new AnalyticsController(context).sendAnalytics(
                AnalyticsConstants.Category.PERMISSION,
                AnalyticsConstants.Action.UNKNOWN_SOURCES_AUTO,
                AnalyticsConstants.Label.FIRED,
                null,
                false,
                null);
        AccessibilityAutomation.getSharedInstance().overLayScanning(intentActivty);
        PERMISSION_GRANTED = false;
        UNKNOWN_SOURCES_CLICKED = false;
        this.context = context;
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS);
        if (intent.resolveActivity(packageManager) != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Log.d("component", "Intent available to handle action" + intent.resolveActivity(packageManager));
            try {
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        AccessibilityAutomation.getSharedInstance().startUnkownPermission();
    }

    public void takePermissionfromAccessibility(final AccessibilityNodeInfo pNodeInfo) {

        Log.d("STATUS_PERMISSION", PERMISSION_GRANTED + " " + UNKNOWN_SOURCES_CLICKED);
        if (pNodeInfo == null || PERMISSION_GRANTED)
            return;

        // Step 1 find SpaceUp and Click  | Step 2 Click Permit


        //Scroll the page
        gainPermission(pNodeInfo);


    }

    private void gainPermission(final AccessibilityNodeInfo pNodeInfo) {

        List<AccessibilityNodeInfo> list;
        list = pNodeInfo.findAccessibilityNodeInfosByText("Unknown");
        if (list.size() != 0) {
            for (final AccessibilityNodeInfo node : list) {
                // One second Delay
                Log.d(TAG, "gainPermission: UNKNOW_FOUND");
                if (!PERMISSION_GRANTED) {
                    UNKNOWN_SOURCES_CLICKED = true;
                    performEvent(node, 1);

                }

            }
        }


        if (UNKNOWN_SOURCES_CLICKED) {
            pNodeInfo.performAction(ACTION_CLEAR_FOCUS);
            list = pNodeInfo.findAccessibilityNodeInfosByText("OK");
            if (UtilityMethods.isNotEmptyCollection(list)) {
                Log.d(TAG, "gainPermission: OK_FOUND");

                for (final AccessibilityNodeInfo node : list) {

                    performEvent(node, 2);


                }
            }
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scroll(pNodeInfo);
                pNodeInfo.performAction(ACTION_CLEAR_FOCUS);
            }
        }, 1000);

    }


    private void performEvent(AccessibilityNodeInfo node, int event) {

        while (node != null) {
            if (node.isClickable()) {
                node.performAction(ACTION_CLICK);
                if (event == 2) {
                    PERMISSION_GRANTED = true;
                    UNKNOWN_SOURCES_CLICKED = true;

                    AccessibilityAutomation.getSharedInstance().globalActionBack();
                    final AccessibilityNodeInfo finalNode = node;
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AccessibilityAutomation.getSharedInstance().globalActionBack();
                            AccessibilityAutomation.getSharedInstance().removeScanningOverLay();
                            new AnalyticsController(context).sendAnalytics(
                                    AnalyticsConstants.Category.PERMISSION,
                                    AnalyticsConstants.Action.UNKNOWN_SOURCES_AUTO,
                                    AnalyticsConstants.Label.TAKEN,
                                    null,
                                    false,
                                    null);
                            // Above method will also open UninstallerActivity
                        }
                    }, 500);

                    // Start Junk Recommendation Service

                }

                break;
            }
            node = node.getParent();

        }

    }

    private void scroll(AccessibilityNodeInfo node) {
        while (node != null) {

            if (node.isScrollable()) {

                node.performAction(ACTION_SCROLL_FORWARD);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                node.performAction(ACTION_SCROLL_BACKWARD);


                break;
            }
            node = node.getParent();

        }

    }

}
