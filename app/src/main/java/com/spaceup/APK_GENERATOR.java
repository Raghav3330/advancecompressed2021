package com.spaceup;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.apkgenerator.utilities.FileUtils;
import com.spaceup.apkgenerator.utilities.KeyStoreDetails;
import com.spaceup.apkgenerator.utilities.ZipSignerAppResourceAdapter;
import com.spaceup.data.version_one.AppDBHelper;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import kellinwood.security.zipsigner.AutoKeyException;
import kellinwood.security.zipsigner.ZipSigner;
import kellinwood.security.zipsigner.optional.CustomKeySigner;
import pxb.android.axml.AxmlReader;
import pxb.android.axml.AxmlVisitor;
import pxb.android.axml.AxmlWriter;
import pxb.android.axml.NodeVisitor;


/**
 * Created by dhruv on 28-11-2016.
 */


public class APK_GENERATOR extends Thread {
    private final String NS = "http://schemas.android.com/apk/res/android";
    private int mGenerated_apk_count = 0;
    private long t2;

    private Context context;
    private String TAG = "THREAD CLASS";
    private int index;
    private String appname;
    //generate dummy apks
    private String packagename;
    private String newPackageFullName;
    private String finalApk;
    private boolean needRemoveConflict;
    private boolean needRemoveLib;
    private KeyStoreDetails keyDetails;

    public APK_GENERATOR(Context context, int folderIndex, String packagename, String appname, int index) {
        this.packagename = packagename;
        this.context = context;
        this.appname = appname;
        this.index = folderIndex;
    }

    private Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {


        Log.d("width+height", bmp2.getWidth() + " " + bmp2.getHeight());
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth() + (bmp2.getWidth() / 2), bmp1.getHeight() + (bmp2.getHeight() / 2), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, bmp1.getHeight() - (bmp2.getHeight() / 2) - 10, bmp1.getWidth() - (bmp2.getWidth() / 2) - 10, null);
        return bmOverlay;
    }

    @Override

    public void run() {
        Log.d("APK_GEN", "MANIFEST STARTED FOR APP" + packagename);
        t2 = System.currentTimeMillis();
        Log.d(TAG, "MANIFEST STARTED FOR APP" + packagename);
        extract_icon(packagename, "hcjb" + (index + 1), index);
        modifyManifest(new String[]{Constants.UNZIP + "hcjb" + (index + 1) + "/AndroidManifest.xml", packagename , "Whatsapp"}, "hcjb" + (index + 1), index + 1);

    }

    private void extract_icon(String packagename, String temp, int flag) {

        /**
         *
         * Make Black White Icons
         *
         */

        try {

            Drawable icon = null;
            try {
                icon = context.getPackageManager().getApplicationIcon(packagename);
            } catch (PackageManager.NameNotFoundException e) {
                // Shortcut move to menu handling
                String APKFilePath = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + packagename + ".stash";
                PackageManager pm = context.getPackageManager();
                ApplicationInfo pi = pm.getApplicationInfo(APKFilePath, 0);

                // Logo of the app from DIR....
                pi.sourceDir = APKFilePath;
                pi.publicSourceDir = APKFilePath;
                //
                icon = pi.loadIcon(pm);
                e.printStackTrace();
            }
            Bitmap myBitmap = ((BitmapDrawable) icon).getBitmap();
            FileOutputStream out = null;
            try {
                //TODO change it later back to UNZIP
//                out = new FileOutputStream(Constants.UNZIP + temp + "/res/drawable/ic_launcher.png");
//                myBitmap = toGrayscale(myBitmap);
//                myBitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                // PNG is a lossless format, the compression fact   or (100) is ignored

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private Bitmap toGrayscale(Bitmap bmpOriginal) {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();
        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        // /Image made black white adding icon

        Drawable icon = context.getResources().getDrawable(R.drawable.compressed);
        Bitmap stash_icon = ((BitmapDrawable) icon).getBitmap();

        bmpGrayscale = Bitmap.createScaledBitmap(bmpGrayscale, 100, 100, false); //scale dummy icon
        stash_icon = Bitmap.createScaledBitmap(stash_icon, 35, 35, false);       //scale stash icon


        Bitmap new_image = overlay(bmpGrayscale, stash_icon);
        return new_image;

    }

    private void modifyManifest(final String[] args, String temp, int flag) {
        try {
            String androidManifestBinXml = args[0];
            Log.d(TAG, androidManifestBinXml);
            newPackageFullName = args[1].replace("!", "").replace("%", "");
//            InputStream is = new FileInputStream(androidManifestBinXml);
//            byte[] xml = new byte[is.available()];
//            is.read(xml);
//            Log.d(TAG, is.toString());
//            Log.d(TAG, xml.toString());
//            is.close();
//            AxmlReader ar = new AxmlReader(xml);
//            AxmlWriter aw = new AxmlWriter();
//            ar.accept(new AxmlVisitor(aw) {
//                @Override
//                public NodeVisitor child(String ns, String name) {
//                    return new MyNodeVisitor(super.child(ns, name), name);
//                }
//            });
//            // if(changed){
//            byte[] modified = aw.toByteArray();
//            FileOutputStream fos = new FileOutputStream(androidManifestBinXml);
//            fos.write(modified);
//            fos.close();
            Log.d(TAG, "manifest status " + androidManifestBinXml + " index " + flag);
            generate_asset(packagename, packagename, "hcjb" + (index + 1));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void generate_asset(String name_of_apk, String packagename, String temp) {
        Log.d(TAG, "WE ARE IN ASSET");

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {

            docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("package_path");
            String apk_dir = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + name_of_apk + ".stash";
            apk_dir = apk_dir.substring(1, apk_dir.length());
            rootElement.appendChild(doc.createTextNode(apk_dir));
            doc.appendChild(rootElement);
//            File src = new File(Environment.getExternalStorageDirectory().getPath() + Constants.STASH + "/item.xml");
//            File dest = new File(Constants.UNZIP + temp + "/assets/item.xml");
//            FileUtils.copy(src, dest);
//            FileUtils.saveXmlFile(Constants.UNZIP + temp + "/assets/", "item.xml", doc);
            generate(packagename, temp);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void generate(String packagename, String temp) {
        try {
            Log.d(TAG, "WE ARE IN GENERATE");

            //finalApk=getFinalApkPath(Envi);
            String keyPassword = context.getString(R.string.key_password);
            String aliasName = context.getString(R.string.alias_name);
            String aliaspassword = context.getString(R.string.alias_password);
            keyDetails = new KeyStoreDetails(keyPassword, aliasName, aliaspassword);
            zip_signer(temp);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAllFiles(File dir, List<File> fileList) {
        File[] files = dir.listFiles();
//            for (File file : files) {
//                fileList.add(file);
//                if (file.isDirectory()) {
//                    System.out.println("directory:" + file.getCanonicalPath());
//                    getAllFiles(file, fileList);
//                } else {
//                    System.out.println("     file:" + file.getCanonicalPath());
//                }
//            }
    }

    private void addToZip(File directoryToZip, File file, ZipOutputStream zos) throws
            IOException {

        FileInputStream fis = new FileInputStream(file);

        // we want the zipEntry's path to be a relative path that is relative
        // to the directory being zipped, so chop off the rest of the path
        String zipFilePath = file.getCanonicalPath().substring(directoryToZip.getCanonicalPath().length() + 1,
                file.getCanonicalPath().length());
        //System.out.println("Writing '" + zipFilePath + "' to zip file");
        ZipEntry zipEntry = new ZipEntry(zipFilePath);
        zos.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }

        zos.closeEntry();
        fis.close();
    }

    private void writeZipFile(File directoryToZip, List<File> fileList) {

        try {
            FileOutputStream fos = new FileOutputStream(directoryToZip.getAbsolutePath() + ".zip");
            ZipOutputStream zos = new ZipOutputStream(fos);

            for (File file : fileList) {
                if (!file.isDirectory()) { // we only zip files, not directories
                    addToZip(directoryToZip, file, zos);
                }
            }

            zos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void zipFolder(String directoryToZipPath) throws IOException {
        File directoryToZip = new File(directoryToZipPath);

        List<File> fileList = new ArrayList<>();
        System.out.println("---Getting references to all files in: " + directoryToZip.getCanonicalPath());
        getAllFiles(directoryToZip, fileList);
        System.out.println("---Creating zip file");
        writeZipFile(directoryToZip, fileList);
        System.out.println("---Done");
    }

    private void zip_signer(String temp) {
        try {
            Log.d(TAG, "WE ARE IN ZIP_SIGNER");

            zipFolder(Constants.UNZIP + temp);

            String inputFile = Constants.UNZIP + temp + ".zip";
            try {
                ZipSigner zipSigner = new ZipSigner();
                zipSigner.setResourceAdapter(new ZipSignerAppResourceAdapter(context.getResources()));
                File keystoreFile;
                keystoreFile = new File(Environment.getExternalStorageDirectory() + Constants.STASH + keyDetails.getAssetsPath());
                char[] keyPass = keyDetails.getPassword().toCharArray();
                char[] aliasPass = keyDetails.getAliasPassword().toCharArray();
                finalApk = Constants.UNZIP + temp + ".apk";
                String signatureAlgorithm = "SHA1withRSA";
//                CustomKeySigner.signZip(zipSigner, keystoreFile.getAbsolutePath(), keyPass,
//                        keyDetails.getAlias(), aliasPass, signatureAlgorithm, inputFile, finalApk);
                if (zipSigner.isCanceled()) {
                    Log.d(TAG, "Signing cancelled");
                } else {
                    Log.d(TAG, "Signing Complete " + " " + (System.currentTimeMillis() - t2) + " millis");
                    mGenerated_apk_count++;
                    /**
                     *
                     * WAIT FOR BABYSTASH TO INSTALL
                     *
                     */

                    String path = Constants.UNZIP + "/hcjb" + (index + 1) + ".apk";

                    Log.d("APK_GEN", path);
                    ContentValues values = new ContentValues();
                    values.put("DummyAppPath", path);
                    AppDBHelper.getInstance(context).updateCompressedApp(values, packagename);
                    Intent apk_generation_done_event = new Intent("com.times.accessibilityReceiver");
                    apk_generation_done_event.putExtra("single_apk_generation_task_finished", true);
                    apk_generation_done_event.putExtra("apk_path", path);
                    apk_generation_done_event.putExtra("pkg_name", this.packagename);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(apk_generation_done_event);


                    Log.d(TAG, "TIME LINE COUNT apk generated" + mGenerated_apk_count);
                }
            } catch (AutoKeyException x) {
                Log.d(TAG, "Signing cancelled" + x.toString());
            } catch (Throwable t) {
                Log.d(TAG, "Signing cancelled" + t.toString());
                t.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class MyNodeVisitor extends NodeVisitor {
        String level = "";
        String oldPackageName;
        boolean didLogNodeName = false;
        String nodeName = "";

        MyNodeVisitor(NodeVisitor nv, String nodeName) {
            super(nv);
            this.nodeName = nodeName;
        }

        @Override
        public NodeVisitor child(String ns, String name) {
            if (needRemoveConflict && ("original-package".equals(name) || "provider".equals(name)) && ns == null) {
                return null;
            } else if (needRemoveLib && ("uses-library".equals(name)) && ns == null) {
                return null;
            }
            level += "    ";
            return new MyNodeVisitor(super.child(ns, name), name);
        }

        @Override
        public void attr(String ns, String name, int resourceId, int type, Object val) {
            String oldName = name;
            Object oldVal = val;
            if (ns == null && "package".equals(name) && "manifest".equals(nodeName) && type == NodeVisitor.TYPE_STRING && level.length() == 0) {
                oldPackageName = (String) val;
                if (!newPackageFullName.equals(val)) {
                    val = newPackageFullName;
                }
            } else if (type == NodeVisitor.TYPE_STRING && ("name".equals(name) || "backupAgent".equals(name) || "manageSpaceActivity".equals(name) || "targetActivity".equals(name)) && NS.equals(ns) && val != null && val instanceof String) {
                if (((String) val).startsWith(".")) {
                    val = oldPackageName + val;
                } else if (!((String) val).contains(".") && ((String) val).length() > 0) {
                    val = oldPackageName + "." + val;
                }
            } else if (type == NodeVisitor.TYPE_STRING && "value".equals(name) && NS.equals(ns) && val != null && val instanceof String) {
                if (((String) val).startsWith(".")) {
                    val = oldPackageName + val;
                }
            } else if (needRemoveConflict && ("protectionLevel".equals(name) || "process".equals(name) || "sharedUserId".equals(name)) && NS.equals(ns)) {
                name = null;
            } else if (needRemoveConflict && ("coreApp".equals(name)) && ns == null) {
                name = null;
            }
            if ((!((name == null && oldName == null) || (name != null && name.equals(oldName)))) || val != oldVal) {
                if (!didLogNodeName) {
                    didLogNodeName = true;
                }
                if (name == null) {
                    return;
                }
            }
            if (NS.equals(ns) && "label".equals(name) && "application".equals(nodeName) && type == NodeVisitor.TYPE_STRING && level.length() == 0) {
                val = appname;
            }
            super.attr(ns, name, resourceId, type, val);
        }

        @Override
        public void end() {
            level = level.length() > 4 ? level.substring(4) : "";
            super.end();
        }
    }


}