package com.spaceup.Utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.spaceup.Analytics.AnalyticsConstants;

/**
 * Created by Sajal on 16/12/2016.
 */
public class PrefManager {

    public static final String KEY_CAMPAIGN_SOURCE = "campaign_source";
    public static final String KEY_FB_CAMPAIGN_SOURCE = "fb_campaign_source";
    public static final String KEY_SHORTCUT_CREATED = "shortcut_created";
    //if reformat is set to true then don't run  _db.appUninstalled(appInfo.getPkgName()) function
    public static final String REFORMATING_DB = "reformat";
    public static final String KEY_REMOTE_CONFIG_INITIALIZED = "remote_config_initialized";
    public static final String KEY_COMPRESSION_NUMBER_COUNT = "compression_number_count";
    public static final String KEY_UNCOMPRESSION_NUMBER_COUNT = "uncompression_number_count";
    public static final String KEY_WA_POP_UP_OPEN_COUNT = "wa_pop_up_open_count";
    public static final String KEY_WA_RESULT_OPEN_COUNT = "wa_result_open_count";
    public static final String KEY_REVIEW_SCREEN_HINT = "uncompression_review_hint";
    //default value is false
    public static final String initialJunkAnalytics = "initial_junk_analytics";
    //default value is false
    public static final String initialTotalImages = "initial_total_images";
    //default value is false
    public static final String dayCountFlag = "day_count_flag";
    //default value is false
    public static final String dayCountFirst = "day_count_first";
    //return true for seconds run on-wards
    public static final String imageAnalyserFirstRun = "image_analyser_first_run";
    public static final String KEY_LAST_JUNK_RECOMM_TIME = "last_junk_recom_time";
    // Shared preferences file name
    private static final String PREF_NAME = "STASH_PREF";
    private static final String APP_VERSION = "app_version";
    //was the process interrupted
    private static final String ABORTED = "aborted";
    //Used to track If user closed compress popup on uninstalling app then show it in gap of 2 apps & if again closes then inc. gap by 1 more
    public static final String USER_ACTION_CANCEL_UNINSTALL = "USER_ACTION_CANCEL_UNINSTALL";
    public static final String USER_ACTION_CANCEL_UNINSTALL_NOTIFICATION = "user_action_cancel_uninstall_notification";

    public static final String ALREADY_SCANNING_COMPLETE = "already_scanning_complete";
    public static final String IS_WARNING_ACCEPTED_IN_SESSION = "is_warning_accepted_in_session";
    public static final String CURRENT_DAY_TRACKER = "current_day_tracker";
    public static final String GLOBAL_TURNOFF_BUBBLE_PERMANENT = "global_turnoff_bubble_permanent";
    public static final String PRESSED_STASH_INSTALL = "pressed_stash_install";
    public static final String ACCEPTED_ACCCESSIBILITY_TERMS = "accepted_acccessibility_terms";
    public static final String SEEN_MANAGE_APPS_TUTORIAL = "seen_manage_apps_tutorial";
    public static final String INCREMENT_BY_ONE_CONTROLLER = "increment_by_one_controller";
    public static final String HAS_SENT_ANALYTICS_ACCESSIBILITY = "has_sent_analytics_accessibility";
    public static final String HAS_SENT_INTERNET_ANALYTICS = "has_sent_internet_analytics";
    public static final String PERCENTAGE_HOME = "percentage_home";
    public static final String APP_STORAGE_PREF = "app_storage_pref";
    public static final String PHONE_STORAGE_PREF = "phone_storage_pref";
    public static final String TOTAL_APPS_PREF = "total_apps_pref";
    public static final String START_TIME_COMPRESSION = "start_time_compression";
    public static final String FIRST_TIME_CONTROLLER = "first_time_controller";
    public static final String SYSTEM_MILLIES_WHEN_APP_LAUNCHED_FIRST_TIME = "system_millies_when_app_launched_first_time";
    public static final String APP_UNISTALL_COUNT_VIA_DRAG_COMPRESS = "app_unistall_count_via_drag_compress";
    public static final String FIRST_TIME_COMPRESS_TIME = "first_time_compress_time";
    public static final String UNCOMPRESSION_COUNT_NEW = "uncompression_count_new";
    public static final String UNCOMPRESS_TIME_START = "uncompress_time_start";
    public static final String STASH_INSTALLED = "stash_installed";
    public static final String UNKNOW_SOURCE_EXIST = "unknow_source_exist";
    public static final String SENT_FIRST_TIME_REVIEW_SCREEN = "sent_first_time_review_screen";
    public static final String SENT_FIRST_TIME_UNCOMPRESS_SCREEN = "sent_first_time_uncompress_screen";
    public static final String SENT_FIRST_TIME_ACCESSIBILITY_EVENT = "sent_first_time_accessibility_event";
    public static final String SENT_FIRST_TIME_REVIEW_ACCESSIBILITY_EVENT = "sent_first_time_review_accessibility_event";
    public static final String APP_COMPRESSED = "app_compressed";
    public static final String GET_REACTIVATE_COUNT = "get_reactivate_count";
    public static final String APP_SYSTEM_PREF = "app_system_pref";
    public static final String COMPRESSED_TIME = "compressed_time";
    public static final String PREVIOUS_PROGRESS_VAL = "PREVIOUS_PROGRESS_VAL";
    public static final String PREVIOUS_PROGRESS_VAL_FINAL = "previous_progress_val_final";
    public static final String TOTAL_APPS_UNINSTALLED = "total_app_uninstalled";
    public static final String TOTAL_SPACE_SAVED = "total_space_saved";
    public static final String TOTAL_APP_SPACE_SAVED = "total_app_space_saved";
    public static final String TOTAL_CACHE_SAVED = "total_cache_saved";
    public static final String TOTAL_RAM_SAVED = "total_ram_saved";
    //show ads to new user only
    public static final String IS_NEW_USER = "is_new_user";
    public static final String LAST_TIME_SEEN_AD = "last_time_seen_ad";
    public static final String CAN_SHOW_RESULT_ADVERT = "can_show_result_advert";
    //returns false by default
    public static String whatAppCleanModule = "whatsAppsCleanModule";
    private static PrefManager sInstance = null;
    // All Shared Preferences Keys
    private final String NOTIFICATION_SHOWN_COUNT = "notificationDAILY";
    private final String IS_First_Run = "stashFirstRun";
    private final String USAGE_STATS_ACCESS_COUNT = "usageStatsAccessCount";
    private final String APP_RUN_COUNT = "appRunCount";
    private final String IS_FIRST_NOTI = "notificationFirstRUN";
    //these are the status of previous check for the permission
    private final String ACCESSIBILITY_PERMISSION_STATUS = "acc";
    private final String UNKNOWN_SOURCE_PERMISSION_STATUS = "unknownSource";
    private final String APP_USAGE_PERMISSION_STATUS = "appUsage";
    private final String INSTALLED_APP_TRACKER = "installed_APP_TRACKER";
    //to detect which theme is choosen
    private final String THEME = "theme";
    private final String LAST_COMPRESS_STAMP = "lastCompressTimeStamp";
    private final String COMPRESS_COUNT = "compressCount";
    //detect total async is complete or not
    private final String TOTAL_ASYNC_STATUS = "totalAsyncStatus";
    private final String TWO_APP_INSTALL = "twoappinstall";
    private final String TWO_APP_INSTALL_TIME = "two_App_Timestamp";
    private final String LOST_AN_APP_ALTERNATE = "lostAPPTRACKER_ALT";
    //too many apps selected reminder
    private final String TMA_REMINDER = "tmaReminder";
    //timestamp
    private final String TIME_STAMP = "time_stamp";
    //REVIEW if reviewed app then set to true else default is false
    private final String REVIEW = "review";
    //is first run permission handler
    private final String FIRST_RUN_PERMISSION = "permission_handler";
    //check weather analytics is send or not
    private final String mAnalyticsScanDetailSend = "analytics_scan_detail_send";
    //user pressed compress button first time
    private final String mOnReviewScreenFirst = "on_review_screen_first";
    //tracks uncompression count
    private final String mUncompressionCount = "uncompression_count";
    private final String mDayCount = "day_count";
    private final String mWACount = "wa_access_count";
    // Notification Prefs
    private SharedPreferences pref;
    // Shared pref mode
    private int PRIVATE_MODE = 0;
    private int FIRE_BASE_REMOTE_CONFIG_COUNT = 2;
    private boolean analyticsScanDetailSend;

    private PrefManager(Context context) {

        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }

    public static PrefManager getInstance(Context context) {
        if (sInstance == null) {
            synchronized (PrefManager.class) {
                if (sInstance == null) {
                    sInstance = new PrefManager(context);
                }
            }
        }
        return sInstance;
    }

    public boolean getPermissionFirst() {

        return pref.getBoolean(FIRST_RUN_PERMISSION, true);
    }

    public boolean isCompresssingFirst() {

        return pref.getBoolean("COMPRESSING_FIST_TIME", false);
    }

    public void setisCompresssingFirst(boolean status) {

        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("COMPRESSING_FIST_TIME", status);
        editor.commit();
    }

    public void setPermissionFirst() {

        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(FIRST_RUN_PERMISSION, false);
        editor.commit();
    }


    public void deactivateReviewHint() {
        Log.d("LOST_AN_APP_ALTERNATE", "SET");
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(KEY_REVIEW_SCREEN_HINT, false);
        editor.commit();
    }

    public boolean getReviewHintStatus() {
        return pref.getBoolean(KEY_REVIEW_SCREEN_HINT,true);

    }

    /***
     * 2 apps installed in a day Notification Prefs
     */

    private int getTwoappInstall() {

        return pref.getInt(TWO_APP_INSTALL_TIME, 0);
    }

    public void setTwoappInstall(int count) {
        Log.d("LOST_AN_APP_ALTERNATE", "SET");
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(TWO_APP_INSTALL_TIME, count);
        editor.commit();
    }

    public boolean show_noti_two(int new_count) {
        Log.d("INSTALLED_TRACKER_1", "New App Installed");
        int count = getTwoappInstall();
        if (new_count > count + FIRE_BASE_REMOTE_CONFIG_COUNT) {
            setTwoappInstall(new_count);
            return true;
        } else {
            setTwoappInstall(new_count);
            return false;
        }

    }

    public long getLastDayTrigger() {
        return pref.getLong("LAST_DAY", 0);

    }

    public void setLastDayTrigger() {

        SharedPreferences.Editor editor = pref.edit();
        editor.putLong("LAST_DAY", System.currentTimeMillis());
        editor.commit();
    }
    /*App Uninstalled YourReceiver alternate Login controller*/

    public void set_Alternate_LOSTAPP(String type) {
        Log.d("LOST_AN_APP_ALTERNATE", "SET");
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(LOST_AN_APP_ALTERNATE, type);
        editor.commit();
    }

    public String getAlternate_LOSTAPP() {
        return pref.getString(LOST_AN_APP_ALTERNATE, "pop_up");
    }

    public String getAlternate_BOTTOM() {
        return pref.getString("BOTTOM_POPUP", "pop_up");
    }

    public void set_Alternate_BOTTOM(String type) {
        Log.d("LOST_AN_APP_ALTERNATE", "SET");
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("BOTTOM_POPUP", type);
        editor.commit();
    }

    /* Ends of 2 apps installed in a day Notification Prefs*/

    public String getINSTALLED_APP_TRACKER() {
        Log.d("INSTALLED_TRACKER_1", "GET" + pref.getString(INSTALLED_APP_TRACKER, null));

        return pref.getString(INSTALLED_APP_TRACKER, null);
    }

    public void setINSTALLED_APP_TRACKER(String packagename) {
        Log.d("INSTALLED_TRACKER_1", "SET");
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(INSTALLED_APP_TRACKER, packagename);
        editor.commit();
    }

    public boolean get_is_FIRSTNOTIFICATION(String TAG) {
        return pref.getBoolean(TAG, true);
    }

    public void set_is_FIRSTNOTIFICATION(boolean status, String TAG) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(TAG, status);
        editor.commit();
    }

    public int getNotificationCount() {
        Log.d("NOTIFICATION", "GET");
        return pref.getInt(NOTIFICATION_SHOWN_COUNT, 0);
    }

    public void setNotificationCount(int new_count) {
        Log.d("setNotificationCount", "SET");
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(NOTIFICATION_SHOWN_COUNT, new_count);
        editor.commit();
    }

    public boolean isFirstRun() {
        Log.d("FIRST_RUN", "GET");
        return pref.getBoolean(IS_First_Run, true);
    }

    public void setFirstRun(boolean firstRun) {
        Log.d("FIRST_RUN", "SET");
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(IS_First_Run, firstRun);
        editor.commit();
    }

    public int getUsageStatsAccessCount() {
        return pref.getInt(USAGE_STATS_ACCESS_COUNT, 0);
    }

    public void setUsageStatsAccessCount(int count) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(USAGE_STATS_ACCESS_COUNT, count);
        editor.commit();
    }

    public int getAppRunCount() {
        return pref.getInt(APP_RUN_COUNT, 0);
    }

    public void setAppRunCount(int count) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(APP_RUN_COUNT, count);
        editor.commit();
    }

    public boolean isAcc() {
        return pref.getBoolean(ACCESSIBILITY_PERMISSION_STATUS, false);
    }

    public void setAcc(boolean acc) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(ACCESSIBILITY_PERMISSION_STATUS, acc);
        editor.commit();
    }

    public boolean isUnknownSource() {
        return pref.getBoolean(UNKNOWN_SOURCE_PERMISSION_STATUS, false);
    }

    public void setUnknownSource(boolean unknownSource) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(UNKNOWN_SOURCE_PERMISSION_STATUS, unknownSource);
        editor.commit();
    }

    public boolean isAppUsage() {
        return pref.getBoolean(APP_USAGE_PERMISSION_STATUS, false);
    }

    public void setAppUsage(boolean unknownSource) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(APP_USAGE_PERMISSION_STATUS, unknownSource);
        editor.commit();
    }

    //0 theme is blue
    //1 theme is red
    public int getTheme() {
        return pref.getInt(THEME, 0);
    }

    public void setTheme(int theme) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(THEME, theme);
        editor.commit();
    }

    public long getLastCompressTimeStamp() {
        Log.d("Theme", "BUG TestActivity from pref manager getLastCompressTimeStamp " + pref.getLong(LAST_COMPRESS_STAMP, 0));
        return pref.getLong(LAST_COMPRESS_STAMP, 0);
    }

    public void setLastCompressTimeStamp(long timeStamp) {
        Log.d("Theme", "BUG TestActivity from pref manager setLastCompressTimeStamp " + timeStamp);
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(LAST_COMPRESS_STAMP, timeStamp);
        editor.commit();
    }

    public long getSessionCompressCount() {
        Log.d("CompressCount", "getSessionCompressCount " + pref.getInt(COMPRESS_COUNT, 0));
        return pref.getInt(COMPRESS_COUNT, 0);
    }

    public void setSessionCompressCount(int count) {
        Log.d("CompressCount", "setSessionCompressCount " + count);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(COMPRESS_COUNT, count);
        editor.commit();
    }

    public int getTotalAsyncStatus() {
        //Log.d("ThreadErr", "getTotalAsyncStatus " + pref.getInt(TOTAL_ASYNC_STATUS, 0));
        return pref.getInt(TOTAL_ASYNC_STATUS, 0);
    }

    public void setTotalAsyncStatus(int count) {
        //Log.d("ThreadErr", "setTotalAsyncStatus " + count);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(TOTAL_ASYNC_STATUS, count);
        editor.commit();
    }

    //by default : reminder is on by default
    public boolean isTooManyAppsReminder() {
        Log.d("reminder", "isTooManyAppsReminder " + pref.getBoolean(TMA_REMINDER, true));
        return pref.getBoolean(TMA_REMINDER, true);

    }

    public void setTooManyAppsReminder(boolean unknownSource) {
        Log.d("reminder", "setTooManyAppsReminder" + unknownSource);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(TMA_REMINDER, unknownSource);
        editor.commit();
    }

    //for analytics
    public long getTimeStamp() {
        Log.d(AnalyticsConstants.TAG, "getTimeStamp " + pref.getLong(TIME_STAMP, 0));
        return pref.getLong(TIME_STAMP, 0);
    }

    //for analytics
    public void setTimeStamp(long timeStamp) {
        Log.d(AnalyticsConstants.TAG, "setTimeStamp " + timeStamp);
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(TIME_STAMP, timeStamp);
        editor.commit();
    }

    public boolean isReviewed() {
        return pref.getBoolean(REVIEW, false);
    }

    public void setReview(boolean review) {
        Log.d("review", "setReview: " + review);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(REVIEW, review);
        editor.commit();
    }

    public int getVersionCode() {
        Log.d("version", "getVersionCode: " + pref.getInt(APP_VERSION, 0));
        return pref.getInt(APP_VERSION, 0);
    }

    public void setVersionCode(int pVersionCode) {

        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(APP_VERSION, pVersionCode);
        editor.commit();
        Log.d("version", "setVersionCode: " + pVersionCode);
    }

    public String getString(String pKey) {
        return pref.getString(pKey, null);
    }

    public void putString(String pKey, String pValue) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(pKey, pValue);
        editor.commit();
    }

    public boolean getBoolean(String pKey) {
        return pref.getBoolean(pKey, false);
    }

    public void putBoolean(String pKey, boolean pValue) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(pKey, pValue);
        editor.commit();
    }

    public int getInt(String pKey) {
        return pref.getInt(pKey, 0);
    }

    public void putInt(String pKey, int pValue) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(pKey, pValue);
        editor.commit();
    }

    public long getLong(String pKey) {
        return pref.getLong(pKey, 0l);
    }

    public void putLong(String pKey, long pValue) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(pKey, pValue);
        editor.commit();
    }


    public boolean isAnalyticsScanDetailSend() {
        return pref.getBoolean(mAnalyticsScanDetailSend, false);
    }

    public void setAnalyticsScanDetailSend(boolean pAnalyticsScanDetail) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(mAnalyticsScanDetailSend, pAnalyticsScanDetail);
        editor.commit();
    }


    public boolean onReviewScreenFirst() {
        return pref.getBoolean(mOnReviewScreenFirst, true);
    }

    public void setReviewScreenFirst() {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(mOnReviewScreenFirst, false);
        editor.commit();
    }

   /* public void setUninstallProgress(String packageName) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.UNINSTALL_PROGRESS, packageName);
        editor.commit();
    }

    public void getUninstallProgress(String packageName) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.UNINSTALL_PROGRESS, packageName);
        editor.commit();
    }*/

    /**
     * This method returns uncompression count
     *
     * @return A int value to represent uncompression
     */
    public int incrementUncompressCount() {
        SharedPreferences.Editor editor = pref.edit();
        //add one to existing theme
        editor.putInt(mUncompressionCount, pref.getInt(mUncompressionCount, 0) + 1);
        editor.commit();
        //return incremented value
        return pref.getInt(mUncompressionCount, 0);
    }

    public int getUncompressCount() {
        return pref.getInt(mUncompressionCount, 0);
    }

    public int setDayCount() {
        SharedPreferences.Editor editor = pref.edit();
        //add one to existing theme
        editor.putInt(mDayCount, pref.getInt(mDayCount, 0) + 1);
        editor.commit();
        //return incremented value
        return pref.getInt(mDayCount, 1);
    }

    public int setWACount() {
        SharedPreferences.Editor editor = pref.edit();
        //add one to existing theme
        editor.putInt(mWACount, pref.getInt(mWACount, 0) + 1);
        editor.commit();
        //return incremented value
        return pref.getInt(mWACount, 1);
    }

}
