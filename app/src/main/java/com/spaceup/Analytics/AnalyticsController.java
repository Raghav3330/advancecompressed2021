package com.spaceup.Analytics;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.spaceup.Utility.PrefManager;

/**
 * Created by dhurv on 06-12-2017.
 */

public class AnalyticsController {
    private Bundle mGAParams;
    private Context mContext;
    private final String TAG = "AnalyticsController";

    public AnalyticsController(Context pContext) {
        this.mContext = pContext;

    }

    /**
     * @param pCategory value of Analytics Bundle
     * @param pAction value of Analytics Bundle
     * @param pLabel value of Analytics Bundle
     * @param pValue value of Analytics Bundle
     * @param pSendOnce true if analytics to be send only once per user
     * @param pPreferenceText String to be set for SharePreference flag valid for only where pSendOnce is true
     * @see  AnalyticsConstants for Category, Action and Label name used in project
     */
    public void sendAnalytics(String pCategory, String pAction, String pLabel, String pValue, boolean pSendOnce, String pPreferenceText) {
        mGAParams = new Bundle();
        if (pCategory != null)
            mGAParams.putString("category", pCategory);
        if (pAction != null)
            mGAParams.putString("action", pAction);
        if (pLabel != null)
            mGAParams.putString("label", pLabel);
        if (pValue != null) {
            try {
                long Val = Long.parseLong(pValue);
                mGAParams.putLong("value", Val);

            } catch (NumberFormatException e) {
                e.printStackTrace();
                return;
            }
        }

        if( pSendOnce ) {

            if(PrefManager.getInstance(mContext).getString(pPreferenceText) == null){
                new AnalyticsHandler().logGAEvent(mGAParams);
                PrefManager.getInstance(mContext).putString(pPreferenceText, "sent");
            }
        }
        else {
            new AnalyticsHandler().logGAEvent(mGAParams);
        }
        Log.d(TAG, "sendAnalyticsNew: " + mGAParams.toString());
    }
}

