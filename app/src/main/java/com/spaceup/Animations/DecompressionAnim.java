package com.spaceup.Animations;

import android.accessibilityservice.AccessibilityService;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.skyfishjy.library.RippleBackground;
import com.spaceup.Activities.ManageApps;
import com.spaceup.Alarm.AlarmService_Controller;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.App;
import com.spaceup.R;
import com.spaceup.UncompressingActivity;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityAutomation;
import com.spaceup.accessibility.AccessibilityCommunicator_REACTIVATE;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.models.DataModel;
import com.spaceup.notifications.NotificationHandler;
import com.spaceup.notifications.NotificationService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import static android.content.Context.WINDOW_SERVICE;
import static com.facebook.FacebookSdk.getApplicationContext;
import static com.spaceup.Utility.PrefManager.KEY_UNCOMPRESSION_NUMBER_COUNT;

/**
 * Created by Dhruv on 16-01-2017.
 */

public class DecompressionAnim implements View.OnClickListener {
    private ImageView inner, outer;
    private NotificationHandler nHandler;

    private Bundle mGAParams;
    private String TAG = getClass().getName(), uncompressAppName;
    private long time = 0, size = 0;
    private ProgressBar decompress_progress;
    private TextView percentage_decompression, bottom_text, percentage_decompression_CHAR, app_nameET, doinbkg_btn;
    //errorInProcessTimer for making the notification cancelable
    private CountDownTimer countDownTimer, countDownTimerMini, countDownTimerTicker;
    private String packagename;
    private AccessibilityAutomation sSharedInstance;
    private WindowManager windowManager_decompression;
    private View decompress_view;
    private Context mContext;
    private ObjectAnimator anim_prev;
    private RippleBackground ripples_fill, ripples_stroke;
    private boolean doinbkg_btn_pressed = false;
    private Button open;
    private boolean mShowBackground = true;
    //app installed in device
    private BroadcastReceiver getAccessibilityReceiver_reactivate = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.times.accessibilityREACTIVATE1")) {

                /***
                 *
                 * Show notification if user has not compressed any app till date
                 *
                 */
                Log.d("SHORTCUT_TB", " DELETED PACKAGE BROADCAST");

                AlarmService_Controller alarmService_controller = new AlarmService_Controller(context);
                alarmService_controller.fiveMinAppNotCompressed();


                String package_name = intent.getExtras().getString("packageName_of_apps_uninstalled");
                try {
                    AppDBHelper.getInstance(context).deleteFromShortcut(package_name);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("shortcutfile", "Decompress: Remove to file");
                UtilityMethods.getInstance().removeFromJSONFile(package_name);


                Log.d(TAG, "Checker_Package Installed in Broadcast Receiver " + package_name);
                Log.d("REACTIVATE", "Updating DBs in reactivate APPS");
                try {
                    //canceling alarm service if we already received cancel notification

                    Log.d("canceluncompress", "Alarm Service unregistered for removing notification");
                    Intent removeUncompressIntent = new Intent(mContext, NotificationService.class);
                    intent.putExtra(Constants.NOTIFICATION_TYPE, Constants.REMOVE_UNCOMPRESS_NOTIFICATION);
                    PendingIntent pendingIntent = PendingIntent.getService(mContext, Constants.UNCOMPRESSION_NOTIFICATION_ID, removeUncompressIntent, 0);
                    AlarmManager alarm = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
                    alarm.cancel(pendingIntent);

                    Log.d("Accessibility2", "Package decompressed " + package_name);
                    //TODO : can be removed from here
                    Log.d("Accessibility2", "Package decompressed " + package_name);
                    //TODO : can be removed from here

                    Log.d("doinbkg", "onReceive: updating ");
                    String originalPkgName = packagename.replace(".stash", "");
                    if (doinbkg_btn_pressed) {
                        nHandler = NotificationHandler.getInstance(context);
                        nHandler.updateProgressNotification(context, originalPkgName);


                        // Analytics Start
                        mGAParams = new Bundle();
                        mGAParams.putString("category", AnalyticsConstants.Category.UNCOMPRESS);
                        mGAParams.putString("action", AnalyticsConstants.Action.UNCOMPRESSING);
                        mGAParams.putString("label", AnalyticsConstants.Label.DO_IN_BKG);

                        new AnalyticsHandler().logGAEvent(mGAParams);
                        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());


                    }
                    clean_db_folder(package_name);

                    //end of uncompression
                    // Start Time to calculate execution time of decompression
                    long timePeriod = (new AnalyticsHandler().timePeriod()) / 1000;
                    Bundle paramBundle = new Bundle();
                    paramBundle.putString(AnalyticsConstants.Params.PACKAGE_NAME, originalPkgName);
                    paramBundle.putString(AnalyticsConstants.Params.DO_IN_BKG, "" + doinbkg_btn_pressed);
                    paramBundle.putString(AnalyticsConstants.Params.TIME, "" + timePeriod);
                    Log.d(AnalyticsConstants.TAG, "UNCOMPRESS_FINISHED: " + paramBundle);
                    new AnalyticsHandler().logEvent(AnalyticsConstants.Event.UNCOMPRESS_FINISHED, paramBundle);
                    if (AccessibilityAutomation.getSharedInstance() != null && !doinbkg_btn_pressed) {


                        try {
                            Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(originalPkgName);
                            launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(launchIntent);
                        } catch (Exception e) {
                            Log.d(TAG, e.getMessage(), e);
                        }
                    }


                    //Apsalar Analytics START
                    JSONObject appsalarTracking = new JSONObject();
                    try {
                        appsalarTracking.put("" + PrefManager.getInstance(context).incrementUncompressCount(), 1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d(AnalyticsConstants.APSALARTAG, "UNCOMPRESS_FINISHED: " + appsalarTracking.toString());
                    new AnalyticsHandler().logApsalarJsonEvent(AnalyticsConstants.Key.UNCOMPRESS_FINISHED, appsalarTracking);
                    //Apsalar Analytics END


                    PrefManager pManager = PrefManager.getInstance(App.getInstance());
                    int finalCount = pManager.getInt(KEY_UNCOMPRESSION_NUMBER_COUNT) + 1;
                    pManager.putInt(KEY_UNCOMPRESSION_NUMBER_COUNT, finalCount);

                    mGAParams.putInt("cd", AnalyticsConstants.CustomDim.UNCOMPRESSION_COUNT);
                    mGAParams.putString("cd_value", finalCount + "");

//                    mGAParams.putInt("cm", AnalyticsConstants.CustomMetric.APPS_UNCOM);
//                    mGAParams.putInt("cm_value", 1);

                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("INSTALLED_TRACKER", "Checker_Package NOT FOUND " + package_name);
                } finally {

                    AccessibilityAutomation.getSharedInstance().removeDecompressionOverLay();
                    App.getInstance().stopService(new Intent(App.getInstance().getApplicationContext(), AccessibilityCommunicator_REACTIVATE.class));
                    LocalBroadcastManager.getInstance(context).unregisterReceiver(getAccessibilityReceiver_reactivate);

                    // AccessibilityAutomation.getSharedInstance().fromActivity(false);
                    // clean_project();
                }
            }
        }
    };

    private BroadcastReceiver showDoInBackGroundReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(showDoInBackGroundReceiver);
            if (intent.getAction().equals(AccessibilityAutomation.SHOW_DO_IN_BKG_BTN_INTENT)) {
                Log.d("doinbkg", "boardcast received in decompression anim");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            sSharedInstance.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            Intent startMain = new Intent(Intent.ACTION_MAIN);
                            startMain.addCategory(Intent.CATEGORY_HOME);
                            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(startMain);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent startMain = new Intent(Intent.ACTION_MAIN);
                                    startMain.addCategory(Intent.CATEGORY_HOME);
                                    startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(startMain);
                                    try {
                                        // doinbkg_btn.setVisibility(View.VISIBLE);

                                        if (open.getVisibility() == View.VISIBLE || !mShowBackground) {
                                            doinbkg_btn.setVisibility(View.GONE);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, 3000);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 5000);


            }
            Log.d("doinbkg", "boardcast is unregistered");
        }
    };

    public DecompressionAnim(String packagename, AccessibilityAutomation sSharedInstance, Context mContext) {
        this.packagename = packagename;
        this.sSharedInstance = sSharedInstance;
        this.mContext = mContext;
    }

    public void overlayDecompression() {

        PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.UNCOMPRESS_TIME_START, System.currentTimeMillis());

        AlarmService_Controller alarm = new AlarmService_Controller(mContext);
        alarm.forceRemoveNotification();

        Log.d(TAG, "DECOMPRESSION_OVERLAY STARTED");
        IntentFilter accessibilityReceiverIntentFilter = new IntentFilter();
        accessibilityReceiverIntentFilter.addAction("com.times.accessibilityREACTIVATE1");
        //do in background intent filter
        IntentFilter showDoInBackIF = new IntentFilter();
        showDoInBackIF.addAction(AccessibilityAutomation.SHOW_DO_IN_BKG_BTN_INTENT);
        LocalBroadcastManager.getInstance(mContext).registerReceiver(getAccessibilityReceiver_reactivate, accessibilityReceiverIntentFilter);

        //LocalBroadcastManager.getInstance(mContext).registerReceiver(showDoInBackGroundReceiver, showDoInBackIF);
        Log.d("doinbkg", "boardcast is registered");
        // Wait fo Broadcast from Installing Broadcast

        if (windowManager_decompression == null) {
            overlayDecompressionUI(packagename);
            analytics(packagename);
            UtilityMethods.getInstance().handleSound(true, mContext);   // put to mute call
            WindowManager.LayoutParams params;

            try {
                params = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                        PixelFormat.TRANSLUCENT);
                params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

                windowManager_decompression = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
                try {
                    windowManager_decompression.addView(decompress_view, params);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception j) {
                j.printStackTrace();
                params = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.TYPE_PHONE,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                        PixelFormat.TRANSLUCENT);
                params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

                windowManager_decompression = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
                try {
                    windowManager_decompression.addView(decompress_view, params);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private void analytics(String packagename) {
        // Analytics Start
        mGAParams = new Bundle();
        mGAParams.putString("category", AnalyticsConstants.Category.UNCOMPRESS);
        mGAParams.putString("action", AnalyticsConstants.Action.UNCOMPRESSED_PKG);

        mGAParams.putString("label", packagename);
        new AnalyticsHandler().logGAEvent(mGAParams);
        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
        // Analytics end


    }

    private void overlayDecompressionUI(String package_name) {

        // Start Time to calculate execution time of decompression
        new AnalyticsHandler().startTimeStamp();
        Bundle paramBundle = new Bundle();
        if (package_name.contains(".stash")) {
            paramBundle.putString(AnalyticsConstants.Params.PACKAGE_NAME, package_name.substring(0, package_name.indexOf(".stash")));
            new AnalyticsHandler().logEvent(AnalyticsConstants.Event.UNCOMPRESS, paramBundle);
            Log.d(AnalyticsConstants.TAG, "overlayDecompressionUI: " + package_name.substring(0, package_name.indexOf(".stash")));
        } else {
            paramBundle.putString(AnalyticsConstants.Params.PACKAGE_NAME, package_name);
            new AnalyticsHandler().logEvent(AnalyticsConstants.Event.UNCOMPRESS, paramBundle);
            Log.d(AnalyticsConstants.TAG, "overlayDecompressionUI: " + package_name);

        }
        int size_compressed_apps = 0;
        //start timer
        if (!package_name.contains(".stash")) {
            package_name = package_name + ".stash";
        }

        startDecompressTimer(package_name);

        LayoutInflater inflater = (LayoutInflater) sSharedInstance.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        decompress_view = inflater.inflate(R.layout.single_compression_overlay_after, null);
        decompress_progress = (ProgressBar) decompress_view.findViewById(R.id.final_tick_background);
        percentage_decompression = (TextView) decompress_view.findViewById(R.id.compressed_number);
        percentage_decompression_CHAR = (TextView) decompress_view.findViewById(R.id.compressed_unit);
        bottom_text = (TextView) decompress_view.findViewById(R.id.compressed_text);
        bottom_text.setText("Uncompressing");
        ripples_fill = (RippleBackground) decompress_view.findViewById(R.id.content);
        ripples_stroke = (RippleBackground) decompress_view.findViewById(R.id.content_stroke);
        ripples_fill.startRippleAnimation();


        app_nameET = (TextView) decompress_view.findViewById(R.id.app_name);

        //set visibility
        doinbkg_btn = (TextView) decompress_view.findViewById(R.id.doinbkg_btn);
        doinbkg_btn.setOnClickListener(this);


        final ImageView iconIV;

        try {

            String APKFilePath = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + package_name;
            PackageManager pm = mContext.getPackageManager();
            PackageInfo pi = pm.getPackageArchiveInfo(APKFilePath, 0);

            // Logo of the app from DIR....
            pi.applicationInfo.sourceDir = APKFilePath;
            pi.applicationInfo.publicSourceDir = APKFilePath;
            //
            Drawable APKicon = pi.applicationInfo.loadIcon(pm);

            iconIV = (ImageView) decompress_view.findViewById(R.id.single_app_image);
            iconIV.setImageDrawable(APKicon);

            final ColorMatrix matrix = new ColorMatrix();
            ValueAnimator animation_greyscale = ValueAnimator.ofFloat(0f, 1f);
            animation_greyscale.setStartDelay(500);
            animation_greyscale.setDuration(7000);

            animation_greyscale.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    matrix.setSaturation(animation.getAnimatedFraction());
                    ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                    iconIV.setColorFilter(filter);
                }
            });
            animation_greyscale.start();
            //icon scale down animation


            inner = (ImageView) decompress_view.findViewById(R.id.stars_inner);
            outer = (ImageView) decompress_view.findViewById(R.id.stars_outer);

            Animation clockwise = AnimationUtils.loadAnimation(mContext, R.anim.rotate_image_clockwise);
            inner.startAnimation(clockwise);

            Animation anticlockwise = AnimationUtils.loadAnimation(mContext, R.anim.rotate_image_anticlockwise);
            outer.startAnimation(anticlockwise);

            inner.setAlpha(0.5f);
            outer.setAlpha(0.5f);

        } catch (Exception e) {
            e.printStackTrace();
        }
        AppDBHelper db = AppDBHelper.getInstance(mContext);
        // get size

        try {
            package_name = package_name.substring(0, package_name.indexOf(".stash"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        DataModel data = db.getDataSize(package_name);
        Log.d("AppDBHelper", "getApkSize -->" + data.getApkSize());
        Log.d("AppDBHelper", "getCodeSize -->" + data.getCodeSize());
        Log.d("AppDBHelper", "getCacheSize -->" + data.getCacheSize());
        Log.d("AppDBHelper", "getDataSize -->" + data.getDataSize());
        size_compressed_apps += data.getApkSize() + data.getCodeSize() + data.getCacheSize() + data.getDataSize();
        size = size_compressed_apps;
        File file = new File(Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + "/" + package_name + ".stash");

        long apk_size = file.length();
        if (size == 0) {
            file = new File(Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + "/" + package_name + ".stash");
            size = file.length() * 3;

        }
        percentage_decompression.setText(UtilityMethods.getInstance().getSize_number(size, false));
        percentage_decompression_CHAR.setText("" + UtilityMethods.getInstance().getSize_charecter(size));
        uncompressAppName = db.getAppName(package_name);
        if (uncompressAppName == null) {
            uncompressAppName = "";

        }
        try {
            app_nameET.setText("Uncompressing " + uncompressAppName);
        } catch (Exception e) {
            app_nameET.setText("Uncompressing");

            e.printStackTrace();
        }


        time = 0;
        if (UtilityMethods.getInstance().getSize_charecter(apk_size).equals("KB")) {
            time = 5 * 3000;
        } else {
            time = Long.parseLong(UtilityMethods.getInstance().getSize_number(apk_size, false)) * 3000;
        }

        anim_prev = ObjectAnimator.ofInt(decompress_progress, "progress", 0, 1000);
        anim_prev.setDuration(time);
        // show progress bar for 3 seconds per MB

        anim_prev.setInterpolator(new DecelerateInterpolator());

        anim_prev.start();
        startMiniTimer();

    }

    private void OptimisingAppsUI() {

        try {
            //start percentage boosted Ticker
            percentage_decompression.setText("" + 0);
            startTickerTimer();

            ripples_fill.stopRippleAnimation();
            ripples_stroke.setVisibility(View.VISIBLE);
            ripples_stroke.startRippleAnimation();
            app_nameET.setText("Optimising " + uncompressAppName);
            bottom_text.setText("Optimising");
            percentage_decompression_CHAR.setText("%");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void turnOnOnBoadingLoader() {
        try {
            TextView preparing = (TextView) decompress_view.findViewById(R.id.text_hint);
            preparing.setVisibility(View.VISIBLE);
            preparing.setText("Preparing");
            ProgressBar progressOnBoard = (ProgressBar) decompress_view.findViewById(R.id.progressBarOnBoard);
            progressOnBoard.setVisibility(View.VISIBLE);
            progressOnBoard.getIndeterminateDrawable().setColorFilter(0xFFFFFFFF, android.graphics.PorterDuff.Mode.MULTIPLY);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void turnOffOnBoadingLoader() {
        try {

            ProgressBar progressOnBoard = (ProgressBar) decompress_view.findViewById(R.id.progressBarOnBoard);
            progressOnBoard.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeDecompressionOverLay() {
        long startTime = PrefManager.getInstance(mContext).getLong(PrefManager.UNCOMPRESS_TIME_START);
        long timeTaken = (System.currentTimeMillis() - startTime) / 1000;
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.UNCOMPRESS,
                AnalyticsConstants.Action.TIME,
                null,
                String.valueOf(timeTaken),
                false,
                null);

        turnOnOnBoadingLoader();
        UtilityMethods.getInstance().handleSound(false, mContext);    //put back to normal mode
        UtilityMethods.getInstance().playFinishSound();
        try {
            AccessibilityAutomation.getSharedInstance().allowReading(false);
            AccessibilityAutomation.getSharedInstance().fromActivity(false);

            anim_prev.cancel(); // Cancel the previous progress Animation
            ripples_stroke.stopRippleAnimation();
            ripples_fill.stopRippleAnimation();
            inner.setAlpha(0.5f);
            outer.setAlpha(1f);

            ObjectAnimator anim = ObjectAnimator.ofInt(decompress_progress, "progress", decompress_progress.getProgress(), 1000);
            anim.setDuration(1000);
            // show progress bar for 3 seconds per MB
            anim.setInterpolator(new DecelerateInterpolator());
            anim.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //stopping all timers
        try {
            countDownTimer.cancel();
            countDownTimerMini.cancel();
            countDownTimerTicker.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            // TRY because if time out occurs decompress_view will be null
            TextView cancel = (TextView) decompress_view.findViewById(R.id.textView43);
            cancel.setVisibility(View.VISIBLE);
            TextView tip = (TextView) decompress_view.findViewById(R.id.text_hint);
            tip.setVisibility(View.VISIBLE);
            TextView boostedET = (TextView) decompress_view.findViewById(R.id.textView8);
            boostedET.setVisibility(View.VISIBLE);
            TextView boostedText = (TextView) decompress_view.findViewById(R.id.textView9);
            boostedText.setText(uncompressAppName + " optimised for optimal use");
            boostedText.setVisibility(View.VISIBLE);
            app_nameET.setVisibility(View.INVISIBLE);
            bottom_text.setVisibility(View.INVISIBLE);
            doinbkg_btn.setVisibility(View.GONE);
            percentage_decompression_CHAR.setVisibility(View.INVISIBLE);
            percentage_decompression.setVisibility(View.INVISIBLE);
            open = (Button) decompress_view.findViewById(R.id.optimize_btn);
            open.setVisibility(View.GONE);
            if (doinbkg_btn.getVisibility() == View.VISIBLE)
                doinbkg_btn.setVisibility(View.GONE);
                mShowBackground = false;
            String temp = packagename.replace(".stash", "");
            if (!Constants.DUMMY_APP_VERSION_NAME.equalsIgnoreCase(UtilityMethods.getInstance().getCurrentAppVersionName(mContext, packagename)) && UtilityMethods.getInstance().isPackageExisted(temp, mContext)) {
                tip.setTextColor(Color.BLACK);
                tip.setText("TIP: Compress again after use.");

                new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.ERROR,
                        AnalyticsConstants.Action.UNCOMPRESSION_ERROR,
                        packagename,
                        String.valueOf(1),
                        false,
                        null);
            } else {
                tip.setTextColor(Color.RED);
                tip.setText("Something Went Wrong! Please retry");
                new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.ERROR,
                        AnalyticsConstants.Action.UNCOMPRESSION_ERROR,
                        packagename,
                        String.valueOf(0),
                        false,
                        null);
                open.setText("Try Again");

            }
            open.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Log.d("DECOMPRESSION ANIM ",temp);
                    Intent intent = getApplicationContext().getPackageManager().getLaunchIntentForPackage(temp);
                    if (intent != null) {
                        // We found the activity now start the activity
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getApplicationContext().startActivity(intent);
                    }else{
//                        Toast.makeText(getApplicationContext(),"Retry",Toast.LENGTH_LONG).show();
                        Intent intent1 = new Intent(getApplicationContext(), ManageApps.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getApplicationContext().startActivity(intent1);
                    }

                    // Analytics Start
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.UNCOMPRESS);
                    mGAParams.putString("action", AnalyticsConstants.Action.UNCOMPRESSING);

                    mGAParams.putString("label", AnalyticsConstants.Label.OPEN);

                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

                    cleanViews();


                }
            });
            final TextView cancelET = (TextView) decompress_view.findViewById(R.id.textView43);
            cancelET.setVisibility(View.GONE);
            cancelET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // Analytics Start
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.UNCOMPRESS);
                    mGAParams.putString("action", AnalyticsConstants.Action.UNCOMPRESSING);

                    mGAParams.putString("label", AnalyticsConstants.Label.CANCEL);

                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                    cleanViews();
                    if (AccessibilityAutomation.getSharedInstance() != null) {
                        AccessibilityAutomation.getSharedInstance().performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                    }

                }
            });

            open.setVisibility(View.VISIBLE);
            turnOffOnBoadingLoader();
            cancelET.setVisibility(View.VISIBLE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cleanViews() {

        try {
            if (windowManager_decompression != null) {
                windowManager_decompression.removeView(decompress_view);
            }
            decompress_view = null;
            windowManager_decompression = null;
            sSharedInstance = null;
        } catch (Exception e) {
            e.printStackTrace();
            if (windowManager_decompression != null) {
                windowManager_decompression.removeView(decompress_view);
            }
            decompress_view = null;
            windowManager_decompression = null;
        }
    }

    private void startMiniTimer() {
        Log.d(TAG, "DECOMPRESSION_OVERLAY STARTED MINI TIMER STARTED");

        countDownTimerMini = null;
        countDownTimerMini = new CountDownTimer(time, 1000) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                Log.d(TAG, "DECOMPRESSION_OVERLAY STARTED MINI TIMER FINISHED");
                OptimisingAppsUI();

            }
        }.start();

    }

    private void startDecompressTimer(final String package_name) {
        File file = new File(Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + "/" + package_name);
        long time = 3 * 60 * 1000; //Max 3 minutes
        Log.d(TAG, "TIMER_DECOMPRESS APK Size " + file.length() + "Timer Time " + time);
        countDownTimer = null;
        countDownTimer = new CountDownTimer(time, 1000) {

            public void onTick(long millisUntilFinished) {
                Log.d("timeout", "I am ticking");
            }

            public void onFinish() {
                removeDecompressionOverLay();
                FirebaseCrashlytics.getInstance().log("UNCOMPRESS GLOBAL TIMEOUT: " + package_name);
                //user will come here when there is an error in uncompression
                //todo : we need  a try catch
                Log.d(TAG, "TIMER_DECOMPRESS REACHED");

                // This method will be executed once the timer is over
                // Start your app main activity

                try {
                    NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancel(69);
                } catch (Exception e) {
                    Log.d("timeout", Log.getStackTraceString(e));
                }

            }
        }.start();

    }

    private void startTickerTimer() {
        Log.d(TAG, "Boosted Percentage Ticker Timer");

        countDownTimerTicker = null;
        countDownTimerTicker = new CountDownTimer(100000, 100) {

            public void onTick(long millisUntilFinished) {
                int prev_text = Integer.parseInt(String.valueOf(percentage_decompression.getText()));
                if (prev_text < 100)
                    percentage_decompression.setText("" + (prev_text + 1));
            }

            public void onFinish() {
            }
        }.start();

    }

    private void clean_db_folder(String packagename) {

        //delete the apk from the DB directory once the app is installed
        File file = new File(Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + packagename + ".stash");
        file.delete();
        Log.d("Cleaning project", "Deleting " + Constants.APK_DB + packagename + ".stash");

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.doinbkg_btn:

                try {
                    try {
                        sSharedInstance.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String originalPkgName = packagename.replace(".stash", "");

                    Bundle paramBundle = new Bundle();
                    paramBundle.putString(AnalyticsConstants.Params.PACKAGE_NAME, originalPkgName);
                    new AnalyticsHandler().logEvent(AnalyticsConstants.Event.DO_IN_BKG, paramBundle);
                    Log.d(AnalyticsConstants.TAG, "do in bkg pressed");

                    removeOverlayUnCompression(originalPkgName);

                    Log.d("doinbkg", "Package name" + originalPkgName);
                    doinbkg_btn_pressed = true;

                    doinbkg_btn.setOnClickListener(null);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void notification(final String pkgName) {
        nHandler = NotificationHandler.getInstance(mContext);
        nHandler.createProgressNotification(mContext, pkgName);

    }

    //to remove overlay ui on "DO IN BACKGROUND CLICK"
    private void removeOverlayUnCompression(String pOriginalPkgName) {

        UtilityMethods.getInstance().handleSound(false, mContext);    //put back to normal mode
        try {
            AccessibilityAutomation.getSharedInstance().allowReading(false);
            AccessibilityAutomation.getSharedInstance().fromActivity(false);


        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            notification(pOriginalPkgName);
        }
        try {
            if (windowManager_decompression != null) {
                windowManager_decompression.removeView(decompress_view);
            }
            decompress_view = null;
            windowManager_decompression = null;

            notification(pOriginalPkgName);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
