package com.spaceup.data.version_one;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.models.AppsCompressedModel;
import com.spaceup.models.DataModel;
import com.spaceup.models.ImageModel;
import com.spaceup.models.StateRetrieveModel;
import com.spaceup.uninstall.activities.AppInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Sajal Jain on 01-12-2016.
 * COMPRESS STATUS STATES
 * 1 = compressed apps
 * -1 = uncompressed apps
 * -2 = system apps
 * 2 = .stash apps installed
 * -3 = .stash apps uninstalled
 * -4 = app uninstalled
 */

public class AppDBHelper {
    public static final String SHORTCUT_TB = "Shortcut";
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 2;
    // Database Name
    private static final String DATABASE_NAME = "Stash";
    // Contacts table name
    private static final String TABLE_APP_DETAILS = "AppDetails";
    private static final String TOP_FIVE_TABLE = "TopFiveApps";
    private static final String APP_NAME = "AppName";
    private static final String PKG_NAME = "PkgName";
    private static final String COMPRESSED_STATUS = "CompressedStatus";
    private static final String COMPRESSED_COUNT = "CompressedCount";
    private static final String UNCOMPRESSED_COUNT = "UncompressedCount";
    private static final String USAGE_COUNT = "UsageCount";
    private static final String APK_SIZE = "ApkSize";
    private static final String CODE_SIZE = "CodeSize";
    private static final String CACHE_SIZE = "CacheSize";
    private static final String DATA_SIZE = "DataSize";
    private static final String PRIORITY_SCORE = "PriorityScore";
    private static final String TIME = "Time";
    private static final String CATERGORY = "Category";
    private static final String MODIFIED_DATE = "ModifiedDate";
    private static final String EXTRACT = "Extract";
    private static final String COMPRESSION_TABLE = "CompressedAppDetails";   //compressed apps TABLE
    private static final String COMPRESSED_APP_NAME = "AppName";
    private static final String COMPRESSED_PKG_NAME = "PkgName";
    private static final String COMPRESSED_APP_PATH = "DummyAppPath";
    private static final String COMPRESSED_APP_FOLDER = "folder";
    private static final String COMPRESSED_INSTALL_STATUS = "install_status";
    private static final String COMPRESSED_UNINSTALL_STATUS = "uninstall_status";
    private static DBCreator mDBConnection;
    private static AppDBHelper helper;
    SQLiteDatabase db;


    private AppDBHelper(Context context) {

        mDBConnection = DBCreator.getInstance(context);
        db = mDBConnection.getWritableDatabase();

    }

    public static AppDBHelper getInstance(Context context) {

        if (helper == null)
            helper = new AppDBHelper(context);

        return helper;
    }

    public void open() throws SQLException {
    }

    public void close() {
    }


    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    public synchronized boolean noAppsCompressed() {
        // return true if user has ever used our app
        List<AppInfo> appList = new ArrayList<AppInfo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_APP_DETAILS + " WHERE " + COMPRESSED_STATUS + " = " + Constants.COMPRESSED;

        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.getCount() == 0) {
                Log.d("ALARM_RECEIVER", "COUNT_IN_DB_IS_ZERO");

                return true;
            } else {
                Log.d("ALARM_RECEIVER", "COUNT_IN_DB_IS_NOT_ZERO");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public synchronized List<AppInfo> getAppDetails() {

        List<AppInfo> appList = new ArrayList<AppInfo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_APP_DETAILS;

        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    AppInfo dummy = new AppInfo();
                    dummy.setPkgName(cursor.getString(0));
                    dummy.setAppName(cursor.getString(1));
                    dummy.setApkSize(Long.parseLong(cursor.getString(2)));
                    dummy.setCodeSize(Long.parseLong(cursor.getString(3)));
                    dummy.setCacheSize(Long.parseLong(cursor.getString(4)));
                    dummy.setSize(Long.parseLong(cursor.getString(5)));
                    dummy.setIndex(Long.parseLong(cursor.getString(6)));
                    dummy.setTotalTimeInForeground(Long.parseLong(cursor.getString(7)));
                    dummy.setLastModifiedDate(Long.parseLong(cursor.getString(8)));
                    dummy.setCategory(cursor.getString(9));
                    dummy.setLocation(cursor.getString(10));
                    //some random usage count
                    dummy.setUsageCount(2137123);
                    dummy.setCompressed_status(cursor.getInt(12));
                    dummy.setCompressed_count(cursor.getInt(13));
                    dummy.setUnCompressed_count(cursor.getInt(14));
                    // Adding contact to list
                    appList.add(dummy);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return appList;
    }

    public long getAppStorageIncreasedUsage(String packageName) {
        String selectQuery = "SELECT  * FROM " + TABLE_APP_DETAILS + " where " + PKG_NAME + " = '" + packageName + "'";

        long appStorage = 0;
        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                appStorage = Long.parseLong(cursor.getString(4)) + Long.parseLong(cursor.getString(5));

            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return appStorage;
    }

    public boolean updateAppStoreIncreased(long dataSize, long cacheSize,String pkgName) {
        int rowsAffected = 0;
        ContentValues values = new ContentValues();
        values.put(DATA_SIZE, dataSize);
        values.put(CACHE_SIZE, cacheSize);
        try {
            open();
            rowsAffected = db.update(TABLE_APP_DETAILS, values, PKG_NAME + "=" + "'" + pkgName + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
            if (rowsAffected > 0)
                return true;
            else
                return false;
        }
    }


    public synchronized List<AppInfo> getAdvanceCompressedApps() {
        List<AppInfo> appList = new ArrayList<AppInfo>();
        // Select All Query
        //String selectQuery = "SELECT  * FROM " + TABLE_APP_DETAILS + " where " + PKG_NAME + " LIKE " + "\"%.stash\" AND " + COMPRESSED_STATUS + " = 2" + " ORDER BY " + APP_NAME;
        String selectQuery = "SELECT  * FROM " + TABLE_APP_DETAILS + " where " + COMPRESSED_STATUS + " = " + Constants.COMPRESSED + " ORDER BY " + APP_NAME;

        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    AppInfo dummy = new AppInfo();
                    dummy.setPkgName(cursor.getString(0));
                    dummy.setAppName(cursor.getString(1));
                    dummy.setApkSize(Long.parseLong(cursor.getString(2)));
                    dummy.setCodeSize(Long.parseLong(cursor.getString(3)));
                    dummy.setCacheSize(Long.parseLong(cursor.getString(4)));
                    dummy.setSize(Long.parseLong(cursor.getString(5)));
                    dummy.setIndex(Long.parseLong(cursor.getString(6)));
                    dummy.setTotalTimeInForeground(Long.parseLong(cursor.getString(7)));
                    dummy.setLastModifiedDate(Long.parseLong(cursor.getString(8)));
                    dummy.setCategory(cursor.getString(9));
                    dummy.setLocation(cursor.getString(10));
                    //some random usage count
                    dummy.setUsageCount(2137123);
                    dummy.setCompressed_status(cursor.getInt(12));
                    dummy.setCompressed_count(cursor.getInt(13));
                    dummy.setUnCompressed_count(cursor.getInt(14));
                    // Adding contact to list
                    appList.add(dummy);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return appList;
    }


    //Used to show data on final Screen
    public synchronized DataModel getDataSize(String pkgName) {

        DataModel data = new DataModel();
        // Select All Query
        String selectQuery = "SELECT  " + APK_SIZE + "," + CACHE_SIZE + "," + CODE_SIZE + "," + DATA_SIZE + "," + MODIFIED_DATE + " FROM " + TABLE_APP_DETAILS + " where " + PKG_NAME + "=" + "'" + pkgName + "'";
        Log.d("Notification", "" + selectQuery);
        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    data.setApkSize(Long.parseLong(cursor.getString(0)));
                    data.setCacheSize(Long.parseLong(cursor.getString(1)));
                    data.setCodeSize(Long.parseLong(cursor.getString(2)));
                    data.setDataSize(Long.parseLong(cursor.getString(3)));
                    data.setLastModifiedDate(Long.parseLong(cursor.getString(4)));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return data;
    }


    //return App Name
    public synchronized String getAppName(String pkgName) {
        String appName = null;
        DataModel data = new DataModel();
        // Select All Query
        String selectQuery = "SELECT  " + APP_NAME + " FROM " + TABLE_APP_DETAILS + " where " + PKG_NAME + "=" + "'" + pkgName + "'";

        Log.d("AppDBHelper", "SELECT data from db" + selectQuery);
        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    appName = cursor.getString(0);

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
        if (null == appName) {
            return "App";
        }

        return appName;
    }

    //return App Name
    public synchronized String totalCache() {
        String size = "0";
        String selectQuery = "SELECT  SUM(" + CACHE_SIZE + ")  FROM " + TABLE_APP_DETAILS + "";

        Log.d("AppDBHelper", "SELECT data from db" + selectQuery);
        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    size = cursor.getString(0);

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return size;
    }

    public synchronized String totalAppsSizes() {
        String size = "0";
        String selectQuery = "SELECT  SUM(" + APK_SIZE + ")+SUM(" + CODE_SIZE + ")  FROM " + TABLE_APP_DETAILS + "";

        Log.d("AppDBHelper", "SELECT data from db" + selectQuery);
        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    size = cursor.getString(0);

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return size;
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new application
    public synchronized void addApplication(AppInfo app) {
        Log.d("YourReceiver", "new package added " + app.getPkgName());
        ContentValues values = new ContentValues();
        values.put("AppName", app.getAppName());
        values.put("PkgName", app.getPkgName());
        values.put("ApkSize", app.getApkSize());
        values.put("CodeSize", app.getCodeSize());
        values.put("CacheSize", app.getCacheSize());
        values.put("DataSize", app.getSize());
        values.put("PriorityScore", app.getIndex());
        values.put("Time", app.getTotalTimeInForeground());
        values.put("Category", app.getCategory());
        //set to -1 never ever compressed
        //values.put("SpecialFlag", app.getSpecialFlag());
        values.put("ModifiedDate", app.getLastModifiedDate());
        values.put("Extract", app.getLocation());
        values.put(COMPRESSED_STATUS, app.getCompressed_status());
        values.put(COMPRESSED_COUNT, app.getCompressed_count());
        values.put(UNCOMPRESSED_COUNT, app.getUnCompressed_count());
        // Inserting Row
        try {
            open();
            db.insert(TABLE_APP_DETAILS, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }


    /*
    * was used in accessibility communicator
    * */
    public synchronized void applicationCompressed(String pkgName) {
        ContentValues values = new ContentValues();
        values.put(COMPRESSED_STATUS, 1);
        try {
            open();
            db.update(TABLE_APP_DETAILS, values, PKG_NAME + "=" + "'" + pkgName + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

    }

    public synchronized List<AppInfo> getQuickCompressedApps() {
        List<AppInfo> appList = new ArrayList<AppInfo>();
        // Select All Query
        //String selectQuery = "SELECT  * FROM " + TABLE_APP_DETAILS + " where " + PKG_NAME + " LIKE " + "\"%.stash\" AND " + COMPRESSED_STATUS + " = 2" + " ORDER BY " + APP_NAME;
        String selectQuery = "SELECT  * FROM " + TABLE_APP_DETAILS + " where " + COMPRESSED_STATUS + " = " + Constants.QUICK_COMPRESSED + " ORDER BY " + APP_NAME;

        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    AppInfo dummy = new AppInfo();
                    dummy.setPkgName(cursor.getString(0));
                    dummy.setAppName(cursor.getString(1));
                    dummy.setApkSize(Long.parseLong(cursor.getString(2)));
                    dummy.setCodeSize(Long.parseLong(cursor.getString(3)));
                    dummy.setCacheSize(Long.parseLong(cursor.getString(4)));
                    dummy.setSize(Long.parseLong(cursor.getString(5)));
                    dummy.setIndex(Long.parseLong(cursor.getString(6)));
                    dummy.setTotalTimeInForeground(Long.parseLong(cursor.getString(7)));
                    dummy.setLastModifiedDate(Long.parseLong(cursor.getString(8)));
                    dummy.setCategory(cursor.getString(9));
                    dummy.setLocation(cursor.getString(10));
                    //some random usage count
                    dummy.setUsageCount(2137123);
                    dummy.setCompressed_status(cursor.getInt(12));
                    dummy.setCompressed_count(cursor.getInt(13));
                    dummy.setUnCompressed_count(cursor.getInt(14));
                    // Adding contact to list
                    appList.add(dummy);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return appList;
    }

    /*
    * was used in accessibility communicator reactivate
    * */
    public synchronized void applicationUnCompressed(String pkgName) {

        //SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COMPRESSED_STATUS, -1);
        try {
            open();
            int row = db.update(TABLE_APP_DETAILS, values, PKG_NAME + "=" + "'" + pkgName + "'", null);
            Log.e("row affected", "" + row);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

    }


    /*
    * Update
    * dummyAppRemoved
    * */
    public synchronized void dummyAppRemoved(String pkgName) {
        //changing dummy apps and real app status
        //TODO : change here for change in dummy app pkg name
        String realPkgName = pkgName.substring(0, pkgName.length() - 6);


        ContentValues dummyUninstall = new ContentValues();
        //-3 .stash dummy app is uninstalled
        dummyUninstall.put(COMPRESSED_STATUS, -3);

        ContentValues realUninstall = new ContentValues();
        //-4 .stash real app is uninstalled
        realUninstall.put(COMPRESSED_STATUS, -4);
        try {
            open();
            int row = db.update(TABLE_APP_DETAILS, dummyUninstall, PKG_NAME + "=" + "'" + pkgName + "'", null);
            Log.e("row affected", "" + row);
            //row = db.update(TABLE_APP_DETAILS, realUninstall, PKG_NAME + "=" + "'" + realPkgName + "'", null);
            Log.e("row affected", "" + row);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }


    /*
    Update
    *Actual application uninstalled by user
    */
    public synchronized void appUninstalled(String pkgName) {

        Log.d("YourReceiver", "Some app will be uninstalled " + pkgName);
        //changing real app status
        ContentValues realUninstall = new ContentValues();
        realUninstall.put(COMPRESSED_STATUS, Constants.APP_UNINSTALLED);
        try {
            open();
            int row = db.update(TABLE_APP_DETAILS, realUninstall, PKG_NAME + "=" + "'" + pkgName + "'", null);
            Log.e("row affected", "" + row);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }


    /*
    Update
    *Actual application uninstalled by user
    */
    public synchronized void appQuickCompressed(String pkgName) {

        Log.d("YourReceiver", "Some app will be quick compressed " + pkgName);
        //changing real app status
        ContentValues quickCompress = new ContentValues();
        quickCompress.put(COMPRESSED_STATUS, Constants.QUICK_COMPRESSED);
        try {
            open();
            int row = db.update(TABLE_APP_DETAILS, quickCompress, PKG_NAME + "=" + "'" + pkgName + "'", null);
            Log.e("row affected", "" + row);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }


    /*
    Update
    *Actual application compressed by user
    */
    public synchronized void appCompressed(String pkgName) {

        Log.d("YourReceiver", "Some app will be quick compressed " + pkgName);
        //changing real app status
        ContentValues quickCompress = new ContentValues();
        quickCompress.put(COMPRESSED_STATUS, Constants.COMPRESSED);
        try {
            open();
            int row = db.update(TABLE_APP_DETAILS, quickCompress, PKG_NAME + "=" + "'" + pkgName + "'", null);
            Log.e("row affected", "" + row);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }


    /*
    * @param pkgName A variable of type String.
    * */

    // Adding contact to list
    public synchronized void dataUpdate(String pkgName, long dataSize, long cacheSize, long codeSize, long apkSize, long lastModifiedCacheDate) {

        // SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("ApkSize", apkSize);
        values.put("CodeSize", codeSize);
        values.put("CacheSize", cacheSize);
        values.put("DataSize", dataSize);
        values.put("ModifiedDate", lastModifiedCacheDate);

        try {
            open();
            db.update(TABLE_APP_DETAILS, values, PKG_NAME + "=" + "'" + pkgName + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }


    public synchronized void dataUpdateForegroundTime(String pkgName, long foreGroundTime, long lastDateAccessed) {
        ContentValues values = new ContentValues();
        values.put("Time", foreGroundTime);
        values.put("ModifiedDate", lastDateAccessed);
        try {
            open();
            db.update(TABLE_APP_DETAILS, values, PKG_NAME + "=" + "'" + pkgName + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    /*
    *
    * */
    public synchronized boolean appExists(String pkgName) {
        //SELECT  * FROM AppDetails where PkgName = 'com.whatsapp'
        String selectQuery = "SELECT  * FROM " + TABLE_APP_DETAILS + " where " + PKG_NAME + "=" + "'" + pkgName + "'";
        Log.d("TotalAppSizeAsync", selectQuery);
        Cursor cursor = null;

        try {
            open();
            cursor = db.rawQuery(selectQuery, null);

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                return true;
            }
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    //cursor.close();
                    return true;
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (cursor != null)
                cursor.close();
            close();
        }

        return false;
    }

    /*
    * This function list all the package in the AppDetails Table and save them in a hashset
    * @return HashSet constaining all the package in the db
    * */
    public synchronized HashSet<String> listAllPackageInDB() {
        HashSet<String> packagesSet = new HashSet<>();


        String selectQuery = "SELECT " + PKG_NAME + " FROM " + TABLE_APP_DETAILS;
        Cursor cursor = null;

        try {
            open();
            cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Log.d("hashset", "listAllPackageInDB: " + cursor.getString(0));
                    packagesSet.add(cursor.getString(0));
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (cursor != null)
                cursor.close();
            close();
        }
        return packagesSet;
    }

    public synchronized List<AppInfo> getCompressedApps() {
        //TODO:can be improved
        List<AppInfo> appList = new ArrayList<AppInfo>();
        String selectQuery = "SELECT  * FROM " + TABLE_APP_DETAILS + " where " + COMPRESSED_STATUS + "=" + "'1'";
        Log.d("DatabaseHandler", selectQuery);
        //SQLiteDatabase db = this.getWritableDatabase();
        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    AppInfo dummy = new AppInfo();
                    dummy.setPkgName(cursor.getString(0));
                    dummy.setAppName(cursor.getString(1));
                    dummy.setApkSize(Long.parseLong(cursor.getString(2)));
                    dummy.setCodeSize(Long.parseLong(cursor.getString(3)));
                    dummy.setCacheSize(Long.parseLong(cursor.getString(4)));
                    dummy.setSize(Long.parseLong(cursor.getString(5)));
                    dummy.setIndex(Long.parseLong(cursor.getString(6)));
                    dummy.setTotalTimeInForeground(Long.parseLong(cursor.getString(7)));
                    dummy.setLastModifiedDate(Long.parseLong(cursor.getString(8)));
                    dummy.setCategory(cursor.getString(9));
                    dummy.setLocation(cursor.getString(10));
                    //some random usage count
                    dummy.setUsageCount(cursor.getInt(11));
                    dummy.setCompressed_status(cursor.getInt(12));
                    dummy.setCompressed_count(cursor.getInt(13));
                    dummy.setUnCompressed_count(cursor.getInt(14));
                    // Adding contact to list
                    appList.add(dummy);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return appList;
    }

    public synchronized List<AppInfo> getUnCompressedApps() {
        //TODO:can be improved
        List<AppInfo> appList = new ArrayList<AppInfo>();
        String selectQuery = "SELECT  * FROM " + TABLE_APP_DETAILS + " where " + COMPRESSED_STATUS + "=" + "'-1'";
        Log.d("DatabaseHandler", selectQuery);
        //SQLiteDatabase db = this.getWritableDatabase();
        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    AppInfo dummy = new AppInfo();
                    dummy.setPkgName(cursor.getString(0));
                    dummy.setAppName(cursor.getString(1));
                    dummy.setApkSize(Long.parseLong(cursor.getString(2)));
                    dummy.setCodeSize(Long.parseLong(cursor.getString(3)));
                    dummy.setCacheSize(Long.parseLong(cursor.getString(4)));
                    dummy.setSize(Long.parseLong(cursor.getString(5)));
                    dummy.setIndex(Long.parseLong(cursor.getString(6)));
                    dummy.setTotalTimeInForeground(Long.parseLong(cursor.getString(7)));
                    dummy.setLastModifiedDate(Long.parseLong(cursor.getString(8)));
                    dummy.setCategory(cursor.getString(9));
                    dummy.setLocation(cursor.getString(10));
                    //some random usage count
                    dummy.setUsageCount(cursor.getInt(11));
                    dummy.setCompressed_status(cursor.getInt(12));
                    dummy.setCompressed_count(cursor.getInt(13));
                    dummy.setUnCompressed_count(cursor.getInt(14));
                    // Adding contact to list
                    appList.add(dummy);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return appList;
    }

    public synchronized boolean addCompressedApp(AppsCompressedModel app) {
        ContentValues values = new ContentValues();
        values.put(COMPRESSED_APP_NAME, app.getAppName());
        values.put(COMPRESSED_PKG_NAME, app.getPackageName());
        values.put(COMPRESSED_APP_FOLDER, app.getFolder());
        values.put(COMPRESSED_INSTALL_STATUS, app.getInstall_status());
        values.put(COMPRESSED_UNINSTALL_STATUS, app.getUninstall_status());
        //SQLiteDatabase db = this.getWritableDatabase();
        try {
            open();
            long row_id = db.insert(COMPRESSION_TABLE, null, values);
            Log.e("AccessibilityY", "inserted " + app.getPackageName() + " " + row_id);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return true;
    }

    public synchronized int updateCompressedApp(AppsCompressedModel app) {
        ContentValues values = new ContentValues();
        int rows = -1;
        values.put(COMPRESSED_PKG_NAME, app.getPackageName());
        values.put(COMPRESSED_INSTALL_STATUS, app.getInstall_status());
        values.put(COMPRESSED_UNINSTALL_STATUS, app.getUninstall_status());
        //SQLiteDatabase db = this.getWritableDatabase();
        try {
            open();
            rows = db.update(COMPRESSION_TABLE, values, COMPRESSED_PKG_NAME + "='" + app.getPackageName() + "'", null);
            Log.e("AccessibilityY", rows + " update " + app.getPackageName() + " COMPRESSED_INSTALL_STATUS " + app.getInstall_status() + " COMPRESSED_UNINSTALL_STATUS " + app.getUninstall_status());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return rows;
    }

    public synchronized int updateCompressedApp(ContentValues values, String app_package) {

        int rows = -1;
        //SQLiteDatabase db = this.getWritableDatabase();
        try {
            open();
            rows = db.update(COMPRESSION_TABLE, values, COMPRESSED_PKG_NAME + "='" + app_package + "'", null);
            Log.e("AccessibilityY", rows + " update " + app_package);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return rows;
    }

    public synchronized String getDummyPath(String app_package) {

        String dummyPath = null;
        Cursor cursor = null;
        String selectQuery = "SELECT " + COMPRESSED_APP_PATH + " FROM " + COMPRESSION_TABLE + " WHERE " + COMPRESSED_PKG_NAME + "='" + app_package + "'";

        try {
            open();
            cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                dummyPath = cursor.getString(0);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
            if (cursor != null)
                cursor.close();
        }

        if (dummyPath == null)
            Log.e("AccessibilityY", app_package + "get dummy path = null");
        else
            Log.e("AccessibilityY", app_package + "get dummy path = " + dummyPath);

        return dummyPath;
    }


    public boolean isDummy(String appname) {
        String package_name = "";

        String selectQuery = "SELECT  * FROM " + TABLE_APP_DETAILS + " where " + APP_NAME + "='" + appname + "' and " + COMPRESSED_STATUS + "=" + Constants.COMPRESSED;
        Log.d("DatabaseHandler", selectQuery);
        //SQLiteDatabase db = this.getWritableDatabase();
        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.getCount() > 0) {
                return true;
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return false;
    }

    public synchronized void deleteCompressedAppData() {

        try {
            open();
            Log.d("AccessibilityY", "delete all");
            db.delete(COMPRESSION_TABLE, null, null);
        } finally {
            close();
        }
    }

    public synchronized int deleteCompressedAppItemPackageData(String app_package) {
        int row = 0;
        try {
            open();
            row = db.delete(COMPRESSION_TABLE, COMPRESSED_PKG_NAME + "='" + app_package + "'", null);
            Log.e("AccessibilityY", row + " delete Item " + app_package);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
            return row;
        }

    }

    public synchronized int getInstallationFlag(String app_package) {
        int install_status = -1;
        Cursor cursor = null;
        String selectQuery = "SELECT install_status FROM " + COMPRESSION_TABLE + " WHERE " + COMPRESSED_PKG_NAME + "='" + app_package + "'";
        Log.e("AccessibilityY", "get Install status" + app_package);
        Log.e("AccessibilityY", selectQuery);
        try {
            open();
            cursor = db.rawQuery(selectQuery, null);

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                String install = cursor.getString(cursor.getColumnIndex(COMPRESSED_INSTALL_STATUS));
                install_status = Integer.parseInt(install);
            }

        } catch (Exception e) {
            install_status = -1;
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
            close();
            Log.e("AccessibilityY", "get Install status" + app_package + install_status);
            return install_status;
        }

    }

    public synchronized int getUninstallFlag(String app_package) {
        int uninstall_status = -1;
        Cursor cursor = null;
        String selectQuery = "SELECT uninstall_status FROM " + COMPRESSION_TABLE + " WHERE " + COMPRESSED_PKG_NAME + "='" + app_package + "'";
        Log.e("AccessibilityY", "get Uninstall status" + app_package);
        try {
            open();
            cursor = db.rawQuery(selectQuery, null);

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                String uninstall = cursor.getString(cursor.getColumnIndex(COMPRESSED_UNINSTALL_STATUS));
                uninstall_status = Integer.parseInt(uninstall);
            }

        } catch (Exception e) {
            uninstall_status = -1;
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
            close();

            Log.e("AccessibilityY", "get Uninstall status" + uninstall_status);
            return uninstall_status;
        }
    }


    public synchronized StateRetrieveModel getUncompressedAppStateData() {

        ArrayList<String> appList = new ArrayList<String>();
        ArrayList<String> appNameList = new ArrayList<String>();
        ArrayList<String> finishedAppList = new ArrayList<String>();

        HashMap dummyApkPathList = new HashMap<>();


        String uninstallRemaining = null;
        String selectQuery = "SELECT  * FROM " + COMPRESSION_TABLE;
        Log.d("AccessibilityY DB", selectQuery);
        //SQLiteDatabase db = this.getWritableDatabase();
        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {


                    AppsCompressedModel dummy = new AppsCompressedModel();
                    dummy.setFolder(cursor.getString(cursor.getColumnIndex(COMPRESSED_APP_FOLDER)));
                    dummy.setAppName(cursor.getString(cursor.getColumnIndex(COMPRESSED_APP_NAME)));
                    dummy.setPackageName(cursor.getString(cursor.getColumnIndex(COMPRESSED_PKG_NAME)));
                    dummy.setDummyApkPath(cursor.getString(cursor.getColumnIndex(COMPRESSED_APP_PATH)));
                    dummy.setUninstall_status(cursor.getString(cursor.getColumnIndex(COMPRESSED_UNINSTALL_STATUS)));
                    dummy.setInstall_status(cursor.getString(cursor.getColumnIndex(COMPRESSED_INSTALL_STATUS)));


                    if (dummy.getInstall_status().equals("0") && dummy.getUninstall_status().equals("0")) {
                        appList.add(dummy.getPackageName());
                        appNameList.add(dummy.getAppName());

                        Log.e("Accessibility", dummy.getPackageName() + " Install " + dummy.getInstall_status() + " Uninstall " + dummy.getUninstall_status());

                        if (dummy.getDummyApkPath() != null && !dummy.getDummyApkPath().equalsIgnoreCase(""))
                            dummyApkPathList.put(dummy.getPackageName(), dummy.getDummyApkPath());
                    } else if (dummy.getInstall_status().equals("1") && dummy.getUninstall_status().equals("0")) {
                        Log.e("Accessibility", dummy.getPackageName() + " Install " + dummy.getInstall_status() + " Uninstall " + dummy.getUninstall_status());

                        uninstallRemaining = dummy.getPackageName();
                    } else if (dummy.getInstall_status().equals("1") && dummy.getUninstall_status().equals("1")) {
                        Log.e("Accessibility", dummy.getPackageName() + " Install " + dummy.getInstall_status() + " Uninstall " + dummy.getUninstall_status());

                        finishedAppList.add(dummy.getPackageName());
                    }
                    // Adding contact to list


                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();

            StateRetrieveModel stateRetrieveModel = new StateRetrieveModel();
            stateRetrieveModel.setUncompressedAppList(appList);
            stateRetrieveModel.setUninstalRemainingPackage(uninstallRemaining);
            stateRetrieveModel.setDummyApkPathList(dummyApkPathList);
            stateRetrieveModel.setUncompressedAppNameList(appNameList);
            stateRetrieveModel.setCompressedAppList(finishedAppList);
            return stateRetrieveModel;

        }
    }

    public synchronized boolean deleteAppsCompressed(String app_package) {
        //SQLiteDatabase db = this.getWritableDatabase();
        int rowsAffected = -1;
        open();
        try {
            rowsAffected = db.delete(COMPRESSION_TABLE, COMPRESSED_APP_FOLDER + "='" + app_package + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }


        return rowsAffected > 0;
    }

    public synchronized String getFolderOfCompressedApp(String app_package) {
        String getFolderQuery = "SELECT " + COMPRESSED_APP_FOLDER + " FROM " + COMPRESSION_TABLE + " where " + COMPRESSED_PKG_NAME + "=" + "'" + app_package + "'";
        //SQLiteDatabase db = this.getWritableDatabase();
        String folder = "";
        Cursor cursor = null;

        try {

            open();
            cursor = db.rawQuery(getFolderQuery, null);

            if (cursor.moveToFirst()) {
                folder = cursor.getString(0);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
            close();
            return folder;
        }

    }

    public synchronized void cleanDb() {
        try {
            open();
            db.execSQL("delete from " + TOP_FIVE_TABLE);
        } finally {
            close();
        }
    }

    public synchronized void addTop5Application(AppInfo app) {
        // Made by Dhruv

        ContentValues values = new ContentValues();
        values.put("AppName", app.getAppName());
        values.put("PkgName", app.getPkgName());
        values.put("ApkSize", app.getApkSize());
        try {
            open();
            db.insert(TOP_FIVE_TABLE, null, values);
            Log.d("DatabaseHandlerINSERTED", app.getAppName());

        } finally {
            close();
        }
    }

    // ShorCt Table Logic
    public synchronized void addToShorcutTable(String name, String packagename, long size) {
        // Made by Dhruv

        ContentValues values = new ContentValues();
        values.put("AppName", name);
        values.put("PkgName", packagename);
        values.put("ApkSize", size);
        try {
            open();
            db.insert(SHORTCUT_TB, null, values);
            Log.d("DatabaseHandlerINSERTED", name);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    public synchronized int deleteFromShortcut(String app_package) {
        int row = 0;
        Log.d("REMOVE_FROM_SHORTCUT_", "trying to removeFromJSONFile: " + app_package);

        Log.d("SHORTCUT_TB", " DELETED PACKAGE" + app_package);
        try {
            open();
            row = db.delete(SHORTCUT_TB, PKG_NAME + "='" + app_package + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
            return row;
        }

    }

    public synchronized List<AppInfo> getShorcutApps() {
        //TODO:can be improved
        List<AppInfo> appList = new ArrayList<AppInfo>();
        String selectQuery = "SELECT  * FROM " + SHORTCUT_TB + " " + " ORDER BY " + APP_NAME;
        //SQLiteDatabase db = this.getWritableDatabase();
        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    AppInfo shortcut = new AppInfo();
                    shortcut.setPkgName(cursor.getString(0));
                    shortcut.setAppName(cursor.getString(1));

                    shortcut.setApkSize(Long.parseLong(cursor.getString(2)));

                    Log.d("compresssize", cursor.getString(0));
                    Log.d("compresssize", cursor.getString(1));
                    Log.d("compresssize", cursor.getString(2));
                    // Adding contact to list
                    appList.add(shortcut);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } finally {
            close();
        }
        return appList;
    }

    public synchronized List<AppInfo> getTop5Apps() {
        //TODO:can be improved
        List<AppInfo> appList = new ArrayList<AppInfo>();
        String selectQuery = "SELECT  * FROM " + TOP_FIVE_TABLE + " limit 5";
        Log.d("DatabaseHandler", selectQuery);
        //SQLiteDatabase db = this.getWritableDatabase();
        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    AppInfo top5 = new AppInfo();
                    top5.setPkgName(cursor.getString(0));
                    top5.setAppName(cursor.getString(1));
                    top5.setApkSize(Long.parseLong(cursor.getString(2)));

                    // Adding contact to list
                    appList.add(top5);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } finally {
            close();
        }
        return appList;
    }

    public synchronized void updateApplication(AppInfo app) {
        ContentValues values = new ContentValues();
        values.put("AppName", app.getAppName());
        values.put("PkgName", app.getPkgName());
        values.put("ApkSize", app.getApkSize());
        values.put("CodeSize", app.getCodeSize());
        values.put("CacheSize", app.getCacheSize());
        values.put("DataSize", app.getSize());

        //values.put("Time", app.getTotalTimeInForeground());
        //values.put("Category", app.getCategory());

        //we will not modify the data related to history of application being used
        values.put("ModifiedDate", app.getLastModifiedDate());
        //values.put("Extract", app.getLocation());
        values.put(COMPRESSED_STATUS, app.getCompressed_status());
        //values.put(COMPRESSED_COUNT, app.getCompressed_count());
        //values.put(UNCOMPRESSED_COUNT, app.getUnCompressed_count());

        //updating
        try {
            open();

            db.update(TABLE_APP_DETAILS, values, PKG_NAME + "=" + "'" + app.getPkgName() + "'", null);
            Log.d("UpdateDB", "Here");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }


    public int total_compressed_Apps() {
        //TODO:can be improved
        int count = 0;
        String selectQuery = "SELECT  * FROM " + TABLE_APP_DETAILS + " where " + COMPRESSED_STATUS + "=" + "'-1'";
        Log.d("DatabaseHandler", selectQuery);
        //SQLiteDatabase db = this.getWritableDatabase();
        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);
            count = cursor.getCount();
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return count;
    }

    public boolean isTableExists(String tableName) {

        open();
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        close();
        return false;
    }

    public void getLastModifiedDate(String s) {
        open();
        try {
            int row = db.delete(TABLE_APP_DETAILS, PKG_NAME + "='" + s + "'", null);
            Log.d("hurr", "removeStashExtensionFromDB: remove .stash installed " + s + " from db " + row);

            //update normal package name
            s = s.replace(".stash", "");
            ContentValues values = new ContentValues();
            values.put(COMPRESSED_STATUS, Constants.COMPRESSED);
            row = db.update(TABLE_APP_DETAILS, values, PKG_NAME + "=" + "'" + s + "'", null);
            Log.d("hurr", "removeStashExtensionFromDB: update original package " + s + " in db " + row);

        } catch (Exception e) {
            Log.d("hurr", "getLastModifiedDate: " + Log.getStackTraceString(e));


        }

    }

    /*
    * Image AppDBHelper
    * */


    /**
     * This method adds ImageModel img to database table Images
     *
     * @param img ImageModel to enter into database
     */
    public synchronized void addImage(ImageModel img) {

        ContentValues values = new ContentValues();
        values.put(DBCreator.PATH, img.getPath());
        values.put(DBCreator.RESPONSE_STATUS, img.getResponseStatus());
        values.put(DBCreator.ACTION_STATUS, img.getActionStatus());
        values.put(DBCreator.TRASH_PATH, img.getTrashPath());

        // Inserting Row
        try {
            open();
            db.insert(DBCreator.IMAGES_TB, null, values);
            Log.d("ImageDB", "new Image added " + img.getPath());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

    }

    /**
     * This method removes an image permanently from Image Table
     *
     * @param pTrashPath Trash Path of the image to be deleted
     */
    public synchronized void deleteImageFromTrash(String pTrashPath) {
        try {
            open();
            db.delete(DBCreator.IMAGES_TB, DBCreator.TRASH_PATH + "='" + pTrashPath + "'", null);
            Log.d("ImageDB", "removed image From Trash" + pTrashPath);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    /**
     * This method removes an image row permanently from Image Table
     *
     * @param path Path of the image to delete
     */
    public synchronized void destroyImage(String path) {
        int row = 0;
        try {
            open();
            row = db.delete(DBCreator.IMAGES_TB, DBCreator.PATH + "='" + path + "'", null);
            Log.d("ImageDB", "removed image " + path + " row id " + row);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    /*
    *
    *
    *
    */
    public synchronized HashSet<ImageModel> getAllImagePath() {
        HashSet<ImageModel> imagesSet = new HashSet<>();

        // Select All Query
        String selectQuery = "SELECT  " + "*" + " FROM " + DBCreator.IMAGES_TB;

        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    ImageModel img = new ImageModel();
                    img.setPath(cursor.getString(cursor.getColumnIndex(DBCreator.PATH)));
                    img.setResponseStatus(Integer.parseInt(cursor.getString(cursor.getColumnIndex(DBCreator.RESPONSE_STATUS))));
                    img.setActionStatus(Integer.parseInt(cursor.getString(cursor.getColumnIndex(DBCreator.ACTION_STATUS))));
                    img.setTrashPath(cursor.getString(cursor.getColumnIndex(DBCreator.TRASH_PATH)));

                    // Adding contact to list
                    imagesSet.add(img);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return imagesSet;
    }

    /**
     * This method fetch all entries of Image Table
     *
     * @return A list of ImageModel which is having all entries of the Image Table
     */
    public synchronized List<ImageModel> getAllImages() {

        List<ImageModel> imageList = new ArrayList<ImageModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + DBCreator.IMAGES_TB;

        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    ImageModel img = new ImageModel();
                    img.setPath(cursor.getString(cursor.getColumnIndex(DBCreator.PATH)));
                    img.setResponseStatus(Integer.parseInt(cursor.getString(cursor.getColumnIndex(DBCreator.RESPONSE_STATUS))));
                    img.setActionStatus(Integer.parseInt(cursor.getString(cursor.getColumnIndex(DBCreator.ACTION_STATUS))));
                    img.setTrashPath(cursor.getString(cursor.getColumnIndex(DBCreator.TRASH_PATH)));

                    // Adding contact to list
                    imageList.add(img);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return imageList;
    }

    /**
     * This methods updates image path and set its Response Status to Recycled
     *
     * @param path      location of current image
     * @param trashPath location in trash bin
     */
    public synchronized void moveToTrashBin(String path, String trashPath) {
        ContentValues values = new ContentValues();
        values.put(DBCreator.TRASH_PATH, trashPath);
        values.put(DBCreator.RESPONSE_STATUS, Constants.TRASH);
        //we are updating action status because some user may select NOT JUNK images and move them to recycle bin
        values.put(DBCreator.ACTION_STATUS, Constants.NOT_JUNK);
        try {
            open();
            db.update(DBCreator.IMAGES_TB, values, DBCreator.PATH + "=" + "'" + path + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    /**
     * This methods mark the image on given trashPath as Recovered
     *
     * @param trashPath location of the image in TRASH-BIN
     */

    public synchronized void markImageRecovered(String trashPath) {
        //here we are not changing the action status by user
        ContentValues values = new ContentValues();
        values.put(DBCreator.RESPONSE_STATUS, Constants.RECOVERED);
        values.put(DBCreator.ACTION_STATUS, Constants.NOT_JUNK);
        try {
            open();
            db.update(DBCreator.IMAGES_TB, values, DBCreator.TRASH_PATH + "=" + "'" + trashPath + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    /**
     * This methods mark the image on given path as Scanned image and Junk image
     * [OR]  mark the image on given path as Scanned image and NOT - Junk image
     *
     * @param path         location of the image
     * @param actionStatus can be Constants.JUNK or Constants.NOT_JUNK
     */
    public synchronized void markImageScannedAndActionStatus(String path, int actionStatus) {
        ContentValues values = new ContentValues();
        values.put(DBCreator.RESPONSE_STATUS, Constants.SCANNED);
        values.put(DBCreator.ACTION_STATUS, actionStatus);

        try {
            open();
            db.update(DBCreator.IMAGES_TB, values, DBCreator.PATH + "=" + "'" + path + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    /**
     * This methods mark the image on given path as JUNK image
     * PLEASE NOTE : function expects the image to be SCANNED already
     *
     * @param path location of the image
     */
    public synchronized void markImageJunk(String path) {
        ContentValues values = new ContentValues();
        values.put(DBCreator.ACTION_STATUS, Constants.JUNK);
        try {
            open();
            db.update(DBCreator.IMAGES_TB, values, DBCreator.PATH + "=" + "'" + path + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    /**
     * This methods mark the image on given path as NON-JUNK
     * PLEASE NOTE : function expects the image to be SCANNED already
     *
     * @param path location of the image
     */
    public synchronized void markImageNotJunk(String path) {
        ContentValues values = new ContentValues();
        values.put(DBCreator.ACTION_STATUS, Constants.NOT_JUNK);
        try {
            open();
            db.update(DBCreator.IMAGES_TB, values, DBCreator.PATH + "=" + "'" + path + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    /**
     * This methods mark the image on given path as IGNORED
     * PLEASE NOTE : function expects the image to be SCANNED already
     *
     * @param path location of the image
     */
    public synchronized void markImageIgnored(String path) {
        ContentValues values = new ContentValues();
        values.put(DBCreator.ACTION_STATUS, Constants.IGNORED);
        try {
            open();
            db.update(DBCreator.IMAGES_TB, values, DBCreator.PATH + "=" + "'" + path + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }


    /**
     * This method fetch all entries from Image Table where @param actionStatus
     *
     * @param status Constants.JUNK or Constant.IGNORED or Constant.NOT_SCANNED or Constants.TRASH
     * @return A list of ImageModel which is having all entries of the Image Table
     */
    public synchronized List<ImageModel> getImagesWhereStatus(int status) {

        List<ImageModel> imageList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + DBCreator.IMAGES_TB;
        switch (status) {
            case Constants.JUNK:
                selectQuery = selectQuery + " WHERE " + DBCreator.ACTION_STATUS + " = " + Constants.JUNK;
                break;
            case Constants.IGNORED:
                selectQuery = selectQuery + " WHERE " + DBCreator.ACTION_STATUS + " = " + Constants.IGNORED;
                break;
            case Constants.NOT_SCANNED:
                selectQuery = selectQuery + " WHERE " + DBCreator.RESPONSE_STATUS + " = " + Constants.NOT_SCANNED;
                break;
            case Constants.TRASH:
                selectQuery = selectQuery + " WHERE " + DBCreator.RESPONSE_STATUS + " = " + Constants.TRASH;
                break;
            case Constants.JUNK_NOT_TRASH:
                selectQuery = selectQuery + " WHERE " + DBCreator.ACTION_STATUS + " = " + Constants.JUNK + " AND " + DBCreator.RESPONSE_STATUS + " != " + Constants.TRASH;
                break;
            case Constants.NOT_JUNK_NOT_TRASH:
                selectQuery = selectQuery + " WHERE " + DBCreator.ACTION_STATUS + " != " + Constants.JUNK + " AND " + DBCreator.RESPONSE_STATUS + " != " + Constants.TRASH;
                break;
        }

        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    ImageModel img = new ImageModel();
                    img.setPath(cursor.getString(cursor.getColumnIndex(DBCreator.PATH)));
                    img.setResponseStatus(Integer.parseInt(cursor.getString(cursor.getColumnIndex(DBCreator.RESPONSE_STATUS))));
                    img.setActionStatus(Integer.parseInt(cursor.getString(cursor.getColumnIndex(DBCreator.ACTION_STATUS))));
                    img.setTrashPath(cursor.getString(cursor.getColumnIndex(DBCreator.TRASH_PATH)));
                    // Adding contact to list
                    imageList.add(img);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return imageList;

    }

    public synchronized String getRestorePath(String pTrashPath) {
        String path = "";

        String selectQuery = "SELECT  * FROM " + DBCreator.IMAGES_TB;
        selectQuery = selectQuery + " WHERE " + DBCreator.RESPONSE_STATUS + " = " + Constants.TRASH + " AND " + DBCreator.TRASH_PATH + " = '" + pTrashPath + "'";
        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    path = cursor.getString(cursor.getColumnIndex(DBCreator.PATH));
                    break;
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return path;
        //response status trash and trashpath = pTrash apath
    }

    public synchronized String getTrashPath(String pTrashPath) {
        String path = "";

        String selectQuery = "SELECT  * FROM " + DBCreator.IMAGES_TB;
        selectQuery = selectQuery + " WHERE " + DBCreator.RESPONSE_STATUS + " = " + Constants.TRASH + " AND " + DBCreator.PATH + " = '" + pTrashPath + "'";
        try {
            open();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    path = cursor.getString(cursor.getColumnIndex(DBCreator.TRASH_PATH));
                    break;
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return path;
        //response status trash and trashpath = pTrash apath
    }


    public synchronized boolean isImageTableEmpty() {
        boolean empty = false;
        try {
            open();
            Cursor cur = db.rawQuery("SELECT COUNT(*) FROM " + DBCreator.IMAGES_TB, null);
            if (cur != null) {
                cur.moveToFirst();                       // Always one row returned.
                if (cur.getInt(0) == 0) {               // Zero count means empty table.

                    empty = true;

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return empty;
    }

}

