package com.spaceup.app_services;
/**
 * Created by Dhruv on 12/21/2015.
 */

import android.app.IntentService;
import android.content.Intent;

import com.spaceup.PREPARING_ASSETS;

/**
 * Created by Dhruv on 12/01/2016.
 */
public class app_Service extends IntentService {
    public app_Service() {
        super("");
    }

    public app_Service(String name) {
        super(name);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        /**
         * Preparing Folders
         */
        String no_of_folders = intent.getStringExtra("no_of_folders");

        PREPARING_ASSETS preparing = new PREPARING_ASSETS(getApplicationContext(), Integer.parseInt(no_of_folders));
        preparing.start();

    }

}

