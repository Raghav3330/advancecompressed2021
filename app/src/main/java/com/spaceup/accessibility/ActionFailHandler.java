package com.spaceup.accessibility;

import static androidx.core.content.FileProvider.getUriForFile;
import static com.facebook.FacebookSdk.getApplicationContext;

import android.content.Intent;
import android.net.Uri;
import android.os.CountDownTimer;
import androidx.core.app.NavUtils;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.spaceup.App;
import com.spaceup.Utility.UtilityMethods;

import java.io.File;

/**
 * Created by shashank.tiwari on 14/01/17.
 */

public class ActionFailHandler {

    CountDownTimer actionFailCountDownTimer;
    String app_package;
    String app_dummy_path;
    boolean install_handle;
    int install_retry_counter, uninstall_retry_counter;
    boolean uninstall_handler;
    long EVENT_FINISH_TIMEOUT = 50000;
    long EVENT_INITIATED_TIMEOUT = 5000;                // 2 times
    long uninstallFinishTime = 0;
    long mRetryCount = 2;


    public boolean isUninstall_handler() {
        return uninstall_handler;
    }

    public boolean isInstall_handle() {
        return install_handle;
    }

    public String getApp_package() {
        return app_package;
    }

    public void setApp_package(String app_package) {
        this.app_package = app_package;
    }

    public String getApp_dummy_path() {

        return app_dummy_path;
    }

    public void setApp_dummy_path(String app_dummy_path) {
        this.app_dummy_path = app_dummy_path;
    }

    public CountDownTimer getActionFailCountDownTimer() {
        return actionFailCountDownTimer;
    }

    public void setActionFailCountDownTimer(CountDownTimer actionFailCountDownTimer) {
        this.actionFailCountDownTimer = actionFailCountDownTimer;
    }

    public void refreshInstallCounter() {
        install_retry_counter = 0;
    }

    public void refreshUninstallCounter() {
        uninstall_retry_counter = 0;
    }

    public void setUninstall_handler(boolean uninstall_handler, final String app_package, final boolean uninstallFinish) {
        this.uninstall_handler = uninstall_handler;
        this.app_package = app_package;


        if (uninstall_handler == false && actionFailCountDownTimer != null)
            actionFailCountDownTimer.cancel();

        else {
            if (actionFailCountDownTimer != null)
                actionFailCountDownTimer.cancel();
            actionFailCountDownTimer = null;

            if (uninstallFinish) {
                if (uninstallFinishTime != EVENT_FINISH_TIMEOUT) {
                    uninstallFinishTime = EVENT_FINISH_TIMEOUT;
                    mRetryCount = 1;
                    uninstall_retry_counter = 0;
                }
            } else {
                if (uninstallFinishTime != EVENT_INITIATED_TIMEOUT) {
                    uninstallFinishTime = EVENT_INITIATED_TIMEOUT;
                    mRetryCount = 2;
                    uninstall_retry_counter = 0;
                }
            }

            actionFailCountDownTimer = new CountDownTimer(uninstallFinishTime, 1000) {

                public void onTick(long millisUntilFinished) {
//                    Log.d("AccessibilityY", "uninstall " + millisUntilFinished);
                }

                public void onFinish() {
                    if (uninstall_retry_counter < mRetryCount) {
                        if (!UtilityMethods.getInstance().isPackageInstallerBusy(App.getInstance())) {
                            uninstall_retry_counter++;
                            if (AccessibilityAutomation.getSharedInstance() != null) {
                                AccessibilityAutomation.getSharedInstance().getActionFailHandler().setUninstall_handler(true, app_package, uninstallFinish);
                            }
                            if (app_package != null) {
                                Uri packageUri = Uri.parse("package:" + app_package);
                                Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageUri);
                                uninstallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                App.getInstance().startActivity(uninstallIntent);
                            }
                        } else {
                            Log.d("Compression", "package manage busy.. restarting timer");
                            if (AccessibilityAutomation.getSharedInstance() != null)
                                AccessibilityAutomation.getSharedInstance().getActionFailHandler().setUninstall_handler(true, app_package, uninstallFinish);
                        }
                    } else {

                        if (AccessibilityAutomation.getSharedInstance() != null)
                            AccessibilityAutomation.getSharedInstance().deactivateUninstallFlag();

                        Intent i = new Intent("com.times.doneApkGenerator");
                        i.putExtra("status", "UnInstalled");
                        LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(i);

                        Intent uninstallIntent = new Intent("com.times.accessibilityReceiver");
                        uninstallIntent.putExtra("compress_single_app", true);
                        LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(uninstallIntent);

                        /*Intent i = new Intent("com.times.doneApkGenerator");
                        i.putExtra("status", "UnInstalled");
                        LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(i);

                        Intent uninstallIntent = new Intent("com.times.accessibilityReceiver");
                        uninstallIntent.putExtra("compress_single_app", true);
                        LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(uninstallIntent);*/
                    }

                }
            }.start();
        }

    }

    public void setInstall_handle(boolean install_handle, final String app_dummy_path, final boolean installFinish) {
        this.install_handle = install_handle;
        this.app_dummy_path = app_dummy_path;

        if (install_handle == false && actionFailCountDownTimer != null)
            actionFailCountDownTimer.cancel();

        else {
            if (actionFailCountDownTimer != null)
                actionFailCountDownTimer.cancel();
            actionFailCountDownTimer = null;


            if (installFinish) {
                if (uninstallFinishTime != EVENT_FINISH_TIMEOUT) {
                    uninstallFinishTime = EVENT_FINISH_TIMEOUT;
                    install_retry_counter = 0;
                    mRetryCount = 1;
                }
            } else {
                if (uninstallFinishTime != EVENT_INITIATED_TIMEOUT) {
                    uninstallFinishTime = EVENT_INITIATED_TIMEOUT;
                    install_retry_counter = 0;
                    mRetryCount = 2;
                }
            }

            actionFailCountDownTimer = new CountDownTimer(uninstallFinishTime, 1000) {

                public void onTick(long millisUntilFinished) {

                    Log.d("Compression", "install " + millisUntilFinished);
                    Log.d("AccessibilityY", "install " + millisUntilFinished);
                }

                public void onFinish() {
                    if (install_retry_counter < mRetryCount) {

                        if (ActionFailHandler.this.app_dummy_path != null) {
                            if (!UtilityMethods.getInstance().isPackageInstallerBusy(App.getInstance())) {
                                if (AccessibilityAutomation.getSharedInstance() != null)
                                    AccessibilityAutomation.getSharedInstance().getActionFailHandler().setInstall_handle(true, ActionFailHandler.this.app_dummy_path, installFinish);

                                File f = new File(ActionFailHandler.this.app_dummy_path);

//                                Uri path_uri = Uri.fromFile(f);
                                Uri path_uri = getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".fileprovider",f);

//                                UtilityMethods.getInstance().throwInstallPopup(path_uri, App.getInstance());

                                Log.d("Compression", "install Retry " + install_retry_counter);
                                install_retry_counter++;
                            } else {
                                Log.d("Compression", "package manage busy.. restarting timer");
                                if (AccessibilityAutomation.getSharedInstance() != null)
                                    AccessibilityAutomation.getSharedInstance().getActionFailHandler().setInstall_handle(true, ActionFailHandler.this.app_dummy_path, installFinish);

                            }
                        } else {
                            Log.d("app_dummy_path", "null");
                            try {
                                UtilityMethods.getInstance().startNextCompression(App.getInstance());
                            } catch (NullPointerException e) {
                                Log.d("Compression", "App Instance null");
                            }

                        }
                    } else {
                        if (AccessibilityAutomation.getSharedInstance() != null)
                            AccessibilityAutomation.installingFlag = false;


                        Intent i = new Intent("com.times.doneApkGenerator");
                        i.putExtra("status", "UnInstalled");
                        LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(i);

                        Intent uninstallIntent = new Intent("com.times.accessibilityReceiver");
                        uninstallIntent.putExtra("compress_single_app", true);
                        LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(uninstallIntent);

                    }
                }
            }.start();
        }
    }


}
