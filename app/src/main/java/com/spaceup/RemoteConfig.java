package com.spaceup;

import android.content.Context;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.spaceup.Utility.PrefManager;

import static com.spaceup.Utility.PrefManager.KEY_REMOTE_CONFIG_INITIALIZED;

/**
 * Created by Ananda.Kumar on 07-03-2017.
 */

public class RemoteConfig {

    private static final String DEFAULT_STRING = "";
    private static final boolean DEFAULT_BOOLEAN = false;
    private static final int DEFAULT_INT = 0;

    private static final String LOG_TAG = RemoteConfig.class.getSimpleName();

    public static void initialize(Context pContext) {
        if (!PrefManager.getInstance(pContext).getBoolean(KEY_REMOTE_CONFIG_INITIALIZED)) {
            initializeRemoteConfig();
        }
    }

    private static void initializeRemoteConfig() {

        final FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .build();
        firebaseRemoteConfig.setConfigSettingsAsync(configSettings);
        firebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);

        firebaseRemoteConfig.fetch(7200)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // Once the config is successfully fetched it must be activated before newly fetched
                            // values are returned.
                            firebaseRemoteConfig.fetchAndActivate();
                            PrefManager.getInstance(App.getInstance()).putBoolean(KEY_REMOTE_CONFIG_INITIALIZED, true);
                        }
                    }
                });
    }

    public static String getString(@StringRes int pResId) {
        String resourceName;
        Resources resources = App.getInstance().getResources();
        try {
            resourceName = resources.getResourceEntryName(pResId);
            Log.d("getString: ",resourceName);
        } catch (Resources.NotFoundException e) {
            Log.d(LOG_TAG, e.getMessage(), e);
            return DEFAULT_STRING;
        }
        String value = FirebaseRemoteConfig.getInstance().getString(resourceName);
        Log.d("getString: ",value);
        if (value == null || value.equals(DEFAULT_STRING)) {
            return resources.getString(pResId);
        }
        return value;
    }

    public static boolean getBoolean(@StringRes int pResId) {
        String resourceName;
        Resources resources = App.getInstance().getResources();
        try {
            resourceName = resources.getResourceEntryName(pResId);
        } catch (Resources.NotFoundException e) {
            Log.d(LOG_TAG, e.getMessage(), e);
            return DEFAULT_BOOLEAN;
        }
        String value = FirebaseRemoteConfig.getInstance().getString(resourceName);
        if (value == null || value.equals(DEFAULT_STRING)) {
            value = resources.getString(pResId);
        }
        return Boolean.valueOf(value);
    }

    public static int getInt(@StringRes int pResId) {
        String resourceName;
        Resources resources = App.getInstance().getResources();
        try {
            resourceName = resources.getResourceEntryName(pResId);
        } catch (Resources.NotFoundException e) {
            Log.d(LOG_TAG, e.getMessage(), e);
            return DEFAULT_INT;
        }
        String value = FirebaseRemoteConfig.getInstance().getString(resourceName);
        if (value == null || value.equals(DEFAULT_STRING)) {
            value = resources.getString(pResId);
        }
        try {
            return Integer.valueOf(value);
        } catch (NumberFormatException e) {
            Log.d(LOG_TAG, e.getMessage(), e);
            return DEFAULT_INT;
        }
    }
}
