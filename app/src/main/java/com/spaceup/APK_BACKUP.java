package com.spaceup;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.spaceup.apkgenerator.constants.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Created by dhurv on 28-11-2016.
 */

public class APK_BACKUP {
    public static int BACKED_APK_COUNT = 0;
    String TAG = "APK_BACKUP_ACTIVITY";
    String packagename;
    Context context;
    int index, size;
    ArrayList<String> list;

    public APK_BACKUP(Context context, ArrayList<String> list, String packagename, int size, int index) {
        this.packagename = packagename;
        this.context = context;
        this.index = index;
        this.list = list;
        this.size = size;
    }

    public void load_apk() {

        extractapk(packagename, index);
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    public void extractapk(String packagename, int index) {
        Log.d(TAG, "APK BACKUP STARTED FOR APP" + packagename);
        PackageManager packageManager = context.getPackageManager();
        try {
            File file = new File(packageManager.getApplicationInfo(packagename, PackageManager.GET_META_DATA).publicSourceDir);
            File output = new File(Environment.getExternalStorageDirectory().getPath() + Constants.STASH + "/db/" + packagename + ".apk");
            try {
                output.createNewFile();
                FileOutputStream fos;
                try {

                    InputStream assetFile = new FileInputStream(file);
                    fos = new FileOutputStream(output);
                    copyFile(assetFile, fos);
                    fos.close();
                    assetFile.close();
                    Log.d("DHRUV", "DELETE ALL INTENT" + packagename);
                    BACKED_APK_COUNT++;
                    Uri packageUri = Uri.parse("package:" + packagename);
                    Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageUri);
                    //    uninstallIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    uninstallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    context.startActivity(uninstallIntent);

//                    if(BACKED_APK_COUNT==size){

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }


}