package com.spaceup.AppShortcut;

/**
 * Created by dhurv on 25-01-2017.
 */

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spaceup.R;

/**
 * Created by dhurv on 11-01-2017.
 */

class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

    TextView countryName,convertAdvcane;
    ImageView countryPhoto, badge;
    RelativeLayout relLayout;
    CardView create_shortcut_Button;
    LinearLayout shortcut_not_created, shortcut_created;
    CardView convert;


    RecyclerViewHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        countryName = (TextView)itemView.findViewById(R.id.country_name);
        countryPhoto = (ImageView)itemView.findViewById(R.id.country_photo);
        relLayout = (RelativeLayout)itemView.findViewById(R.id.card_item);
        badge= (ImageView)itemView.findViewById(R.id.imageView5);
        convertAdvcane = (TextView)itemView.findViewById(R.id.textView28);
        shortcut_not_created = (LinearLayout) itemView.findViewById(R.id.shortcut_not_created);
        shortcut_created = (LinearLayout) itemView.findViewById(R.id.shortcut_created);
        create_shortcut_Button = itemView.findViewById(R.id.create_shortcut_button);
        convert = (CardView)itemView.findViewById(R.id.convert_card);
    }

    @Override
    public void onClick(View view) {

    }
}