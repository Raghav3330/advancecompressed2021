package com.spaceup.Adapter;

/**
 * Created by shashank.tiwari on 16/03/17.
 */


import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spaceup.R;
import com.spaceup.models.ImageModel;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class RecyclerViewAdapterTrash extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Handler.Callback {
    Handler handler;
    private List<ImageModel> junkList;
    private Context context;
    public final int TRASH_HEADER = 0, SELECTER_HEADER = 1, GRID_ITEM = 2;
    ArrayList<String> selectedImages;
    private int selectedCount = 0;
    private ItemSelectCallBack mCallBack;
    private boolean allSelected;


    public RecyclerViewAdapterTrash(Context context, List<ImageModel> junkList, int gridSize, ArrayList<String> selectedImages, ItemSelectCallBack pCallBack) {
        this.junkList = junkList;
        this.context = context;
        this.selectedImages = selectedImages;
        handler = new Handler(this);
        this.mCallBack = pCallBack;


    }

    @Override
    public boolean handleMessage(Message message) {

        if (message.arg1 == 1) {
            allSelected = true;

            mCallBack.totalSelected(1);


        } else if (message.arg1 == -1) {
            allSelected = false;
            mCallBack.totalSelected(0);
        }

        //sendJunkSelectedCount(junkSelectedCount+ignoreSelectedCount);
        notifyDataSetChanged();

        return false;
    }

    public class JunkImageHeader1 extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView junkSelectedCount, junkCount, junk_image_txtET;
        public RelativeLayout junk_header;
        public CheckBox junkSelector;

        public JunkImageHeader1(View view) {
            super(view);
            junk_image_txtET = (TextView) view.findViewById(R.id.junk_image_txt);
            junkCount = (TextView) view.findViewById(R.id.junk_count);
            junkSelectedCount = (TextView) view.findViewById(R.id.junk_selected_count);
            junk_header = (RelativeLayout) view.findViewById(R.id.junk_header);
            junkSelector = (CheckBox) view.findViewById(R.id.selected);

            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            boolean add;
            if (junkSelector.isChecked())
                add = false;
            else
                add = true;
            new listSelected(add, getPosition(), handler).execute();
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case TRASH_HEADER:

                View trashView = LayoutInflater.from(parent.getContext()).inflate(R.layout.trash_header, parent, false);
                RecyclerViewHoldersMesage rcv = new RecyclerViewHoldersMesage(trashView);

                return rcv;
            case SELECTER_HEADER:
                View junkView = LayoutInflater.from(parent.getContext()).inflate(R.layout.junk_header, parent, false);
                JunkImageHeader1 junkHolder1 = new JunkImageHeader1(junkView);
                return junkHolder1;

            case GRID_ITEM:
                View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item, null);
                RecyclerViewHolders rcv1 = new RecyclerViewHolders(layoutView);
                return rcv1;
            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof RecyclerViewHolders) {
            RecyclerViewHolders viewHolder = (RecyclerViewHolders) holder;
            Uri uri = null;
            if (position <= junkList.size() + 1) {
                uri = Uri.fromFile(new File(junkList.get(position - 2).getTrashPath()));
                boolean checked = junkList.get(position - 2).isChecked();
                if (checked)
                    viewHolder.selector.setBackgroundResource(R.drawable.junk_image_background_selected_drawable);
                else
                    viewHolder.selector.setBackground(null);
                viewHolder.checkBox.setChecked(junkList.get(position - 2).isChecked());
            }

            Picasso.with(context).load(uri)
                    .resize(120, 120).centerCrop().error(R.mipmap.ic_launcher).into(viewHolder.image);
        } else if (holder instanceof JunkImageHeader1) {
            JunkImageHeader1 viewHolder = (JunkImageHeader1) holder;
            viewHolder.junkCount.setVisibility(View.INVISIBLE);
            //viewHolder.junkSelectedCount
            if(junkList.size() == selectedImages.size()){

                allSelected = true;
            }
            else {
                allSelected = false;
            }
            viewHolder.junkSelector.setChecked(allSelected);
            viewHolder.junkSelectedCount.setText(selectedImages.size() + " ");
            viewHolder.junk_image_txtET.setText(junkList.size() + " Images");

        }


    }


    @Override
    public int getItemCount() {
        return (this.junkList.size() + 2);
    }

    public interface ItemSelectCallBack {

        void totalSelected(int total);
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0)
            return TRASH_HEADER;
        if (position == 1)
            return SELECTER_HEADER;

        return GRID_ITEM;
    }

    public class RecyclerViewHoldersMesage extends RecyclerView.ViewHolder implements View.OnClickListener {

        public RecyclerViewHoldersMesage(View itemView) {
            super(itemView);
        }

        @Override
        public void onClick(View v) {

        }
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

        //public TextView countryName;
        public ImageView image, selector;
        public CheckBox checkBox;

        public RecyclerViewHolders(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.whatsapp_image_grid);
            selector = (ImageView) view.findViewById(R.id.selector);
            checkBox = (CheckBox) view.findViewById(R.id.selected);
            //checkBox.setButtonDrawable(R.drawable.checkbox_drawable);
            image.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getPosition();
            if (position <= junkList.size() + 1) {
                boolean checked = !junkList.get(position - 2).isChecked();
                junkList.get(position - 2).setChecked(checked);

                if (checked) {
                    selectedImages.add(junkList.get(position - 2).getTrashPath());
                    selectedCount++;
                } else {
                    selectedImages.remove(junkList.get(position - 2).getTrashPath());
                    selectedCount++;
                }
                notifyItemChanged(1);
                notifyItemChanged(position);

            }
            mCallBack.totalSelected(selectedImages.size());
        }
    }

    public class listSelected extends AsyncTask<String, Void, Message> {
        boolean add;
        Handler handler;
        int position;

        listSelected(boolean add, int position, Handler handler) {
            this.add = add;
            this.handler = handler;
            this.position = position;
        }


        @Override
        protected Message doInBackground(String... strings) {

            if (add) {

                selectedImages.removeAll(selectedImages);
                for (ImageModel img : junkList) {
                    selectedImages.add(img.getTrashPath());
                    img.setChecked(true);

                }
                selectedCount = junkList.size();
            } else {

                for (ImageModel img : junkList) {
                    selectedImages.remove(img.getTrashPath());
                    img.setChecked(false);

                }
                selectedCount = 0;
            }

            Message message = new Message();
            if (add)
                message.arg1 = 1;
            else
                message.arg1 = -1;

            return message;
        }

        @Override
        protected void onPostExecute(Message message) {
            super.onPostExecute(message);
            handler.dispatchMessage(message);
        }
    }

}