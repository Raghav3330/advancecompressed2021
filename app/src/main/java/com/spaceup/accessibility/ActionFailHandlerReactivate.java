package com.spaceup.accessibility;


import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.CountDownTimer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.spaceup.App;

import java.util.List;

/**
 * Created by shashank.tiwari on 14/01/17.
 */

public class ActionFailHandlerReactivate {

    private CountDownTimer actionFailCountDownTimer;
    private String app_package;
    private int uninstall_retry_counter;
    private boolean uninstall_handler;
    private long EVENT_INITIATED_TIMEOUT = 5000;                // 2 times
    private long uninstallFinishTime = 0;
    private long mRetryCount = 2;

    boolean install_handle;

    private  Context context;
    int install_retry_counter;
    String app_dummy_path;


    public void refreshUninstallCounter() {
        uninstall_retry_counter = 0;
    }

    public boolean isPackageExisted(String targetPackage, Context context) {
        List<ApplicationInfo> packages;
        PackageManager pm;

        pm = context.getPackageManager();
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            if (packageInfo.packageName.equals(targetPackage))
                return true;
        }
        return false;
    }
    public void setUninstall_handler(boolean uninstall_handler, final String app_package, final boolean uninstallFinish, final Context context) {
        this.uninstall_handler = uninstall_handler;
        this.app_package = app_package;
        this.context = context;


        if (uninstall_handler == false && actionFailCountDownTimer != null)
            actionFailCountDownTimer.cancel();

        else {
            if (actionFailCountDownTimer != null)
                actionFailCountDownTimer.cancel();

            actionFailCountDownTimer = null;


            uninstallFinishTime = EVENT_INITIATED_TIMEOUT;
            mRetryCount=2;

            actionFailCountDownTimer = new CountDownTimer(uninstallFinishTime, 1000) {

                public void onTick(long millisUntilFinished) {
                    Log.d("AccessibilityY", "uninstall " + millisUntilFinished);
                }

                public void onFinish() {
                    if (uninstall_retry_counter < mRetryCount) {
                        uninstall_retry_counter ++;
                        if (AccessibilityAutomation.getSharedInstance() != null) {
                            AccessibilityAutomation.getSharedInstance().getActionFailHandlerReactivate().setUninstall_handler(true, app_package, uninstallFinish,context);
                        }
                        if (app_package != null && isPackageExisted(app_package,context) ) {
                            Log.d("TAGGING", "CALL THIS WHEN DUMMY RETRY");

                            Uri packageUri = Uri.parse("package:" + app_package);
                            Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageUri);

                            uninstallIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            App.getInstance().startActivity(uninstallIntent);
                        }
                    } else {
                        Log.d("TAGGING", "CALL THIS WHEN RETRY CANCEL");

                        if (AccessibilityAutomation.getSharedInstance() != null)
                            AccessibilityAutomation.getSharedInstance().deactivateUninstallFlag();

                        if (app_package != null) {
                            Intent uninstallIntent = new Intent("com.times.accessibilityReceiver");
                            uninstallIntent.putExtra("cleaning", true);
                            uninstallIntent.putExtra("clean_package", app_package);
                            LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(uninstallIntent);
                        }

                    }

                }
            }.start();
        }

    }


}

