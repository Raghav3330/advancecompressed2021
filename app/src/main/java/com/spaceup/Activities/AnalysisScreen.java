package com.spaceup.Activities;


/**
 * Created by shashanktiwari on 26/11/16.
 */

import android.Manifest;
import android.accessibilityservice.AccessibilityService;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.AppOpsManager;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import androidx.cardview.widget.CardView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import com.facebook.ads.*;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.devadvance.circularseekbar.CircularSeekBar;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.App;
import com.spaceup.BuildConfig;
import com.spaceup.R;
import com.spaceup.RemoteConfig;
import com.spaceup.Runnable.AccRunnable;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityAutomation;
import com.spaceup.accessibility.permission.AppUsagePemissionHandler;
import com.spaceup.accessibility.permission.UnknownSourcePermission;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.app_services.app_Service;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.uninstall.activities.AppInfo;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan;
import io.github.inflationx.calligraphy3.TypefaceUtils;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class AnalysisScreen extends BaseActivity implements Handler.Callback, ViewPager.OnPageChangeListener, View.OnClickListener {
    public static boolean requestTurnOn = false;
    private final int TOTAL_APP_SIZE = 1;
    private final String path = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + "/" + Constants.STASH_DB_FILE + ".json";
    public TextView difficult, app_crashed, data_lost, slow_process;
    private View oView1;
    private WindowManager wm1;
    private RelativeLayout layout;
    private FragmentPageAdapter mPagerAdapter;
    private long size_of_system_apps;
    private UtilityMethods methods;
    private Thread usageCheckThread;
    private UsageRunnable usageRunnable;
    //inform onActivityResult case 44 : don't start scanning
    boolean start_scanning = false;
    private Dialog dialogReview;
    private Dialog appUsageDialog;
    private CircularSeekBar circularSeekBar;
    //button click status
    boolean difficult_clicked = false, app_crashed_clicked = false, data_lost_clicked = false, slow_process_clicked = false, send_button_clicked = false;
    EditText negative_feedback_editText;
    int app_gray, transparent, stroke;
    long mTotalStorage;
    private Bundle mGAParams;
    private RelativeLayout upper_layout;
    private CardView card_view;
    private ImageView more_options;
    private TextView percentage_app_storage, memory_in_device, total_memory_in_device, scan_btn, scoretext;
    private int STORAGE_PERMISSION_CODE = 23;
    private ViewPager mPager;
    private long total_app_size = 0;
    private double available_internal_memory;
    private int total_apps = 0, previous_progress_perc;
    private boolean thread_execution = true;
    private String TAG = "Analysis";
    private long compressed_app_size = 0, compressed_apps = 0;
    private String no_of_folders;
    //private ProgressBar proress_foreground, proress_background;
    private PrefManager prefManager;
    private PopupWindow popupWindow;

    private int mMemoryLeft;
    private boolean mAnalyticsSend = false;
    private LinearLayout dotsLayout, manageApps, trashBtn;
    private View trash_manage_seprator;

    private TextView[] dots;
    private View card_view_19;
    private CardView card_view_20;
    private long analyticsPercentage = -1;
    private WindowManager windowManager;
    private View overlayLayout;
    private AccRunnable accRunnable;
    private Thread accCheckThread;
    private boolean mAppUsageRequest = false;
    private View optionMenu;


    //finalActivityFinishBroadCastReceiver
    BroadcastReceiver finalFinishBroadCastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            previous_progress_perc = previous_progress_perc + 5;
            boolean abort = intent.getExtras().getBoolean("aborted", false);
            Log.d("finish", "value in abort " + abort + " getSessionCompressCount " + prefManager.getSessionCompressCount() + " isReviewed " + prefManager.isReviewed() + " getAppRunCount " + prefManager.getAppRunCount());
            /*show dialog when
            * user did not abort compression
            * App run count is odd
            * number of apps compressed is greater than zero
            * not reviewed earlier
            */
            if (!abort && (prefManager.getAppRunCount() % 2 != 0) && prefManager.getSessionCompressCount() > 0 && !prefManager.isReviewed()) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("finish", "open dialog after 1 second");
                        reviewDialog();
                    }
                }, 1000);
            }
        }
    };

    //for analytics
    public static long getApkSize(Context context, String packageName)
            throws PackageManager.NameNotFoundException {
        return new File(context.getPackageManager().getApplicationInfo(
                packageName, 0).publicSourceDir).length();
    }

    public static String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
            suffix = "KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = "MB";
                size /= 1024;
            }
        }

        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        if (suffix != null) resultBuffer.append(suffix);
        return resultBuffer.toString();
    }

    public static String getSizeinMB(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    public static String getSizeinMBZeroDecimal(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "");
        return String.format("%.0f %sB", bytes / Math.pow(unit, exp), pre);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAppUsageRequest) {
            mAppUsageRequest = false;
            Log.d("Theme", "BUG TestActivity onResume: of analysis screen");
            if (UtilityMethods.getInstance().isAccessibilityServiceRunning(AnalysisScreen.this)) {
                if (!UtilityMethods.getInstance().isHavingUnkownSourcePermission(getApplicationContext())) {
                    UnknownSourcePermission.getInstance().launchIntent(getApplicationContext(), getIntent());
                } else {
                    if (!UtilityMethods.getInstance().isHavingUsageStatsPermission(getApplicationContext())) {
                        AppUsagePemissionHandler.getInstance().launchIntent(getApplicationContext(), getIntent());
                    } else {
                        startActivity(new Intent(AnalysisScreen.this, Scanning.class));
                    }
                }
            }
        }

        Update();
    }

    private AdView mAdView;
    private com.facebook.ads.AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stash_background);


        Log.d(TAG, "onCreate: ACCESIBILITY_STATUS" + AccessibilityAutomation.getSharedInstance());

        Bundle b = getIntent().getExtras();
        if (b != null) {
            if ("true".equals(b.getString(Constants.INCOME_FROM_ONBOARDING))) {
                new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.ONBOARD,
                        AnalyticsConstants.Action.ONBOARDED_NEW,
                        AnalyticsConstants.Label.OPEN,
                        null, false, null);
            }
        }
        try {
            int currentAppVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            int remoteAppVersion = RemoteConfig.getInt(R.string.remote_version_code);
            if (remoteAppVersion >= currentAppVersion) {
                Intent intent = new Intent(this, AppUpgradeActivity.class);
                startActivity(intent);
                finish();
                return;
            }
        } catch (PackageManager.NameNotFoundException | NumberFormatException e) {
            Log.d(TAG, e.getMessage(), e);
        }

//        if (RemoteConfig.getString(R.string.ad_network).equals("google")) {
//            mAdView = new AdView(this);
//            mAdView.setAdSize(AdSize.FULL_BANNER);
//            RelativeLayout layout = (RelativeLayout) findViewById(R.id.add_view_rel);
//            if (BuildConfig.DEBUG) {
//            mAdView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");
//            Log.d("onCreate: ",UtilityMethods.getInstance().getAdmobID(UtilityMethods.HOME_SCREEN));
//            mAdView.setAdUnitId(UtilityMethods.getInstance().getAdmobID(UtilityMethods.HOME_SCREEN));
//            } else {
//            mAdView.setAdUnitId(UtilityMethods.getInstance().getAdmobID(UtilityMethods.HOME_SCREEN));
//            }
//            AdRequest adRequest = new AdRequest.Builder().build();
//
//            mAdView.loadAd(adRequest);
//
//            if( RemoteConfig.getBoolean(R.string.show_banner_ads) && UtilityMethods.getInstance().isNewUser(getApplicationContext())) {
//
//                layout.addView(mAdView);
//            }
//        }else if(RemoteConfig.getString(R.string.ad_network).equals("facebook")) {
//            adView = new com.facebook.ads.AdView(this, "IMG_16_9_APP_INSTALL#600591120962050_601274687560360", com.facebook.ads.AdSize.BANNER_HEIGHT_50);
//            RelativeLayout layout = (RelativeLayout) findViewById(R.id.add_view_rel);
//            adView.loadAd();
//
//            if( RemoteConfig.getBoolean(R.string.show_banner_ads) && UtilityMethods.getInstance().isNewUser(getApplicationContext())) {
//
//                layout.addView(adView);
//            }
//        }

        TextView crash_app_force = findViewById(R.id.crash_app_force);
        crash_app_force.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BuildConfig.DEBUG) {
                    int a = (1 / 0);
                }
            }
        });

        methods = UtilityMethods.getInstance();
        init();


        Log.d("finish", "registered finalFinishBroadCastReceiver");
        IntentFilter finalActivityFinishIntentFilter = new IntentFilter();
        finalActivityFinishIntentFilter.addAction(FinalResultScreen.FINAL_ACTIVITY_FINISHED);
        LocalBroadcastManager.getInstance(this).registerReceiver(finalFinishBroadCastReceiver, finalActivityFinishIntentFilter);

        /*
        * from second run, check if app run is % 2 then start a thread to show review dialog
        * */
        if (prefManager.getAppRunCount() >= 2 && prefManager.getAppRunCount() % 2 == 0 && prefManager.getSessionCompressCount() > 0 && !prefManager.isReviewed()) {
            Log.d("finish", "handler started");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d("finish", " getSessionCompressCount " + prefManager.getSessionCompressCount() + " isReviewed " + prefManager.isReviewed() + " getAppRunCount " + prefManager.getAppRunCount());
                    //if more than one app compressed and user have not reviewed

                    reviewDialog();

                }
            }, 1000);
        }


        if (Build.VERSION.SDK_INT >= 21) {
            PackageInstaller packageInstaller = getPackageManager().getPackageInstaller();
            List<PackageInstaller.SessionInfo> list = packageInstaller.getAllSessions();

            if (list.size() == 0) {
                Log.d("PackageInstaller", "size zero");
            }

            /*for (PackageInstaller.SessionInfo info : list) {
                Log.d("PackageInstaller", info.getAppPackageName());
            }*/
        }

//        displayWelcomeMessage(scan_btn);

        //for full screen
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            card_view_20.setVisibility(View.VISIBLE);
            card_view_19.setVisibility(View.GONE);
        } else if (Build.VERSION.SDK_INT >= 19) {
            setStatusBarTranslucent(true);
            card_view_20.setVisibility(View.GONE);
            card_view_19.setVisibility(View.VISIBLE);
        }

        //mPager.setOnClickListener((View.OnClickListener) this);
        //subtracting our own com.stash application
        /**
         *
         * Start Service
         *
         */
        Log.d("ServiceStatus", "I am here1");

        no_of_folders = "6";    //means 5 folders will be created
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("no_of_folders", no_of_folders).apply();
        if (Build.VERSION.SDK_INT >= 23) {
            Log.d("ServiceStatus", "I am here2");
            if (isReadStorageAllowed()) {
                //If permission is already having then showing the toast
                //Existing the method with return

                start_service();

            } else {
                Log.d("ServiceStatus", "I am here6");
                //If the app has not the permission then asking for the permission
                requestStoragePermission();
            }
        } else {
            start_service();
        }
        addBottomDots(0);

    }
    private boolean isAccessGranted() {
        try {
            PackageManager packageManager = getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager) getSystemService(Context.APP_OPS_SERVICE);
            int mode = 0;
            if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.KITKAT) {
                mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                        applicationInfo.uid, applicationInfo.packageName);
            }
            return (mode == AppOpsManager.MODE_ALLOWED);

        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }


    private void init() {
        circularSeekBar = (CircularSeekBar) findViewById(R.id.circularSeekBar);
        circularSeekBar.setIsTouchEnabled(false);
        scoretext = findViewById(R.id.score_text);
        layout = (RelativeLayout) findViewById(R.id.text_container);
        upper_layout = (RelativeLayout) findViewById(R.id.upper_layout);
        card_view = (CardView) findViewById(R.id.card_view);
        card_view.setOnClickListener(this);
        more_options = (ImageView) findViewById(R.id.more_options);
        more_options.setOnClickListener(this);


        percentage_app_storage = (TextView) findViewById(R.id.app_usage_percentage);
        total_memory_in_device = (TextView) findViewById(R.id.total_memory_in_device);
        memory_in_device = (TextView) findViewById(R.id.memory_in_device);

        prefManager = PrefManager.getInstance(App.getInstance());
        mPager = (ViewPager) findViewById(R.id.pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);

        scan_btn = (TextView) findViewById(R.id.scan_btn);
        card_view_19 = findViewById(R.id.card_view_v19);
        card_view_20 = (CardView) findViewById(R.id.card_view_v20);

        PrefManager.getInstance(getApplicationContext()).setisCompresssingFirst(false);
        Log.d("UNINSTALLER", "setisCompresssingFirst" + false);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        optionMenu = inflater.inflate(R.layout.menu, null, false);
        float widthPopUp = UtilityMethods.getInstance().convertDpToPixel(170, AnalysisScreen.this);
        popupWindow = new PopupWindow(optionMenu, (int) widthPopUp,
                WindowManager.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setContentView(optionMenu);

        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.CYAN));
        popupWindow.setOutsideTouchable(true);


        manageApps = (LinearLayout) optionMenu.findViewById(R.id.manage_apps);

        trashBtn = (LinearLayout) optionMenu.findViewById(R.id.trash);
        trash_manage_seprator = optionMenu.findViewById(R.id.trash_manage_seprator);
        //NOTIFICATION PENDINGS

    }
    public static long getFileFolderSize(File dir) {
        long size = 0;
        if (dir.isDirectory()) {
            for (File file : dir.listFiles()) {
                if (file.isFile()) {
                    size += file.length();
                } else
                    size += getFileFolderSize(file);
            }
        } else if (dir.isFile()) {
            size += dir.length();
        }
        return size;
    }

    void Update() {
        thread_execution = true;
        addBottomDots(0);

        List<AppInfo> appInDb = AppDBHelper.getInstance(AnalysisScreen.this).getAppDetails();
        //list of quick compressed apps
        //List<AppInfo> quickCompressedApps = AppDBHelper.getInstance(AnalysisScreen.this).getShorcutApps();
        compressed_apps = 0;
        compressed_app_size = 0;
        total_apps = 0;
        total_app_size = 0;
        size_of_system_apps = 0;

        try {
            File file = new File(Environment.getExternalStorageDirectory() + "/" + Constants.APK_DB);
            if (file != null) {
                long size = 0;
                size = getFileFolderSize(file);

                double sizeMB = (double) size / 1024 / 1024;
                String s = " MB";
                if (sizeMB < 1) {
                    sizeMB = (double) size / 1024;
                    s = " KB";
                }
//              compressed_app_size = file.getTotalSpace();
                Log.d("Update: ", String.valueOf(sizeMB));
                if (file.listFiles() != null) {
                    compressed_app_size = compressed_app_size + Math.round(sizeMB) - 17;
                    compressed_apps = file.listFiles().length - 2;
                }
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        long temp_Size = 0;
        for (AppInfo app : appInDb) {
            Log.d("DBOperation", "Analysis Screen :Package Name " + app.getPkgName());
            //check that application status is compressed and package name is not com.stash i.e. we are excluding our own application
            if (app.getCompressed_status() == Constants.QUICK_COMPRESSED || app.getCompressed_status() == Constants.COMPRESSED) {
                Log.d("DBOperation", "Analysis Screen : Compress Status COMPRESSED");
                temp_Size = app.getCodeSize() + app.getApkSize() + app.getSize() + app.getCacheSize();
                Log.d("DBOperation", "Analysis Screen : Data Size " + temp_Size);
//                compressed_app_size = compressed_app_size + temp_Size;
                long appSizeSaved = PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.TOTAL_APP_SPACE_SAVED);
                Log.d("Update: ",String.valueOf(appSizeSaved));
//                compressed_app_size = Long.parseLong(UtilityMethods.getSizeinMBOnly((long) (appSizeSaved)));
//                compressed_apps++;
            } else if (app.getCompressed_status() == Constants.UNCOMPRESSED) {
                //counting uncompressed apps
                Log.d("DBOperation", "Analysis Screen : Compress Status UNCOMPRESSED");
                temp_Size = app.getCodeSize() + app.getApkSize() + app.getSize() + app.getCacheSize();
                total_app_size = total_app_size + temp_Size;
                Log.d("DBOperation", "Analysis Screen : Data Size " + temp_Size);
                total_apps++;
            } else if (app.getCompressed_status() == Constants.SYSTEM_APP) {
                Log.d("DBOperation", "Analysis Screen : Compress Status SYSTEM APP");
                temp_Size = app.getCodeSize() + app.getApkSize() + app.getSize() + app.getCacheSize();
                Log.d("DBOperation", "Analysis Screen : Data Size " + temp_Size);
                size_of_system_apps = size_of_system_apps + temp_Size;
            }

            //reinitialize temp_Size
            temp_Size = 0;
        }

        available_internal_memory = getAvailableInternalMemorySize();

        Log.d(TAG, "Compressed apps size " + getSizeinMB(compressed_app_size, false));
        Log.d(TAG, "Total app size " + getSizeinMB(total_app_size, false));
        Log.d(TAG, "Total number of apps " + total_apps);
        Log.d(TAG, "available_internal_memory " + available_internal_memory);

        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");

        Calendar c = Calendar.getInstance();

        Log.d("Theme", "BUG TestActivity Current time" + df.format(c.getTime()));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(prefManager.getLastCompressTimeStamp());

        long timediff = c.getTimeInMillis() - prefManager.getLastCompressTimeStamp();
        Log.d("Theme", "BUG TestActivity time difference " + timediff);
        //for first run set theme to red
        if (!prefManager.isFirstRun() && (timediff <= 86400000)) {
            //blue theme
            Log.d("Theme", "BUG TestActivity setting blue theme");
            prefManager.setTheme(0);
            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.HOMESCREEN, AnalyticsConstants.Action.CONVERSION_TO_SCAN_BLUE, AnalyticsConstants.Label.HOME_LANDING, null, false, null);

        } else {
            //red theme
            Log.d("Theme", "BUG TestActivity setting red theme");
            prefManager.setTheme(1);
            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.HOMESCREEN, AnalyticsConstants.Action.CONVERSION_TO_SCAN_RED, AnalyticsConstants.Label.HOME_LANDING, null, false, null);

        }


        mPagerAdapter = new FragmentPageAdapter(getSupportFragmentManager(), total_apps, total_app_size, compressed_apps, compressed_app_size);
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(this);

        ViewTreeObserver vto = layout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                int height = layout.getMeasuredHeight();
                Log.d("height", "" + height);

                new Thread() {

                    public void run() {

                        while (thread_execution) {
                            (AnalysisScreen.this).runOnUiThread(new Runnable() {
                                public void run() {

                                    int percentage = (int) (circularSeekBar.getProgress() / 10);


                                    percentage_app_storage.setText((percentage) + "");
                                }
                            });
                            try {
                                Thread.sleep(50);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }.start();

                //size excluding os in long
                long totalInternalMemorySize = getTotalInternalMemorySize();
                //size excluding os in GB
                long sizeExcludingOS = totalInternalMemorySize / (1024 * 1024 * 1024);

                //size of operating system
                long size_of_os = 0;
                long mOnlyAppSize = 0;
                //internal memory of device
                mTotalStorage = 0;

                long totalAppSize = 0;
                if (sizeExcludingOS < 4) {
                    size_of_os = 4 * 1024 * 1024 * 1024L - totalInternalMemorySize;
                    total_memory_in_device.setText("-- 4 GB (Total)");
                    mTotalStorage = 4 * 1024 * 1024 * 1024l;
                    new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.HOMESCREEN, AnalyticsConstants.Action.PHONE_STORAGE, AnalyticsConstants.Label.STORAGE_4GB, String.valueOf(4), true, PrefManager.PHONE_STORAGE_PREF);


                } else if (sizeExcludingOS < 8) {
                    size_of_os = 8 * 1024 * 1024 * 1024L - totalInternalMemorySize;
                    total_memory_in_device.setText("-- 8 GB (Total)");
                    new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.HOMESCREEN, AnalyticsConstants.Action.PHONE_STORAGE, AnalyticsConstants.Label.STORAGE_8GB, String.valueOf(8), true, PrefManager.PHONE_STORAGE_PREF);

                    mTotalStorage = 8 * 1024 * 1024 * 1024L;
                } else if (sizeExcludingOS < 16) {
                    size_of_os = 16 * 1024 * 1024 * 1024L - totalInternalMemorySize;
                    total_memory_in_device.setText("-- 16 GB (Total)");
                    new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.HOMESCREEN, AnalyticsConstants.Action.PHONE_STORAGE, AnalyticsConstants.Label.STORAGE_16GB, String.valueOf(16), true, PrefManager.PHONE_STORAGE_PREF);

                    mTotalStorage = 16 * 1024 * 1024 * 1024L;
                } else if (sizeExcludingOS < 32) {
                    size_of_os = 32 * 1024 * 1024 * 1024L - totalInternalMemorySize;
                    new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.HOMESCREEN, AnalyticsConstants.Action.PHONE_STORAGE, AnalyticsConstants.Label.STORAGE_32GB, String.valueOf(32), true, PrefManager.PHONE_STORAGE_PREF);

                    total_memory_in_device.setText("-- 32 GB (Total)");

                    mTotalStorage = 32 * 1024 * 1024 * 1024L;
                } else if (sizeExcludingOS < 64) {
                    size_of_os = 64 * 1024 * 1024 * 1024L - totalInternalMemorySize;
                    total_memory_in_device.setText("-- 64 GB (Total)");
                    new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.HOMESCREEN, AnalyticsConstants.Action.PHONE_STORAGE, AnalyticsConstants.Label.STORAGE_64GB, String.valueOf(64), true, PrefManager.PHONE_STORAGE_PREF);

                    mTotalStorage = 64 * 1024 * 1024 * 1024L;
                } else {
                    size_of_os = 128 * 1024 * 1024 * 1024L - totalInternalMemorySize;
                    total_memory_in_device.setText("-- 128 GB (Total)");
                    new AnalyticsController(getApplicationContext()).sendAnalytics(
                            AnalyticsConstants.Category.HOMESCREEN,
                            AnalyticsConstants.Action.PHONE_STORAGE,
                            AnalyticsConstants.Label.STORAGE_128GB,
                            String.valueOf(128), true, PrefManager.PHONE_STORAGE_PREF);


                    mTotalStorage = 128 * 1024 * 1024 * 1024L;
                }
                if (prefManager.getTheme() == 0) {
                    //blue theme
                    try {
                        long sessionCount = prefManager.getSessionCompressCount();
                        if (sessionCount == 0) {
                            sessionCount = 1;
                        }
                        totalAppSize = total_app_size + (size_of_system_apps / sessionCount) + size_of_os + (long) available_internal_memory;
                        mOnlyAppSize = total_app_size;
                        long mOnlySystem = size_of_system_apps + size_of_os;

                        if (totalAppSize >= (.90 * mTotalStorage)) {
                            totalAppSize = (long) (0.90 * mTotalStorage);
                            memory_in_device.setText("-- " + getSizeinMBZeroDecimal(totalAppSize, false) + " (Apps)");
                        } else {
                            memory_in_device.setText("-- " + getSizeinMBZeroDecimal(totalAppSize, false) + " (Apps)");
                        }
                        new AnalyticsController(getApplicationContext()).sendAnalytics(
                                AnalyticsConstants.Category.HOMESCREEN,
                                AnalyticsConstants.Action.APPS_STORAGE,
                                null,
                                String.valueOf(Math.round(mOnlyAppSize / (1000000f))),
                                true, PrefManager.APP_STORAGE_PREF);
                        new AnalyticsController(getApplicationContext()).sendAnalytics(
                                AnalyticsConstants.Category.HOMESCREEN,
                                AnalyticsConstants.Action.SYSTEM_STORAGE,
                                null,
                                String.valueOf(Math.round(mOnlySystem / (1000000f))),
                                true, PrefManager.APP_SYSTEM_PREF);

                        Log.d("Marker", "" + getSizeinMBZeroDecimal(totalAppSize, false));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                    //red theme
                    totalAppSize = total_app_size + size_of_system_apps + size_of_os + (long) available_internal_memory;
                    mOnlyAppSize = total_app_size;
                    long mOnlySystem = size_of_system_apps + size_of_os;

                    if (totalAppSize >= (.90 * mTotalStorage)) {

                        Log.d("Memory", "TOTAL APP SIZE BEFORE" + getSizeinMBZeroDecimal(totalAppSize, false));
                        totalAppSize = (long) (.90 * mTotalStorage);
                        Log.d("Memory", "TOTAL APP SIZE AFTER" + getSizeinMBZeroDecimal(totalAppSize, false));
                        memory_in_device.setText("-- " + getSizeinMBZeroDecimal(totalAppSize, false) + " (Apps)");

                    } else {
                        memory_in_device.setText("-- " + getSizeinMBZeroDecimal(totalAppSize, false) + " (Apps)");
                    }
                    new AnalyticsController(getApplicationContext()).sendAnalytics(
                            AnalyticsConstants.Category.HOMESCREEN,
                            AnalyticsConstants.Action.APPS_STORAGE,
                            null,
                            String.valueOf(Math.round(mOnlyAppSize / (1000000f))),
                            true, PrefManager.APP_STORAGE_PREF);

                    new AnalyticsController(getApplicationContext()).sendAnalytics(
                            AnalyticsConstants.Category.HOMESCREEN,
                            AnalyticsConstants.Action.SYSTEM_STORAGE,
                            null,
                            String.valueOf(Math.round(mOnlySystem / (1000000f))),
                            true, PrefManager.APP_SYSTEM_PREF);

                }

                Log.d("Memory", "see this  ");
                Log.d("Memory", "Internal Memory " + getSizeinMB(totalInternalMemorySize, false));
                Log.d("Memory", "Size of os " + getSizeinMB(size_of_os, false));
                Log.d("Memory", "Avaiable Memory " + getSizeinMB((long) available_internal_memory, false));
                Log.d("Memory", "see this  ");

                Log.d("Memory", "size of system apps " + getSizeinMB(size_of_system_apps, false));


                //for analytics
                mMemoryLeft = (int) Math.round((
                        available_internal_memory / (1024 * 1024 * 1024)
                ));

                if (!mAnalyticsSend) {
                    mAnalyticsSend = true;
                    new AnalyticsHandler().logGAScreenCustomDim(AnalyticsConstants.Screens.HOME,
                            AnalyticsConstants.CustomDim.STORAGE_LEFT,
                            mMemoryLeft + "");

                }


                long filled_marker = 0;
                if (prefManager.getTheme() == 0) {   //blue
                    long sessionCount = prefManager.getSessionCompressCount();
                    if (sessionCount == 0) {
                        sessionCount = 1;
                    }
                    filled_marker = total_app_size + (size_of_system_apps / sessionCount) + size_of_os;
                    //filled_marker = total_app_size + (size_of_system_apps / 5) + size_of_os;
                } else {
                    //red
                    filled_marker = total_app_size + size_of_system_apps + size_of_os;
                }
                Log.d("CompressCount", "filled marker size" + getSizeinMB(filled_marker, false));


                int progress_perc = getPercentage(total_apps, (int) (mOnlyAppSize / (1000000f)));
                if (progress_perc < 50) {
                    circularSeekBar.setCircleProgressColor(getResources().getColor(R.color.colorOrange));
                    scoretext.setText("BAD");
                    scoretext.setTextColor(getResources().getColor(R.color.colorOrange));
                } else if (progress_perc >= 50 && progress_perc <= 75) {
                    circularSeekBar.setCircleProgressColor(getResources().getColor(R.color.app_green_color));
                    scoretext.setText("GOOD");
                    scoretext.setTextColor(getResources().getColor(R.color.app_green_color));


                } else {
                    circularSeekBar.setCircleProgressColor(getResources().getColor(R.color.colorBlue));
                    scoretext.setTextColor(getResources().getColor(R.color.colorBlue));

                    scoretext.setText("EXCELLENT");

                }
                //TODO MAKE CHANGES

                PrefManager.getInstance(getApplicationContext()).putInt(PrefManager.PREVIOUS_PROGRESS_VAL, progress_perc);
                ObjectAnimator circleSeekAnim = ObjectAnimator.ofInt(circularSeekBar, "progress", previous_progress_perc, progress_perc * 10);
                circleSeekAnim.setDuration(2000);
                circleSeekAnim.setInterpolator(new DecelerateInterpolator());
                circleSeekAnim.start();

                circleSeekAnim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thread_execution = false;
                        if (circularSeekBar.getProgress() < 500) {
                            mPager.setCurrentItem(1);
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });

                percentage_app_storage.setText(((int) progress_perc) + "");
                new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.HOMESCREEN, AnalyticsConstants.Action.PERCENTAGE_STORAGE, null, String.valueOf(Math.round(progress_perc)), true, PrefManager.PERCENTAGE_HOME);
                analyticsPercentage = (long) progress_perc;
                previous_progress_perc = progress_perc;
            }
        });
    }

    int randomWithRange(int min, int max) {
        int range = (max - min) + 1;
        return (int) (Math.random() * range) + min;
    }

    private int getPercentage(int totalAppsInstalled, int mAppStorage) {
        if (mAppStorage == 0) {
            mAppStorage = 1;
        }
        if (totalAppsInstalled == 0) {
            totalAppsInstalled = 1;
        }
        String constantParamas = RemoteConfig.getString(R.string.value_param);
        int y = Integer.parseInt(constantParamas.substring(0, constantParamas.indexOf(",")));
        int ab = Integer.parseInt(constantParamas.substring(constantParamas.indexOf(",") + 1, constantParamas.lastIndexOf(",")));
        int sb = Integer.parseInt(constantParamas.substring(constantParamas.lastIndexOf(",") + 1, constantParamas.length()));

        int progress = (100 - y) * ab / totalAppsInstalled + y * sb / mAppStorage;
        if (progress < 25) {
            progress = randomWithRange(20, 25);
        }
        if (progress > 90) {
            progress = randomWithRange(90, 95);

        }
        // If reached here within half an hour then reduce the progress

        if (RemoteConfig.getBoolean(R.string.allow_progress_bar_test)) {
            if (System.currentTimeMillis() - PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.COMPRESSED_TIME) < 1800000) {
                //if previous color was above 75
                int previousRunProgress = PrefManager.getInstance(getApplicationContext()).getInt(PrefManager.PREVIOUS_PROGRESS_VAL_FINAL);
                if (previousRunProgress <= 50) {
                    progress = randomWithRange(51, 55);
                } else if (previousRunProgress <= 75) {
                    //if previous was 50-75
                    progress = randomWithRange(76, 80);
                }
            }
        }
        return progress;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("finish", "unregister finalFinishBroadCastReceiver");
        PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.SENT_FIRST_TIME_ACCESSIBILITY_EVENT, true);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(finalFinishBroadCastReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case 44:
                if (usageRunnable != null) {
                    usageRunnable.stop();
                }
                Log.d("backdebug", "I am called onActivityResult");
                try {
                    //try catch : user get back to here by closing the setting activity himself and not by timer
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    Log.d(TAG, e.getMessage(), e);
                }

                //Scanning.java started by thread don't start from here
                if (!start_scanning) {
                    start_scanning = true;
                    startActivity(new Intent(AnalysisScreen.this, Scanning.class));
                }

                break;
            case 55:
                String label = AnalyticsConstants.Label.SCANNING_SCREEN;
                if (PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.SENT_FIRST_TIME_ACCESSIBILITY_EVENT)) {
                    label = AnalyticsConstants.Label.SCANNING_SCREEN_1ST;
                    //value changes on onDestroy
                }
                new AnalyticsController(getApplicationContext()).sendAnalytics(
                        AnalyticsConstants.Category.PERMISSION,
                        AnalyticsConstants.Action.ACCESSIBILITY,
                        label,
                        String.valueOf(1),
                        false,
                        null);
            default:

        }

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        closeOptionMenu();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    void start_service() {

        if (isReadStorageAllowed()) {
            //If permission is already having then showing the toast
            //Existing the method with return
            //TODO :  SAJAL REMVOE THIS START CODE FROM HERE 29-12-2016
            Intent intentService = new Intent(this, app_Service.class);
            intentService.putExtra("no_of_folders", no_of_folders);
            this.startService(intentService);

        } else {
            //If the app has not the permission then asking for the permission
            requestStoragePermission();
        }
    }

    //We are calling this method to check the permission status
    private boolean isReadStorageAllowed() {
        Log.d("ServiceStatus", "I am here3");
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED) {
            Log.d("ServiceStatus", "I am here4");
            return true;

        }
        Log.d("ServiceStatus", "I am here5");
        //If permission is not granted returning false
        return true;
    }

    //Requesting permission
    private void requestStoragePermission() {
        Log.d("ServiceStatus", "I am here7");

//        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this, String.valueOf(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}))) {
//                //If the user has denied the permission previously your code will come to this block
//                //Here you can explain why you need this permission
//                //Explain here why you need this permission
//                Log.d("ServiceStatus", "I am here8");
//                //Toast.makeText(this, "we need to take sd card permission", Toast.LENGTH_SHORT).show();
//            }
//        }
//        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                start_service();

            } else {
                //Displaying another toast if permission is not granted
                requestStoragePermission();
            }
        }
    }

    public boolean externalMemoryAvailable() {
        return android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED);
    }

    public double getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return (double) (availableBlocks * blockSize)/* / (double) (1073741824)*/;
    }

    public long getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return (totalBlocks * blockSize);
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case TOTAL_APP_SIZE:
                Bundle data = msg.getData();
                total_app_size = (long) data.getDouble("appsize", 0);
                total_apps = data.getInt("totalapps", 0);

                Log.d("total app size", "" + total_app_size);
                Log.d("total apps", "" + total_apps);
                break;

        }
        return false;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        addBottomDots(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[2];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, 28);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    private void openAppUsageHelper() {
        Log.d("scan", "openAppUsageHelper called");
        // Analytics Start
        mGAParams = new Bundle();
        mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
        mGAParams.putString("action", AnalyticsConstants.Action.USAGE_STATS);
        mGAParams.putString("label", AnalyticsConstants.Label.POPUP);
        new AnalyticsHandler().logGAEvent(mGAParams);
        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
        // Analytics end
        AppUsagePemissionHandler.getInstance().launchIntent(getApplicationContext(), getIntent());
        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.PERMISSION_USAGE_ACCESS);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.card_view:

//                if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
//                    if (!isAccessGranted()) {
//                        Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
//                        startActivity(intent);
//                        if (!UtilityMethods.isAccessibilityServiceEnabled(getApplicationContext(), AccessibilityAutomation.class)
//                                && !PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.ACCEPTED_ACCCESSIBILITY_TERMS)) {
//                        showAccessibilityPopup();
//                        return;
//                        }
//                    }
//                }else {
//
//                    if (!UtilityMethods.isAccessibilityServiceEnabled(getApplicationContext(), AccessibilityAutomation.class)
//                            && !PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.ACCEPTED_ACCCESSIBILITY_TERMS)) {
//                        showAccessibilityPopup();
//                        return;
//                    }
//
//                }
                postConditions();


                break;


            case R.id.positive:
                Bundle paramBundle = new Bundle();
                Log.d(AnalyticsConstants.TAG, "STATUS " + "positive button clicked");
                dialogReview.setContentView(R.layout.positive_feedback_card);
                dialogReview.findViewById(R.id.sure).setOnClickListener(this);
                dialogReview.findViewById(R.id.maybe).setOnClickListener(this);
                paramBundle = new Bundle();
                paramBundle.putString(AnalyticsConstants.Params.STATUS, "positive");
                new AnalyticsHandler().logEvent(AnalyticsConstants.Event.INIT_FEEDBACK, paramBundle);
                new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.FEEDBACK_HOME);
                break;
            case R.id.negative:
                dialogReview.setContentView(R.layout.negative_feedback_layout);
                dialogReview.findViewById(R.id.send).setOnClickListener(this);
                dialogReview.findViewById(R.id.cancel).setOnClickListener(this);

                difficult = (TextView) dialogReview.findViewById(R.id.difficult);
                difficult.setOnClickListener(this);
                app_crashed = (TextView) dialogReview.findViewById(R.id.app_crashed);
                app_crashed.setOnClickListener(this);
                data_lost = (TextView) dialogReview.findViewById(R.id.data_lost);
                data_lost.setOnClickListener(this);
                slow_process = (TextView) dialogReview.findViewById(R.id.slow_process);
                slow_process.setOnClickListener(this);
                negative_feedback_editText = (EditText) dialogReview.findViewById(R.id.negative_feedback_editText);

                negative_feedback_editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            GradientDrawable drawable = (GradientDrawable) negative_feedback_editText.getBackground();
                            drawable.setStroke(2, stroke);
                        } else {
                            GradientDrawable drawable = (GradientDrawable) negative_feedback_editText.getBackground();
                            //remove border
                            drawable.setStroke(0, transparent);
                        }
                    }
                });


                Bundle param = new Bundle();
                param.putString(AnalyticsConstants.Params.STATUS, "negative");
                Log.d(AnalyticsConstants.TAG, "STATUS " + "negative button clicked");
                new AnalyticsHandler().logEvent(AnalyticsConstants.Event.INIT_FEEDBACK, param);
                new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.FEEDBACK_HOME);
                break;

            case R.id.send:

                showFeedbackRecordedPopup();

                send_button_clicked = !send_button_clicked;

                PrefManager.getInstance(AnalysisScreen.this).setReview(true);
                if (difficult_clicked || app_crashed_clicked || data_lost_clicked || slow_process_clicked || (negative_feedback_editText.getText().length() > 0)) {
                    Bundle paramBundl = new Bundle();
                    paramBundl.putString(AnalyticsConstants.Params.DIFFICULT, "" + difficult_clicked);
                    paramBundl.putString(AnalyticsConstants.Params.APP_CRASHED, "" + app_crashed_clicked);
                    paramBundl.putString(AnalyticsConstants.Params.DATA_LOST, "" + data_lost_clicked);
                    paramBundl.putString(AnalyticsConstants.Params.SLOW_PROCESS, "" + slow_process_clicked);
                    paramBundl.putString(AnalyticsConstants.Params.SEND, "" + send_button_clicked);
                    paramBundl.putString(AnalyticsConstants.Params.REVIEW, negative_feedback_editText.getText().toString());
                    new AnalyticsHandler().logEvent(AnalyticsConstants.Event.FEEDBACK, paramBundl);

                    Log.d(AnalyticsConstants.TAG, "send button clicked");
                }

                try {
                    InputMethodManager inputManager =
                            (InputMethodManager) AnalysisScreen.this.
                                    getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(
                            AnalysisScreen.this.getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                } catch (Exception e) {
                    Log.d("FinalResult", Log.getStackTraceString(e));
                }
                dialogReview.dismiss();

                break;
            case R.id.cancel:
                dialogReview.dismiss();

                break;

            case R.id.difficult:
                if (difficult_clicked) {
                    //unselected
                    difficult_clicked = false;
                    GradientDrawable drawable = (GradientDrawable) difficult.getBackground();
                    drawable.setColor(transparent); // set solid color
                } else {
                    //selected
                    difficult_clicked = true;
                    GradientDrawable drawable = (GradientDrawable) difficult.getBackground();
                    drawable.setColor(app_gray); // set solid color
                }

                break;
            case R.id.app_crashed:
                if (app_crashed_clicked) {
                    //unselected
                    app_crashed_clicked = false;
                    GradientDrawable drawable = (GradientDrawable) app_crashed.getBackground();
                    drawable.setColor(transparent); // set solid color

                } else {
                    //selected
                    app_crashed_clicked = true;
                    GradientDrawable drawable = (GradientDrawable) app_crashed.getBackground();
                    drawable.setColor(app_gray); // set solid color
                }
                break;
            case R.id.data_lost:
                if (data_lost_clicked) {
                    //unselected
                    data_lost_clicked = false;
                    GradientDrawable drawable = (GradientDrawable) data_lost.getBackground();
                    drawable.setColor(transparent); // set solid color

                } else {
                    //selected
                    data_lost_clicked = true;
                    GradientDrawable drawable = (GradientDrawable) data_lost.getBackground();
                    drawable.setColor(app_gray); // set solid color
                }
                break;
            case R.id.slow_process:
                if (slow_process_clicked) {
                    //unselected
                    slow_process_clicked = false;
                    GradientDrawable drawable = (GradientDrawable) slow_process.getBackground();
                    drawable.setColor(transparent); // set solid color

                } else {
                    //selected
                    slow_process_clicked = true;
                    GradientDrawable drawable = (GradientDrawable) slow_process.getBackground();
                    drawable.setColor(app_gray); // set solid color
                }
                break;


            case R.id.sure:

                PrefManager.getInstance(AnalysisScreen.this).setReview(true);
                Bundle paramBund = new Bundle();
                paramBund.putString(AnalyticsConstants.Params.PLAY_STORE, "true");
                new AnalyticsHandler().logEvent(AnalyticsConstants.Event.FEEDBACK, paramBund);
                Log.d(AnalyticsConstants.TAG, "PLAY_STORE true");
                dialogReview.cancel();
                launchMarket();
                break;

            case R.id.maybe:

                dialogReview.dismiss();
                paramBund = new Bundle();
                paramBund.putString(AnalyticsConstants.Params.PLAY_STORE, "false");
                new AnalyticsHandler().logEvent(AnalyticsConstants.Event.FEEDBACK, paramBund);
                Log.d(AnalyticsConstants.TAG, "PLAY_STORE false");
                break;


            case R.id.more_options:
                openManageApps();

//                int marginFromTop = (int) UtilityMethods.getInstance().convertDpToPixel(24, AnalysisScreen.this);
//                int marginFromRight = (int) UtilityMethods.getInstance().convertDpToPixel(145, AnalysisScreen.this);
//                popupWindow.showAsDropDown(more_options, -marginFromRight, -marginFromTop);
//
//                manageApps.setOnClickListener(this);
//                trashBtn.setOnClickListener(this);
//
//                if (PrefManager.getInstance(AnalysisScreen.this).getBoolean(PrefManager.whatAppCleanModule)) {
//                    trash_manage_seprator.setVisibility(View.VISIBLE);
//                    trashBtn.setVisibility(View.VISIBLE);
//                }

                break;


            case R.id.manage_apps:


                break;


        }
    }

    private void postConditions() {
        Log.d("scan", "SCANBUTTON CLICKED");
        mAppUsageRequest = true;
//        Bundle paramBundle = new Bundle();
//        paramBundle.putString(AnalyticsConstants.Params.PERCENTAGE, percentage_app_storage.getText().toString());
//        new AnalyticsHandler().logEvent(AnalyticsConstants.Event.SCAN, paramBundle);
//        if (methods.isAccessibilityServiceRunning(AnalysisScreen.this)) {
//            mAppUsageRequest = false;
//            if (!UtilityMethods.getInstance().isHavingUnkownSourcePermission(getApplicationContext()) && RemoteConfig.getBoolean(R.string.take_auto_unknown)) {
//                UnknownSourcePermission.getInstance().launchIntent(getApplicationContext(), getIntent());
//            } else {
//                if (Build.VERSION.SDK_INT <=Build.VERSION_CODES.P && !methods.isHavingUsageStatsPermission(AnalysisScreen.this)) {
//                    openAppUsageHelper();
//                } else {

                    startActivity(new Intent(AnalysisScreen.this, Scanning.class));
//                }
//            }
//
//        } else {
//            openAccessibilityHelper();
//        }


        mGAParams = new Bundle();
        mGAParams.putString("category", AnalyticsConstants.Category.SCAN);
        mGAParams.putString("action", AnalyticsConstants.Action.TOTAL_COMP);
        mGAParams.putLong("value", compressed_apps);
        if (compressed_apps > 0) {
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
        }


        if (!PrefManager.getInstance(getApplicationContext()).isAnalyticsScanDetailSend()) {
            PrefManager.getInstance(getApplicationContext()).setAnalyticsScanDetailSend(true);

            mGAParams.putString("action", AnalyticsConstants.Action.TOTAL_APPS);
            mGAParams.putLong("value", (total_apps + compressed_apps));
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

            mGAParams.putString("category", AnalyticsConstants.Category.SCAN);
            mGAParams.putString("action", AnalyticsConstants.Action.TOTAL_COMP_1ST);
            mGAParams.putLong("value", compressed_apps);
            if (compressed_apps > 0) {
                new AnalyticsHandler().logGAEvent(mGAParams);
                Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
            }


            int space = (int) (mTotalStorage / (1024 * 1024 * 1024));
            mGAParams.putInt("cd", AnalyticsConstants.CustomDim.TOTAL_STORAGE);
            switch (space) {
                case 4:
                    mGAParams.putString("cd_value", AnalyticsConstants.CustomDimValue.FOUR_GB);
                    break;
                case 8:
                    mGAParams.putString("cd_value", AnalyticsConstants.CustomDimValue.EIGHT_GB);
                    break;
                case 16:
                    mGAParams.putString("cd_value", AnalyticsConstants.CustomDimValue.SIXTEEN_GB);
                    break;
                case 32:
                    mGAParams.putString("cd_value", AnalyticsConstants.CustomDimValue.THIRTY_TWO_GB);
                    break;
                case 64:
                    mGAParams.putString("cd_value", AnalyticsConstants.CustomDimValue.SIXTY_FOUR_GB);
                    break;
            }
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
        }

        mGAParams = new Bundle();
        mGAParams.putString("category", AnalyticsConstants.Category.SCAN);
        mGAParams.putString("action", AnalyticsConstants.Action.SCAN_START);
        new AnalyticsHandler().logGAEvent(mGAParams);
        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
    }

    private void showAccessibilityPopup() {


        final Dialog dialog = new Dialog(this, R.style.Dialog1);
        dialog.setContentView(R.layout.accessibility_terms);
        final TextView cancelButton = (TextView) dialog.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFirst = !PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.HAS_SENT_ANALYTICS_ACCESSIBILITY);
                if (isFirst) {
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.NOTIFICATION);
                    mGAParams.putString("action", AnalyticsConstants.Action.ACC_GREENIFY_POPUP);
                    mGAParams.putLong("value", 0);
                    new AnalyticsHandler().logGAEvent(mGAParams);

                    PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.HAS_SENT_ANALYTICS_ACCESSIBILITY, true);
                }
                dialog.dismiss();
            }
        });
        final TextView acceptTerms = (TextView) dialog.findViewById(R.id.select_button);
        acceptTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.ACCEPTED_ACCCESSIBILITY_TERMS, true);
                postConditions();
                boolean isFirst = !PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.HAS_SENT_ANALYTICS_ACCESSIBILITY);
                if (isFirst) {
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.NOTIFICATION);
                    mGAParams.putString("action", AnalyticsConstants.Action.ACC_GREENIFY_POPUP);
                    mGAParams.putLong("value", 1);
                    new AnalyticsHandler().logGAEvent(mGAParams);


                    PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.HAS_SENT_ANALYTICS_ACCESSIBILITY, true);
                }
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();

    }

    private void openManageApps() {

        Intent intent = new Intent(AnalysisScreen.this, ManageApps.class);
        startActivity(intent);
        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.MANAGE_APPS);

    }

    private void showFeedbackRecordedPopup() {
        final Dialog dialog = new Dialog(this, R.style.Dialog1);
        dialog.setContentView(R.layout.negative_review_feedback_message);
        final TextView cancelButton = (TextView) dialog.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }


    public void openAccessibilityHelper() {


        try {
            windowManager.removeView(overlayLayout);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String label = AnalyticsConstants.Label.SCANNING_SCREEN;
        if (PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.SENT_FIRST_TIME_ACCESSIBILITY_EVENT)) {
            label = AnalyticsConstants.Label.SCANNING_SCREEN_1ST;
            //value changes on onDestroy
        }
        new AnalyticsController(getApplicationContext()).sendAnalytics(
                AnalyticsConstants.Category.PERMISSION,
                AnalyticsConstants.Action.ACCESSIBILITY,
                label,
                String.valueOf(0),
                false,
                null);
        LayoutInflater accessibilityHelper = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        overlayLayout = accessibilityHelper.inflate(R.layout.accessibility_permission, null);
        WindowManager.LayoutParams params1;
        TextView span_text_view = (TextView) overlayLayout.findViewById(R.id.span_text_view);
        TextView permission_helper_text = (TextView) overlayLayout.findViewById(R.id.permission_helper_text);
        String first = "";
        String second = "\uD83C\uDF1F SpaceUp \uD83C\uDF1F";
        final String totalString = first + second;
        Spannable spanText = new SpannableString(totalString);
        //set color to white
        spanText.setSpan(new ForegroundColorSpan(getResources()
                .getColor(R.color.spaceup_black)), first.length(), first.length() + second.length(), 0);
        //set text to bradon medium
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon_medium.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spanText.setSpan(typefaceSpan, first.length(), first.length() + second.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        span_text_view.setText(spanText, TextView.BufferType.SPANNABLE);

        String f = "Give permission to ";
        String s = "save upto 1.2 GB";
        final String t = f + s;
        Spannable spannableText = new SpannableString(t);
        //set color to white
        spannableText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(AnalysisScreen.this, R.color.text_color)), f.length(), f.length() + s.length(), 0);
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spannableText.setSpan(typefaceSpan, f.length(), f.length() + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        permission_helper_text.setText(spannableText, TextView.BufferType.SPANNABLE);

        ImageView cancel = (ImageView) overlayLayout.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    windowManager.removeView(overlayLayout);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        params1 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);
        params1.gravity = Gravity.BOTTOM;


        windowManager = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        try {
            windowManager.addView(overlayLayout, params1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        accRunnable = new AccRunnable(windowManager, overlayLayout, this);
        accCheckThread = new Thread(accRunnable, "acc");
        accCheckThread.start();


        Intent accSettingsIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        try {
            startActivityForResult(accSettingsIntent, 55);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "This phone does not support Accessibility Settings!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        AccessibilityAutomation.allowSpecialBack();
        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.PERMISSION_ACCESSIBILITY);
    }

    public void reviewDialog() {
        dialogReview = new Dialog(AnalysisScreen.this, R.style.Dialog1);
        app_gray = ContextCompat.getColor(AnalysisScreen.this, R.color.app_gray);
        stroke = ContextCompat.getColor(AnalysisScreen.this, R.color.statusbar);
        transparent = Color.TRANSPARENT;
        dialogReview.setContentView(R.layout.final_screen_rate_card);

        dialogReview.findViewById(R.id.positive).setOnClickListener(this);
        dialogReview.findViewById(R.id.negative).setOnClickListener(this);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialogReview.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        window.setAttributes(lp);
        try {

            dialogReview.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void launchMarket() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Log.d("FinalResultScreen", "play store redirection failed ");
        }
    }

    private void displayWelcomeMessage(TextView mWelcomeTextView) {
        String welcomeMessage = RemoteConfig.getString(R.string.scan_btn_text);
        mWelcomeTextView.setText(welcomeMessage);
    }

    class UsageRunnable implements Runnable {
        private volatile boolean exit = false;

        @Override
        public void run() {
            int count = 0;
            do {
                try {
                    Thread.sleep(1000);
                    //show dialog for 8 seconds only
                    if (count > 8) {
                        try {
                            wm1.removeView(oView1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    count++;
                } catch (Exception e) {
                    Log.d(TAG, Log.getStackTraceString(e));

                }
            } while (!methods.isHavingUsageStatsPermission(AnalysisScreen.this) && !exit);

            Log.d("backdebug", "usage runnable out of while");

            if (methods.isHavingUsageStatsPermission(AnalysisScreen.this)) {
                // Analytics Start
                mGAParams = new Bundle();
                mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
                mGAParams.putString("action", AnalyticsConstants.Action.USAGE_STATS);
                mGAParams.putString("label", AnalyticsConstants.Label.GIVEN);
                mGAParams.putLong("value", 1);
                new AnalyticsHandler().logGAEvent(mGAParams);
                Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                //Analytics Stop


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            wm1.removeView(oView1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (AccessibilityAutomation.getSharedInstance() != null) {
                            AccessibilityAutomation.getSharedInstance().performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);

                        }
                        Log.d("backdebug", "perform back ");
                    }
                });


                if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP) {
                    //for lollipop
                    try {
                        Thread.sleep(500);
                        Log.d("backdebug", "wait for 500 ms");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("backdebug", "perform back again");
                            finishActivity(44);
                        }
                    });

                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("backdebug", "opening scanning");
                        if (!start_scanning) {
                            start_scanning = true;
                            startActivity(new Intent(AnalysisScreen.this, Scanning.class));
                        }
                    }
                });

            }
        }

        public void stop() {
            exit = true;
        }
    }

    public void closeOptionMenu() {
        if (popupWindow != null && popupWindow.isShowing()) {
            popupWindow.dismiss();
        }
    }
}