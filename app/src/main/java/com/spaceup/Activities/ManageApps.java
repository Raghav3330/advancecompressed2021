package com.spaceup.Activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.AppShortcut.DataInterface;
import com.spaceup.AppShortcut.ItemObject;
import com.spaceup.AppShortcut.RecyclerViewAdapterManageApps;
import com.spaceup.R;
import com.spaceup.Runnable.AccRunnable;
import com.spaceup.Splash_Screen;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityCommunicator;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.app_services.app_Service;
import com.spaceup.data.version_one.AppDBHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.spaceup.AppShortcut.ShortCutFolder.MOVE_TO_MENU;
import static com.spaceup.accessibility.AccessibilityAutomation.SHOW_DO_IN_BKG_BTN_INTENT;

import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan;
import io.github.inflationx.calligraphy3.TypefaceUtils;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class ManageApps extends AppCompatActivity implements DataInterface {
    public static boolean requestTurnOn = false;
    public ArrayList<String> toCompressAppList = new ArrayList<>();
    public ArrayList<String> toCompressAppName = new ArrayList<>();
    RecyclerView rView;
    private Toolbar mToolBar;

    int divider_mid;
    List<ItemObject> rowListItem;
    RecyclerViewAdapterManageApps rcAdapter;
    Dialog permissionDialog;
    Button accessibility_permission_btn;
    Button unknown_permission_btn;
    ImageView acc_tick, unknown_tick, cancel_btn;
    View overlayLayout;
    WindowManager windowManager;
    AccRunnable accRunnable;
    UnknownRunnable unknownRunnable;
    Thread accCheckThread, unknownThread;
    View oView1;
    WindowManager wm1;
    // declaring list of uncompressed apps:- Used in sequentially uninstalling - installing apps
    BroadcastReceiver accessibilityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("com.times.convertToAdvance")) {
                if (intent.getExtras().getString("get_packagename") != null) {
                    String packageName = intent.getExtras().getString("get_packagename");
                    try {
                        AppDBHelper.getInstance(context).deleteFromShortcut(packageName);
                        Log.d("shortcutfile", "onReceive: Remove to file");

                        UtilityMethods.getInstance().removeFromJSONFile(packageName);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
    private GridLayoutManager lLayout;
    BroadcastReceiver getAccessibilityReceiver_reactivate = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SHOW_DO_IN_BKG_BTN_INTENT)) {
                Log.d("SHORTCUT_FOLDER", "RECYCLER_UPDATED");
                setupRecyclerView();
            }
            LocalBroadcastManager.getInstance(context).unregisterReceiver(getAccessibilityReceiver_reactivate);

        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("MANAGEAPPS", "ONCREATE");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_short_cut_folder);
        wm1 = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);

        mToolBar = findViewById(R.id.toolbar);
        mToolBar.setTitle("Compressed apps");
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (!UtilityMethods.isPackageExisted(getApplicationContext(), "com.stash.junkcleaner")) {
            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.STASH_REF,
                    AnalyticsConstants.Action.POPUP_MANAGEAPPS,
                    AnalyticsConstants.Label.FIRED,
                    null,
                    false,
                    null);
        }

    }

    void setupRecyclerView() {
        try {


            rowListItem = getItemFromFolder();
            rView = (RecyclerView) findViewById(R.id.recycler_view);
            if (rowListItem.size() == 4) {
                // Show no apps found popup
                rView.setVisibility(View.GONE);
                LinearLayout noAppsLayout = findViewById(R.id.no_apps_found_shortcut);
                noAppsLayout.setVisibility(View.VISIBLE);
                CardView startCompressingText = noAppsLayout.findViewById(R.id.card_start_scan);
                startCompressingText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = null;
                        if (PrefManager.getInstance(ManageApps.this).isFirstRun()) {
                            intent = new Intent(ManageApps.this, Splash_Screen.class);
                        } else {
                            intent = new Intent(ManageApps.this, Scanning.class);
                        }
                        ManageApps.this.startActivity(intent);
                        ManageApps.this.finish();
                    }
                });


            } else {
                rView.setVisibility(View.VISIBLE);

            }
            rcAdapter = new RecyclerViewAdapterManageApps(this, rowListItem, divider_mid, ManageApps.this, 0);

            lLayout = new GridLayoutManager(ManageApps.this, 4);
            lLayout.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (position == 0 || position == 1 || position == divider_mid || position == rowListItem.size() - 1) {
                        return 4;
                    } else {
                        return 1;
                    }
                }
            });
            rView.setHasFixedSize(false);
            rView.setLayoutManager(lLayout);

            rView.setAdapter(rcAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<ItemObject> getItemFromFolder() {
        toCompressAppList.clear();
        toCompressAppName.clear();

        List<ItemObject> allItems = new ArrayList<>();
        allItems.add(new ItemObject("", null, ""));
        allItems.add(new ItemObject("", null, ""));

        String path = Environment.getExternalStorageDirectory().getPath() + "/.Stash/db/";
        Log.d("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
        Log.d("Files", "Size: " + files.length);
        PackageManager pm = getPackageManager();
        for (int i = 0; i < files.length; i++) {
            if (files[i].getName().contains(".stash") && !files[i].getName().equals("spaceup.stash")) {
                Log.d("Files", "FileName:" + files[i].getName());
                Drawable icon = null;
                try {
                    String APKFilePath = Environment.getExternalStorageDirectory().getPath() + "/" + Constants.APK_DB + files[i].getName();
                    PackageInfo pi = pm.getPackageArchiveInfo(APKFilePath, 0);
                    // Logo of the app from DIR....
                    pi.applicationInfo.sourceDir = APKFilePath;
                    pi.applicationInfo.publicSourceDir = APKFilePath;
                    String appName = pi.applicationInfo.loadLabel(pm).toString();
                    String pKGName = pi.applicationInfo.packageName;
                    //
                    Drawable APKicon = pi.applicationInfo.loadIcon(pm);
                    if ( !UtilityMethods.getInstance().isPackageExisted(pKGName, getApplicationContext())) {
                        allItems.add(new ItemObject(appName, UtilityMethods.getInstance().drawableToBitmap(APKicon), pKGName));
                        toCompressAppList.add(pKGName);
                        toCompressAppName.add(appName);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        divider_mid = allItems.size();
        allItems.add(new ItemObject("", null, ""));
        for (int i = 0; i < files.length; i++) {
            if (files[i].getName().contains(".stash") && !files[i].getName().equals("spaceup.stash")) {
                Log.d("Files", "FileName:" + files[i].getName());
                Drawable icon = null;
                try {
                    String APKFilePath = Environment.getExternalStorageDirectory().getPath() + "/" + Constants.APK_DB + files[i].getName();
                    PackageInfo pi = pm.getPackageArchiveInfo(APKFilePath, 0);
                    // Logo of the app from DIR....
                    pi.applicationInfo.sourceDir = APKFilePath;
                    pi.applicationInfo.publicSourceDir = APKFilePath;
                    String appName = pi.applicationInfo.loadLabel(pm).toString();
                    String pKGName = pi.applicationInfo.packageName;
                    String version = UtilityMethods.getCurrentAppVersionName(getApplicationContext(), pKGName);


                    Drawable APKicon = pi.applicationInfo.loadIcon(pm);
                    if (Constants.DUMMY_APP_VERSION_NAME.equals(version) && UtilityMethods.getInstance().isPackageExisted(pKGName, getApplicationContext())) {
                        allItems.add(new ItemObject(appName, UtilityMethods.getInstance().drawableToBitmap(APKicon), pKGName));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        }
        allItems.add(new ItemObject("", null, ""));

        return allItems;

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MANAGEAPPS", "ONSTART");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MANAGEAPPS", "ONDESTROY");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MANAGEAPPS", "ONSTOP");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MANAGEAPPS", "ONPAUSE");
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupRecyclerView();
        Log.d("MANAGEAPPS", "ONRESUME");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("MANAGEAPPS", "ONRESTART");

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }


    @Override
    public void setValues(boolean status) {

        final Dialog dialog_unknown;
        dialog_unknown = new Dialog(ManageApps.this, R.style.Dialog1);
        dialog_unknown.setContentView(R.layout.popup_into_advance);
        try {
            TextView total = (TextView) dialog_unknown.findViewById(R.id.textView44);
            total.setText(toCompressAppList.size() + " Apps");
            ImageView icon = (ImageView) dialog_unknown.findViewById(R.id.imageView6);
            String APKFilePath = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + toCompressAppList.get(0) + ".stash";
            PackageManager pm = getPackageManager();
            PackageInfo pi = pm.getPackageArchiveInfo(APKFilePath, 0);

            // Logo of the app from DIR....
            pi.applicationInfo.sourceDir = APKFilePath;
            pi.applicationInfo.publicSourceDir = APKFilePath;
            //
            Drawable APKicon = pi.applicationInfo.loadIcon(pm);
            icon.setImageDrawable(APKicon);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ImageView later = (ImageView) dialog_unknown.findViewById(R.id.imageView7);
        later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_unknown.dismiss();
            }
        });
        TextView advance = (TextView) dialog_unknown.findViewById(R.id.textView281);
        advance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_unknown.dismiss();
                if (toCompressAppList.size() > 0) {

                    startAdvcanceProcess();
                } else {
                    Toast.makeText(getApplicationContext(), "No apps to convert!", Toast.LENGTH_LONG).show();
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog_unknown.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog_unknown.show();
    }

    public void startAdvcanceProcess() {
        int permission_counter = 0;
        Intent intentService = new Intent(this, app_Service.class);
        intentService.putExtra("no_of_folders", "6");
        intentService.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startService(intentService);

        if (!UtilityMethods.getInstance().isAccessibilityServiceRunning(ManageApps.this)) {
            permission_counter++;
        }
        if (!UtilityMethods.getInstance().isHavingUnkownSourcePermission(ManageApps.this)) {
            permission_counter++;
        }

        if (!UtilityMethods.getInstance().isAccessibilityServiceRunning(ManageApps.this) || !UtilityMethods.getInstance().isHavingUnkownSourcePermission(ManageApps.this)) {
            permissionDialog = new Dialog(ManageApps.this, R.style.Dialog);
            permissionDialog.setContentView(R.layout.bothpermission);
            accessibility_permission_btn = (Button) permissionDialog.findViewById(R.id.accessibility_permission_btn);
            unknown_permission_btn = (Button) permissionDialog.findViewById(R.id.unknown_permission_btn);
            acc_tick = (ImageView) permissionDialog.findViewById(R.id.accessibility_permission_tick);
            unknown_tick = (ImageView) permissionDialog.findViewById(R.id.unknown_permission_tick);
            cancel_btn = (ImageView) permissionDialog.findViewById(R.id.cancel);
            cancel_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    permissionDialog.cancel();
                }
            });
            setDialogBox();
            accessibility_permission_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openAccessibilityHelper();
                }
            });

            unknown_permission_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    unknown_bottom_helper();
                }
            });

            if (permission_counter == 1) {
                if (!UtilityMethods.getInstance().isAccessibilityServiceRunning(ManageApps.this)) {
                    openAccessibilityHelper();
                }
                if (!UtilityMethods.getInstance().isHavingUnkownSourcePermission(ManageApps.this)) {
                    unknown_bottom_helper();
                }
            }

            permissionDialog.setCanceledOnTouchOutside(false);

            WindowManager.LayoutParams lp = permissionDialog.getWindow().getAttributes();
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            lp.gravity = Gravity.CENTER_VERTICAL;

            permissionDialog.getWindow().setAttributes(lp);
            permissionDialog.show();
        } else if (UtilityMethods.getInstance().isAccessibilityServiceRunning(ManageApps.this) && UtilityMethods.getInstance().isHavingUnkownSourcePermission(ManageApps.this)) {

            convertAll();

        }
    }

    private void convertAll() {

        IntentFilter accessibilityReceiverIntentFilter = new IntentFilter();

        accessibilityReceiverIntentFilter.addAction("com.times.convertToAdvance");   //Broadcast Listner
        LocalBroadcastManager.getInstance(this).registerReceiver(accessibilityReceiver,
                accessibilityReceiverIntentFilter);


        MOVE_TO_MENU = 1;
        Intent intent = new Intent(ManageApps.this, AccessibilityCommunicator.class);
        intent.putStringArrayListExtra("compressList", toCompressAppList);
        intent.putStringArrayListExtra("compressName", toCompressAppName);
        intent.putExtra("state", "multiple_app");
        startService(intent);


    }

    public void setDialogBox() {
        if (UtilityMethods.getInstance().isAccessibilityServiceRunning(ManageApps.this)) {
            acc_tick.setVisibility(View.VISIBLE);
            accessibility_permission_btn.setVisibility(View.GONE);
        } else {
            acc_tick.setVisibility(View.GONE);
            accessibility_permission_btn.setVisibility(View.VISIBLE);
        }
        if (UtilityMethods.getInstance().isHavingUnkownSourcePermission(ManageApps.this)) {
            unknown_tick.setVisibility(View.VISIBLE);
            unknown_permission_btn.setVisibility(View.GONE);
        } else {
            unknown_tick.setVisibility(View.GONE);
            unknown_permission_btn.setVisibility(View.VISIBLE);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 55:

                if (null != accRunnable) {
                    Log.d("backdebug", "stopping acc runnable");
                    accRunnable.stop();
                }
                Log.d("backdebug", "inside finish 55 ");

                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {

                }
                //setup ui of dialog box
                setDialogBox();
                if (UtilityMethods.getInstance().isAccessibilityServiceRunning(ManageApps.this) && UtilityMethods.getInstance().isHavingUnkownSourcePermission(ManageApps.this)) {
                    permissionDialog.cancel();
                    // Toast.makeText(this, "Start Compression 1", Toast.LENGTH_SHORT).show();
                    //unCompressionPopup(myString,source);

                }
                break;
            case 33:
                unknownRunnable.stop();
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                setDialogBox();
                if (UtilityMethods.getInstance().isAccessibilityServiceRunning(ManageApps.this) && UtilityMethods.getInstance().isHavingUnkownSourcePermission(ManageApps.this)) {
                    permissionDialog.cancel();
                    // Toast.makeText(this, "Start Compression 2", Toast.LENGTH_SHORT).show();
                    //unCompressionPopup(myString,source);
                }
                break;
            default:
                //Toast.makeText(this, "default", Toast.LENGTH_SHORT).show();

        }
    }

    public void openAccessibilityHelper() {
        try {
            windowManager.removeView(overlayLayout);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        //startActivity(intent);
        requestTurnOn = true;

        startActivityForResult(intent, 55);

        LayoutInflater accessibilityHelper = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        overlayLayout = accessibilityHelper.inflate(R.layout.accessibility_permission, null);
        WindowManager.LayoutParams params1;
        TextView span_text_view = (TextView) overlayLayout.findViewById(R.id.span_text_view);
        TextView permission_helper_text = (TextView) overlayLayout.findViewById(R.id.permission_helper_text);
        String first = "";
        String second = "\uD83C\uDF1F SpaceUp \uD83C\uDF1F";
        final String totalString = first + second;
        Spannable spanText = new SpannableString(totalString);
        //set color to white
        spanText.setSpan(new ForegroundColorSpan(getResources()
                .getColor(R.color.spaceup_black)), first.length(), first.length() + second.length(), 0);
        //set text to bradon medium
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon_medium.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spanText.setSpan(typefaceSpan, first.length(), first.length() + second.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        span_text_view.setText(spanText, TextView.BufferType.SPANNABLE);

        String f = "Give permission to ";
        String s = "save upto 1.2 GB";
        final String t = f + s;
        Spannable spannableText = new SpannableString(t);
        //set color to white
        spannableText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ManageApps.this, R.color.text_color)), f.length(), f.length() + s.length(), 0);
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spannableText.setSpan(typefaceSpan, f.length(), f.length() + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        permission_helper_text.setText(spannableText, TextView.BufferType.SPANNABLE);

        ImageView cancel = (ImageView) overlayLayout.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    windowManager.removeView(overlayLayout);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        params1 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);
        //params1.gravity = Gravity.TOP;
        params1.gravity = Gravity.BOTTOM;
        params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;


        windowManager = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        try {
            windowManager.addView(overlayLayout, params1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        accRunnable = new AccRunnable(windowManager, overlayLayout, this);

        accCheckThread = new Thread(accRunnable, "acc");
        accCheckThread.start();

        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.PERMISSION_ACCESSIBILITY);
    }

    private void unknown_bottom_helper() {
        Intent unknownSource = new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS);
        startActivityForResult(unknownSource, 33);
        LayoutInflater accessibilityHelper = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        oView1 = accessibilityHelper.inflate(R.layout.unknown_permission, null);
        WindowManager.LayoutParams params1;

        TextView permission_helper_text = (TextView) oView1.findViewById(R.id.permission_helper_text);
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon_medium.ttf"));
        CalligraphyTypefaceSpan calligraphyRegular = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon.ttf"));
        String f = "Give permission for ";
        String s = "upto 90% compression";
        final String t = f + s;
        Spannable spannableText = new SpannableString(t);
        //set color to white
        spannableText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ManageApps.this, R.color.text_color)), f.length(), f.length() + s.length(), 0);
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spannableText.setSpan(calligraphyRegular, 0, f.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableText.setSpan(typefaceSpan, f.length(), f.length() + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        permission_helper_text.setText(spannableText, TextView.BufferType.SPANNABLE);


        ImageView cancel = (ImageView) oView1.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        params1 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);

        params1.gravity = Gravity.TOP;
        params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        try {
            wm1.addView(oView1, params1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        unknownRunnable = new ManageApps.UnknownRunnable();
        unknownThread = new Thread(unknownRunnable, "unknown");
        unknownThread.start();
        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.PERMISSION_UNKNOWN_SOURCE);
    }

   /* class AccRunnable implements Runnable {
        private volatile boolean exit = false;

        @Override
        public void run() {
            int count = 0;
            do {
                try {
                    Thread.sleep(1000);
                    //show dialog for 8 seconds only
                    if (count > 8) {
                        try {
                            windowManager.removeView(overlayLayout);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    count++;
                } catch (Exception e) {

                }
            }
            while (!UtilityMethods.getInstance().isAccessibilityServiceRunning(ManageApps.this) && !exit);

            if (UtilityMethods.getInstance().isAccessibilityServiceRunning(ManageApps.this)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //inform onActivityResult if called not to start scanning
                        //start_scanning = true;

                        try {
                            windowManager.removeView(overlayLayout);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //onActivityResult(55,0,null);
                        finishActivity(55);
                        Log.d("close", "I am in thread 55");
                    }
                });

            }
        }//end of run methods

        public void stop() {
            Log.d("TAG", "stop: thread");
            exit = true;
        }
    }*/

    class UnknownRunnable implements Runnable {
        int count = 0;
        private volatile boolean exit = false;

        @Override
        public void run() {


            do {
                try {
                    Thread.sleep(1000);
                    //show dialog for 8 seconds only
                    if (count > 8) {
                        try {
                            windowManager.removeView(overlayLayout);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    count++;
                } catch (Exception e) {

                }
            }
            while (!UtilityMethods.getInstance().isHavingUnkownSourcePermission(ManageApps.this) && !exit);


            if (UtilityMethods.getInstance().isHavingUnkownSourcePermission(ManageApps.this)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //inform onActivityResult if called not to start scanning
                        //start_scanning = true;

                        try {
                            windowManager.removeView(overlayLayout);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //onActivityResult(55,0,null);
                        finishActivity(33);
                    }
                });
            }

        }

        public void stop() {
            Log.d("TAG", "stop: thread");
            exit = true;
        }


    }

}