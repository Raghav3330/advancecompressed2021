package com.spaceup.Analytics;

/**
 * Created by Times on 31-03-2017.
 */

public class ImagesAnalyticsConstants {


    public static final String GATAG = "imagega";

    public static class Category {
        public final static String IMAGES_FIRST = "wa_images_first";
        public final static String DELETE_CONFIRM = "wa_delete_confirm";
        public final static String MOVE_TO_TRASH = "wa_move_to_trash";
        public final static String RESULT = "wa_result";
        public final static String TRASH = "wa_trash";
        public final static String INTERNET_STATUS = "internet_status";
    }

    public static class Action {

        //WA_ACCESS
        public final static String INAPP = "inapp";
        public final static String POPUP = "popup";
        public final static String NOTIFICATION = "notification";

        //WA_IMAGES_FIRST & DELETE_CONFIRM
        public final static String TOTAL = "total";
        public final static String JUNK = "junk";
        public final static String OTHER_JUNK = "other_junk";
        public final static String IGNORED_IMG = "ignored_img";

        //MOVE_TO_TRASH
        public final static String REVIEW = "review";
        public final static String CONFIRM = "confirm";

        //RESLUT
        public final static String TRASH = "trash";
        public final static String RATE = "rate";
        public final static String COM_APPS = "com_apps";
        public final static String SHARE = "share";

        //TRASH
        public final static String EMPTY = "empty"; //check
        public final static String RESTORE = "restore";
        public final static String DELETE = "delete";

        //INTERNET_STATUS
        public final static String SCAN_WIFI = "scan_wifi";
        public final static String TWO_G = "2g";
        public final static String THREE_G = "3g";
        public final static String FOUR_G = "4g";

    }


    public static class Label {
        public final static String SELECTED = "selected";
        public final static String UNSELECTED = "unselected";

        //TRASH
        public final static String EMPTY = "empty";
        public final static String RESTORE = "restore";
        public final static String DELETE = "delete";

    }

    public static class CustomDim {
        public final static int INTERNET_TYPE = 10;
        public final static int WA_COUNT = 11;
        public final static int WA_POPUP = 12;
        public static final int WA_RESULT = 13;
    }

    public static class CustomDimVal {
        //INTERNET_TYPE
        public final static String TWO_G = "2g";
        public final static String THREE_G = "3g";
        public final static String FOUR_G = "4g";
        public final static String WIFI = "wifi";
    }

    public static class Screens {
        public final static String WA_SCANNING = "wa_scanning";
        public final static String WA_REVIEW = "wa_review";
        public final static String WA_DELETING = "wa_deleting";
        public final static String WA_RESULT = "wa_result";
        public final static String WA_TRASH = "wa_trash";
        public final static String WA_POPUP = "wa_popup";
    }
}