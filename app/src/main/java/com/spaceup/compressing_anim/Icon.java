package com.spaceup.compressing_anim;

import android.graphics.drawable.Drawable;

/**
 * Created by dhurv on 11-12-2016.
 */

public class Icon {
    private Drawable icon;

    public Icon() {
    }

    public Icon(Drawable icon) {
        this.icon = icon;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }


}