package com.spaceup.Alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.spaceup.Utility.PrefManager;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.notifications.DailyAnalyticsService;
import com.spaceup.notifications.NotificationService;
import com.spaceup.notifications.NotificationService_4days;
import com.spaceup.notifications.NotificationService_7PM;
import com.spaceup.notifications.StashInstalledCheckService;
import com.spaceup.services.DayCountService;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by dhurv on 23-01-2017.
 */

public class AlarmService_Controller {
    Context context;

    public AlarmService_Controller(Context context) {
        this.context = context;
    }

    public void AlarmDaily12PM() {

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, DailyAnalyticsService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, 120, intent, 0);
        Log.d("ALARM_RECEIVER2PM", "0PM NOTIFICATION REGISTER");
        // reset previous pending intent
        alarmManager.cancel(pendingIntent);

        // Set the alarm to start at approximately 07:00 PM.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        // if the scheduler date is passed, move scheduler time to tomorrow
        if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    public void AlarmDaily6PM() {

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, StashInstalledCheckService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, 121, intent, 0);
        // reset previous pending intent
        alarmManager.cancel(pendingIntent);

        // Set the alarm to start at approximately 07:00 PM.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        calendar.set(Calendar.MINUTE, 0);

        // if the scheduler date is passed, move scheduler time to tomorrow
        if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    public void Alarmat2PM() {

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, NotificationService_7PM.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);
        Log.d("ALARM_RECEIVER2PM", "2PM NOTIFICATION REGISTER");
        // reset previous pending intent
        alarmManager.cancel(pendingIntent);

        // Set the alarm to start at approximately 09:00 PM.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 21);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        // if the scheduler date is passed, move scheduler time to tomorrow
        if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    public void fiveMinAppNotCompressed() {
        /***
         *
         * Show notification if user has not compressed any app till 5 minutes
         *
         */
        Log.d("ALARM_RECEIVER", "STARTED_ALARM_INIT");
        Calendar cur_cal = Calendar.getInstance();
        cur_cal.setTimeInMillis(System.currentTimeMillis());
        Intent intent1 = new Intent(context, NotificationService.class);
        intent1.putExtra(Constants.NOTIFICATION_TYPE, Constants.NOT_COMPRESS_WITHIN_FIVE_NOTIFICATION);
        PendingIntent pintent = PendingIntent.getService(context, 0, intent1, 0);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cur_cal.getTimeInMillis(), 5 * 60 * 1000, pintent);

    }

    public void every4days() {
        /***
         *
         * Show notification if user has not compressed any app till 5 minutes
         *
         *
         */

        Log.d("ALARM_RECEIVER_4DAY", "STARTED_ALARM_INIT");
        Calendar cur_cal = Calendar.getInstance();
        cur_cal.setTimeInMillis(System.currentTimeMillis());
        Intent intent = new Intent(context, NotificationService_4days.class);
        PendingIntent pintent = PendingIntent.getService(context, 0, intent, 0);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cur_cal.getTimeInMillis(), 3 * 24 * 60 * 60 * 1000, pintent);

    }

    public void forceRemoveNotification() {

        /*
         *
         * Remove uncompression notification after 2 minutes of "run in background" button press
         *
         */
        Log.d("canceluncompress", "Alarm Service registered for removing notification");
        Calendar cur_cal = Calendar.getInstance();
        cur_cal.setTimeInMillis(System.currentTimeMillis());
        Intent intent = new Intent(context, NotificationService.class);
        intent.putExtra(Constants.NOTIFICATION_TYPE, Constants.REMOVE_UNCOMPRESS_NOTIFICATION);
        PendingIntent pendingIntent = PendingIntent.getService(context, Constants.UNCOMPRESSION_NOTIFICATION_ID, intent, 0);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.set(AlarmManager.RTC_WAKEUP, cur_cal.getTimeInMillis() + (3 * 60 * 1000), pendingIntent);

    }



    public void startDayCountAlarm() {
        Log.d("AlarmController", "Started Alarm For DayCount Service");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        Intent intent = new Intent(context, DayCountService.class);
        PendingIntent pIntent = PendingIntent.getService(context, 0, intent, 0);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pIntent);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pIntent);
    }
}
