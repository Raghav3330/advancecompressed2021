# SpaceUp Coding Guidelines

* ##### Activity’s name:
    `Eg.“SplashActivity”`  
* ##### Service’s name:
    `Eg. “AppService”` 
* ##### Application Components: 
    `Eg. BroadcastReceiver, ContentProvider, Fragments, Adapters, Views, ViewHolders etc` 
* ##### Private Member Variable’s:
   `Eg. “mCount”` should start with `‘m’` 
* ##### Static Variables:
    `Eg. “sInstance”` names should start with `‘s’` 
* ##### Constants:
    `Eg. “KEY_APP_FIRST_RUN”`  should be named using `Upper case letters` and `‘_’` 
* ##### Method Parameters:
    `Eg. int add(int pNumOne, intpNumTwo) { return pNumOne + pNumTwo; }`    name starting with ‘p’ 

*  ##### Class names: 
    `Eg. “LandingPage”` is correct class name according to guidelines `not Landing_Page`

* ##### Packages Name: 
    `Eg. “com.spaceup.cache.logcleaner.cacheclean”` is correct, `use only lower case letters` and `‘.’`

* ##### Packaging of `classes` should be `based on functionality` 
    `Eg.“ui.activity”,”ui.fragment”,”service”,”receiver”,“network”,”dbmanager”,”view”,”model”,”util” etc.`

* ##### Variables, Constants, Class and Method names `should be self explanatory.`


* ##### Always use `correct Access Modifier (private or public or protected or default)` depending on the need while declaring variables and methods.


* ##### Define the String literals as `constants` and always use that constant instead of creating the string literal every time:
    ```sh 
    Saves heap space as well as it is easily maintainable. Eg. “Google Fit” should be declared as 'public static final String GOOGLE_FIT = “Google Fit”'
    ```


* ##### Do not keep commented out codes, if it’s of no use. Instead just use files from git history.


* ##### `Do not use empty Catch Blocks`. Either log the exception or re-throw it.


* ##### `Do not keep unused variables, methods and classes`


* ##### `Do not declare unnecessary permissions in Manifest file`, it could have bad effect on App rating. 
*       Also if you add any extra permission in later version of application then the application will not be updated automatically by Google Play Store.


* ##### `Do not declare unnecessary Application Components eg. Services, Receivers in Manifest File.`


* ##### Use `“Ctrl + Shift + F”` to format your code, Use `“Ctrl + Shift +O”`

---
## Also keep these things in mind:

* #### Memory Leaks:
  - 'Avoid passing references of any object', which may prevent the object for getting garbage collected. 
  -        Try to use WeakReference wherever possible. 
  -        If you need to accesscontext from any worker thread then use singleton ApplicationContext.

* #### Resource Leaks: 
  - 		 Always close the resources after use like input/output streams, cursors etc. 
  -        Always unregister broadcast receivers when they are no more needed.
  -        Use Local Broadcast Manager for broadcasts specific to your application.

* #### Battery/Resource Optimization: 
  - Do not keep unnecessary services running, it affects CPU/battery performance. 
  -        New thread creation is costlier, use ExecutorService if you need to create many threads.

* #### Layout Optimization: 
  -       A deep nested View hierarchy takes more time in drawing/measuring, try flattening the view hierarchy using RelativeLayout or FrameLayout, wherever possible. 
  -       Also if you keep nesting RelativeLayout inside RelativeLayout the drawing/measuring time will degrade. Try using &lt;merge&gt; tag ifyou need to include a layout. 
  -       Remove unnecessary parents/layouts/background resources.

---
# Commit message guidelines
### Push on master branch : 
```Check Analytics and Crash-Analytics:``` `ON`
VERSION_NAME : 1.0
VERSION_CODE : 4
```sh
Release 1.0 VC 4
```
### Push on dev branch: 
`Check Analytics and Crash-Analytics:` `OFF `
VERSION_NAME : 1.0
VERSION_CODE : 4
```sh
Debug Release 1.0 VC 4
```

### Building for release
WORKS/CHECKS to do BEFORE EVERY LAUNCH VERSION:
* ##### Increase Version Code


* ##### Analytics Dependencies exists


* ##### analyticshandler.java:
    ```sh
    private static boolean sDisableAnalytics = BuildConfig.DEBUG; // Set true to turn off the Analytics
    ```
* ##### Crosscheck `ProGaurd` is TRUE


* ##### `minSDK == 19 & targetSDK == 22`


* ##### `Only 2 Launcher` (App Icon + 1 Tap Comp. Apps launcher)


* ##### 2 Star in Accessibility Label in Manifest
