package com.spaceup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.provider.Settings;
import androidx.core.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.spaceup.Alarm.AlarmService_Controller;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.Runnable.AccRunnable;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityCommunicator;
import com.spaceup.accessibility.AccessibilityCommunicator_ShortCut;
import com.spaceup.app_services.app_Service;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.uninstall.activities.AppInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan;
import io.github.inflationx.calligraphy3.TypefaceUtils;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class OuterUninstallerLayout extends Activity {

    private static final String LOG_TAG = OuterUninstallerLayout.class.getSimpleName();

    public static boolean requestTurnOn = false;
    public List<AppInfo> appList;
    ArrayList<String> compressList, compressListSize, toCompressList, toCompressAppName;
    CheckBox ap1, ap2, ap3;
    TextView apT1, apT2, apT3;
    TextView apST1, apST2, apST3;
    long total_Size = 0;
    String TAG = getClass().getName();
    TextView compress;
    View bar11;
    View bar22;
    View bar33;
    UtilityMethods methods;
    View oView1;
    WindowManager wm1;
    AccRunnable accRunnable;
    Thread accCheckThread;
    long totalSize = 0;
    private Bundle mGAParams;
    JSONObject mApsalarTracking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stash_outside_layout);
        Intent intentService = new Intent(this, app_Service.class);
        intentService.putExtra("no_of_folders", "6");
        intentService.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startService(intentService);
        if (getIntent().getExtras().getString("title") != null && getIntent().getExtras().getString("title").equals("UPPER_NOTIFICATION")) {
            Log.d("LOG", "NOTIFICATION_TOP");

            mGAParams = new Bundle();
            mGAParams.putString("category", AnalyticsConstants.Category.NOTIFICATION);
            mGAParams.putString("action", AnalyticsConstants.Action.TOP_NOTIFICATION);
            mGAParams.putString("label", AnalyticsConstants.Label.CLICKED);
            mGAParams.putLong("value", 1);
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
            // Analytics end

            //Apsalar Analytics START
            mApsalarTracking = new JSONObject();
            try {
                mApsalarTracking.put(AnalyticsConstants.Action.TOP_NOTIFICATION, 1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(AnalyticsConstants.APSALARTAG, "NOTIFICATION: " + mApsalarTracking.toString());
            new AnalyticsHandler().logApsalarJsonEvent(AnalyticsConstants.Key.NOTIFICATION, mApsalarTracking);
            //Apsalar Analytics END


        } else {
            Log.d("LOG", "NOTIFICATION_BOTTOM");

            mGAParams = new Bundle();
            mGAParams.putString("category", AnalyticsConstants.Category.NOTIFICATION);
            mGAParams.putString("action", AnalyticsConstants.Action.BOTTOM_NOTIFICATION);
            mGAParams.putString("label", AnalyticsConstants.Label.CLICKED);
            mGAParams.putLong("value", 1);
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
// Analytics end

            //Apsalar Analytics START
            mApsalarTracking = new JSONObject();
            try {
                mApsalarTracking.put(AnalyticsConstants.Action.BOTTOM_NOTIFICATION, 1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(AnalyticsConstants.APSALARTAG, "NOTIFICATION: " + mApsalarTracking.toString());
            new AnalyticsHandler().logApsalarJsonEvent(AnalyticsConstants.Key.NOTIFICATION, mApsalarTracking);
            //Apsalar Analytics END

        }

        methods = UtilityMethods.getInstance();

        ImageView close = (ImageView) findViewById(R.id.cross);
        compress = (TextView) findViewById(R.id.do_not_uninstall);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        compressList = new ArrayList<>();
        compressListSize = new ArrayList<>();
        appList = AppDBHelper.getInstance(getApplicationContext()).getTop5Apps();
        if (UtilityMethods.isEmptyCollection(appList) || appList.size() < 3) {
            finish();
            return;
        }
        for (int i = 0; i < appList.size(); i++) {
            Log.d("POP_WASTING", appList.get(i).getAppName());
            compressList.add(appList.get(i).getPkgName());
            compressListSize.add("" + (appList.get(i).getApkSize() + appList.get(i).getCodeSize() + appList.get(i).getSize() + appList.get(i).getCacheSize()));
            totalSize = totalSize + (appList.get(i).getApkSize() + appList.get(i).getCodeSize() + appList.get(i).getSize() + appList.get(i).getCacheSize());
        }

        toCompressList = new ArrayList<>();
        toCompressAppName = new ArrayList<>();
        bar11 = findViewById(R.id.bar1);
        bar22 = findViewById(R.id.bar2);
        bar33 = findViewById(R.id.bar3);


        ImageView img1 = (ImageView) bar11.findViewById(R.id.icon);
        ap1 = (CheckBox) bar11.findViewById(R.id.check_box);
        apT1 = (TextView) bar11.findViewById(R.id.app_name);
        apST1 = (TextView) bar11.findViewById(R.id.size);

        try {
            Drawable iconThree = getPackageManager().getApplicationIcon(compressList.get(0));
            img1.setImageDrawable(iconThree);
            apT1.setText(AppDBHelper.getInstance(getApplicationContext()).getAppName(compressList.get(0)));
            apST1.setText(UtilityMethods.getSizeinMBZeroDecimal(Long.parseLong(compressListSize.get(0)), true));
            total_Size = total_Size + Long.parseLong(compressListSize.get(0));
        } catch (PackageManager.NameNotFoundException | IndexOutOfBoundsException | NumberFormatException e) {
            Log.d(LOG_TAG, e.getMessage(), e);
        }


        ap2 = (CheckBox) bar22.findViewById(R.id.check_box);
        apT2 = (TextView) bar22.findViewById(R.id.app_name);
        apST2 = (TextView) bar22.findViewById(R.id.size);

        ImageView img2 = (ImageView) bar22.findViewById(R.id.icon);
        try {
            Drawable iconThree = getPackageManager().getApplicationIcon(compressList.get(1));
            img2.setImageDrawable(iconThree);
            apT2.setText(AppDBHelper.getInstance(getApplicationContext()).getAppName(compressList.get(1)));
            apST2.setText(UtilityMethods.getSizeinMBZeroDecimal(Long.parseLong(compressListSize.get(1)), true));
            total_Size = total_Size + Long.parseLong(compressListSize.get(1));

        } catch (PackageManager.NameNotFoundException | IndexOutOfBoundsException | NumberFormatException e) {
            Log.d(LOG_TAG, e.getMessage(), e);
        }


        ImageView img3 = (ImageView) bar33.findViewById(R.id.icon);
        ap3 = (CheckBox) bar33.findViewById(R.id.check_box);
        apT3 = (TextView) bar33.findViewById(R.id.app_name);
        apST3 = (TextView) bar33.findViewById(R.id.size);

        try {
            Drawable iconThree = getPackageManager().getApplicationIcon(compressList.get(2));
            img3.setImageDrawable(iconThree);
            apT3.setText(AppDBHelper.getInstance(getApplicationContext()).getAppName(compressList.get(2)));
            apST3.setText(UtilityMethods.getSizeinMBZeroDecimal(Long.parseLong(compressListSize.get(2)), true));
            total_Size = total_Size + Long.parseLong(compressListSize.get(2));

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        compress.setText("Compress to Save " + UtilityMethods.getSizeinMBZeroDecimal(total_Size, true));


        compress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (UtilityMethods.isAppUpdateGoingOn()) {
                    Toast.makeText(OuterUninstallerLayout.this, OuterUninstallerLayout.this.getString(R.string.app_updating_msg), Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                }

                AlarmService_Controller alarm = new AlarmService_Controller(getApplicationContext());
                Log.d("recommend", "register alarm");
                if (ap1.isChecked()) {
                    toCompressList.add(compressList.get(0));
                    toCompressAppName.add(AppDBHelper.getInstance(getApplicationContext()).getAppName(compressList.get(0)));

                }
                if (ap2.isChecked()) {
                    toCompressList.add(compressList.get(1));
                    toCompressAppName.add(AppDBHelper.getInstance(getApplicationContext()).getAppName(compressList.get(1)));
                }
                if (ap3.isChecked()) {
                    toCompressList.add(compressList.get(2));
                    toCompressAppName.add(AppDBHelper.getInstance(getApplicationContext()).getAppName(compressList.get(2)));

                }
                if (toCompressList.size() > 0) {


                    if (!UtilityMethods.getInstance().isAccessibilityServiceRunning1(getApplicationContext())) {
                        openAccessibilityHelper();

                    } else {
                        if (UtilityMethods.getInstance().isHavingUnkownSourcePermission(getApplication())) {
                            compressApps();
                        } else {
                            compressAppsQuick();
                        }
                    }
                }


            }
        });
        ap1.setChecked(true);
        ap2.setChecked(true);
        ap3.setChecked(true);
        ap1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    total_Size = total_Size + Long.parseLong(compressListSize.get(0));
                } else {
                    total_Size = total_Size - Long.parseLong(compressListSize.get(0));
                }
                updateButton(total_Size);
            }
        });
        ap2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    total_Size = total_Size + Long.parseLong(compressListSize.get(1));
                } else {
                    total_Size = total_Size - Long.parseLong(compressListSize.get(1));
                }
                updateButton(total_Size);

            }
        });
        ap3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    total_Size = total_Size + Long.parseLong(compressListSize.get(2));
                } else {
                    total_Size = total_Size - Long.parseLong(compressListSize.get(2));
                }
                updateButton(total_Size);

            }
        });
        RelativeLayout outer_bar1 = (RelativeLayout) bar11;/*(RelativeLayout) bar11.findViewById(R.id.row);*/

        outer_bar1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ap1.toggle();
            }
        });
        RelativeLayout outer_bar2 = (RelativeLayout) bar22;
        outer_bar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ap2.toggle();
            }
        });
        RelativeLayout outer_bar3 = (RelativeLayout) bar33;
        outer_bar3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ap3.toggle();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!UtilityMethods.getInstance().isAccessibilityServiceRunning1(getApplicationContext())) {
            // Analytics Start
            mGAParams = new Bundle();
            mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
            mGAParams.putString("action", AnalyticsConstants.Action.ACCESSIBILITY);

            mGAParams.putString("label", AnalyticsConstants.Label.BOTTOM_NOTIFICATION);
            mGAParams.putLong("value", 0);
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
        }
    }

    public void openAccessibilityHelper() {
        try {
            wm1.removeView(oView1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        //startActivity(intent);
        requestTurnOn = true;

        startActivityForResult(intent, 55);

        LayoutInflater accessibilityHelper = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        oView1 = accessibilityHelper.inflate(R.layout.accessibility_permission, null);
        WindowManager.LayoutParams params1;
        TextView span_text_view = (TextView) oView1.findViewById(R.id.span_text_view);
        TextView permission_helper_text = (TextView) oView1.findViewById(R.id.permission_helper_text);
        String first = "";
        String second = "\uD83C\uDF1F SpaceUp \uD83C\uDF1F";
        final String totalString = first + second;
        Spannable spanText = new SpannableString(totalString);
        //set color to white
        spanText.setSpan(new ForegroundColorSpan(getResources()
                .getColor(R.color.spaceup_black)), first.length(), first.length() + second.length(), 0);
        //set text to bradon medium
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon_medium.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spanText.setSpan(typefaceSpan, first.length(), first.length() + second.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        span_text_view.setText(spanText, TextView.BufferType.SPANNABLE);

        String f = "Give permission to ";
        String s = "save upto 1.2 GB";
        final String t = f + s;
        Spannable spannableText = new SpannableString(t);
        //set color to white
        spannableText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(OuterUninstallerLayout.this, R.color.text_color)), f.length(), f.length() + s.length(), 0);
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spannableText.setSpan(typefaceSpan, f.length(), f.length() + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        permission_helper_text.setText(spannableText, TextView.BufferType.SPANNABLE);

        ImageView cancel = (ImageView) oView1.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        params1 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_TOAST,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);
        //params1.gravity = Gravity.TOP;
        params1.gravity = Gravity.BOTTOM;
        params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;


        wm1 = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        try {
            wm1.addView(oView1, params1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        accRunnable = new AccRunnable(wm1, oView1, this);

        accCheckThread = new Thread(accRunnable, "acc");
        accCheckThread.start();
        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.PERMISSION_ACCESSIBILITY);

    }

    private void compressApps() {
        Log.d("TAG", "Performing FULL");
        finish();
        Intent intent = new Intent(OuterUninstallerLayout.this, AccessibilityCommunicator.class);
        intent.putStringArrayListExtra("compressList", toCompressList);
        intent.putStringArrayListExtra("compressName", toCompressAppName);
        intent.putExtra("state", "multi_outer");
        startService(intent);

    }

    private void compressAppsQuick() {
        Log.d("TAG", "Performing QUICK");
        finish();
        Intent intent = new Intent(OuterUninstallerLayout.this, AccessibilityCommunicator_ShortCut.class);
        intent.putStringArrayListExtra("compressList", toCompressList);
        intent.putStringArrayListExtra("compressName", toCompressAppName);
        intent.putExtra("state", "outer_shortcut");
        startService(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 55:
                if (null != accRunnable) {
                    Log.d("backdebug", "stopping acc runnable");
                    accRunnable.stop();
                }
                Log.d("backdebug", "inside finish 55 ");
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (UtilityMethods.getInstance().isAccessibilityServiceRunning1(getApplicationContext())) {

                    if (UtilityMethods.getInstance().isHavingUnkownSourcePermission(getApplication())) {
                        compressApps();
                    } else {
                        compressAppsQuick();
                    }

                }
        }
    }

    private void updateButton(long total_size) {
        compress.setText("Compress to Save " + UtilityMethods.getSizeinMBZeroDecimal(total_Size, true));

        if (total_size == 0) {

            GradientDrawable bgShape = (GradientDrawable) compress.getBackground();
            bgShape.setColor(this.getResources().getColor(R.color.deactivate_btn));


        } else {

            GradientDrawable bgShape = (GradientDrawable) compress.getBackground();
            bgShape.setColor(this.getResources().getColor(R.color.activate_btn));
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
