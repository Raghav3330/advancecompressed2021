package com.spaceup.Animations;


import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.skyfishjy.library.RippleBackground;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.R;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityAutomation;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by dhurv on 17-01-2017.
 */
public class SingleAppCompressionAnim {
    View compress_view;
    TextView textView_ticker, textView_ticker_char, app_nameET;
    Context context;
    String app_name, packageName;
    long initial_val, final_val, prev_size;
    Drawable icon;
    CountDownTimer countDownTimer;
    ImageView inner, outer;
    private String TAG = getClass().getName(), uncompressAppName;
    private WindowManager windowManager_compression;
    private AccessibilityAutomation sSharedInstance;
    private ProgressBar decompress_progress;
    private Animation clockwise = null, anticlockwise = null;

    public SingleAppCompressionAnim(String Appname, String Packagename, long prev_size, AccessibilityAutomation sSharedInstance, Context context) {
        this.sSharedInstance = sSharedInstance;
        this.app_name = Appname;
        this.packageName = Packagename;
        this.context = context;
        this.prev_size = prev_size;
    }

    public void overlay_singleApp() {
        //start Timer

        Log.d(TAG, "COMPRESSING_OVERLAY");


        if (windowManager_compression == null) {


            overlaySingleAppUI();
            UtilityMethods.getInstance().handleSound(true, context);   // put to mute call
            // TextView counter = (TextView) compress_view.findViewById(R.id.textView22);
            WindowManager.LayoutParams params;

            try {
                params = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                        PixelFormat.TRANSLUCENT);
                params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                windowManager_compression = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
                try {
                    windowManager_compression.addView(compress_view, params);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception j) {
                j.printStackTrace();
                params = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.TYPE_PHONE,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT);
                params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                windowManager_compression = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
                try {
                    windowManager_compression.addView(compress_view, params);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //  Toast.makeText(getApplicationContext(),"CATCH",Toast.LENGTH_SHORT).show();
            }

            Log.d("Accessibility", "start_baby_stash_creation thrown");
            /*Intent intent = new Intent("com.times.accessibilityReceiver");
            intent.putExtra("start_baby_stash_creation", true);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

            if (throw_intent) {*/
            Log.d("Accessibility", "start_baby_stash_creation thrown");
            Intent intent = new Intent("com.times.accessibilityReceiver");
            intent.putExtra("icon_edit_finished", true);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            ///}
        }


    }

    void overlaySingleAppUI() {
        startSingleAppTimer();

        final ImageView app_icon;
        LayoutInflater inflater = (LayoutInflater) sSharedInstance.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        compress_view = inflater.inflate(R.layout.single_compression_overlay_after, null);
        app_icon = (ImageView) compress_view.findViewById(R.id.single_app_image);
        decompress_progress = (ProgressBar) compress_view.findViewById(R.id.final_tick_background);
        /*
                percentage_decompression = (TextView) compress_view.findViewById(R.id.percentage_decompression);
        */

        // start ticker
        textView_ticker = (TextView) compress_view.findViewById(R.id.compressed_number);
        textView_ticker_char = (TextView) compress_view.findViewById(R.id.compressed_unit);
        final_val = 1024 * 1024;
        initial_val = prev_size;
        int val = 1 + (int) (Math.random() * 5);

        textView_ticker.setText("" + val);
        textView_ticker_char.setText("MB");

        app_nameET = (TextView) compress_view.findViewById(R.id.app_name);
        RippleBackground rp = (RippleBackground) compress_view.findViewById(R.id.content);
        rp.startRippleAnimation();
        app_nameET.setText("Compressing " + app_name);

        //TODO MAKE CHANGES HERE
        /*app_sizeET.setText("40 MB");*/
        try {
            icon = context.getPackageManager().getApplicationIcon(packageName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        app_icon.setImageDrawable(icon);
        ObjectAnimator anim = ObjectAnimator.ofInt(decompress_progress, "progress", 0, 1000);
        anim.setDuration(1200);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();

        //Color to white animation
        final ColorMatrix matrix = new ColorMatrix();
        ValueAnimator animation_greyscale = ValueAnimator.ofFloat(0f, 1f);
        animation_greyscale.setStartDelay(500);
        animation_greyscale.setDuration(7000);

        //   animation.setInterpolator();
        animation_greyscale.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                matrix.setSaturation(animation.getAnimatedFraction());
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                app_icon.setColorFilter(filter);
            }
        });
        animation_greyscale.reverse();
        //icon scale down animation

        LinearLayout app_icon_white = (LinearLayout) compress_view.findViewById(R.id.disc);
        Animation animationScaleUp = null;
        animationScaleUp = AnimationUtils.loadAnimation(context.getApplicationContext(), R.anim.scale_down);
        animationScaleUp.setStartOffset(500);
        AnimationSet growShrink = new AnimationSet(true);
        growShrink.addAnimation(animationScaleUp);
        growShrink.setFillAfter(true);
        app_icon_white.startAnimation(growShrink);
        inner = (ImageView) compress_view.findViewById(R.id.stars_inner);
        outer = (ImageView) compress_view.findViewById(R.id.stars_outer);

        Animation clockwise = AnimationUtils.loadAnimation(context, R.anim.rotate_image_clockwise);
        inner.startAnimation(clockwise);

        Animation anticlockwise = AnimationUtils.loadAnimation(context, R.anim.rotate_image_anticlockwise);
        outer.startAnimation(anticlockwise);

        inner.setAlpha(0.5f);
        outer.setAlpha(0.5f);
        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.COMPRESSION);

    }

    public void removeSingleAppUI() {
        UtilityMethods.getInstance().handleSound(false, context);

        Log.d(TAG, "OVERLAY_SINGLEAPP_REMOVED_CALLED");
        try {
            windowManager_compression.removeView(compress_view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            countDownTimer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        LayoutInflater inflater = (LayoutInflater) sSharedInstance.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        compress_view = inflater.inflate(R.layout.single_compression_overlay_before, null);
        WindowManager.LayoutParams params;
        try {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                            WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                    PixelFormat.TRANSLUCENT);
            params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

            windowManager_compression = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
            try {
                windowManager_compression.addView(compress_view, params);
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception j) {
            j.printStackTrace();
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
            params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
            windowManager_compression = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
            try {
                windowManager_compression.addView(compress_view, params);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  Toast.makeText(getApplicationContext(),"CATCH",Toast.LENGTH_SHORT).show();
        }

        Button done = (Button) compress_view.findViewById(R.id.optimize_btn);
        ProgressBar progressBar = (ProgressBar) compress_view.findViewById(R.id.final_tick_background);
        // set Icon
        ImageView app_icon = (ImageView) compress_view.findViewById(R.id.single_app_image);
        app_icon.setImageDrawable(icon);

        // set Name of App
        TextView app_nameET = (TextView) compress_view.findViewById(R.id.app_name);
        app_nameET.setText(app_name + " Compressed");

        // App Size to compress

        TextView app_sizeET = (TextView) compress_view.findViewById(R.id.textView35);
        app_sizeET.setText(  UtilityMethods.getInstance().getSize_number(prev_size, false));

        TextView app_SIZEinMB = (TextView) compress_view.findViewById(R.id.textView36);
        app_SIZEinMB.setText("" + UtilityMethods.getInstance().getSize_charecter(prev_size));


        //stars

        inner = (ImageView) compress_view.findViewById(R.id.stars_inner);
        outer = (ImageView) compress_view.findViewById(R.id.stars_outer);


        clockwise = AnimationUtils.loadAnimation(context, R.anim.rotate_image_clockwise);
        inner.startAnimation(clockwise);


        anticlockwise = AnimationUtils.loadAnimation(context, R.anim.rotate_image_anticlockwise);
        outer.startAnimation(anticlockwise);

        //Done button cale Up


        done.setVisibility(View.VISIBLE);
        final Animation animationScaleUp = AnimationUtils.loadAnimation(context.getApplicationContext(), R.anim.scale_big_small);
        animationScaleUp.setStartOffset(0);
        AnimationSet growShrink = new AnimationSet(true);
        growShrink.addAnimation(animationScaleUp);
        growShrink.setFillAfter(true);
        done.setAnimation(growShrink);
        done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Bundle paramBundle = new Bundle();
                paramBundle.putString(AnalyticsConstants.Params.PACKAGE_NAME, packageName);
                paramBundle.putString(AnalyticsConstants.Params.CANCEL_TYPE, "null");
                paramBundle.putString(AnalyticsConstants.Params.STATUS, "success");
                Log.d(AnalyticsConstants.TAG, "Drag compress success" + packageName);
                new AnalyticsHandler().logEvent(AnalyticsConstants.Event.DRAG_COMPRESS, paramBundle);
                if (windowManager_compression != null) {
                    windowManager_compression.removeView(compress_view);
                }
                compress_view = null;
                windowManager_compression = null;
            }
        });


        try {
            // Progress BAr animation
            ObjectAnimator anim = ObjectAnimator.ofInt(progressBar, "progress", 0, 100);

            anim.setDuration(1000);
            anim.setInterpolator(new DecelerateInterpolator());
            anim.start();

            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    Log.d("REACTIVATING_OVERLAY", "REMOVED");
                    //Do something after 100ms


                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (windowManager_compression != null) {
                windowManager_compression.removeView(compress_view);
            }

            compress_view = null;
        }

    }

    void startSingleAppTimer() {
        countDownTimer = null;
        countDownTimer = new CountDownTimer(50000, 1000) {

            public void onTick(long millisUntilFinished) {
                //mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                //  Toast.makeText(getApplicationContext(),"Time Out",Toast.LENGTH_SHORT).show();
                removeSingleAppUI();
                //   allowed = false;
            }
        }.start();

    }

}
