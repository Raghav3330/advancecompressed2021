package com.spaceup.Activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import com.facebook.ads.Ad;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAd;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAdLoadCallback;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.ads.InterstitialAd;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.BuildConfig;
import com.spaceup.R;
import com.spaceup.RemoteConfig;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.uninstall.activities.UninstallerActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import static com.spaceup.Utility.PrefManager.KEY_COMPRESSION_NUMBER_COUNT;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class FinalResultScreen extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener{

    private static final String LOG_TAG = FinalResultScreen.class.getSimpleName();
    public static String FINAL_ACTIVITY_FINISHED = "final.activity.finished";
    private static int size = 4;
    final int APP_HINT_CARD_ADVANCE = 0;
    final int APP_HINT_CARD = 6;
    final int SHARE_APP_CARD = 1;
    final int FEEDBACK_CARD = 2;
    final int NEGATIVE_FEEDBACK_CARD = 4;
    final int POSITIVE_FEEDBACK_CARD = 5;
    final int FEEDBACK_THANK_YOU_CARD = 7;
    final int STASH_INSTALL_CARD = 8;
    final int BLANK_CARD = 9;

    TextView no_of_compressed_apps, size_saved_unit, app_size_saved, cache_size_saved, ram_size_saved;
    TextView size_of_compressed_apps, size_saved_no;
    ImageView back_btn, process_signifying_image;
    RelativeLayout final_result_image;
    UtilityMethods methods;
    PrefManager pm;
    CollapsingToolbarLayout collapse_toolbar;
    int feedbackFlag = 0;
    boolean advance_compression, clickedShareCard, clickedRatingCard, clickedInstallStashCard;
    private RewardedInterstitialAd mInterstitialAd;
    private com.facebook.ads.InterstitialAd ad;

    //button click status
    boolean difficult_clicked = false, app_crashed_clicked = false, data_lost_clicked = false, slow_process_clicked = false, send_button_clicked = false, negative_feedback_cancel_pressed = false, clickedCancelInstall = false, mSelectedMayBLater = false;
    boolean abort = false;
    EditText negative_feedback_editText;
    private Bundle mGAParams;
    //colors
    private int blue, transparent, stroke, mAppTextColor;
    private int mStokeEditTextColor;
    //stroke of textview
    private int mStrokePxText;
    String mAppCompressed;
    androidx.appcompat.widget.Toolbar mToolBar;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (!UtilityMethods.isPackageExisted(getApplicationContext(), "com.stash.junkcleaner")) {
//            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.STASH_REF,
//                    UtilityMethods.getInstance().getActionRefferalResult(),
//                    AnalyticsConstants.Label.FIRED,
//                    null,
//                    false,
//                    null);
//        }
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.RESULT,
                AnalyticsConstants.Action.FIRED_RESULT,
                null,
                null,
                false,
                null);

        Bundle b = getIntent().getExtras();

        assert b != null;
        mAppCompressed = b.getString("no_of_apps_compressed");
        int totalApps = PrefManager.getInstance(getApplicationContext()).getInt(PrefManager.TOTAL_APPS_UNINSTALLED) + Integer.parseInt(String.valueOf(mAppCompressed));
        PrefManager.getInstance(getApplicationContext()).putInt(PrefManager.TOTAL_APPS_UNINSTALLED, Integer.parseInt(String.valueOf(totalApps)));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.final_result_layout);

        AudienceNetworkAds.initialize(this);

        //dont show ads on first time loading
        boolean isSeenTutorial = PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.SEEN_MANAGE_APPS_TUTORIAL);
//        boolean isSeenTutorial = true;
        Log.d("FINALRESULT",UtilityMethods.getInstance().getAdmobID(UtilityMethods.RESULT_SCREEN));
        if(isSeenTutorial) {
            if (RemoteConfig.getBoolean(R.string.show_interstitial_ads) && UtilityMethods.getInstance().isNewUser(getApplicationContext())) {
                if(RemoteConfig.getString(R.string.ad_network).equals("google")) {
//                    loadAd();
                }else if (RemoteConfig.getString(R.string.ad_network).equals("facebook")){
//                    loadFBAds();
                }
            }
        }

        size_saved_no = findViewById(R.id.size_saved_no);
        size_saved_unit = findViewById(R.id.size_saved_unit);
        app_size_saved = findViewById(R.id.app_size_saved);
        cache_size_saved = findViewById(R.id.cache_size_saved);
        ram_size_saved = findViewById(R.id.ram_size_saved);
        long appSizeSaved = PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.TOTAL_APP_SPACE_SAVED);
        long cacheSizeSaved = PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.TOTAL_CACHE_SAVED);
        long ramSizeSaved = PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.TOTAL_RAM_SAVED);
        if (mAppCompressed == null) {
            mAppCompressed = "1";
        }

        long totalAppsize = PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.TOTAL_SPACE_SAVED);
        PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.TOTAL_SPACE_SAVED, Integer.parseInt(String.valueOf(totalAppsize)));

        app_size_saved.setText("Apps   - " + UtilityMethods.getSizeinMBOnly((long) (appSizeSaved * Integer.parseInt(mAppCompressed))) + " saved");
        cache_size_saved.setText("Cache - " + UtilityMethods.getSizeinMBOnly((long) (cacheSizeSaved * Integer.parseInt(mAppCompressed))) + " saved");
        ram_size_saved.setText("RAM   - " + UtilityMethods.getSizeinMBOnly((long) (ramSizeSaved)) + " saved");
        long saved = appSizeSaved + cacheSizeSaved + ramSizeSaved;
        size_saved_no.setText(UtilityMethods.getInstance().getSize_numberResult(saved, false));
        size_saved_unit.setText(UtilityMethods.getInstance().getSize_charecter(saved));

        mToolBar = findViewById(R.id.toolbar);
        mToolBar.setTitle(mAppCompressed + " Apps Uninstalled");
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        size = 4;
        blue = ContextCompat.getColor(FinalResultScreen.this, R.color.blue);
        stroke = ContextCompat.getColor(FinalResultScreen.this, R.color.statusbar);
        transparent = Color.TRANSPARENT;
        mAppTextColor = ContextCompat.getColor(FinalResultScreen.this, R.color.app_text_color);
        mStokeEditTextColor = ContextCompat.getColor(FinalResultScreen.this, R.color.fortyBlack);
        methods = UtilityMethods.getInstance();
        mStrokePxText = (int) methods.convertDpToPixel(1, FinalResultScreen.this);
        pm = PrefManager.getInstance(FinalResultScreen.this);

        /*no_of_compressed_apps = findViewById(R.id.no_of_compressed_apps);
        size_of_compressed_apps = findViewById(R.id.size_of_compressed_apps);
        */
        process_signifying_image = findViewById(R.id.process_signifying_image);
        collapse_toolbar = findViewById(R.id.collapse_toolbar);
        final_result_image = findViewById(R.id.result_image);
        UninstallerActivity.allowed = 0;


        Intent intent = getIntent();
        int apps_compressed = 0;
        long size_compressed_apps = 0;

        // Start TIme to calculate execution time of compression
        long timePeriod = (new AnalyticsHandler().timePeriod()) / 1000;

        //analytics start
        Bundle paramBundle = new Bundle();
        paramBundle.putString(AnalyticsConstants.Params.APP_COMPRESSED, mAppCompressed);
        paramBundle.putString(AnalyticsConstants.Params.STOP, "" + intent.getBooleanExtra("process_abort", false));
        paramBundle.putString(AnalyticsConstants.Params.TIME, "" + timePeriod);
        new AnalyticsHandler().logEvent(AnalyticsConstants.Event.RESULT, paramBundle);

        Log.d(AnalyticsConstants.TAG, "no_of_apps_compressed : " + mAppCompressed);
        Log.d(AnalyticsConstants.TAG, "process_abort : " + intent.getBooleanExtra("process_abort", false));

        mGAParams = new Bundle();
        mGAParams.putString("category", AnalyticsConstants.Category.COMPRESS);
        mGAParams.putString("action", AnalyticsConstants.Action.RESULT);
        mGAParams.putString("label", AnalyticsConstants.Label.APPS_COMPRESSED);


        try {
            mGAParams.putLong("value", Long.parseLong(mAppCompressed));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }


        PrefManager prefManager = PrefManager.getInstance(this);
        int finalCount = prefManager.getInt(KEY_COMPRESSION_NUMBER_COUNT) + 1;
        prefManager.putInt(KEY_COMPRESSION_NUMBER_COUNT, finalCount);
        mGAParams.putInt("cd", AnalyticsConstants.CustomDim.COMPRESSION_COUNT);
        mGAParams.putString("cd_value", finalCount + "");

        mGAParams.putInt("cm", AnalyticsConstants.CustomMetric.APPS_COM);
        try {
            mGAParams.putInt("cm_value", Integer.parseInt(mAppCompressed));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        new AnalyticsHandler().logGAEvent(mGAParams);
        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
        //Apsalar Analytics START
        JSONObject appsalarTracking = new JSONObject();
        try {
            if (mAppCompressed != null)
                appsalarTracking.put(AnalyticsConstants.Params.APP_COMPRESSED, Integer.parseInt(mAppCompressed));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(AnalyticsConstants.APSALARTAG, appsalarTracking.toString());
        new AnalyticsHandler().logApsalarJsonEvent(AnalyticsConstants.Key.COMPRESSION_COMPLETE, appsalarTracking);
        //Apsalar Analytics END

        if (intent.getBooleanExtra("process_abort", false)) {
            mGAParams.putString("label", AnalyticsConstants.Label.STOP);
            mGAParams.putLong("value", 1);
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
        }


        //analytics end


        advance_compression = intent.getBooleanExtra("advance_compression", false);

        if (intent.getBooleanExtra("process_abort", false)) {

            no_of_compressed_apps.setText("Process Aborted!");
            try {
                apps_compressed = Integer.parseInt(mAppCompressed);
                size_compressed_apps = Long.parseLong(intent.getStringExtra("size_saved"));
            } catch (NumberFormatException e) {
                Log.d(LOG_TAG, e.getMessage(), e);
            }
            size_of_compressed_apps.setText(apps_compressed + " Apps Uninstalled. " + UtilityMethods.getInstance().getSizeinMB(size_compressed_apps) + " Saved");
            process_signifying_image.setImageResource(R.mipmap.warning);
            process_signifying_image.setVisibility(View.VISIBLE);
            final_result_image.setVisibility(View.GONE);
            abort = true;
            Log.d("finish", "onCreate: value of abort " + abort);
            //app compressed Analytics
            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.RESULT,
                    AnalyticsConstants.Action.APPS_COMPRESSED,
                    null,
                    String.valueOf(apps_compressed),
                    false,
                    null);

            //Space Saved analytics
            long sizeInMB = size_compressed_apps / (1048576);

            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.RESULT,
                    AnalyticsConstants.Action.SPACE_SAVED,
                    null,
                    String.valueOf(sizeInMB),
                    false,
                    null);

        } else {
            abort = false;
            Log.d("finish", "onCreate: value of abort " + abort);
            /*process_signifying_image.setVisibility(View.GONE);
            final_result_image.setVisibility(View.VISIBLE);*/
            try {
                apps_compressed = Integer.parseInt(mAppCompressed);
                size_compressed_apps = Long.parseLong(intent.getStringExtra("size_saved"));
            } catch (NumberFormatException e) {
                Log.d(LOG_TAG, e.getMessage(), e);
            }
            if (getIntent() != null) {
                /*no_of_compressed_apps.setText(apps_compressed + " Apps Compressed");
                size_of_compressed_apps.setText(UtilityMethods.getInstance().getSizeinMB(size_compressed_apps) + " Saved");
*/
            }
            //app compressed Analytics
            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.RESULT,
                    AnalyticsConstants.Action.APPS_COMPRESSED,
                    null,
                    String.valueOf(apps_compressed),
                    false,
                    null);

            //Space Saved analytics
            long sizeInMB = size_compressed_apps / (1048576);

            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.RESULT,
                    AnalyticsConstants.Action.SPACE_SAVED,
                    null,
                    String.valueOf(sizeInMB),
                    false,
                    null);


            //recording LastCompressTimeStamp to support theme changes on quick compression
            Calendar d = Calendar.getInstance();
            PrefManager.getInstance(FinalResultScreen.this).setLastCompressTimeStamp(d.getTimeInMillis());
            Log.d("Theme", "BUG TestActivity time set in last compress time stamp " + d.getTimeInMillis());

        }
        if (apps_compressed == 0) {
            //no apps compressed, may be process aborted
            //to avoid divide by zero exception in analysis screen
            pm.setSessionCompressCount(1);
        } else if (apps_compressed == 1) {
            //only one app compressed
            pm.setSessionCompressCount(2);
        } else {
            //more than 1 app compressed
            pm.setSessionCompressCount(apps_compressed);
        }

        AppBarLayout appbar = (AppBarLayout) findViewById(R.id.appbar);

        appbar.addOnOffsetChangedListener(this);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        /*int space = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12,
                getResources().getDisplayMetrics());*/
        int space = (int) dipToPixels(this, 12);
        CoordinatorLayout.LayoutParams params =
                (CoordinatorLayout.LayoutParams) recyclerView.getLayoutParams();
        AppBarLayout.ScrollingViewBehavior behavior =
                (AppBarLayout.ScrollingViewBehavior) params.getBehavior();
        behavior.setOverlayTop((int) UtilityMethods.getInstance().convertDpToPixel(20, FinalResultScreen.this));
        CompressedAdapter mAdapter = new CompressedAdapter();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new SpacesItemDecoration(space));
        recyclerView.setAdapter(mAdapter);

        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.RESULT);
        //view_comp_apps Analytics | CLick Analytics in Adapter
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.RESULT,
                AnalyticsConstants.Action.VIEW_COMP_APPS,
                AnalyticsConstants.Label.FIRED,
                null,
                false,
                null);


        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
    }

    private void loadAd() {
        RewardedInterstitialAd.load(FinalResultScreen.this, UtilityMethods.getInstance().getAdmobID(UtilityMethods.RESULT_SCREEN),
                new AdRequest.Builder().build(),  new RewardedInterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(RewardedInterstitialAd ad) {
                        mInterstitialAd = ad;
                        Log.d("TAG == rk", "onAdLoaded");
                                mInterstitialAd.show(FinalResultScreen.this, new OnUserEarnedRewardListener() {
                                    @Override
                                    public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                                    }
                                });
                                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                                    /** Called when the ad failed to show full screen content. */
                                    @Override
                                    public void onAdFailedToShowFullScreenContent(AdError adError) {
                                        Log.d("TAG == rk", "onAdFailedToShowFullScreenContent");
                                        Log.d( "onAdFailedToShow : ",adError.getMessage().toString());
                                    }
                                    /** Called when ad showed the full screen content. */
                                    @Override
                                    public void onAdShowedFullScreenContent() {
                                        Log.d("TAG == rk", "onAdShowedFullScreenContent");
                                    }

                                     /** Called when full screen content is dismissed. */
                                    @Override
                                    public void onAdDismissedFullScreenContent() {
                                    Log.d("TAG == rk", "onAdDismissedFullScreenContent");
                                    }
                                });
                    }
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        Log.e("TAG", "onAdFailedToLoad");
                    }
                });
    }

    private void loadFBAds() {
        ad = new com.facebook.ads.InterstitialAd(this, "600591120962050_601832880837874");
        InterstitialAdListener interstitialAdListener = new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback
                Log.d( "ad displayed.","I am here");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                // Interstitial dismissed callback
                Log.d( "ad dismissed.","I am here");
            }
            @Override
            public void onError(Ad ad, com.facebook.ads.AdError adError) {
                Log.d("AD ERROR ",adError.getErrorMessage().toString());

            }

            @Override
            public void onAdLoaded(Ad adm) {
                // Interstitial ad is loaded and ready to be displayed
                Log.d( "ad is loaded ","i am here");
                // Show the ad
                ad.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                Log.d( "ad clicked!","AD CLICKED");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d( "ad impression logged!","LOGGING IMPRESSION");
            }
        };

        // For auto play video ads, it's recommended to load the ad
        // at least 30 seconds before it is shown
        ad.loadAd(
                ad.buildLoadAdConfig()
                        .withAdListener(interstitialAdListener)
                        .build());
    }


    private void analyticsCompressionComplete() {

    }


    @Override
    protected void onPause() {
        super.onPause();
        //If not clicked on Share Card and Not clicked on Play Store Rating (Basically to detect Home Screen Click Event)
        boolean isSeenTutorial = PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.SEEN_MANAGE_APPS_TUTORIAL);
        if (!isSeenTutorial) {

            if (!clickedRatingCard && !clickedShareCard && !clickedInstallStashCard) {
//                showCompressedAppTutorial();
            }
        }
    }

    public float dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }

    @Override
    protected void onResume() {
        clickedRatingCard = false;
        clickedShareCard = false;
        super.onResume();
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        if (abort && Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(ContextCompat.getColor(FinalResultScreen.this, R.color.statusbar_red));
        } else if (Build.VERSION.SDK_INT >= 21 && pm.getTheme() == 0) {
            //blue
            getWindow().setStatusBarColor(ContextCompat.getColor(FinalResultScreen.this, R.color.statusbar_blue));
            Resources res = getResources(); //resource handle
            /*Drawable drawable = res.getDrawable(R.drawable.blue_appbar_gradient);
           collapse_toolbar.setBackground(drawable);*/
        } else if (Build.VERSION.SDK_INT >= 21 && pm.getTheme() == 1) {
            //REd
            getWindow().setStatusBarColor(ContextCompat.getColor(FinalResultScreen.this, R.color.statusbar_red));
            // Resources res = getResources();
           /* Drawable drawable = res.getDrawable(R.drawable.red_appbar_gradient);
             collapse_toolbar.setBackground(drawable);*/
        } else if (Build.VERSION.SDK_INT >= 19) {

            setStatusBarTranslucent(true);
        }
    }

    @Override
    public void onBackPressed() {
        Log.d("finish", "Boroadcast send");
        Intent it = new Intent(FINAL_ACTIVITY_FINISHED);
        it.putExtra("aborted", abort);
        Log.d("finish", "value send to " + abort);
        Log.d("finish", "Final result screen finished");

        LocalBroadcastManager
                .getInstance(FinalResultScreen.this).sendBroadcast(it);

        //analytics start
        mGAParams = new Bundle();
        mGAParams.putString("category", AnalyticsConstants.Category.COMPRESS);
        mGAParams.putString("action", AnalyticsConstants.Action.ANIMATION_STATS);
        mGAParams.putString("label", AnalyticsConstants.Label.RESULT_BACK);
        new AnalyticsHandler().logGAEvent(mGAParams);
        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());


        //analytics stop
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.RESULT,
                AnalyticsConstants.Action.RESULT_BACK,
                null,
                null,
                false,
                null);
        startActivity(new Intent(this, AnalysisScreen.class));
        finish();
        //super.onBackPressed();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    int mVerticalOffsetGlobal = -1;

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }



    public class CompressedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

        private static final int ANIMATED_ITEMS_COUNT = 2;
        public TextView send, cancel, difficult, app_crashed, data_lost, slow_process, sure, maybe;
        boolean animateItems = true;
        int lastAnimatedPosition = -1;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case APP_HINT_CARD_ADVANCE:

//                    View app_hint_advance = LayoutInflater.from(parent.getContext())
//
//                            .inflate(R.layout.finalscreen_compressed_app_hint, parent, false);
//                    CardView openShotCutFolderTutorial = (CardView) app_hint_advance.findViewById(R.id.hint_card);
//                    openShotCutFolderTutorial.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.RESULT,
//                                    AnalyticsConstants.Action.VIEW_COMP_APPS,
//                                    AnalyticsConstants.Label.CLICKED,
//                                    null,
//                                    false,
//                                    null);
//                            showCompressedAppTutorial();
//
//
//                        }
//                    });
//                    return new AppHintAdvanceCard(app_hint_advance);


                case APP_HINT_CARD:
//
//                    View app_hint_normal = LayoutInflater.from(parent.getContext())
//                            .inflate(R.layout.finalscreen_normal_hint_layout, parent, false);
//                    TextView shortcutFolder = (TextView) app_hint_normal.findViewById(R.id.textView46);
//                    shortcutFolder.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            //analytics start
//                            mGAParams = new Bundle();
//                            mGAParams.putString("category", AnalyticsConstants.Category.COMPRESS);
//                            mGAParams.putString("action", AnalyticsConstants.Action.RESULT);
//                            mGAParams.putString("label", AnalyticsConstants.Label.SHORTCUT_BTN);
//                            new AnalyticsHandler().logGAEvent(mGAParams);
//                            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
//                            //analytics stop
//                            finish();
//                            Intent intent = new Intent(getApplicationContext(), ManageApps.class);
//                            startActivity(intent);
//                        }
//                    });
//                    return new AppHintNormalCard(app_hint_normal);

                case STASH_INSTALL_CARD:
//                    View stashCard = LayoutInflater.from(parent.getContext())
//                            .inflate(R.layout.stash_install_card, parent, false);
//                    TextView installStash = (TextView) stashCard.findViewById(R.id.select_button);
//                    String experimentVal = RemoteConfig.getString(R.string.stash_insall_test);
//                    TextView parentText = stashCard.findViewById(R.id.parent_text);
//                    TextView bottomText = stashCard.findViewById(R.id.bottom_text);
//
//                    if( experimentVal.equals("test1")){
//                        parentText.setTextColor(Color.parseColor("#ff4949"));
//                        parentText.setText("10+ Junk photos found!");
//                        bottomText.setVisibility(View.GONE);
//                    }
//                    else{
//                        bottomText.setVisibility(View.VISIBLE);
//                        parentText.setTextColor(Color.parseColor("#000000"));
//                        parentText.setText("Irritated with Junk Photos also ?");
//                    }
//                    installStash.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            clickedInstallStashCard = true;
//                            Uri uri = Uri.parse(getString(R.string.market_app_url_stash));
//                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                            try {
//                                startActivity(intent);
//                            } catch (ActivityNotFoundException e) {
//                                Log.d("AppUpgradeActivity", "play store redirection failed ");
//                            }
//
//                            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.STASH_REF,
//                                    UtilityMethods.getInstance().getActionRefferalResult(),
//                                    AnalyticsConstants.Label.CLICKED,
//                                    null,
//                                    false,
//                                    null);
//                        }
//                    });
//                    TextView cancel_button = (TextView) stashCard.findViewById(R.id.cancel_button);
//                    cancel_button.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            clickedCancelInstall = true;
//                            notifyDataSetChanged();
//                            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.STASH_REF,
//                                    UtilityMethods.getInstance().getActionRefferalResult(),
//                                    AnalyticsConstants.Label.CLOSED,
//                                    null,
//                                    false,
//                                    null);
//                        }
//                    });
//                    return new StashCardHolder(stashCard);

                case BLANK_CARD:
                    View blank = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.blank_row, parent, false);

                    return new BlankHolder(blank);

                case SHARE_APP_CARD:
                    View shareAppCard = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.share_app_card, parent, false);
                    return new ShareAppCardHolder(shareAppCard);

                case FEEDBACK_CARD:

                    View finalScreenRateCard = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.final_screen_rate_card, parent, false);

                    return new RatingCard(finalScreenRateCard);

                case NEGATIVE_FEEDBACK_CARD:

                    View finalScreenNegativeRateCard = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.negative_feedback_layout, parent, false);

                    return new FourthCard(finalScreenNegativeRateCard);

                case POSITIVE_FEEDBACK_CARD:

                    View finalScreenPositiveRateCard = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.positive_feedback_card, parent, false);

                    return new FifthCard(finalScreenPositiveRateCard);
                case FEEDBACK_THANK_YOU_CARD:
                    View feedBackThankYouCard = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.feedback_thank_you_card, parent, false);

                    return new FeedBackThankYouCard(feedBackThankYouCard);
            }
            return null;
        }

        private void runEnterAnimation(View view, int position) {
           /* if (!animateItems || position > ANIMATED_ITEMS_COUNT - 1) {
                return;
            }
*/
            if (position > lastAnimatedPosition) {
                Log.d("animation", "" + lastAnimatedPosition);
                lastAnimatedPosition = position;
                view.setTranslationY(Scanning.height);
                view.animate()
                        .translationY(0)
                        .setInterpolator(new DecelerateInterpolator(3.f))
                        .setDuration(2000)
                        .start();
            }
        }


        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


            if (holder instanceof RatingCard) {

                RatingCard viewHolder = (RatingCard) holder;
                runEnterAnimation(viewHolder.card, position);
                viewHolder.negative.setOnClickListener(this);
                viewHolder.positive.setOnClickListener(this);
                return;
            }

            if (holder instanceof ShareAppCardHolder) {
                ShareAppCardHolder shareAppCardHolder = (ShareAppCardHolder) holder;
                runEnterAnimation(shareAppCardHolder.shareCard, position);
                shareAppCardHolder.shareButton.setOnClickListener(this);
                return;
            }

            if (holder instanceof FifthCard) {
                FifthCard viewHolder = (FifthCard) holder;
                sure.setOnClickListener(this);
                maybe.setOnClickListener(this);
                return;
            }

            if (holder instanceof FourthCard) {
                FourthCard viewHolder = (FourthCard) holder;
                send.setOnClickListener(this);
                cancel.setOnClickListener(this);
                difficult.setOnClickListener(this);
                app_crashed.setOnClickListener(this);
                data_lost.setOnClickListener(this);
                slow_process.setOnClickListener(this);

                negative_feedback_editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            GradientDrawable drawable = (GradientDrawable) negative_feedback_editText.getBackground();
                            drawable.setStroke(mStrokePxText, mStokeEditTextColor);
                        } else {
                            GradientDrawable drawable = (GradientDrawable) negative_feedback_editText.getBackground();
                            //remove border
                            drawable.setStroke(0, transparent);
                        }
                    }
                });
            }

            if (holder instanceof AppHintAdvanceCard) {

                AppHintAdvanceCard appHintAdvanceCard = (AppHintAdvanceCard) holder;
                runEnterAnimation(appHintAdvanceCard.hintCard, position);
            }

            if (holder instanceof AppHintNormalCard) {

                AppHintNormalCard appHintNormalCard = (AppHintNormalCard) holder;
                runEnterAnimation(appHintNormalCard.hintCard, position);
            }

        }

        @Override
        public int getItemViewType(int position) {

            Log.d("pos", "" + position);
            if (abort) {
//Position 0 Compress Card
                if (position == 0) {
                    if (advance_compression)
                        return APP_HINT_CARD_ADVANCE;
                    else
                        return APP_HINT_CARD;
                }

                if (position == 1) {
                    if (!clickedCancelInstall && !UtilityMethods.isPackageExisted(getApplicationContext(), "com.stash.junkcleaner")) {
                        return STASH_INSTALL_CARD;
                    } else {
                        return BLANK_CARD;
                    }

                }

                if (position == 2 && (negative_feedback_cancel_pressed || send_button_clicked)) {
                    if (send_button_clicked) {
                        return FEEDBACK_THANK_YOU_CARD;
                    }
                    return SHARE_APP_CARD;
                } else if (position == 2) {
                    return NEGATIVE_FEEDBACK_CARD;
                }

                if (position == 3) {
                    return SHARE_APP_CARD;
                }


            } else {
                //Position 0 Compress Card
                if (position == 1) {
                    if (advance_compression)
                        return APP_HINT_CARD_ADVANCE;
                    else
                        return APP_HINT_CARD;
                }
                //Position 1 Stash Install Card
                if (position == 0) {
                    if (!clickedCancelInstall && !UtilityMethods.isPackageExisted(getApplicationContext(), "com.stash.junkcleaner")) {
                        return STASH_INSTALL_CARD;
                    } else {
                        return BLANK_CARD;

                    }
                }

                //Position 2 LIKE CARD

                if (position == 2) {
                    if (!mSelectedMayBLater) {
                        if (feedbackFlag == 0) {
                            return FEEDBACK_CARD;
                        } else if (feedbackFlag == 1) {
                            return NEGATIVE_FEEDBACK_CARD;
                        } else if (feedbackFlag == 2) {
                            return POSITIVE_FEEDBACK_CARD;
                        } else if (feedbackFlag == 3) {
                            return FEEDBACK_THANK_YOU_CARD;
                        }
                    } else {
                        return BLANK_CARD;
                    }
                }
                //Position 3 SHARE CARD

                if (position == 3) {
                    return SHARE_APP_CARD;
                }
            }


            return position;
        }

        @Override
        public int getItemCount() {

            Log.d("FinalScreen", size + "");
            return size;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.positive:
                    //analytics start
                    Bundle paramBundle = new Bundle();
                    paramBundle.putString(AnalyticsConstants.Params.STATUS, "positive");
                    new AnalyticsHandler().logEvent(AnalyticsConstants.Event.INIT_FEEDBACK, paramBundle);
                    Log.d(AnalyticsConstants.TAG, "STATUS " + "positive button clicked");

                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.FEEDBACK);
                    mGAParams.putString("action", AnalyticsConstants.Action.POSITIVE);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                    //analytics end
                    feedbackFlag = 2;
                    notifyItemChanged(FEEDBACK_CARD);
                    new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.FEEDBACK_RESULT);
                    break;
                case R.id.negative:
//analytics start
                    Bundle param = new Bundle();
                    param.putString(AnalyticsConstants.Params.STATUS, "negative");
                    Log.d(AnalyticsConstants.TAG, "STATUS " + "negative button clicked");
                    new AnalyticsHandler().logEvent(AnalyticsConstants.Event.INIT_FEEDBACK, param);

                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.FEEDBACK);
                    mGAParams.putString("action", AnalyticsConstants.Action.NEGATIVE);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                    //analytics end
                    feedbackFlag = 1;
                    notifyItemChanged(FEEDBACK_CARD);
                    new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.FEEDBACK_RESULT);
                    break;

                case R.id.send:
                    send_button_clicked = true;

                    PrefManager.getInstance(FinalResultScreen.this).setReview(true);


                    if (difficult_clicked || app_crashed_clicked || data_lost_clicked || slow_process_clicked || (negative_feedback_editText.getText().length() > 0)) {
                        Bundle paramBundl = new Bundle();

                        paramBundl.putString(AnalyticsConstants.Params.DIFFICULT, "" + difficult_clicked);
                        paramBundl.putString(AnalyticsConstants.Params.APP_CRASHED, "" + app_crashed_clicked);
                        paramBundl.putString(AnalyticsConstants.Params.DATA_LOST, "" + data_lost_clicked);
                        paramBundl.putString(AnalyticsConstants.Params.SLOW_PROCESS, "" + slow_process_clicked);
                        paramBundl.putString(AnalyticsConstants.Params.SEND, "" + send_button_clicked);
                        paramBundl.putString(AnalyticsConstants.Params.REVIEW, negative_feedback_editText.getText().toString());
                        new AnalyticsHandler().logEvent(AnalyticsConstants.Event.FEEDBACK, paramBundl);
                        Log.d(AnalyticsConstants.TAG, "send button clicked");


                        mGAParams = new Bundle();
                        mGAParams.putString("category", AnalyticsConstants.Category.FEEDBACK);
                        mGAParams.putString("action", AnalyticsConstants.Action.NEGATIVE);

                        if (difficult_clicked) {
                            mGAParams.putString("label", AnalyticsConstants.Label.DIFFICULT_CLICKED);
                            new AnalyticsHandler().logGAEvent(mGAParams);
                            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                        }

                        if (app_crashed_clicked) {
                            mGAParams.putString("label", AnalyticsConstants.Label.APP_CRASHED_CLICKED);
                            new AnalyticsHandler().logGAEvent(mGAParams);
                            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                        }

                        if (data_lost_clicked) {
                            mGAParams.putString("label", AnalyticsConstants.Label.DATA_LOST_CLICKED);
                            new AnalyticsHandler().logGAEvent(mGAParams);
                            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                        }
                        if (slow_process_clicked) {
                            mGAParams.putString("label", AnalyticsConstants.Label.SLOW_PROCESS_CLICKED);
                            new AnalyticsHandler().logGAEvent(mGAParams);
                            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                        }

                        if (negative_feedback_editText.getText().length() > 0) {
                            mGAParams.putString("action", AnalyticsConstants.Action.NEGATIVE_TEXT);
                            mGAParams.putString("label", negative_feedback_editText.getText().toString());
                            new AnalyticsHandler().logGAEvent(mGAParams);
                            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                        }
                        if (abort) {
                            notifyItemRemoved(1);
                        } else {
                            notifyItemRemoved(2);
                        }
                        notifyItemRangeChanged(0, size);
                        feedbackFlag = 3;
                    } else {
                        Toast.makeText(FinalResultScreen.this, "Please choose a cause", Toast.LENGTH_SHORT).show();
                    }

                    try {
                        InputMethodManager inputManager =
                                (InputMethodManager) FinalResultScreen.this.
                                        getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(
                                FinalResultScreen.this.getCurrentFocus().getWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);
                    } catch (Exception e) {
                        Log.d("FinalResult", Log.getStackTraceString(e));
                    }
                    mSelectedMayBLater = true;

                    break;


                case R.id.maybe:
                    Bundle bundle = new Bundle();
                    bundle.putString(AnalyticsConstants.Params.PLAY_STORE, "false");
                    new AnalyticsHandler().logEvent(AnalyticsConstants.Event.FEEDBACK, bundle);
                    Log.d(AnalyticsConstants.TAG, "PLAY_STORE false");
                    mSelectedMayBLater = true;

                case R.id.cancel:
                    mSelectedMayBLater = true;
                    if (abort) {
                        negative_feedback_cancel_pressed = true;
                        notifyItemRemoved(1);
                    } else {
                        notifyItemRemoved(2);
                    }
                    notifyItemRangeChanged(0, size);
                    break;


                case R.id.difficult:
                    if (difficult_clicked) {
                        //unselected
                        difficult_clicked = false;
                        GradientDrawable drawable = (GradientDrawable) difficult.getBackground();
                        drawable.setColor(transparent); // set solid color
                        drawable.setStroke(mStrokePxText, stroke);
                        difficult.setTextColor(mAppTextColor);

                    } else {
                        //selected
                        difficult_clicked = true;
                        GradientDrawable drawable = (GradientDrawable) difficult.getBackground();
                        drawable.setColor(blue); // set solid color
                        drawable.setStroke(0, transparent);
                        difficult.setTextColor(Color.WHITE);
                    }

                    break;
                case R.id.app_crashed:
                    if (app_crashed_clicked) {
                        //unselected
                        app_crashed_clicked = false;
                        GradientDrawable drawable = (GradientDrawable) app_crashed.getBackground();
                        drawable.setColor(transparent); // set solid color
                        drawable.setStroke(mStrokePxText, stroke);
                        app_crashed.setTextColor(mAppTextColor);

                    } else {
                        //selected
                        app_crashed_clicked = true;
                        GradientDrawable drawable = (GradientDrawable) app_crashed.getBackground();
                        drawable.setColor(blue); // set solid color
                        drawable.setStroke(0, transparent);
                        app_crashed.setTextColor(Color.WHITE);
                    }
                    break;
                case R.id.data_lost:
                    if (data_lost_clicked) {
                        //unselected
                        data_lost_clicked = false;
                        GradientDrawable drawable = (GradientDrawable) data_lost.getBackground();
                        drawable.setColor(transparent); // set solid color
                        drawable.setStroke(mStrokePxText, stroke);
                        data_lost.setTextColor(mAppTextColor);
                    } else {
                        //selected
                        data_lost_clicked = true;
                        GradientDrawable drawable = (GradientDrawable) data_lost.getBackground();
                        drawable.setColor(blue); // set solid color
                        drawable.setStroke(0, transparent);
                        data_lost.setTextColor(Color.WHITE);
                    }
                    break;
                case R.id.slow_process:
                    if (slow_process_clicked) {
                        //unselected
                        slow_process_clicked = false;
                        GradientDrawable drawable = (GradientDrawable) slow_process.getBackground();
                        drawable.setColor(transparent); // set solid color
                        drawable.setStroke(mStrokePxText, stroke);
                        slow_process.setTextColor(mAppTextColor);
                    } else {
                        //selected
                        slow_process_clicked = true;
                        GradientDrawable drawable = (GradientDrawable) slow_process.getBackground();
                        drawable.setColor(blue); // set solid color
                        drawable.setStroke(0, transparent);
                        slow_process.setTextColor(Color.WHITE);
                    }
                    break;
                case R.id.negative_feedback_editText:

                    break;

                case R.id.sure:
                    PrefManager.getInstance(FinalResultScreen.this).setReview(true);
                    Bundle paramBund = new Bundle();
                    paramBund.putString(AnalyticsConstants.Params.PLAY_STORE, "true");
                    new AnalyticsHandler().logEvent(AnalyticsConstants.Event.FEEDBACK, paramBund);
                    Log.d(AnalyticsConstants.TAG, "PLAY_STORE true");
                    launchMarket();
                    clickedRatingCard = true;
                    notifyItemRemoved(2);
                    notifyItemRangeChanged(0, size);

                    break;


                case R.id.share_button:

                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.FEEDBACK);
                    mGAParams.putString("action", AnalyticsConstants.Action.SHARE);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                    clickedShareCard = true;
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_TEXT, Constants.GOOGLE_PLAY_APP_URL);
                    intent.setType(Constants.MIME_TYPE_TEXT);
                    FinalResultScreen.this.startActivity(Intent.createChooser(intent, getResources().getText(R.string.share_app)));
                    break;

            }

        }


        private void launchMarket() {
            Uri uri = Uri.parse("market://details?id=" + getPackageName());
            Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
            try {
                startActivity(myAppLinkToMarket);
            } catch (ActivityNotFoundException e) {
                Log.d("FinalResultScreen", "play store redirection failed ");
            }
        }

        class AppHintAdvanceCard extends RecyclerView.ViewHolder {

            CardView hintCard;

            public AppHintAdvanceCard(View itemView) {
                super(itemView);
                hintCard = (CardView) itemView.findViewById(R.id.hint_card);
            }
        }

        class AppHintNormalCard extends RecyclerView.ViewHolder {

            CardView hintCard;

            public AppHintNormalCard(View itemView) {
                super(itemView);
                hintCard = (CardView) itemView.findViewById(R.id.hint_card);
            }
        }

        private class FeedBackThankYouCard extends RecyclerView.ViewHolder {

            public FeedBackThankYouCard(View itemView) {
                super(itemView);
            }
        }

        private class ShareAppCardHolder extends RecyclerView.ViewHolder {

            TextView shareButton;
            CardView shareCard;

            public ShareAppCardHolder(View itemView) {
                super(itemView);
                shareCard = (CardView) itemView.findViewById(R.id.share_card);
                shareButton = (TextView) itemView.findViewById(R.id.share_button);
            }
        }

        private class StashCardHolder extends RecyclerView.ViewHolder {

//            TextView shareButton;
//            CardView shareCard;

            public StashCardHolder(View itemView) {
                super(itemView);
//                shareCard = (CardView) itemView.findViewById(R.id.share_card);
//                shareButton = (TextView) itemView.findViewById(R.id.share_button);
            }
        }

        private class BlankHolder extends RecyclerView.ViewHolder {

            public BlankHolder(View itemView) {
                super(itemView);
            }
        }

        public class SecondCard extends RecyclerView.ViewHolder {
            public TextView title, year, genre;

            public SecondCard(View view) {
                super(view);
                /*title = (TextView) view.findViewById(R.id.title);
                genre = (TextView) view.findViewById(R.id.genre);
                year = (TextView) view.findViewById(R.id.year);*/
            }
        }

        public class RatingCard extends RecyclerView.ViewHolder {
            public TextView negative, positive;
            public CardView card;

            public RatingCard(View view) {
                super(view);
                card = (CardView) view.findViewById(R.id.card_view);
                negative = (TextView) view.findViewById(R.id.negative);
                positive = (TextView) view.findViewById(R.id.positive);
                //year = (TextView) view.findViewById(R.id.year);
            }
        }

        public class FourthCard extends RecyclerView.ViewHolder {


            public FourthCard(View view) {
                super(view);
                send = (TextView) view.findViewById(R.id.send);
                cancel = (TextView) view.findViewById(R.id.cancel);
                difficult = (TextView) view.findViewById(R.id.difficult);
                app_crashed = (TextView) view.findViewById(R.id.app_crashed);
                data_lost = (TextView) view.findViewById(R.id.data_lost);
                slow_process = (TextView) view.findViewById(R.id.slow_process);
                negative_feedback_editText = (EditText) view.findViewById(R.id.negative_feedback_editText);
            }
        }

        public class FifthCard extends RecyclerView.ViewHolder {

            public FifthCard(View view) {
                super(view);
                sure = (TextView) view.findViewById(R.id.sure);
                maybe = (TextView) view.findViewById(R.id.maybe);

            }
        }
    }

    private void showCompressedAppTutorial() {
        boolean isSeenTutorial = PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.SEEN_MANAGE_APPS_TUTORIAL);
        if (isSeenTutorial) {

            startActivity(new Intent(FinalResultScreen.this, ManageApps.class));
        } else {

            finish();
            UtilityMethods.getInstance().addShortcut(getApplicationContext());

            PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.SEEN_MANAGE_APPS_TUTORIAL, true);
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(FinalResultScreen.this, FolderDiscoveryPopup.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);

                }
            }, 700);
        }
    }


    private class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space = 12;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            if (parent.getChildAdapterPosition(view) == state.getItemCount() - 1) {
                outRect.bottom = space;
                outRect.top = 0; //don't forget about recycling...
            }
        }
    }

}
