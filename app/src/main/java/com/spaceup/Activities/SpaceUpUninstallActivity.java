package com.spaceup.Activities;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.R;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.hosting.SendFeedbackSpaceUpUninstall;

/**
 * Created by dhurv on 09-03-2017.
 */

public class SpaceUpUninstallActivity extends Activity {
    boolean space_check, data_check, crash_Check, battery_check;
    private TextView space_not_Saved;
    private TextView data_lost;
    private TextView app_crashed;
    private TextView battery_consuming;
    private int blue, transparent, stroke, mAppTextColor, mStokeEditTextColor;
    private int mStrokePxText;
    private Bundle mGAParams;
    private long mTimeWhenAppInstalled = 0, mTotalTimeUserHadAppOnHisPhone = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.negative_feedback_layout_uninstall);
        timeAPpInstalledAnalytics();


        space_check = false;
        data_check = false;
        crash_Check = false;
        battery_check = false;

        // Sending Uninstall Popup Event to Hosting Server
        SendFeedbackSpaceUpUninstall send = new SendFeedbackSpaceUpUninstall(getApplicationContext(), space_check, data_check, crash_Check, battery_check, "UNINSTALL_SPACEUP", true);
        send.send_to_server();

        TextView dont_uninstall = (TextView) findViewById(R.id.do_not_uninstall);
        TextView dont_uninstall_outside = (TextView) findViewById(R.id.uninstall_stash);
        final EditText negative_feedback_editText = (EditText) findViewById(R.id.negative_feedback_editText);
        space_not_Saved = (TextView) findViewById(R.id.space_not_Saved);

        data_lost = (TextView) findViewById(R.id.data_lost);

        app_crashed = (TextView) findViewById(R.id.app_crashed);

        battery_consuming = (TextView) findViewById(R.id.battery_consuming);
        blue = ContextCompat.getColor(getApplicationContext(), R.color.blue);
        stroke = ContextCompat.getColor(getApplicationContext(), R.color.statusbar);
        transparent = Color.TRANSPARENT;
        mAppTextColor = ContextCompat.getColor(getApplicationContext(), R.color.app_text_color);
        mStokeEditTextColor = ContextCompat.getColor(getApplicationContext(), R.color.fortyBlack);
        mStrokePxText = (int) UtilityMethods.getInstance().convertDpToPixel(1, getApplicationContext());

        GradientDrawable drawable = (GradientDrawable) space_not_Saved.getBackground();
        drawable.setColor(transparent); // set solid color
        drawable.setStroke(mStrokePxText, stroke);
        space_not_Saved.setTextColor(mAppTextColor);

        GradientDrawable drawable1 = (GradientDrawable) data_lost.getBackground();
        drawable1.setColor(transparent); // set solid color
        drawable1.setStroke(mStrokePxText, stroke);
        data_lost.setTextColor(mAppTextColor);

        GradientDrawable drawable2 = (GradientDrawable) app_crashed.getBackground();
        drawable2.setColor(transparent); // set solid color
        drawable2.setStroke(mStrokePxText, stroke);
        app_crashed.setTextColor(mAppTextColor);

        GradientDrawable drawable3 = (GradientDrawable) battery_consuming.getBackground();
        drawable3.setColor(transparent); // set solid color
        drawable3.setStroke(mStrokePxText, stroke);
        battery_consuming.setTextColor(mAppTextColor);


        space_not_Saved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (space_check) {
                    //unselected
                    space_check = false;
                    GradientDrawable drawable = (GradientDrawable) space_not_Saved.getBackground();
                    drawable.setColor(transparent); // set solid color
                    drawable.setStroke(mStrokePxText, stroke);
                    space_not_Saved.setTextColor(mAppTextColor);

                } else {
                    //selected
                    space_check = true;
                    GradientDrawable drawable = (GradientDrawable) space_not_Saved.getBackground();
                    drawable.setColor(blue); // set solid color
                    drawable.setStroke(0, transparent);
                    space_not_Saved.setTextColor(Color.WHITE);
                }

            }
        });
        data_lost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data_check) {
                    //unselected
                    data_check = false;
                    GradientDrawable drawable = (GradientDrawable) data_lost.getBackground();
                    drawable.setColor(transparent); // set solid color
                    drawable.setStroke(mStrokePxText, stroke);
                    data_lost.setTextColor(mAppTextColor);

                } else {
                    //selected
                    data_check = true;
                    GradientDrawable drawable = (GradientDrawable) data_lost.getBackground();
                    drawable.setColor(blue); // set solid color
                    drawable.setStroke(0, transparent);
                    data_lost.setTextColor(Color.WHITE);
                }
            }
        });
        app_crashed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (crash_Check) {
                    //unselected
                    crash_Check = false;
                    GradientDrawable drawable = (GradientDrawable) app_crashed.getBackground();
                    drawable.setColor(transparent); // set solid color
                    drawable.setStroke(mStrokePxText, stroke);
                    app_crashed.setTextColor(mAppTextColor);

                } else {
                    //selected
                    crash_Check = true;
                    GradientDrawable drawable = (GradientDrawable) app_crashed.getBackground();
                    drawable.setColor(blue); // set solid color
                    drawable.setStroke(0, transparent);
                    app_crashed.setTextColor(Color.WHITE);
                }
            }
        });
        battery_consuming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (battery_check) {
                    //unselected
                    battery_check = false;
                    GradientDrawable drawable = (GradientDrawable) battery_consuming.getBackground();
                    drawable.setColor(transparent); // set solid color
                    drawable.setStroke(mStrokePxText, stroke);
                    battery_consuming.setTextColor(mAppTextColor);

                } else {
                    //selected
                    battery_check = true;
                    GradientDrawable drawable = (GradientDrawable) battery_consuming.getBackground();
                    drawable.setColor(blue); // set solid color
                    drawable.setStroke(0, transparent);
                    battery_consuming.setTextColor(Color.WHITE);
                }
            }
        });
        dont_uninstall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.FEEDBACK);
                    mGAParams.putString("action", AnalyticsConstants.Action.NEGATIVE);
                    if (space_check) {
                        mGAParams.putString("label", AnalyticsConstants.Label.APP_SPACE_NOT_SAVED_CLICKED);
                        new AnalyticsHandler().logGAEvent(mGAParams);
                        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                    }
                    if (data_check) {
                        mGAParams.putString("label", AnalyticsConstants.Label.DATA_LOST_CLICKED);
                        new AnalyticsHandler().logGAEvent(mGAParams);
                        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                    }
                    if (crash_Check) {
                        mGAParams.putString("label", AnalyticsConstants.Label.APP_CRASHED_CLICKED);
                        new AnalyticsHandler().logGAEvent(mGAParams);
                        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                    }
                    if (battery_check) {
                        mGAParams.putString("label", AnalyticsConstants.Label.SLOW_PROCESS_CLICKED);
                        new AnalyticsHandler().logGAEvent(mGAParams);
                        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                    }

                    if (negative_feedback_editText.getText().length() > 0) {
                        mGAParams.putString("action", AnalyticsConstants.Action.NEGATIVE_TEXT);
                        mGAParams.putString("label", negative_feedback_editText.getText().toString());
                        new AnalyticsHandler().logGAEvent(mGAParams);
                        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                    }
                    // Send to Hosting Server
                    SendFeedbackSpaceUpUninstall send = new SendFeedbackSpaceUpUninstall(getApplicationContext(), space_check, data_check, crash_Check, battery_check, negative_feedback_editText.getText().toString(), false);
                    send.send_to_server();
                    finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dont_uninstall_outside.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });

    }

    private void timeAPpInstalledAnalytics() {
        mTimeWhenAppInstalled = PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.SYSTEM_MILLIES_WHEN_APP_LAUNCHED_FIRST_TIME);
        mTotalTimeUserHadAppOnHisPhone = System.currentTimeMillis() - mTimeWhenAppInstalled;
        //Send Analytics
        int halfAnHour = 0, oneDay = 0, threeDay = 0, sevenDay = 0, moreThanSevenDay = 0;
        if (mTotalTimeUserHadAppOnHisPhone < 1800000) {
            //Half an hour
            halfAnHour = 1;

        } else if (mTotalTimeUserHadAppOnHisPhone < 86400000) {
            //one day
            oneDay = 1;


        } else if (mTotalTimeUserHadAppOnHisPhone < 259200000) {
            // three days
            threeDay = 1;

        } else if (mTotalTimeUserHadAppOnHisPhone < 604800000) {
            // seven days
            sevenDay = 1;

        } else {
            // more than seven days
            moreThanSevenDay = 1;


        }

        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.UNINSTALL,
                AnalyticsConstants.Action.LESS_THAN_30,
                null,
                String.valueOf(halfAnHour),
                false,
                null);

        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.UNINSTALL,
                AnalyticsConstants.Action.ONE_DAY,
                null,
                String.valueOf(oneDay),
                false,
                null);
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.UNINSTALL,
                AnalyticsConstants.Action.THREE_DAYS,
                null,
                String.valueOf(threeDay),
                false,
                null);
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.UNINSTALL,
                AnalyticsConstants.Action.SEVEN_DAY,
                null,
                String.valueOf(sevenDay),
                false,
                null);
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.UNINSTALL,
                AnalyticsConstants.Action.MORE_THAN_SEVEN,
                null,
                String.valueOf(moreThanSevenDay),
                false,
                null);


    }
}
