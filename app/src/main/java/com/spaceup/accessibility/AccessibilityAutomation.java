package com.spaceup.accessibility;

import static androidx.core.content.FileProvider.getUriForFile;

import android.accessibilityservice.AccessibilityService;
import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Notification;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.os.Vibrator;
import android.provider.Settings;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.cardview.widget.CardView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.skyfishjy.library.RippleBackground;
import com.spaceup.Activities.FinalResultScreen;
import com.spaceup.Activities.SpaceUpUninstallActivity;
import com.spaceup.Activities.TransitionActivity;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.Animations.DecompressionAnim;
import com.spaceup.Animations.ScanningOverlay;
import com.spaceup.Animations.SingleAppCompressionAnim;
import com.spaceup.App;
import com.spaceup.BuildConfig;
import com.spaceup.R;
import com.spaceup.RemoteConfig;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.permission.AppUsagePemissionHandler;
import com.spaceup.accessibility.permission.UnknownSourcePermission;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.app_services.app_Service;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.models.DataModel;
import com.spaceup.popup_bottom_play_store;
import com.spaceup.services.StashBubbleService;
import com.spaceup.uninstall.activities.AppInfo;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import static com.spaceup.AppShortcut.ShortCutFolder.MOVE_TO_MENU;

import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan;
import io.github.inflationx.calligraphy3.TypefaceUtils;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class AccessibilityAutomation extends AccessibilityService {
    private static boolean isServiceRunning = false;

    private static final String TAG = "Accessibility";
    public static boolean installingFlag, uninstallingFlag, mRequestTurnOn;
    public static String SHOW_DO_IN_BKG_BTN_INTENT = "doinbkg.btn";
    public static boolean callFlag, cleaningFlag;
    public static int repeat_uninstall = 0;
    private static AccessibilityAutomation sSharedInstance;
    private static boolean allowed = false, from_Activity = false;
    private static boolean sCleanRam = false;

    private static long sRamCleanSize = 0;
    public String currentVisiblePackage;
    //static RippleBackground final_tick_background_ripple;
    private int on_play_store = 0;
    private boolean pause_flag = true;
    private boolean resume_flag = true;
    private int light_red_blue_value = 0;
    private int dark_red_blue_value = 0;
    private long size;
    private String compressedAppPackage;
    private ArrayList<String> temp_app_to_compress, temp_app_names_to_compress;
    private int appsToBeCompressed;
    private int stash_count = 0, dummy_count = 0;
    private String global_uninstall_eventappname;
    private int need_benifits = 0, dummy_benifit = 0;
    private WindowManager wm1, wm5;
    private Method mGetPackageSizeInfoMethod;
    private int unknown_tracker = 0;
    private Drawable icon;
    private ActionFailHandler actionFailHandler;
    private long prev_size;
    private String package_name, app_name;
    private View oView1, oView5;
    private AccessibilityNodeInfo nodeInfo;
    private int x;
    private Animation animationScaleUp = null, animationScaleUp_ripple = null, clockwise = null, anticlockwise = null;
    private AnimationSet growShrink, shrink;
    private DecompressionAnim decompression;
    private ScanningOverlay scanning;
    private SingleAppCompressionAnim singleAppUI;
    private Button got_it;
    private String currentInstallation;
    private boolean abortFlag;
    private String deviceManufacturer;
    private RelativeLayout mUpperLayout;
    private int ringstate = 0;
    private RippleBackground ripple_background;
    private ProgressBar progress_background, final_tick_progress, stop_progress;
    private LinearLayout app_icon_white;
    private ImageView app_icon, inner, outer;
    private TextView app_compressed, totalapps, compressed_text;
    private TextView stop_compression, stop_compression_bg;
    private TextView app_name_text, apps_compressed_text, apps_compressed_size_text;
    private RelativeLayout remover_overlay_ui;
    private LinearLayout compressed_number, space_saved, stop_compression_layout;
    private View compress_view;
    private WindowManager windowManager_compression;
    private TextView app_nameET;
    //ad static private SeekBar decompress_progress;
    private ProgressBar decompress_progress;
    private CountDownTimer countDownTimer;
    private boolean unInstallRead, installFlag;
    private Bundle mGAParams;
    private GradientDrawable red;
    private ValueAnimator light_red_blue, dark_red_blue, play_pause;
    private ActionFailHandlerReactivate actionFailHandlerReactivate;
    private boolean mGetAppUsagePermission = false, mGetUnknownSourcesPermission;


    public static AccessibilityAutomation getSharedInstance() {
        if (sSharedInstance == null)
            if (isServiceRunning && RemoteConfig.getBoolean(R.string.share_instance_reset))
                sSharedInstance = new AccessibilityAutomation();

        return sSharedInstance;
    }

    public static boolean isCallFlag() {
        return callFlag;
    }

    public static void allowSpecialBack() {
        mRequestTurnOn = true;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mRequestTurnOn = false;
            }
        }, 20000);
    }

    public static boolean getAllowed() {
        return allowed;
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static boolean isCleanRam() {
        return sCleanRam;
    }

    public static void setCleanRam(boolean pCleanRam) {
        sCleanRam = pCleanRam;
    }

    public static long getRamCleanSize() {
        return sRamCleanSize;
    }

    public static void setRamCleanSize(long pRamCleanSize) {
        sRamCleanSize = pRamCleanSize;
    }

    public void startAccessiblityUsageStats() {
        mGetAppUsagePermission = true;
    }

    public void startUnkownPermission() {
        mGetUnknownSourcesPermission = true;
    }

    @Override
    protected void onServiceConnected() {
        sSharedInstance = this;
        Log.d(TAG, "Accessibility service started");
        installingFlag = false;
        uninstallingFlag = false;
        Log.d(TAG, "" + AccessibilityAutomation.installingFlag + " " + AccessibilityAutomation.uninstallingFlag);
        callFlag = false;
        isServiceRunning = true;
    }

    public boolean isAbortFlag() {
        return abortFlag;
    }

    public void deactivateAbortFlag() {
        abortFlag = false;
    }

    public void activateCleaningFlag() {
        cleaningFlag = true;
    }

    public boolean getCleaningFlag() {
        return cleaningFlag;
    }

    public void deactivateCleaningFlag() {
        cleaningFlag = false;
    }

    public ActionFailHandler getActionFailHandler() {
        return actionFailHandler;
    }

    public ActionFailHandlerReactivate getActionFailHandlerReactivate() {
        return actionFailHandlerReactivate;
    }

    void initializeAppsToCompress(ArrayList<String> compressAppList, ArrayList<String> compressAppNameList) {
        temp_app_to_compress = compressAppList;
        temp_app_names_to_compress = compressAppNameList;

        if (temp_app_to_compress != null) {
            appsToBeCompressed = temp_app_to_compress.size();
        } else {
            appsToBeCompressed = 0;
        }

        if (temp_app_names_to_compress != null) {
            appsToBeCompressed = temp_app_names_to_compress.size();
        } else {
            appsToBeCompressed = 0;
        }
    }

    public void allowReading(boolean allowed) {
        AccessibilityAutomation.allowed = allowed;
        Log.d(TAG, "Reading allowed" + allowed);
        unInstallRead = true;

    }

    public void fromActivity(boolean from_Acivity) {
        AccessibilityAutomation.from_Activity = from_Acivity;
    }

    public void activateIncomingCallFlag() {

        callFlag = true;

        // incomingCallAction();
    }

    public void deactivateIncomingCallFlag() {
        callFlag = false;
    }

    public void deactivateUninstallFlag() {
        uninstallingFlag = false;
        Log.d(TAG, "UnInstalling_flag" + AccessibilityAutomation.uninstallingFlag);
    }

    public String getCurrentInstallation() {
        return currentInstallation;
    }

    public void setCurrentInstallation(String currentInstallation) {
        this.currentInstallation = currentInstallation;
    }

    private void performEvent(AccessibilityNodeInfo node, int event) {

        while (node != null) {

            if (node.isClickable()) {
                node.performAction(event);
                Log.d("ACCESSIBILITY_SCROLL", "Pressed SCROLL");
                return;
            } else {
                Log.d("ACCESSIBILITY_SCROLL", "NOT SCROLLABLRE");
            }
            node = node.getParent();
        }
    }


    private void tryAppUsageHandling(AccessibilityNodeInfo pNodeInfo) {

        //   String launched = PrefManager.getInstance(this).getString(KEY_LAST_UNCOMPRESSED_PACKAGE);
        if (pNodeInfo != null) {
            AppUsagePemissionHandler.getInstance().takePermissionfromAccessibility(pNodeInfo);
        }
    }

    public void globalActionBack() {
        performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {

        if (sSharedInstance == null)
            sSharedInstance = this;
        if (accessibilityEvent.getPackageName() != null) {
            currentVisiblePackage = accessibilityEvent.getPackageName().toString();
            nodeInfo = accessibilityEvent.getSource();
          /*  List<AccessibilityNodeInfo> list = nodeInfo.findAccessibilityNodeInfosByText("tracking");
            for( AccessibilityNodeInfo node : list) {
                try {
                    performEvent(node, ACTION_CLICK);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            list = nodeInfo.findAccessibilityNodeInfosByText("SpaceUp");
            for( AccessibilityNodeInfo node : list) {
                try {
                    performEvent(node, ACTION_CLICK);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }*/

            /**
             *
             * If Stash Cleaner not installed and time is between 6Pm and 12 PM and Whatsapp is just exited
             * then show this popup once a day
             *
             *
             */
            showStashInstallerCondition(currentVisiblePackage);
            if (currentVisiblePackage.equals("com.android.vending")) {
                nodeInfo = accessibilityEvent.getSource();

                if (nodeInfo != null && (nodeInfo.toString().contains("Rect(204") || nodeInfo.toString().contains("Rect(408") || nodeInfo.toString().contains("Rect(180") || nodeInfo.toString().contains("Rect(306")) && on_play_store == 0) {
                    List<AppInfo> quickCompressedApps = AppDBHelper.getInstance(getApplicationContext()).getShorcutApps();
                    for (AppInfo app : quickCompressedApps) {

                        Log.d("LAUNCHER_COMPRESSED", app.getAppName());

                        if (nodeInfo.getText() != null && nodeInfo.getText().toString().toLowerCase().contains(app.getAppName().toLowerCase())) {
                            try {
                                Log.d("LAUNCHER_INFO", "" + nodeInfo.toString());
                                Intent intent2 = new Intent(AccessibilityAutomation.this, popup_bottom_play_store.class);
                                intent2.putExtra("name", app.getAppName());
                                intent2.putExtra("packageName", app.getPkgName());
                                intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                getApplicationContext().startActivity(intent2);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            on_play_store = 1;

                        }

                    }
                }
            } else {
                on_play_store = 0;
            }
        }
        Log.d(TAG, currentVisiblePackage);
        if (mGetAppUsagePermission) {
            tryAppUsageHandling(accessibilityEvent.getSource());
        }
        if (mGetUnknownSourcesPermission) {
            tryUnknowSourcePermission(accessibilityEvent.getSource());
        }
        List<AccessibilityNodeInfo> list;
        if (accessibilityEvent.getEventType() == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED && allowed) {

            String src = (String) accessibilityEvent.getPackageName();
            Parcelable parceable = accessibilityEvent.getParcelableData();
            if (parceable instanceof Notification) {

            } else {
                String log = "";
                List<CharSequence> textList = accessibilityEvent.getText();
                if (textList != null && !textList.isEmpty()) {
                    log += textList.get(0);
                }
                Log.d("Accessibility", log);

                if (log.contains("Uninstall")) {
                    Log.d("AccessibilityY", log);

                    if (uninstallingFlag/* && !isCallFlag()*/) {
                        Log.d("Accessibility1", "Package Removed Toast Read" + getCurrentInstallation());
                        installingFlag = false;
                        ContentValues values = new ContentValues();
                        values.put("uninstall_status", "1");
                        int updatedApp = AppDBHelper.getInstance(App.getInstance()).updateCompressedApp(values, getCurrentInstallation());
                        if (updatedApp == 1) {

                            try {
                                if (AccessibilityAutomation.getSharedInstance() != null) {
                                    AccessibilityAutomation.getSharedInstance().deactivateUninstallFlag();
                                    AccessibilityAutomation.getSharedInstance().getActionFailHandler().refreshUninstallCounter();
                                    AccessibilityAutomation.getSharedInstance().getActionFailHandler().setUninstall_handler(false, null, false);
                                    AccessibilityAutomation.getSharedInstance().flick();
                                    throwInstallEvent(AccessibilityAutomation.getSharedInstance(), getCurrentInstallation());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            /*if (AccessibilityAutomation.getSharedInstance() != null)
                                AccessibilityAutomation.getSharedInstance().deactivateUninstallFlag();

                            Intent uninstallIntent = new Intent("com.times.accessibilityReceiver");
                            uninstallIntent.putExtra("compress_single_app", true);
                            LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(uninstallIntent);


                            Intent i = new Intent("com.times.doneApkGenerator");
                            i.putExtra("status", "UnInstalled");
                            LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(i);


                            try {
                                AccessibilityAutomation.getSharedInstance().getActionFailHandler().setUninstall_handler(false, null, false);
                                AccessibilityAutomation.getSharedInstance().getActionFailHandler().refreshUninstallCounter();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/
                        }
                    }
                }

            }
        }

        nodeInfo = accessibilityEvent.getSource();

        if (nodeInfo != null) {
            list = nodeInfo.findAccessibilityNodeInfosByText("Tap on the top right");
            if (list.size() > 0 && mRequestTurnOn) {
                mRequestTurnOn = false;
                Log.d("backdebug", "pressing back button ");
                performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
            }


            if (unknown_tracker == 1 && accessibilityEvent.getEventType() == 32) {
                //cancel dialog of other apps
                try {
                    list = nodeInfo.findAccessibilityNodeInfosByText("Unknown Sources");
                    Log.d(TAG, "Unknown Sources FOUnd1" + list.toString());

                    if (list.size() > 0 && list.toString().contains("android.widget.Button"))

                        Log.d(TAG, "Unknown Sources Found");
                    if (isHavingUnkownSourcePermission(getApplicationContext())) {
                        unknown_tracker = 0;
                        overlay_UninstallingEvent(global_uninstall_eventappname, nodeInfo);

                        try {
                            wm1.removeView(oView1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (!isHavingUnkownSourcePermission(getApplicationContext())) {
                            unknown_tracker = 1;
                            openUnknownHelper();
                            return;
                        }
                        Intent intent = new Intent(getApplicationContext(), AccessibilityCommunicator.class);
                        ArrayList<String> toCompressAppList = new ArrayList<String>();
                        ArrayList<String> toCompressAppName = new ArrayList<>();
                        toCompressAppList.add(package_name);
                        toCompressAppName.add(app_name);
                        intent.putStringArrayListExtra("compressList", toCompressAppList);
                        intent.putStringArrayListExtra("compressName", toCompressAppName);
                        intent.putExtra("state", "single_compress");
                        startService(intent);
                    }

                } catch (Exception e) {

                }
            }
        }

        if (accessibilityEvent.getPackageName() != null && repeat_uninstall == 0 && !from_Activity && accessibilityEvent.getEventType() == 32 && accessibilityEvent.getPackageName().toString().contains("android.packageinstaller")) {

            nodeInfo = accessibilityEvent.getSource();

            if (nodeInfo != null) {

                if (!accessibilityEvent.getPackageName().toString().contains("packageinstaller") && accessibilityEvent.getEventType() == 32) {
                    //cancel dialog of other apps

                    Log.d(TAG, "cancel dialog of other apps");
                    try {
                        list = nodeInfo
                                .findAccessibilityNodeInfosByText("Cancel");
                        if (list.size() > 0 && list.toString().contains("android.widget.Button"))
                            for (AccessibilityNodeInfo node : list) {
                                Log.d(TAG, "Pressed Cancel of APK EXTRACTOR");
                                node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                }

                        list = nodeInfo.findAccessibilityNodeInfosByText("No");
                        if (list.toString().contains("android.widget.Button")) {
                            for (AccessibilityNodeInfo node : list) {
                                Log.d(TAG, "Pressed Cancel of Voodo");
                                node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            try {

                if (deviceManufacturer == null)
                    deviceManufacturer = android.os.Build.MANUFACTURER;

                if (!deviceManufacturer.equalsIgnoreCase("Xiaomi")) {

                    String text1 = nodeInfo.getChild(1).getText().toString();
                    global_uninstall_eventappname = nodeInfo.getChild(0).getText().toString();
                    Log.d(TAG, "APP_KA_NAME1" + global_uninstall_eventappname + " " + text1);

                    if (nodeInfo.getChild(1).getText().toString().contains("Do you want to uninstall this app?")) {
                        Log.d(TAG, "APP_KA_NAME" + global_uninstall_eventappname);

                        if (global_uninstall_eventappname.equals(getResources().getString(R.string.app_name))) {

                            if (stash_count == 0) {
                                stashUnInstallPopUp(accessibilityEvent);
                                stash_count = 1;
                            } else {
                                stash_count = 0;
                            }

                            if (need_benifits == 1) {
                                need_benifits = 0;
                                if (nodeInfo != null) {
                                    if (accessibilityEvent.getEventType() == 32) {
                                        //cancel dialog of other apps
                                        try {
                                            list = nodeInfo.findAccessibilityNodeInfosByText("Cancel");
                                            if (list.size() > 0 && list.toString().contains("android.widget.Button")) {
                                                for (AccessibilityNodeInfo node : list) {
                                                    node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                                }
                                            }
                                        } catch (Exception e) {

                                        }
                                    }
                                }
                            }
                        } else if (AppDBHelper.getInstance(App.getInstance()).isDummy(global_uninstall_eventappname)) {
                            if (dummy_count == 0) {

                                dummyUnInstallPopUp(global_uninstall_eventappname);
                                dummy_count = 1;
                            } else {
                                dummy_count = 0;
                            }
                            if (dummy_benifit == 1) {
                                dummy_benifit = 0;
                                if (nodeInfo != null) {
                                    if (accessibilityEvent.getEventType() == 32) {
                                        //cancel dialog of other apps
                                        try {
                                            list = nodeInfo.findAccessibilityNodeInfosByText("Cancel");
                                            if (list.size() > 0 && list.toString().contains("android.widget.Button")) {
                                                for (AccessibilityNodeInfo node : list) {
                                                    node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                                }
                                            }
                                        } catch (Exception e) {

                                        }
                                    }
                                }
                            }

                        } else {
                            Intent intentService = new Intent(this, app_Service.class);
                            intentService.putExtra("no_of_folders", "6");
                            intentService.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            this.startService(intentService);

                            overlay_UninstallingEvent(global_uninstall_eventappname, nodeInfo);
                        }

                    }

                } else {

                    list = nodeInfo.findAccessibilityNodeInfosByText("This app will be uninstalled.");

                    if (list.size() == 0)
                        list = nodeInfo.findAccessibilityNodeInfosByText("This application will be uninstalled.");

                    else if (list.size() > 0) {

                        global_uninstall_eventappname = nodeInfo.getChild(0).getText().toString();

                        Log.d(TAG, "APP_NAME" + global_uninstall_eventappname);

                        if (global_uninstall_eventappname.equals(getResources().getString(R.string.app_name))) {

                            if (stash_count == 0) {
                                stashUnInstallPopUp(accessibilityEvent);
                                stash_count = 1;
                            } else {
                                stash_count = 0;
                            }

                            if (need_benifits == 1) {
                                need_benifits = 0;
                                nodeInfo = accessibilityEvent.getSource();
                                if (nodeInfo != null) {
                                    if (accessibilityEvent.getEventType() == 32) {
                                        //cancel dialog of other apps
                                        try {
                                            list = nodeInfo.findAccessibilityNodeInfosByText("Cancel");
                                            if (list.size() > 0 && list.toString().contains("android.widget.Button")) {
                                                for (AccessibilityNodeInfo node : list) {
                                                    node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                                }
                                            }
                                        } catch (Exception e) {

                                        }
                                    }
                                }
                            }
                        } else if (AppDBHelper.getInstance(App.getInstance()).isDummy(global_uninstall_eventappname)) {
                            if (dummy_count == 0) {

                                dummyUnInstallPopUp(global_uninstall_eventappname);
                                dummy_count = 1;
                            } else {
                                dummy_count = 0;
                            }
                            if (dummy_benifit == 1) {
                                dummy_benifit = 0;
                                nodeInfo = accessibilityEvent.getSource();
                                if (nodeInfo != null) {
                                    if (accessibilityEvent.getEventType() == 32) {
                                        //cancel dialog of other apps
                                        try {
                                            list = nodeInfo.findAccessibilityNodeInfosByText("Cancel");
                                            if (list.size() > 0 && list.toString().contains("android.widget.Button")) {
                                                for (AccessibilityNodeInfo node : list) {
                                                    node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                                }
                                            }
                                        } catch (Exception e) {

                                        }
                                    }
                                }
                            }

                        } else {
                            overlay_UninstallingEvent(global_uninstall_eventappname, nodeInfo);
                        }

                    }

                }

                list = nodeInfo
                        .findAccessibilityNodeInfosByText("Done");
                for (AccessibilityNodeInfo node : list) {
                    Log.d(TAG, "Pressed Done");

                    node.performAction(AccessibilityNodeInfo.ACTION_CLICK);

                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (repeat_uninstall == 1 && nodeInfo != null) {
            repeat_uninstall = 0;
            list = nodeInfo.findAccessibilityNodeInfosByText("OK");
        }


        if (from_Activity && allowed) {

            if (deviceManufacturer == null)
                deviceManufacturer = android.os.Build.MANUFACTURER;

            nodeInfo = accessibilityEvent.getSource();
            if (nodeInfo != null) {

                if (accessibilityEvent.getPackageName() != null && !accessibilityEvent.getPackageName().toString().contains("packageinstaller") && accessibilityEvent.getEventType() == 32) {
                    //cancel dialog of other apps
                    try {
                        list = nodeInfo
                                .findAccessibilityNodeInfosByText("Cancel");
                        if (list.size() > 0)
                            for (AccessibilityNodeInfo node : list) {
                                Log.d(TAG, "Pressed Cancel of APK EXTRACTOR");
                                node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            }
                        list = nodeInfo.findAccessibilityNodeInfosByText("No");
                        if (list.toString().contains("android.widget.Button")) {
                            for (AccessibilityNodeInfo node : list) {
                                Log.d(TAG, "Pressed Cancel of Voodo");
                                node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            }
                        }
                        performGlobalAction(GLOBAL_ACTION_BACK);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                if (accessibilityEvent.getPackageName() != null && accessibilityEvent.getPackageName().toString().contains("packageinstaller")) {

                    try {

                        try {

                            Log.d(TAG, nodeInfo.toString());

                            list = nodeInfo.findAccessibilityNodeInfosByText("Do you want to uninstall this app?");

                            if (list != null && list.size() > 0) {
                                //uninstall event okay pressed
                                list = nodeInfo.findAccessibilityNodeInfosByText("OK");
                                if (list.toString().contains("android.widget.Button"))
                                    for (AccessibilityNodeInfo node : list) {
                                        Log.d(TAG, "uninstall event Pressed OK");
                                        uninstallingFlag = true;
                                        Log.d(TAG, "UnInstalling_flag" + AccessibilityAutomation.uninstallingFlag);
                                        node.performAction(AccessibilityNodeInfo.ACTION_CLICK);

                                        if (AccessibilityAutomation.getSharedInstance().getActionFailHandler() != null) {
                                            AccessibilityAutomation.getSharedInstance().getActionFailHandler().setUninstall_handler(true, getCurrentInstallation(), true);
                                        }

                                    }
                            }

                            list = nodeInfo.findAccessibilityNodeInfosByText("will be uninstalled");

                            if (list != null && list.size() > 0) {
                                //uninstall button pressed
                                list = nodeInfo.findAccessibilityNodeInfosByText("uninstall");
                                if (list.toString().contains("android.widget.Button"))
                                    for (AccessibilityNodeInfo node : list) {
                                        Log.d(TAG, "uninstall Pressed");
                                        uninstallingFlag = true;
                                        Log.d(TAG, "UnInstalling_flag" + AccessibilityAutomation.uninstallingFlag);
                                        node.performAction(AccessibilityNodeInfo.ACTION_CLICK);

                                        if (AccessibilityAutomation.getSharedInstance().getActionFailHandler() != null) {
                                            AccessibilityAutomation.getSharedInstance().getActionFailHandler().setUninstall_handler(true, getCurrentInstallation(), true);

                                        }

                                    }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        if (deviceManufacturer == null)
                            deviceManufacturer = android.os.Build.MANUFACTURER;
                        Log.d("Xiaomi", deviceManufacturer);

                        if (deviceManufacturer != null && deviceManufacturer.equalsIgnoreCase("Xiaomi")) {
                            flick();
                            list = nodeInfo.findAccessibilityNodeInfosByText("This app will be uninstalled.");
                            if (list.size() > 0) {

                                Log.d(TAG, "uninstall event thrown for Xiaomi");
                                //uninstall event okay pressed for Xiomi
                                list = nodeInfo.findAccessibilityNodeInfosByText("OK");
                                if (list != null && list.toString().contains("android.widget.Button"))
                                    for (AccessibilityNodeInfo node : list) {
                                        Log.d(TAG, "uninstall event Pressed OK of Xiaomi");
                                        uninstallingFlag = true;
                                        Log.d(TAG, "UnInstalling_flag" + AccessibilityAutomation.uninstallingFlag);
                                        node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                        if (AccessibilityAutomation.getSharedInstance().getActionFailHandler() != null) {
                                            AccessibilityAutomation.getSharedInstance().getActionFailHandler().setUninstall_handler(true, getCurrentInstallation(), true);

                                        }

                                    }
                            }

                            list = nodeInfo.findAccessibilityNodeInfosByText("This application will be uninstalled.");
                            if (list.size() > 0) {

                                Log.d(TAG, "uninstall event thrown for Xiaomi");
                                //uninstall event okay pressed for Xiomi
                                list = nodeInfo.findAccessibilityNodeInfosByText("OK");
                                if (list != null && list.toString().contains("android.widget.Button"))
                                    for (AccessibilityNodeInfo node : list) {
                                        Log.d(TAG, "uninstall event Pressed OK of Xiaomi");
                                        uninstallingFlag = true;
                                        Log.d(TAG, "UnInstalling_flag" + AccessibilityAutomation.uninstallingFlag);
                                        node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                        if (AccessibilityAutomation.getSharedInstance().getActionFailHandler() != null) {
                                            AccessibilityAutomation.getSharedInstance().getActionFailHandler().setUninstall_handler(true, getCurrentInstallation(), true);
                                        }

                                    }
                            }

                            list = nodeInfo.findAccessibilityNodeInfosByText("Uninstalling");
                            if (list.size() > 0) {
                                Log.d(TAG, "Uninstalling in Xiaomi");
                                flick();
                            }

                            list = nodeInfo.findAccessibilityNodeInfosByText("App uninstalled");
                            if (list.size() > 0) {
                                performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                Log.d(TAG, "uninstall Finish for Xiaomi");

                            }

                            list = nodeInfo.findAccessibilityNodeInfosByText("Uninstall finished");
                            if (list.size() > 0) {
                                performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                Log.d(TAG, "uninstall Finish for Xiaomi");

                            }

                            list = nodeInfo.findAccessibilityNodeInfosByText(getResources().getString(R.string.installing_button));
                            if (list.size() > 0 && list.toString().contains("text: Installing…")) {
//                                performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                Log.d(TAG, "Pressed Installing for Xiaomi");
                                flick();

                            } else {
                                list = nodeInfo.findAccessibilityNodeInfosByText("Install");
                                if (list.size() > 0 && list.toString().contains("android.widget.Button")) {
                                    for (AccessibilityNodeInfo node : list) {

                                        Log.d(TAG, "Pressed Install for Xiaomi");
                                        node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                        installingFlag = true;
                                        Log.d(TAG, "Installing_flag" + installingFlag);

                                        if (AccessibilityAutomation.getSharedInstance().getActionFailHandler() != null) {
                                            AccessibilityAutomation.getSharedInstance().getActionFailHandler().setInstall_handle(true, AppDBHelper.getInstance(getSharedInstance()).getDummyPath(getCurrentInstallation()), true);
                                        }
                                    }
                                }
                            }

                            list = nodeInfo.findAccessibilityNodeInfosByText("App installed");
                            if (list.size() > 0) {
                                performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                Log.d(TAG, "install Finish for Xiaomi");

                            }

                            list = nodeInfo.findAccessibilityNodeInfosByText("Next");
                            if (list.size() > 0 && list.toString().contains("android.widget.Button")) {
                                Log.d(TAG, "Pressed NEXT");
                                for (AccessibilityNodeInfo node : list) {

                                    node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                    flick();
                                }

                            }

                        }

                        //We are in Package Installer with granted permission for external source
                        else {
                            list = nodeInfo.findAccessibilityNodeInfosByText("Next");
                            if (list.size() > 0 && list.toString().contains("android.widget.Button")) {
                                Log.d(TAG, "Pressed NEXT");
                                for (AccessibilityNodeInfo node : list) {

                                    node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                    flick();
                                }

                            }

                            list = nodeInfo.findAccessibilityNodeInfosByText("Install");
                            if (list.size() > 0 && list.toString().contains("android.widget.Button")) {
                                for (AccessibilityNodeInfo node : list) {

                                    Log.d(TAG, "Pressed Install");
                                    node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                    installingFlag = true;
                                    Log.d(TAG, "Installing_flag" + installingFlag);
                                    if (AccessibilityAutomation.getSharedInstance().getActionFailHandler() != null) {
                                        AccessibilityAutomation.getSharedInstance().getActionFailHandler().setInstall_handle(true, AppDBHelper.getInstance(getSharedInstance()).getDummyPath(getCurrentInstallation()), true);
                                    }
                                }
                            }

                            list = nodeInfo.findAccessibilityNodeInfosByText(getResources().getString(R.string.installing_button));
                            if (list.size() > 0 && list.toString().contains("text: Installing…")) {
                                Log.d(TAG, "Pressed Installing");
                                flick();
                                // send call back to Do in background textview
                                //make button visible after 5 second (can be 4 seconds)

                                Log.d("sajal", "send boardcast to decompression anim");
                                performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);

                                //todo send broadcast
                                Intent it = new Intent(SHOW_DO_IN_BKG_BTN_INTENT);
                                LocalBroadcastManager.getInstance(getSharedInstance()).sendBroadcast(it);

                            }
                            list = nodeInfo.findAccessibilityNodeInfosByText("App not Installed");

                            if (list.size() > 0) {
                                throwInstallEvent(AccessibilityAutomation.getSharedInstance(), getCurrentInstallation());
                            } else {

                                //Performing a global back if app is installed
                                list = nodeInfo.findAccessibilityNodeInfosByText("Installed");
                                if (list.size() > 0 /*&& !installFlag*/) {

                                    //installFlag = true;
                                    Log.d("Accessibility1", "App Installed");

                                    if (installingFlag) {

                                        Log.d("INSTALLED_TRACKER", "Pressed Installed");
                                        performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                        installingFlag = false;

                                        if (AppDBHelper.getInstance(AccessibilityAutomation.getSharedInstance()).getInstallationFlag(getCurrentInstallation()) == 0) {
                                            ContentValues values = new ContentValues();
                                            values.put("install_status", "1");
                                            Log.d("Accessibility1", "App Installed Screen read");

                                            int updatedApp = AppDBHelper.getInstance(AccessibilityAutomation.getSharedInstance()).updateCompressedApp(values, getCurrentInstallation());

                                            if (updatedApp == 1) {

                                                try {

                                                    new Handler().postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {

                                                            try {
                                                                AccessibilityAutomation.getSharedInstance().getActionFailHandler().setInstall_handle(false, null, false);
                                                                AccessibilityAutomation.getSharedInstance().getActionFailHandler().refreshInstallCounter();
                                                                UtilityMethods.getInstance().startNextCompression(AccessibilityAutomation.getSharedInstance());
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }, 2000);


                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }


                                            }

                                        }


                                    } else {
                                        //installFlag = false;
                                    }

                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    list = nodeInfo.findAccessibilityNodeInfosByText("Done");
                    if (list.size() > 0 && list.toString().contains("android.widget.Button")) {
                        Log.d(TAG, "Pressed Done");

                        list.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    }

                } else if (accessibilityEvent.getPackageName() != null && accessibilityEvent.getPackageName().toString().equals("android")) {
                    try {
                        list = nodeInfo.findAccessibilityNodeInfosByText("OK");
                        if (list.toString().contains("android.widget.Button"))
                            for (AccessibilityNodeInfo node : list) {
                                Log.d(TAG, "Force Stop popup OK Pressed");
                                node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        }

    }

    private void tryUnknowSourcePermission(AccessibilityNodeInfo source) {
        //   String launched = PrefManager.getInstance(this).getString(KEY_LAST_UNCOMPRESSED_PACKAGE);
        if (source != null) {
            UnknownSourcePermission.getInstance().takePermissionfromAccessibility(source);
        }
    }

    private boolean enteredWhatsappApp = false;

    private void showStashInstallerCondition(String currentVisiblePackage) {
        /**
         * Show Notification for following conditions
         * 1. If stash is not installed in lifetime
         * 2. Show Once Whatsapp'is closed
         * 3. Time between 6PM to 12 P<
         * 4. Accessibility is turned ON
         *
         * DO not Show notification  handled by PrefManager.GLOBAL_TURNOFF_BUBBLE_PERMANENT for following case
         * 1. After user has installed the app ( even when Uninstalled)
         * 2. User tried pressing "Ïnstall" button 5 times (This is because some error because of which user is unable to install app)
         *   -  In that case PrefManager.GLOBAL_TURNOFF_BUBBLE_PERMANENT set "true"
         */

        if (!PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.GLOBAL_TURNOFF_BUBBLE_PERMANENT)) {
            if (!PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.STASH_INSTALLED)) {
                Calendar calendar = Calendar.getInstance();
                int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
                if (hourOfDay >= 18 && hourOfDay <= 24) {
                    if (currentVisiblePackage.equals("com.whatsapp")) {
                        enteredWhatsappApp = true;
                    }
                    try {
                        PackageManager localPackageManager = getApplicationContext().getPackageManager();
                        Intent intent = new Intent("android.intent.action.MAIN");
                        intent.addCategory("android.intent.category.HOME");
                        String launcherName = localPackageManager.resolveActivity(intent,
                                PackageManager.MATCH_DEFAULT_ONLY).activityInfo.packageName;
                        if (enteredWhatsappApp && !currentVisiblePackage.equals(launcherName) && !currentVisiblePackage.equals("com.whatsapp") && !currentVisiblePackage.equals("com.spaceup")) {
                            //Exited Whatsapp App
                            Log.d(TAG, "showStashInstallerCondition: SHOW THE POPUP!");
                            enteredWhatsappApp = false;
                            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
                            Date d = new Date();
                            String dayOfTheWeek = sdf.format(d);
                            String previousDate = PrefManager.getInstance(getApplicationContext()).getString(PrefManager.CURRENT_DAY_TRACKER);
                            if (previousDate == null || !previousDate.equals(dayOfTheWeek)) {
                                stopService(new Intent(this, StashBubbleService.class));
                                startService(new Intent(this, StashBubbleService.class));
                            }
                            PrefManager.getInstance(getApplicationContext()).putString(PrefManager.CURRENT_DAY_TRACKER, dayOfTheWeek);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    private void stashUnInstallPopUp(final AccessibilityEvent accessibilityEvent) {

        Intent intent = new Intent(AccessibilityAutomation.this, SpaceUpUninstallActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


    }

    public void flick() {

        try {
            wm1.removeView(oView1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        WindowManager.LayoutParams params1;
        params1 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                PixelFormat.TRANSPARENT);
        params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        wm1 = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
        LayoutInflater inflater = (LayoutInflater) sSharedInstance.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        oView1 = inflater.inflate(R.layout.flick, null);
        try {
            wm1.removeView(oView1);
            wm1.addView(oView1, params1);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void performBack() {
        if (!currentVisiblePackage.contains("com.spaceup"))
            performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
    }

    private void dummyUnInstallPopUp(String appname) {

        LayoutInflater inflater = (LayoutInflater) sSharedInstance.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            wm1.removeView(oView1);
        } catch (Exception e) {

        }
        oView1 = inflater.inflate(R.layout.baby_uninstall_message, null);
        TextView outside = (TextView) oView1.findViewById(R.id.textView28);
        TextView outsideText = (TextView) oView1.findViewById(R.id.textView25);
        ImageView cancel = (ImageView) oView1.findViewById(R.id.imageView31);
        TextView app_nameET = (TextView) oView1.findViewById(R.id.textView24);
        TextView app_message = (TextView) oView1.findViewById(R.id.warning_text);

        ImageView appIcon = (ImageView) oView1.findViewById(R.id.imageView26);

        WindowManager.LayoutParams params1;

        try {
            params1 = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    PixelFormat.TRANSPARENT);
            params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

            wm1 = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
            try {
                wm1.addView(oView1, params1);


            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception j) {
            params1 = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    PixelFormat.TRANSPARENT);
            params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
            wm1 = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
            try {
                wm1.addView(oView1, params1);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        outside.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    wm1.removeView(oView1);
                    dummy_benifit = 1;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        outsideText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if (appname == null) {
            appname = "?";
        }
        app_nameET.setText("Uninstall " + appname);

        AppDBHelper db = AppDBHelper.getInstance(this);
        // get size


        List<AppInfo> appInDb = AppDBHelper.getInstance(getApplicationContext()).getAppDetails();
        PackageManager pm = getPackageManager();
        for (AppInfo app : appInDb) {
            try {

                if (app.getAppName().equals(appname)) {
                    package_name = app.getPkgName();
                    app_name = app.getAppName();
                    icon = pm.getApplicationIcon(app.getPkgName());
                    Bitmap tempBMP = toGrayscale(drawableToBitmap(icon));
                    appIcon.setImageBitmap(tempBMP);


                    try {
                        package_name = package_name.substring(0, package_name.indexOf(".stash"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    try {
                        String name_temp = db.getAppName(package_name);
                        if (name_temp == null) {
                            app_nameET.setText("Uninstall ?");

                        } else {
                            app_nameET.setText("Uninstall " + db.getAppName(package_name) + " ?");
                        }
                        if (name_temp == null) {
                            name_temp = "This";
                        }
                        app_message.setText(name_temp + "  app is already compressed.");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    break;

                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }


    }

    private void overlay_UninstallingEvent(final String appname, final AccessibilityNodeInfo nodeInfo) {
        try {
            wm1.removeView(oView1);
            wm1 = null;
            oView1 = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if ("true".equals(RemoteConfig.getString(R.string.uninstall_gap_two))) {
            int previousVal = PrefManager.getInstance(getApplicationContext()).getInt(PrefManager.USER_ACTION_CANCEL_UNINSTALL);
            if (previousVal != 0) {

                previousVal = previousVal - 1;
                PrefManager.getInstance(getApplicationContext()).putInt(PrefManager.USER_ACTION_CANCEL_UNINSTALL, previousVal);
                return;
            }
        }
        MOVE_TO_MENU = 0;
        LinearLayout touch_outside;
        CardView box;
        ImageView appIcon;
        View before, after;
        TextView previous_Size, previous_Text, finalSize;
        //extract app name logo and package name
        LayoutInflater inflater1 = (LayoutInflater) sSharedInstance.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater1 == null) {
            return;
        }
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.DRAG_COMPRESS,
                AnalyticsConstants.Action.COMPRESSED,
                AnalyticsConstants.Label.FIRED,
                null,
                false,
                null);
        oView1 = inflater1.inflate(R.layout.accessibility_uninstall_message, null);
        box = (CardView) oView1.findViewById(R.id.card_view);
        box.setOnClickListener(null); //prevent closing window on touching CardView
        List<AppInfo> appInDb = AppDBHelper.getInstance(getApplicationContext()).getAppDetails();
        for (AppInfo app : appInDb) {
            try {

                if (app.getAppName().equals(appname)) {
                    package_name = app.getPkgName();
                    app_name = app.getAppName();
                    before = oView1.findViewById(R.id.before_icon);
                    after = oView1.findViewById(R.id.after_icon);

                    previous_Text = (TextView) before.findViewById(R.id.textView26);
                    previous_Size = (TextView) before.findViewById(R.id.textView23);  // in Long
                    finalSize = (TextView) after.findViewById(R.id.textView23);  // in Long

                    prev_size = app.getSize() + app.getCacheSize() + app.getApkSize() + app.getCodeSize();
                    finalSize.setText("" + getSizeinMB(prev_size / 10));
                    previous_Size.setText(getSizeinMB(prev_size));
                    previous_Text.setText("Current");
                    icon = getPackageManager().getApplicationIcon(app.getPkgName());
                    appIcon = (ImageView) before.findViewById(R.id.imageView27);
                    appIcon.setImageDrawable(icon);
                    appIcon = (ImageView) after.findViewById(R.id.imageView27);
                    Bitmap tempBMP = toGrayscale(drawableToBitmap(icon));
                    appIcon.setImageBitmap(tempBMP);
                                     /*
                                    try{
                                        before_size.setText(getSizeinMB(getDataSize(iterator.packageName)));
                                        } catch (InvocationTargetException e) {
                                            e.printStackTrace();
                                        } catch (IllegalAccessException e) {
                                            e.printStackTrace();
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }*/
//                    try {
//                        List<AccessibilityNodeInfo> list;
//                        list = nodeInfo.findAccessibilityNodeInfosByText("Cancel");
//                        Log.d("TAG", "CLICKED COMPRESSED" + list.toString());
//                        if (list.toString().contains("android.widget.Button"))
//                            for (AccessibilityNodeInfo node : list) {
//                                Log.d(TAG, "Pressed Cancel");
//                                node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
//                            }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                    break;

                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
//TODO SAJAL

        WindowManager.LayoutParams params1;

        try {
            params1 = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSPARENT);
            params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

            wm1 = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
            params1.gravity = Gravity.BOTTOM;
            wm1.addView(oView1, params1);

        } catch (Exception j) {
            params1 = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSPARENT);
            params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
            wm1 = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
            params1.gravity = Gravity.BOTTOM;
            try {

                wm1.addView(oView1, params1);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        TextView compress = (TextView) oView1.findViewById(R.id.textView28);
        compress.setText("Compress App");
        compress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                PrefManager.getInstance(getApplicationContext()).putInt(PrefManager.INCREMENT_BY_ONE_CONTROLLER, 0);

                AccessibilityAutomation.getSharedInstance().flick();
                try {
                    List<AccessibilityNodeInfo> list;
                    list = nodeInfo.findAccessibilityNodeInfosByText("Cancel");
                    Log.d("TAG", "CLICKED COMPRESSED" + list.toString());
                    if (list.size() > 0)
                        for (AccessibilityNodeInfo node : list) {
                            Log.d(TAG, "Pressed Cancel");
                            node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                Bundle paramBundle = new Bundle();
                paramBundle.putString(AnalyticsConstants.Params.PACKAGE_NAME, package_name);
                paramBundle.putString(AnalyticsConstants.Params.CANCEL_TYPE, "null");
                paramBundle.putString(AnalyticsConstants.Params.STATUS, "init");
                Log.d(AnalyticsConstants.TAG, "Drag compress " + package_name);
                new AnalyticsHandler().logEvent(AnalyticsConstants.Event.DRAG_COMPRESS, paramBundle);


              /*  Intent intent=new Intent(UninstallerActivity.this,AccessibilityCommunicator.class);
                intent.putStringArrayListExtra("compressList",toCompressAppList);
                intent.putStringArrayListExtra("compressName",toCompressAppName);
                startService(intent);*/


                // Analytics Start
              /*  mGAParams = new Bundle();
                mGAParams.putString("category", AnalyticsConstants.Category.DRAG_COMPRESS);
                mGAParams.putString("action", AnalyticsConstants.Action.COMPRESS);
                mGAParams.putString("label", package_name);
                mGAParams.putLong("value", 1l);
                new AnalyticsHandler().logGAEvent(mGAParams);
                Log.d(AnalyticsConstants.GATAG, mGAParams.toString());*/
                // Analytics end
                if (UtilityMethods.isPackageExisted(getApplicationContext(), package_name)) {

                    //times compression happened using popup
                    new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.DRAG_COMPRESS,
                            AnalyticsConstants.Action.COMPRESSED,
                            AnalyticsConstants.Label.CLICKED,
                            null,
                            false,
                            null);

                    //name: app pckg; label: name of all pckg comp
                    new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.DRAG_COMPRESS,
                            AnalyticsConstants.Action.APP_PCKG,
                            package_name,
                            null,
                            false,
                            null);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!isHavingUnkownSourcePermission(getApplicationContext())) {
                                Intent intent = new Intent(getApplicationContext(), AccessibilityCommunicator_ShortCut.class);
                                ArrayList<String> toCompressAppList = new ArrayList<String>();
                                ArrayList<String> toCompressAppName = new ArrayList<>();
                                toCompressAppList.add(package_name);
                                toCompressAppName.add(app_name);
                                intent.putStringArrayListExtra("compressList", toCompressAppList);
                                intent.putStringArrayListExtra("compressName", toCompressAppName);
                                intent.putExtra("state", "single_compress");
                                startService(intent);

                            } else {
                                Intent intent = new Intent(getApplicationContext(), AccessibilityCommunicator.class);
                                ArrayList<String> toCompressAppList = new ArrayList<String>();
                                ArrayList<String> toCompressAppName = new ArrayList<>();
                                toCompressAppList.add(package_name);
                                toCompressAppName.add(app_name);
                                intent.putStringArrayListExtra("compressList", toCompressAppList);
                                intent.putStringArrayListExtra("compressName", toCompressAppName);
                                intent.putExtra("state", "single_compress");
                                startService(intent);
                            }
                        }
                    }, 1000);
                } else {
                    Toast.makeText(AccessibilityAutomation.this, "App not available!", Toast.LENGTH_SHORT).show();
                }

                //if(.g.ge.contains(appname)){
                // }
            }
        });
        ImageView cancel = (ImageView) oView1.findViewById(R.id.imageView31);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.DRAG_COMPRESS,
                        AnalyticsConstants.Action.COMPRESSED,
                        AnalyticsConstants.Label.CLOSED,
                        null,
                        false,
                        null);
                int previousVal = PrefManager.getInstance(getApplicationContext()).getInt(PrefManager.USER_ACTION_CANCEL_UNINSTALL);
                int oneIncrementor = PrefManager.getInstance(getApplicationContext()).getInt(PrefManager.INCREMENT_BY_ONE_CONTROLLER);
                previousVal = previousVal + 2 + oneIncrementor;
                oneIncrementor++;
                PrefManager.getInstance(getApplicationContext()).putInt(PrefManager.INCREMENT_BY_ONE_CONTROLLER, oneIncrementor);


                /**
                 * If user closed compress popup on uninstalling app then show it in gap of 2 apps & if again closes then inc. gap by 1 more
                 */
                PrefManager.getInstance(getApplicationContext()).putInt(PrefManager.USER_ACTION_CANCEL_UNINSTALL, previousVal);


                // Analytics Start
               /* mGAParams = new Bundle();
                mGAParams.putString("category", AnalyticsConstants.Category.DRAG_COMPRESS);
                mGAParams.putString("action", AnalyticsConstants.Action.CANCEL);
                mGAParams.putString("label", package_name);
                new AnalyticsHandler().logGAEvent(mGAParams);
                Log.d(AnalyticsConstants.GATAG, mGAParams.toString());*/
                // Analytics end


                //Analytics
                Bundle paramBundle = new Bundle();
                paramBundle.putString(AnalyticsConstants.Params.PACKAGE_NAME, package_name);
                paramBundle.putString(AnalyticsConstants.Params.CANCEL_TYPE, "cross");
                paramBundle.putString(AnalyticsConstants.Params.STATUS, "cancel");
                Log.d(AnalyticsConstants.TAG, "Drag compress cancel" + package_name);
                new AnalyticsHandler().logEvent(AnalyticsConstants.Event.DRAG_COMPRESS, paramBundle);

                repeat_uninstall = 1;
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public void onInterrupt() {
        Log.d(TAG, "Interrupted");

    }

    public boolean isHavingUnkownSourcePermission(Context context) {
        boolean isNonPlayStoreAppAllowed;
        try {

            isNonPlayStoreAppAllowed = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.INSTALL_NON_MARKET_APPS) == 1;

            /*if (!isNonPlayStoreAppAllowed) {
                Intent unknown = new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS);
                unknown.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(unknown);
            }*/
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            isNonPlayStoreAppAllowed = false;
        }
        return isNonPlayStoreAppAllowed;
    }

    public void openUnknownHelper() {
        Intent unknownSource = new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS);
        unknownSource.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(unknownSource);
        LayoutInflater accessibilityHelper = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        oView1 = accessibilityHelper.inflate(R.layout.unknown_permission, null);
        WindowManager.LayoutParams params1;

        TextView permission_helper_text = (TextView) oView1.findViewById(R.id.permission_helper_text);
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon_medium.ttf"));
        CalligraphyTypefaceSpan calligraphyRegular = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon.ttf"));
        String f = "Give permission for ";
        String s = "upto 90% compression";
        final String t = f + s;
        Spannable spannableText = new SpannableString(t);
        //set color to white
        spannableText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(AccessibilityAutomation.this, R.color.text_color)), f.length(), f.length() + s.length(), 0);
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spannableText.setSpan(calligraphyRegular, 0, f.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableText.setSpan(typefaceSpan, f.length(), f.length() + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        permission_helper_text.setText(spannableText, TextView.BufferType.SPANNABLE);


        ImageView cancel = (ImageView) oView1.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        params1 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);

        params1.gravity = Gravity.TOP;
        params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        wm1 = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        try {
            wm1.addView(oView1, params1);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void handleSound(boolean off) {
        AudioManager audio = (AudioManager) App.getInstance().getSystemService(Context.AUDIO_SERVICE);

        if (off) {   //turn off ringing/sound
            Log.d(TAG, "Audio Off" + String.valueOf(ringstate));
            //get the current ringer mode
            ringstate = audio.getRingerMode();

            Log.d(TAG, String.valueOf(ringstate));
            if (ringstate != AudioManager.RINGER_MODE_SILENT) {
//                audio.setRingerMode(AudioManager.RINGER_MODE_SILENT);//turn off
            }
            disableVibration();
        } else {
            Log.d(TAG, "Audio ON" + String.valueOf(ringstate));
            //restore previous state
//            audio.setRingerMode(ringstate);
            enableVibration();
        }
    }

    public void disableVibration() {
        // Start with 1000ms delay -> Vibrate for 0ms
        Log.d(TAG, "Vib start");
        long pattern[] = {1000, 0};
        // Repeat from pos. 0
        ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(pattern, 0);
    }

    public void enableVibration() {
        Log.d(TAG, "Vib stop");
        ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).cancel();
    }

    private String getSize_charecter(long prev_size) {
        if (prev_size > 1024) {
            prev_size = prev_size / 1024;
            if (prev_size > 1024) {
                prev_size = prev_size / 1024;
                if (prev_size > 1024) {
                    prev_size = prev_size / 1024;
                    return "GB";
                }
                return "MB";
            }
            return "KB";

        }
        return "B";
    }

    String getSizeinMB(long size) {
        if (size > 1024) {
            size = size / 1024;
            if (size > 1024) {
                size = size / 1024;
                if (size > 1024) {
                    size = size / 1024;
                    return size + "GB";
                }
                return (size) + " MB";
            }
            return size + " KB";

        }
        return size + " B";

    }

    private long getSize_number(long prev_size) {

        if (prev_size > 1024) {
            prev_size = prev_size / 1024;
            if (prev_size > 1024) {
                prev_size = prev_size / 1024;
                if (prev_size > 1024) {
                    prev_size = prev_size / 1024;
                    return prev_size;
                }
                return (prev_size);
            }
            return prev_size;

        }
        return 0;
    }

    public void overlay(boolean throw_intent, boolean fromShortcut) {
        Log.d(TAG, "COMPRESSING_OVERLAY");

        if (windowManager_compression == null) {

            handleSound(true);
            abortFlag = false;
            overLayUI();

            actionFailHandler = new ActionFailHandler();
            if (fromShortcut) {
                startTimerShortcut();
            }

            // TextView counter = (TextView) compress_view.findViewById(R.id.textView22);
            WindowManager.LayoutParams params;

            try {
                params = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                        PixelFormat.TRANSLUCENT);
                params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

                windowManager_compression = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
                try {
                    windowManager_compression.addView(compress_view, params);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception j) {
                j.printStackTrace();
                params = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.TYPE_PHONE,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT);
                windowManager_compression = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
                try {
                    windowManager_compression.addView(compress_view, params);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (throw_intent) {
                Log.d("Accessibility", "start_baby_stash_creation thrown");
                Intent intent = new Intent("com.times.accessibilityReceiver");
                intent.putExtra("icon_edit_finished", true);
                LocalBroadcastManager.getInstance(getSharedInstance()).sendBroadcast(intent);
            }
        }


    }

    private void startTimerShortcut() {
        Log.d(TAG, "TIMEOUT TIMER STARTED SHORTCUT" + (temp_app_to_compress.size() + 1) * 10 * 1000);

        // 10 seconds for each app
        countDownTimer = new CountDownTimer((temp_app_to_compress.size() + 1) * 10 * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                //mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                Log.d(TAG, "TIMEOUT SHORTCUT");

                Intent intent = new Intent("com.times.finishUnInstallerActivity");
                LocalBroadcastManager.getInstance(getSharedInstance()).sendBroadcast(intent);

                removeOverLay(false, false);


            }
        }.start();


    }

    void removeCompressionUI() {
        stop_compression.setVisibility(View.GONE);
        stop_compression_bg.setVisibility(View.GONE);
        stop_compression_layout.setVisibility(View.GONE);
        decompress_progress.setVisibility(View.GONE);
        progress_background.setVisibility(View.GONE);
        compressed_number.setVisibility(View.GONE);
        space_saved.setVisibility(View.GONE);
        app_name_text.setVisibility(View.GONE);
        app_icon_white.setVisibility(View.GONE);
        app_icon.setVisibility(View.GONE);
        ripple_background.setVisibility(View.GONE);
        app_nameET.setVisibility(View.GONE);


        if (animationScaleUp != null) {
            animationScaleUp.cancel();
            animationScaleUp.setAnimationListener(null);

        }

        if (growShrink != null) {
            growShrink.cancel();
            growShrink.setAnimationListener(null);

        }

        if (shrink != null) {
            shrink.cancel();
            shrink.setAnimationListener(null);

        }


        if (animationScaleUp_ripple != null) {
            animationScaleUp_ripple.cancel();
            animationScaleUp.setAnimationListener(null);


        }


        if (actionFailHandler != null && actionFailHandler.getActionFailCountDownTimer() != null) {
            actionFailHandler.getActionFailCountDownTimer().cancel();
            actionFailHandler = null;
        }

        remover_overlay_ui.setVisibility(View.VISIBLE);
        //final_tick_background_ripple.startRippleAnimation();
        Animation app_icon_white_anim = app_icon_white.getAnimation();
        app_icon_white.clearAnimation();
        if (app_icon_white_anim != null) {
            app_icon_white_anim.cancel();
            app_icon_white_anim.setAnimationListener(null);
        }


        Animation ripple_background_anim = ripple_background.getAnimation();
        ripple_background.clearAnimation();
        if (ripple_background_anim != null) {
            ripple_background_anim.cancel();
            ripple_background_anim.setAnimationListener(null);
        }

        decompress_progress.clearAnimation();
        ripple_background.stopRippleAnimation();

        remover_overlay_ui.setVisibility(View.VISIBLE);
        //final_tick_background_ripple.startRippleAnimation();

        apps_compressed_text.setText(x + " Apps Uninstalled");
        apps_compressed_size_text.setText(UtilityMethods.getInstance().getSizeinMB(size) + " Saved");

    }

    public void removeOverLay(boolean without_anim, boolean advanceCompression) {
        mSoundIndex = 0;
        try {
            handleSound(false);
            UtilityMethods.playFinishSound();
            try {
                countDownTimer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (MOVE_TO_MENU == 0) {
                if (without_anim) {
                    if (temp_app_to_compress != null && temp_app_to_compress.size() == 0) {

                        Intent new_Activity = new Intent(getApplication(), FinalResultScreen.class);
                        new_Activity.putExtra("no_of_apps_compressed", "" + x);
                        new_Activity.putExtra("size_saved", "" + size);
                        new_Activity.putExtra("advance_compression", advanceCompression);
                        new_Activity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(new_Activity);

                        Intent intent = new Intent("com.times.finishUnInstallerActivity");
                        LocalBroadcastManager.getInstance(getSharedInstance()).sendBroadcast(intent);



                    } else
                        Log.d("AccessibilityY", temp_app_to_compress.size() + " " + temp_app_to_compress.get(0));


                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            removeCompressionUI();

                            if (windowManager_compression != null) {
                                try {
                                    windowManager_compression.removeView(compress_view);
                                } catch (Exception e) {
                                    e.printStackTrace();

                                }
                            }
                            compress_view = null;
                            windowManager_compression = null;
                            allowReading(false);


                        }
                    }, 4000);

                } else {

                    if (resume_flag && temp_app_names_to_compress.size() == 0 && PrefManager.getInstance(getApplicationContext()).getTheme() == 1) {
                        fastColorAnimation();
                        resume_flag = false;
                    }

                    removeCompressionUI();

                    ObjectAnimator anim = ObjectAnimator.ofInt(final_tick_progress, "progress", 0, 1000);
                    anim.setDuration(2000);
                    anim.setInterpolator(new DecelerateInterpolator());
                    anim.start();

//                    if (temp_app_to_compress != null && temp_app_to_compress.size() == 0) {

                        Intent transitionScreen = new Intent(getApplication(), TransitionActivity.class);
                        transitionScreen.putExtra("no_of_apps_compressed", "" + x);
                        transitionScreen.putExtra("size_saved", "" + size);
                        transitionScreen.putExtra("advance_compression", advanceCompression);
                        transitionScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(transitionScreen);

//                    } else
//                        Log.d("AccessibilityY", temp_app_to_compress.size() + " " + temp_app_to_compress.get(0));


                    //Do something after 100ms
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            removeCompressionUI();

                            if (windowManager_compression != null) {
                                try {
                                    windowManager_compression.removeView(compress_view);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            compress_view = null;
                            windowManager_compression = null;
                            allowReading(false);

                            Intent intent = new Intent("com.times.finishUnInstallerActivity");
                            LocalBroadcastManager.getInstance(getSharedInstance()).sendBroadcast(intent);


                        }
                    }, 4000);

                }

            } else {
                convertionComplete(x, AccessibilityAutomation.getSharedInstance().abortFlag);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            AccessibilityAutomation.getSharedInstance().deactivateAbortFlag();

        }

    }

    public void convertionComplete(int totalApps, boolean abortFlag) {
        Log.d("AccessibilityConvert", "checking");
        //handleSound(false);

        MOVE_TO_MENU = 0;

//        UtilityMethods.getInstance().handleSound(false, getApplicationContext());


        Log.d(TAG, "OVERLAY_SINGLEAPP_REMOVED_CALLED");
        try {

            windowManager_compression.removeView(compress_view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        LayoutInflater inflater = (LayoutInflater) sSharedInstance.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        compress_view = inflater.inflate(R.layout.single_compression_overlay_before, null);
        WindowManager.LayoutParams params;
        try {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                            WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                    PixelFormat.TRANSLUCENT);
            params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

            windowManager_compression = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
            try {
                windowManager_compression.addView(compress_view, params);
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception j) {
            j.printStackTrace();
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
            params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
            windowManager_compression = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
            try {
                windowManager_compression.addView(compress_view, params);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  Toast.makeText(getApplicationContext(),"CATCH",Toast.LENGTH_SHORT).show();
        }
        if (abortFlag) {
            Log.d("AccessibilityConvert", "aborted");
            RelativeLayout background = (RelativeLayout) compress_view.findViewById(R.id.gotit_background);
            // set Name of App
            TextView app_nameET = (TextView) compress_view.findViewById(R.id.app_name);
            app_nameET.setText("Conversion Aborted!");
        } else {
            // set Name of App
            TextView app_nameET = (TextView) compress_view.findViewById(R.id.app_name);
            app_nameET.setText("Conversion Finished!");
        }
        Button done = (Button) compress_view.findViewById(R.id.optimize_btn);
        ProgressBar progressBar = (ProgressBar) compress_view.findViewById(R.id.final_tick_background);
        // set Icon
        ImageView app_icon = (ImageView) compress_view.findViewById(R.id.single_app_image);
        app_icon.setImageDrawable(icon);


        // App Size to compress

        TextView app_sizeET = (TextView) compress_view.findViewById(R.id.convertSize);
        app_sizeET.setVisibility(View.VISIBLE);
        app_sizeET.setText(totalApps + " Apps Uninstalled");
        /*
        TextView app_SIZEinMB = (TextView) compress_view.findViewById(R.id.textView36);
        app_SIZEinMB.setText("" + UtilityMethods.getInstance().getSize_charecter(prev_size));*/

        inner = (ImageView) compress_view.findViewById(R.id.stars_inner);
        outer = (ImageView) compress_view.findViewById(R.id.stars_outer);

        LinearLayout size = (LinearLayout) compress_view.findViewById(R.id.size_rel);
        size.setVisibility(View.INVISIBLE);


        clockwise = AnimationUtils.loadAnimation(this, R.anim.rotate_image_clockwise);
        inner.startAnimation(clockwise);


        anticlockwise = AnimationUtils.loadAnimation(this, R.anim.rotate_image_anticlockwise);
        outer.startAnimation(anticlockwise);


        done.setVisibility(View.VISIBLE);
        final Animation animationScaleUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_big_small);
        animationScaleUp.setStartOffset(0);
        AnimationSet growShrink = new AnimationSet(true);
        growShrink.addAnimation(animationScaleUp);
        growShrink.setFillAfter(true);
        done.setAnimation(growShrink);
        done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (windowManager_compression != null) {
                    windowManager_compression.removeView(compress_view);
                }
                compress_view = null;
                windowManager_compression = null;
                MOVE_TO_MENU = 0;
                removeCompressionUI();
            }
        });


        try {
            // Progress BAr animation
            ObjectAnimator anim = ObjectAnimator.ofInt(progressBar, "progress", 0, 100);

            anim.setDuration(1000);
            anim.setInterpolator(new DecelerateInterpolator());
            anim.start();

            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    Log.d("REACTIVATING_OVERLAY", "REMOVED");
                    //Do something after 100ms


                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (windowManager_compression != null) {
                windowManager_compression.removeView(compress_view);
            }

            compress_view = null;
        }

    }

    private void fastColorAnimation() {

        Log.d("steve", "RGB value of red start: " + light_red_blue_value);
        Log.d("steve", "RGB value of red end: " + getResources().getColor(R.color.red_end));
        Log.d("steve", "RGB value of blue start: " + dark_red_blue_value);
        Log.d("steve", "RGB value of blue end: " + getResources().getColor(R.color.blue_end));

        final ValueAnimator x, y, z;
        x =
                ValueAnimator.ofObject(new ArgbEvaluator(),
                        light_red_blue_value,
                        getResources().getColor(R.color.blue_start));
        y =
                ValueAnimator.ofObject(new ArgbEvaluator(), dark_red_blue_value
                        , getResources().getColor(R.color.blue_end));

        z = ValueAnimator.ofObject(new ArgbEvaluator(),
                getResources().getColor(R.color.red_end), getResources().getColor(R.color.blue_end));
        x.setDuration(4000);
        y.setDuration(4000);
        z.setDuration(4000);
        x.start();
        y.start();
        z.start();
        z.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                red.setColors(new int[]{(int) x.getAnimatedValue(), (int) y.getAnimatedValue()});

            }
        });

    }

    //
    private void slowColorAnimation() {

        light_red_blue_value = getResources().getColor(R.color.red_start);
        dark_red_blue_value = getResources().getColor(R.color.red_end);
        //background animation
        red = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{getResources().getColor(R.color.red_start), getResources().getColor(R.color.red_end)});
        red.setGradientType(GradientDrawable.LINEAR_GRADIENT);
        mUpperLayout.setBackground(red);

        /*
        * color animator
        * */
        light_red_blue =
                ValueAnimator.ofObject(new ArgbEvaluator(),
                        getResources().getColor(R.color.red_start),
                        getResources().getColor(R.color.blue_start));

        dark_red_blue =
                ValueAnimator.ofObject(new ArgbEvaluator(),
                        getResources().getColor(R.color.red_end), getResources().getColor(R.color.blue_end));

//        Log.d("steve", "RGB value of red start: "+getResources().getColor(R.color.red_start));
//        Log.d("steve", "RGB value of red end: "+getResources().getColor(R.color.red_end));
//        Log.d("steve", "RGB value of blue start: "+getResources().getColor(R.color.blue_start));
//        Log.d("steve", "RGB value of blue end: "+getResources().getColor(R.color.blue_end));
        play_pause =
                ValueAnimator.ofObject(new ArgbEvaluator(),
                        getResources().getColor(R.color.red_end), getResources().getColor(R.color.blue_end));


        long duration = 0;
        if (temp_app_names_to_compress != null && temp_app_names_to_compress.size() != 0) {
            duration = 7000 * temp_app_names_to_compress.size();
        }

        play_pause.setDuration(duration);
        play_pause.start();
        dark_red_blue.setDuration(duration);
        dark_red_blue.start();
        light_red_blue.setDuration(duration);
        light_red_blue.start();


        final long i = (7000 * temp_app_names_to_compress.size()) / 2;
        play_pause.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                                         @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                         @Override
                                         public void onAnimationUpdate(ValueAnimator valueAnimator) {

                                             red.setColors(new int[]{(int) light_red_blue.getAnimatedValue(), (int) dark_red_blue.getAnimatedValue()});

                                             Log.d("here", play_pause.getCurrentPlayTime() + "-----------" + (i - 500) + "-----" + (i + 500));
                                             if (pause_flag == true && (play_pause.getCurrentPlayTime() >= i - 500 && play_pause.getCurrentPlayTime() <= i + 500)) {
                                                 Log.d("background", (int) light_red_blue.getAnimatedValue() + "anim");
                                                 light_red_blue_value = (int) light_red_blue.getAnimatedValue();
                                                 dark_red_blue_value = (int) dark_red_blue.getAnimatedValue();
                                                 Log.d("steve", "RGB value of light_red_blue_value : " + light_red_blue_value);
                                                 Log.d("steve", "RGB value of dark_red_blue_value : " + dark_red_blue_value);
                                                 light_red_blue.pause();
                                                 dark_red_blue.pause();
                                                 play_pause.cancel();
                                                 pause_flag = false;
                                             }
                                         }
                                     }
        );
    }

    public void removeOverLayOuter() {
        try {
            handleSound(false);

            removeCompressionUI();
            try {
                countDownTimer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }

            ObjectAnimator anim = ObjectAnimator.ofInt(final_tick_progress, "progress", 0, 1000);
            anim.setDuration(2000);
            anim.setInterpolator(new DecelerateInterpolator());
            anim.start();


            //Do something after 100ms
            got_it.setVisibility(View.VISIBLE);
            got_it.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (windowManager_compression != null) {
                        try {
                            windowManager_compression.removeView(compress_view);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    compress_view = null;
                    windowManager_compression = null;

                }
            });
            allowReading(false);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void overLayUI() {
        Log.d( "Compress - overLayUI: ", "i am here");
        AdView mAdView;
        com.facebook.ads.AdView adView;

        LayoutInflater inflater = (LayoutInflater) sSharedInstance.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        compress_view = inflater.inflate(R.layout.multiple_app_overlay, null);
        RelativeLayout layout = (RelativeLayout) compress_view.findViewById(R.id.add_view_rel);
//        if(RemoteConfig.getString(R.string.ad_network).equals("google")) {
//            mAdView = new AdView(this);
//            if (RemoteConfig.getBoolean(R.string.show_smart_banner_on_compression)) {
//                mAdView.setAdSize(AdSize.BANNER);
//            } else {
//                mAdView.setAdSize(AdSize.LARGE_BANNER);
//            }
//            if (BuildConfig.DEBUG) {
//                mAdView.setAdUnitId("ca-app-pub-9668830280921241/1587342129");
//            } else {
////            mAdView.setAdUnitId(UtilityMethods.getInstance().getAdmobID(UtilityMethods.COMPRESSING_SCREEN));
//                mAdView.setAdUnitId("ca-app-pub-9668830280921241/1587342129");
//            }
//            AdRequest adRequest = new AdRequest.Builder().build();
//
//            mAdView.loadAd(adRequest);
//            if (RemoteConfig.getBoolean(R.string.show_banner_ads) && UtilityMethods.getInstance().isNewUser(getApplicationContext())) {
//                layout.addView(mAdView);
//            }
//        }else if (RemoteConfig.getString(R.string.ad_network).equals("facebook")){
//            adView = new com.facebook.ads.AdView(this, "IMG_16_9_APP_INSTALL#600591120962050_601621614192334", com.facebook.ads.AdSize.BANNER_HEIGHT_50);
//            adView.loadAd();
//
//            if( RemoteConfig.getBoolean(R.string.show_banner_ads) && UtilityMethods.getInstance().isNewUser(getApplicationContext())) {
//
//                layout.addView(adView);
//            }
//        }
        app_icon = (ImageView) compress_view.findViewById(R.id.single_app_image);
        decompress_progress = (ProgressBar) compress_view.findViewById(R.id.final_tick_background);
        progress_background = (ProgressBar) compress_view.findViewById(R.id.final_tick_background_low);
        app_icon_white = (LinearLayout) compress_view.findViewById(R.id.disc);
        app_compressed = (TextView) compress_view.findViewById(R.id.total_compressed_apps);
        totalapps = (TextView) compress_view.findViewById(R.id.total_apps);
        stop_compression = (TextView) compress_view.findViewById(R.id.text_hint);
        stop_compression_layout = (LinearLayout) compress_view.findViewById(R.id.stop_compression_layout);
        stop_compression_bg = (TextView) compress_view.findViewById(R.id.text_hint_bg);
        compressed_number = (LinearLayout) compress_view.findViewById(R.id.compressed_number);
        space_saved = (LinearLayout) compress_view.findViewById(R.id.space_saved);
        app_name_text = (TextView) compress_view.findViewById(R.id.app_name);
        remover_overlay_ui = (RelativeLayout) compress_view.findViewById(R.id.remove_overlay_screen);
        final_tick_progress = (ProgressBar) compress_view.findViewById(R.id.final_tick_progress);
        stop_progress = (ProgressBar) compress_view.findViewById(R.id.stop_progress);
        apps_compressed_text = (TextView) compress_view.findViewById(R.id.apps_compressed_text);
        apps_compressed_size_text = (TextView) compress_view.findViewById(R.id.apps_compressed_size_text);
        compressed_text = (TextView) compress_view.findViewById(R.id.compressed_text);
        inner = (ImageView) compress_view.findViewById(R.id.stars_inner);
        outer = (ImageView) compress_view.findViewById(R.id.stars_outer);
        got_it = (Button) compress_view.findViewById(R.id.optimize_btn);
        //final_tick_background_ripple = (RippleBackground) compress_view.findViewById(R.id.final_tick_background_ripple);

        mUpperLayout = (RelativeLayout) compress_view.findViewById(R.id.upper_layout);

        totalapps.setText("" + appsToBeCompressed);
        app_compressed.setText("0");
        x = 0;
        if (sCleanRam) {
            size = sRamCleanSize * 1024 * 1024;
        } else {
            size = 0;
        }
        compressedAppPackage = null;

        app_nameET = (TextView) compress_view.findViewById(R.id.app_name);
        ripple_background = (RippleBackground) compress_view.findViewById(R.id.content);

        ripple_background.startRippleAnimation();
        clockwise = AnimationUtils.loadAnimation(this, R.anim.rotate_image_clockwise);
        inner.startAnimation(clockwise);

        anticlockwise = AnimationUtils.loadAnimation(this, R.anim.rotate_image_anticlockwise);
        outer.startAnimation(anticlockwise);


        stop_compression.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stop_compression.setVisibility(View.INVISIBLE);
                stop_compression_bg.setVisibility(View.INVISIBLE);
                stop_compression_layout.setVisibility(View.VISIBLE);
                compressed_number.setVisibility(View.GONE);
                space_saved.setVisibility(View.GONE);
                //stop_progress.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
                abortFlag = true;
            }
        });


        //show color animation from
        if (PrefManager.getInstance(getApplicationContext()).getTheme() == 1) {
            slowColorAnimation();
        }

        updateAnimation();

        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.COMPRESSION);
    }

    private int mSoundIndex = 0;

    void updateAnimation() {
        UtilityMethods.getInstance().handleSound(false, getApplicationContext());
        if (mSoundIndex > 0) {
            UtilityMethods.oneAppCompleteSound();
        }
        mSoundIndex++;
        UtilityMethods.getInstance().handleSound(true, getApplicationContext());

        if (temp_app_names_to_compress != null && temp_app_names_to_compress.size() != 0) {

            if (temp_app_names_to_compress.size() <= 1) {
                stop_compression.setVisibility(View.INVISIBLE);
                stop_compression_layout.setVisibility(View.GONE);
                stop_compression_bg.setVisibility(View.INVISIBLE);
            }

            app_nameET.setText("Uninstalling " + temp_app_names_to_compress.remove(0));

        }

        if (animationScaleUp != null) {
            animationScaleUp.cancel();
            animationScaleUp.setAnimationListener(null);

        }

        if (growShrink != null) {
            growShrink.cancel();
            growShrink.setAnimationListener(null);

        }

        if (shrink != null) {
            shrink.cancel();
            shrink.setAnimationListener(null);

        }

        if (animationScaleUp_ripple != null) {
            animationScaleUp_ripple.cancel();
            animationScaleUp.setAnimationListener(null);

        }

        Animation app_icon_white_anim = app_icon_white.getAnimation();
        app_icon_white.clearAnimation();
        if (app_icon_white_anim != null) {
            app_icon_white_anim.cancel();
            app_icon_white_anim.setAnimationListener(null);
        }


        Animation ripple_background_anim = ripple_background.getAnimation();
        ripple_background.clearAnimation();
        if (ripple_background_anim != null) {
            ripple_background_anim.cancel();
            ripple_background_anim.setAnimationListener(null);
        }

        decompress_progress.clearAnimation();
        ripple_background.stopRippleAnimation();
        clockwise.cancel();
        anticlockwise.cancel();
        if (temp_app_to_compress.size() > 0) {

            x++;
            app_compressed.setText("" + x);

            ripple_background.startRippleAnimation();


            //app_nameET.setText("Compressing " + app_name);

            // decompress_progress.getAnimation().cancel();

            // compress_view.invalidate();

            Log.d("progress", "visible");
            decompress_progress.setVisibility(View.VISIBLE);
            progress_background.setVisibility(View.VISIBLE);

            try {

                compressedAppPackage = temp_app_to_compress.remove(0);
                try {
                    icon = getApplicationContext().getPackageManager().getApplicationIcon(compressedAppPackage);
                } catch (PackageManager.NameNotFoundException e) {
                    String APKFilePath = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + compressedAppPackage + ".stash";
                    PackageManager pm = getPackageManager();
                    PackageInfo pi = pm.getPackageArchiveInfo(APKFilePath, 0);

                    // Logo of the app from DIR....
                    pi.applicationInfo.sourceDir = APKFilePath;
                    pi.applicationInfo.publicSourceDir = APKFilePath;
                    //
                    icon = pi.applicationInfo.loadIcon(pm);

                    e.printStackTrace();
                }
                app_icon.setImageDrawable(icon);


            } catch (Exception e) {

                e.printStackTrace();
            }

            if (compressedAppPackage != null) {
                DataModel data = AppDBHelper.getInstance(AccessibilityAutomation.getSharedInstance()).getDataSize(compressedAppPackage);
                Log.d("AppDBHelper", "getApkSize -->" + data.getApkSize());
                Log.d("AppDBHelper", "getCodeSize -->" + data.getCodeSize());
                Log.d("AppDBHelper", "getCacheSize -->" + data.getCacheSize());
                Log.d("AppDBHelper", "getDataSize -->" + data.getDataSize());
                size += data.getApkSize() + data.getCodeSize() + data.getCacheSize() + data.getDataSize();
            }
            if (size > 0) {
                compressed_text.setText(UtilityMethods.getInstance().getSizeinMB(size));
            } else {
                compressed_text.setText("5 MB");
            }

            //TODO MAKE CHANGES HERE
                /*app_sizeET.setText("40 MB");*/
            //app_icon.setImageDrawable(icon);
            ObjectAnimator animator = ObjectAnimator.ofInt(decompress_progress, "progress", 0, 1000);
            animator.setDuration(7000);
            animator.setInterpolator(new DecelerateInterpolator());
            animator.start();

            //Color to white animation
            final ColorMatrix matrix = new ColorMatrix();
            ValueAnimator animation_greyscale = ValueAnimator.ofFloat(0f, 1f);
                /*animation_greyscale.setStartDelay(500);*/
            animation_greyscale.setDuration(7000);

            //   animation.setInterpolator();
            animation_greyscale.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    matrix.setSaturation(animation.getAnimatedFraction());
                    ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                    app_icon.setColorFilter(filter);
                }
            });
            animation_greyscale.reverse();
            //icon scale down animation


            animationScaleUp = AnimationUtils.loadAnimation(getSharedInstance().getApplicationContext(), R.anim.scale_down);
            animationScaleUp_ripple = AnimationUtils.loadAnimation(getSharedInstance().getApplicationContext(), R.anim.scale_down);

            //animationScaleUp.setFillAfter(true);
            animationScaleUp.setStartOffset(500);
            growShrink = new AnimationSet(true);
            growShrink.addAnimation(animationScaleUp);
            growShrink.setFillAfter(true);
            app_icon_white.startAnimation(growShrink);

            //animationScaleUp_ripple.setFillAfter(true);
            animationScaleUp_ripple.setStartOffset(500);

            shrink = new AnimationSet(true);
            shrink.addAnimation(animationScaleUp_ripple);
            shrink.setFillAfter(true);
            ripple_background.startAnimation(shrink);


            animationScaleUp.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    Log.d("progress", "invisible");
                    decompress_progress.setVisibility(View.INVISIBLE);
                    progress_background.setVisibility(View.INVISIBLE);

                }


                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }


    }

    Bitmap toGrayscale(Bitmap bmpOriginal) {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }


    public void overLayScanning(Intent intent) {
        scanning = new ScanningOverlay(getApplicationContext(), intent, this);
        scanning.overlayScanning();
    }

    public void removeScanningOverLay() {
        mGetUnknownSourcesPermission = false;

        mGetAppUsagePermission = false;
        scanning.removeScanningOverLay();
    }


    public void overlayDecompression(String source) {
        actionFailHandlerReactivate = new ActionFailHandlerReactivate();
        decompression = new DecompressionAnim(source, sSharedInstance, getApplicationContext());
        decompression.overlayDecompression();
    }

    public void removeDecompressionOverLay() {
        decompression.removeDecompressionOverLay();
    }

    public void overlay_singleApp() {
        actionFailHandler = new ActionFailHandler();
        singleAppUI = new SingleAppCompressionAnim(app_name, package_name, prev_size, sSharedInstance, getApplicationContext());
        singleAppUI.overlay_singleApp();

    }

    public void removeSingleAppUI() {

        if (actionFailHandler != null && actionFailHandler.getActionFailCountDownTimer() != null) {

            actionFailHandler.getActionFailCountDownTimer().cancel();
            actionFailHandler = null;
        }

        singleAppUI.removeSingleAppUI();

    }

    void throwInstallEvent(Context context, String packageName) {


        String dummyApkPath = AppDBHelper.getInstance(context).getDummyPath(packageName);
        File f = null;

        if (dummyApkPath != null && !"".equalsIgnoreCase(dummyApkPath))
            f = new File(dummyApkPath);

        if (f != null && f.exists()) {
//            Uri path_uri = Uri.fromFile(f);
            Uri path_uri = getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".fileprovider",f);


//            UtilityMethods.getInstance().throwInstallPopup(path_uri, context);

            try {
                Log.d("Compression", "Install intent thrown for " + packageName + " path: " + dummyApkPath);
                AccessibilityAutomation.getSharedInstance().getActionFailHandler().setInstall_handle(true, dummyApkPath, false);
                AccessibilityAutomation.getSharedInstance().getActionFailHandler().refreshInstallCounter();


            } catch (Exception y) {
                y.printStackTrace();
            }
        } else {
            Log.d("Compression", "File do not exists, starting next compression");
            UtilityMethods.getInstance().startNextCompression(context);

        }

    }

    @Override
    public boolean onUnbind(Intent intent) {
        sSharedInstance = null;
        isServiceRunning = false;
        Log.d("AccessibilityY", "Accessibility stopped");
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        sSharedInstance = this;
        isServiceRunning = true;
        super.onRebind(intent);
    }
}