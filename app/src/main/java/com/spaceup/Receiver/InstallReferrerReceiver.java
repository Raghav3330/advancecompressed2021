package com.spaceup.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.spaceup.Utility.PrefManager;

public class InstallReferrerReceiver extends BroadcastReceiver {

    private static final String REFERRER = "referrer";

    public InstallReferrerReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String referrer = intent.getStringExtra(REFERRER);
        if (referrer != null && !referrer.isEmpty()) {
            PrefManager.getInstance(context).putString(PrefManager.KEY_CAMPAIGN_SOURCE, referrer);
        }
    }
}
