package com.spaceup;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import androidx.core.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.Asycs.TotalAppSizeAsync;
import com.spaceup.Runnable.AccRunnable;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityAutomation;
import com.spaceup.accessibility.AccessibilityCommunicatorReactivate2018;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;

import java.io.File;

import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan;
import io.github.inflationx.calligraphy3.TypefaceUtils;


public class UncompressingActivity extends Activity {
    public static boolean requestTurnOn = false;
    private View mView1;
    private WindowManager wm1;
    private String myString, source, type, process_type;
    private ImageView acc_tick;
    private ImageView unknown_tick;
    private UtilityMethods methods;
    private Dialog permissionDialog;
    private Button accessibility_permission_btn;
    private Button unknown_permission_btn;
    private AccRunnable accRunnable;
    private UnknownRunnable unknownRunnable;
    private Dialog askUnCompressDialog;
    boolean confirm_popup = true;
    private Bundle mGAParams;

    public static boolean isAccessibilityServiceEnabled(Context context, Class<?> accessibilityService) {
        ComponentName expectedComponentName = new ComponentName(context, accessibilityService);

        String enabledServicesSetting = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        if (enabledServicesSetting == null)
            return false;

        TextUtils.SimpleStringSplitter colonSplitter = new TextUtils.SimpleStringSplitter(':');
        colonSplitter.setString(enabledServicesSetting);

        while (colonSplitter.hasNext()) {
            String componentNameString = colonSplitter.next();
            ComponentName enabledService = ComponentName.unflattenFromString(componentNameString);

            if (enabledService != null && enabledService.equals(expectedComponentName))
                return true;
        }

        return false;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (UtilityMethods.isAppUpdateGoingOn()) {
            Toast.makeText(this, getString(R.string.app_updating_msg), Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        methods = UtilityMethods.getInstance();
        Bundle b = getIntent().getExtras();

        if (b != null) {
            myString = b.getString("app_location");
            if (b.containsKey("confirm_popup"))
                confirm_popup = b.getBoolean("confirm_popup", true);

            source = b.getString("source", getPackageName());
            type = b.getString("type");
            process_type = b.getString("process_type");

            Log.d("TAG", "CALLED ACCESSIBILITY HELPER" + source + process_type);


            if (AppDBHelper.getInstance(getApplicationContext()).getAppDetails().size() == 0) {
                //Initialize DBS
                new TotalAppSizeAsync(getApplicationContext()).execute();
            }
            //analytics start
            if (methods.isAccessibilityServiceRunning(UncompressingActivity.this)) {

                // Analytics Start
                mGAParams = new Bundle();
                mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
                mGAParams.putString("action", AnalyticsConstants.Action.ACCESSIBILITY);

                mGAParams.putString("label", AnalyticsConstants.Label.UNCOMPRESS_SCREEN + "_" + AnalyticsConstants.Label.EXIST);
                mGAParams.putLong("value", 1);
                new AnalyticsHandler().logGAEvent(mGAParams);
                Log.d(AnalyticsConstants.GATAG, mGAParams.toString());


            }
            if (methods.isHavingUnkownSourcePermission(UncompressingActivity.this)) {
                mGAParams = new Bundle();
                mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
                mGAParams.putString("action", AnalyticsConstants.Action.UNKNOWN_SOURCES);

                mGAParams.putString("label", AnalyticsConstants.Label.UNCOMPRESS_SCREEN + "_" + AnalyticsConstants.Label.EXIST);
                mGAParams.putLong("value", 1);
                new AnalyticsHandler().logGAEvent(mGAParams);
                Log.d(AnalyticsConstants.GATAG, mGAParams.toString());


            }
            //analytics end

            if (confirm_popup) {
                unCompressionPopup();
            } else {
                checkAndReactivate();
            }
        }
    }

    /*checks for acc permissions & unknown permissions as well as
    handles opening both permission dialog, starts reactivate process depending on permission status*/
    public void checkAndReactivate() {
        int permission_counter = 0;
        if (!methods.isAccessibilityServiceRunning(UncompressingActivity.this)) {
            permission_counter++;
        }
        if (!methods.isHavingUnkownSourcePermission(UncompressingActivity.this)) {
            permission_counter++;
        }

        if (!methods.isAccessibilityServiceRunning(UncompressingActivity.this) || !methods.isHavingUnkownSourcePermission(UncompressingActivity.this)) {
            permissionDialog = new Dialog(UncompressingActivity.this, R.style.Dialog);
            permissionDialog.setContentView(R.layout.bothpermission);
            accessibility_permission_btn = (Button) permissionDialog.findViewById(R.id.accessibility_permission_btn);
            unknown_permission_btn = (Button) permissionDialog.findViewById(R.id.unknown_permission_btn);
            acc_tick = (ImageView) permissionDialog.findViewById(R.id.accessibility_permission_tick);
            unknown_tick = (ImageView) permissionDialog.findViewById(R.id.unknown_permission_tick);
            ImageView cancel_btn_img = (ImageView) permissionDialog.findViewById(R.id.cancel);
            cancel_btn_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    permissionDialog.cancel();
                }
            });
            setDialogBox();
            accessibility_permission_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openAccessibilityHelper();
                }
            });

            unknown_permission_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openUnknownHelper();
                }
            });

            if (permission_counter == 1) {
                if (!methods.isAccessibilityServiceRunning(UncompressingActivity.this)) {
                    openAccessibilityHelper();
                }
                if (!methods.isHavingUnkownSourcePermission(UncompressingActivity.this)) {
                    openUnknownHelper();
                }
            }

            permissionDialog.setCanceledOnTouchOutside(false);

            WindowManager.LayoutParams lp;
            lp = permissionDialog.getWindow().getAttributes();
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            lp.gravity = Gravity.CENTER_VERTICAL;

            permissionDialog.getWindow().setAttributes(lp);
            permissionDialog.show();
        } else if (methods.isAccessibilityServiceRunning(UncompressingActivity.this) && methods.isHavingUnkownSourcePermission(UncompressingActivity.this)) {

            reactivating(myString, source);

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 55:

                if (null != accRunnable) {
                    Log.d("backdebug", "stopping acc runnable");
                    accRunnable.stop();
                }
                Log.d("backdebug", "inside finish 55 ");

                try {
                    wm1.removeView(mView1);
                } catch (Exception e) {

                }
                //setup ui of dialog box
                if (permissionDialog != null && permissionDialog.isShowing()) {
                    setDialogBox();
                }
                if (methods.isAccessibilityServiceRunning(UncompressingActivity.this)) {
                    // Analytics Start
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
                    mGAParams.putString("action", AnalyticsConstants.Action.ACCESSIBILITY);
                    mGAParams.putString("label", AnalyticsConstants.Label.UNCOMPRESS_SCREEN);
                    mGAParams.putLong("value", 1);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

                } else {
                    // Analytics Start
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
                    mGAParams.putString("action", AnalyticsConstants.Action.ACCESSIBILITY);
                    mGAParams.putString("label", AnalyticsConstants.Label.UNCOMPRESS_SCREEN);
                    mGAParams.putLong("value", 0);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                }
                if (methods.isAccessibilityServiceRunning(UncompressingActivity.this) && methods.isHavingUnkownSourcePermission(UncompressingActivity.this)) {
                    if (permissionDialog != null && permissionDialog.isShowing()) {
                        permissionDialog.cancel();
                    }
                    reactivating(myString, source);

                }
                break;
            case 33:
                if (unknownRunnable != null) {
                    unknownRunnable.stop();
                }
                try {
                    wm1.removeView(mView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String label = AnalyticsConstants.Label.UNCOMPRESS_SCREEN;
                if (!PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.SENT_FIRST_TIME_UNCOMPRESS_SCREEN)) {
                    PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.SENT_FIRST_TIME_UNCOMPRESS_SCREEN, true);
                    label = AnalyticsConstants.Label.UNCOMPRESS_SCREEN_FIRST;
                }
                if (methods.isHavingUnkownSourcePermission(UncompressingActivity.this)) {
                    // Analytics Start
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
                    mGAParams.putString("action", AnalyticsConstants.Action.UNKNOWN_SOURCES);
                    mGAParams.putString("label", label);
                    mGAParams.putLong("value", 1);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

                } else {
                    // Analytics Start
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
                    mGAParams.putString("action", AnalyticsConstants.Action.UNKNOWN_SOURCES);
                    mGAParams.putString("label", label);
                    mGAParams.putLong("value", 0);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

                }
                if (permissionDialog != null && permissionDialog.isShowing()) {
                    setDialogBox();
                }
                if (methods.isAccessibilityServiceRunning(UncompressingActivity.this) && methods.isHavingUnkownSourcePermission(UncompressingActivity.this)) {
                    if (permissionDialog != null && permissionDialog.isShowing()) {
                        permissionDialog.cancel();
                    }
                    reactivating(myString, source);
                }
                break;
            default:

        }
    }

    public void openAccessibilityHelper() {
        Log.d("TAG", "OPENED ACCESSIBILITY HELPER");
        try {
            wm1.removeView(mView1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        requestTurnOn = true;

        startActivityForResult(intent, 55);

        LayoutInflater accessibilityHelper = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView1 = accessibilityHelper.inflate(R.layout.accessibility_permission, null);
        WindowManager.LayoutParams params1;
        TextView span_text_view = (TextView) mView1.findViewById(R.id.span_text_view);
        TextView permission_helper_text = (TextView) mView1.findViewById(R.id.permission_helper_text);
        String first = "\uD83C\uDF1F";
        String second = " SpaceUp ";
        String third = "\uD83C\uDF1F";
        final String totalString = first + second + third;
        Spannable spanText = new SpannableString(totalString);
        //set color to white
        spanText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(UncompressingActivity.this, R.color.spaceup_black)), first.length(), first.length() + second.length(), 0);
        //set text to bradon medium
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon_medium.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spanText.setSpan(typefaceSpan, first.length(), first.length() + second.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        span_text_view.setText(spanText, TextView.BufferType.SPANNABLE);


        String f = "Give permission to ";
        String s = "save upto 1.2 GB";
        final String t = f + s;
        Spannable spannableText = new SpannableString(t);
        //set color to white
        spannableText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(UncompressingActivity.this, R.color.text_color)), f.length(), f.length() + s.length(), 0);
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spannableText.setSpan(typefaceSpan, f.length(), f.length() + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        permission_helper_text.setText(spannableText, TextView.BufferType.SPANNABLE);

        ImageView cancel = (ImageView) mView1.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    wm1.removeView(mView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        params1 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_TOAST,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);
        params1.gravity = Gravity.BOTTOM;
        params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;


        wm1 = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        try {
            wm1.addView(mView1, params1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        accRunnable = new AccRunnable(wm1, mView1, this);
        Thread accCheckThread = new Thread(accRunnable, "acc");
        accCheckThread.start();
        AccessibilityAutomation.allowSpecialBack();
        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.PERMISSION_ACCESSIBILITY);
    }

    public void openUnknownHelper() {
        try {
            wm1.removeView(mView1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent unknownSource = new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS);
        startActivityForResult(unknownSource, 33);
        LayoutInflater accessibilityHelper = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView1 = accessibilityHelper.inflate(R.layout.unknown_permission, null);
        WindowManager.LayoutParams params1;

        TextView permission_helper_text = (TextView) mView1.findViewById(R.id.permission_helper_text);
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon_medium.ttf"));
        CalligraphyTypefaceSpan calligraphyRegular = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon.ttf"));
        String f = "Give permission for ";
        String s = "upto 90% compression";
        final String t = f + s;
        Spannable spannableText = new SpannableString(t);
        //set color to white
        spannableText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(UncompressingActivity.this, R.color.text_color)), f.length(), f.length() + s.length(), 0);
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spannableText.setSpan(calligraphyRegular, 0, f.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableText.setSpan(typefaceSpan, f.length(), f.length() + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        permission_helper_text.setText(spannableText, TextView.BufferType.SPANNABLE);

        ImageView cancel = (ImageView) mView1.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    wm1.removeView(mView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        params1 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);

        params1.gravity = Gravity.TOP;
        params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        wm1 = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        try {
            wm1.addView(mView1, params1);
        } catch (Exception e) {
            e.printStackTrace();
        }


        unknownRunnable = new UnknownRunnable();
        Thread unknownThread = new Thread(unknownRunnable, "unknown");
        unknownThread.start();
        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.PERMISSION_UNKNOWN_SOURCE);
    }

    public void setDialogBox() {
        if (methods.isAccessibilityServiceRunning(UncompressingActivity.this)) {
            acc_tick.setVisibility(View.VISIBLE);
            accessibility_permission_btn.setVisibility(View.GONE);
        } else {
            acc_tick.setVisibility(View.GONE);
            accessibility_permission_btn.setVisibility(View.VISIBLE);
        }
        if (methods.isHavingUnkownSourcePermission(UncompressingActivity.this)) {
            unknown_tick.setVisibility(View.VISIBLE);
            unknown_permission_btn.setVisibility(View.GONE);
        } else {
            unknown_tick.setVisibility(View.GONE);
            unknown_permission_btn.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (permissionDialog.isShowing()) {
                setDialogBox();
                if (methods.isAccessibilityServiceRunning(UncompressingActivity.this) && methods.isHavingUnkownSourcePermission(UncompressingActivity.this)) {
                    permissionDialog.cancel();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void unCompressionPopup() {

        askUnCompressDialog = new Dialog(UncompressingActivity.this, R.style.Dialog);
        askUnCompressDialog.setContentView(R.layout.uncompression_popup_layout);
        askUnCompressDialog.setCancelable(false);

        TextView dialog_header = (TextView) askUnCompressDialog.findViewById(R.id.uncompression_popup_header);
        ImageView app_icon = (ImageView) askUnCompressDialog.findViewById(R.id.app_icon);
        TextView cancel_btn = (TextView) askUnCompressDialog.findViewById(R.id.cancel);
        TextView uncompress_btn = (TextView) askUnCompressDialog.findViewById(R.id.uncompress);
        TextView warning_txt = (TextView) askUnCompressDialog.findViewById(R.id.warning_text);

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/brandon_medium.ttf");
        cancel_btn.setTypeface(face);
        uncompress_btn.setTypeface(face);
        dialog_header.setTypeface(face);


        Typeface brandon_regular = Typeface.createFromAsset(getAssets(), "fonts/brandon.ttf");
        warning_txt.setTypeface(brandon_regular);


        try {
            Drawable icon = this.getPackageManager().getApplicationIcon(source);
            app_icon.setImageDrawable(icon);
            CharSequence app_name = this.getPackageManager().getApplicationLabel(getPackageManager().getApplicationInfo(source, PackageManager.GET_META_DATA));
            dialog_header.setText("Uncompress " + app_name + " ?");
        } catch (PackageManager.NameNotFoundException e) {


            String APKFilePath = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + source;
            PackageManager pm = getApplicationContext().getPackageManager();
            PackageInfo pi = pm.getPackageArchiveInfo(APKFilePath, 0);

            // Logo of the app from DIR....
            pi.applicationInfo.sourceDir = APKFilePath;
            pi.applicationInfo.publicSourceDir = APKFilePath;
            //
            Drawable APKicon = pi.applicationInfo.loadIcon(pm);
            dialog_header.setText("Uncompress " + pi.applicationInfo.loadLabel(pm));
            app_icon.setImageDrawable(APKicon);

        }


        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askUnCompressDialog.cancel();
                UncompressingActivity.this.finish();
            }
        });

        uncompress_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Analytics Start
                UtilityMethods.sendUnCompressSourceAnalytics(-1, 1);
                // Analytics end


                askUnCompressDialog.cancel();
                checkAndReactivate();


                new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.UNCOMPRESS);
            }
        });

        askUnCompressDialog.show();
    }

    private void reactivating(String path, String source) {

        if (!isAccessibilityServiceEnabled(getApplicationContext(), AccessibilityAutomation.class)) {
            Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
            AccessibilityAutomation.allowSpecialBack();
            startActivity(intent);

        } else {
            if( AccessibilityAutomation.getSharedInstance() == null){
                Toast.makeText(this, "Please Re Enable Accessibility Service from Setting!", Toast.LENGTH_SHORT).show();
                return;
            }

            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.UNCOMPRESS,
                    AnalyticsConstants.Action.UNCOMPRESS,
                    null,
                    null,
                    false,
                    null);

            // Analytics to calculate time taken for first & second uncompression after compression


            if (PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.FIRST_TIME_COMPRESS_TIME) != 0) {
                //Don't send analytics for old previous users
                long timeTaken = System.currentTimeMillis() - PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.FIRST_TIME_COMPRESS_TIME);
                long timeMinute = timeTaken / 1000l;
                long uncompressCount = PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.UNCOMPRESSION_COUNT_NEW);
                int totalTimeReacivated = PrefManager.getInstance(getApplicationContext()).getInt(PrefManager.GET_REACTIVATE_COUNT);
                if (totalTimeReacivated < PrefManager.getInstance(getApplicationContext()).getInt(PrefManager.APP_COMPRESSED)) {
                    PrefManager.getInstance(getApplicationContext()).putInt(PrefManager.GET_REACTIVATE_COUNT, ++totalTimeReacivated);
                    new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.UNCOMPRESS,
                            AnalyticsConstants.Action.TIME_UNCOMP,
                            String.valueOf(uncompressCount),
                            String.valueOf(timeMinute),
                            false,
                            null);
                }
                uncompressCount++;

                PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.UNCOMPRESSION_COUNT_NEW, uncompressCount);
            }
            //storage uncompress time start
            Intent intent;


            // new Process
            intent = new Intent(UncompressingActivity.this, AccessibilityCommunicatorReactivate2018.class);

            intent.putExtra("decompresser_initialate", true);
            intent.putExtra("path", path);

            Log.d("Check at reactivating: ", path + source);
            if (type == null) {
                type = "dummy";
            }
            intent.putExtra("type", type);
            intent.putExtra("source", source);

            PrefManager prefManager = PrefManager.getInstance(getApplicationContext());
            String package_name = source;
            if (process_type == null) {
                package_name = source.substring(0, source.indexOf(".stash"));
            }
            Log.d("INSTALLED_TRACKER_1", package_name);
            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.UNCOMPRESS,
                    AnalyticsConstants.Action.UNCOMP_PCKG,
                    package_name,
                    null,
                    false,
                    null);

            File apkpath = new File(path);

            if (apkpath.exists()) {
                startService(intent);
            } else {
                Toast.makeText(this, "Please Re-Install this app from Play Store(Press Uninstall and then Install)", Toast.LENGTH_LONG).show();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + package_name)));
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + package_name)));
                }
            }
            new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.UNCOMPRESSION);
            finish();
        }
    }

    class UnknownRunnable implements Runnable {
        private volatile boolean exit = false;

        @Override
        public void run() {
            int count = 0;
            do {

                try {
                    Thread.sleep(1000);
                    //show dialog for 8 seconds only
                    if (count > 8) {
                        try {
                            wm1.removeView(mView1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    count++;
                } catch (Exception e) {

                }
            } while (!methods.isHavingUnkownSourcePermission(UncompressingActivity.this) && !exit);


            if (methods.isHavingUnkownSourcePermission(UncompressingActivity.this)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //inform onActivityResult if called not to start scanning
                        //start_scanning = true;

                        try {
                            wm1.removeView(mView1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        finishActivity(33);
                    }
                });
            }

        }

        public void stop() {

            exit = true;
        }
    }
}
