package com.spaceup.notifications;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.util.Log;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.R;
import com.spaceup.RemoteConfig;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.uninstall.activities.AppInfo;

import java.io.File;
import java.lang.reflect.Method;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dhruv Kaushal on 08-12-2017.
 */

public class DailyAnalyticsService extends IntentService {
    private static final String TAG = "DailyAnalyticsService";
    private UsageStatsManager mUsageStatsManager;
    private PackageManager packageManager = null;
    private int mApTotalAppsUsed = 0;
    private static final String SYSTEM_PACKAGE_NAME = "android";
    private int total_apps, app_size_response_increment, noOfAppStorageIncreased;
    private boolean size_calculated;
    private AppDBHelper _db;
    private long totalStorageDiffInADay;

    public DailyAnalyticsService() {

        super(DailyAnalyticsService.class.getName());

    }

    @Override
    public void onCreate() {
        super.onCreate();

    }


    private void appSizeAnother(final PackageManager pm, final PackageInfo appInfo, final List<AppInfo> userApps) {

        Method getPackageSizeInfo = null;
        final String pkgName = appInfo.packageName;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

        } else {

            try {
                getPackageSizeInfo = pm.getClass().getMethod(
                        "getPackageSizeInfo", String.class, IPackageStatsObserver.class);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }


            try {
                // Log.d("Total App size",""+total_app_size);
                if (getPackageSizeInfo != null) {
                    getPackageSizeInfo.invoke(pm, pkgName, new IPackageStatsObserver.Stub() {
                        @Override
                        synchronized public void onGetStatsCompleted(PackageStats pStats, boolean succeeded)
                                throws RemoteException {
                            long appData = pStats.dataSize;
                            long codeSize = pStats.codeSize;
                            long cacheSize = pStats.cacheSize;
                            AppInfo obj = new AppInfo();
                            obj.setPkgName(pkgName);
                            obj.setAppName((String) appInfo.applicationInfo.loadLabel(pm).toString());
                            obj.setSize(appData);
                            obj.setCacheSize(cacheSize);
                            obj.setCodeSize(codeSize);

                            long storageDiff = appData + cacheSize - _db.getAppStorageIncreasedUsage(pkgName);


                            if (storageDiff > 0)
                                totalStorageDiffInADay += storageDiff;

                            if (storageDiff >= 5000000) {
                                new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.APP_USAGE_STATS,
                                        AnalyticsConstants.Action.APP_STORAGE_DATA,
                                        getAppName(pkgName),
                                        "" + (storageDiff / 1000000),
                                        false,
                                        null);
                                noOfAppStorageIncreased++;
                            }

                            try {
                                obj.setApkSize(getApkSize(pkgName));
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                            obj.setLastModifiedDate(listCombineAppsDateModified(pkgName));
                            //user installed apps
                            if (isUserInstalledApplication(appInfo)) {

                                if (Constants.DUMMY_APP_VERSION_NAME.equalsIgnoreCase(UtilityMethods.getCurrentAppVersionName(getApplicationContext(), obj.getPkgName()))) {
                                    //remove com.spaceup  and change status of apps as compressed [DUMMY]
                                    obj.setCompressed_status(Constants.COMPRESSED);
                                } else {
                                    //Apps are uncompressed by default
                                    obj.setCompressed_status(Constants.UNCOMPRESSED);
                                }
                            } else {
                                //System app
                                obj.setCompressed_status(Constants.SYSTEM_APP);
                            }
                            userApps.add(obj);
                            Log.d(TAG, pkgName + " App Name: " + appInfo.applicationInfo.loadLabel(pm).toString());
                            app_size_response_increment++;
                            if (app_size_response_increment == total_apps) {
                                size_calculated = true;

                                new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.APP_USAGE_STATS,
                                        AnalyticsConstants.Action.NO_OF_APP_STORAGE_INCREASED,
                                        null,
                                        "" + noOfAppStorageIncreased,
                                        false,
                                        null);

                                new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.APP_USAGE_STATS,
                                        AnalyticsConstants.Action.APP_STORAGE_INCREASED,
                                        "",
                                        "" + (totalStorageDiffInADay / 1000000),
                                        false,
                                        null);

                            }

                            _db.updateAppStoreIncreased(appData, cacheSize, pkgName);
                            Log.d("TotalSize", "" + pkgName);
                        }
                    });
                }

            } catch (ReflectiveOperationException e) {
                e.printStackTrace();
                app_size_response_increment++;
                if (app_size_response_increment == total_apps)
                    size_calculated = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private HashSet<String> mAppUsedHashSet = new HashSet<>();

    void updateAppStorageStats() {
        packageManager = getPackageManager();
        _db = AppDBHelper.getInstance(this);
        List<AppInfo> userApps = Collections.synchronizedList(new ArrayList<AppInfo>());
        List<PackageInfo> allApps = getPackageManager().getInstalledPackages(0);
        HashSet<String> appsInDB = _db.listAllPackageInDB();

        total_apps = 0;
        noOfAppStorageIncreased = 0;

        for (PackageInfo appInfo : allApps) {
            //if (!appsInDB.contains(appInfo.packageName) && !appInfo.packageName.contains(Constants.APK_EXTENSION)) {
            total_apps++;
            appSizeAnother(packageManager, appInfo, userApps);
            //}
            //CAUTION : donot change if else flow
            //app exists in db
            //do package still exits in package manager, if it does not add to update list to mark it uninstalled
        }
    }

    void sendMediaAnalytics() {
        int noOfImagesClicked = getNoOfImagesClickedInADay(DailyAnalyticsService.this);
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.FILE_USAGE_STATS,
                AnalyticsConstants.Action.MEDIA_CLICKED,
                null,
                "" + noOfImagesClicked,
                false,
                null);


    }
    public int getNoOfImagesClickedInADay(Context activity) {
        int photosClickedInADay = 0;
        Uri uri;
        String photo_Date;
        Cursor cursor;
        int column_date_added;

        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        //FIREBASE CRASH FIX -- TO GRANT URI PERMISSIONS
        activity.grantUriPermission(getPackageName(),uri,Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);

        String[] projection = {MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.Images.ImageColumns.DATE_TAKEN};

        long currentTime = System.currentTimeMillis();


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");


        Date currentDate = new Date(currentTime);
        simpleDateFormat.format(currentDate);


        String orderBy = MediaStore.Images.Media.DATE_TAKEN + " DESC";
        String selection = MediaStore.Images.Media.BUCKET_DISPLAY_NAME + "='Camera'";

        cursor = activity.getContentResolver().query(uri, projection, selection,
                null, orderBy);


        assert cursor != null;
        column_date_added = cursor
                .getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATE_TAKEN);


        //thumb_id = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);

        while (cursor.moveToNext()) {

            photo_Date = cursor.getString(column_date_added);

            Date photoDate = new Date(Long.parseLong(photo_Date.trim()));
            simpleDateFormat.format(photoDate);

            if (printDifference(photoDate, currentDate))
                photosClickedInADay++;


            Log.e("Image", ""+currentDate);
            Log.e("ImageDate", ""+photoDate);

        }

        Log.e("Image", "photosClickedInADay " + photosClickedInADay);
        return photosClickedInADay;
    }


    public boolean printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        Log.e("Time",elapsedDays+" days "+elapsedHours+" hours "+elapsedMinutes+" minutes "+elapsedSeconds+" seconds ");

        if(elapsedDays == 0)
        {
            return true;
        }
        else {
            return false;
        }

    }


    @Override
    protected void onHandleIntent(Intent intent) {

        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.COMPRESS,
                AnalyticsConstants.Action.APP_INSTALL_DAYS,
                null,
                null,
                false,
                null);

        updateAppStorageStats();
        sendMediaAnalytics();
        analyticsMediaRecieved();
        analyticsFileReceived();
        analyticsFileCreated();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {


            mUsageStatsManager = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE); //Context.USAGE_STATS_SERVICE
            packageManager = getPackageManager();
            List<UsageStats> usageStatsList = getUsageStatistics(UsageStatsManager.INTERVAL_DAILY);
            Collections.sort(usageStatsList, new LastTimeLaunchedComparatorDesc());
            for (UsageStats app : usageStatsList) {
                if (isAppPreLoaded(app.getPackageName()) || isSystemApp(app.getPackageName())) {
                    //ignore system apps
                } else {

                    Log.d(TAG, "onHandleIntent: PACKAGE_NAME_USAGE" + app.getPackageName());
                    long millis = app.getLastTimeUsed();


                    Calendar c = Calendar.getInstance();
                    c.setTimeInMillis(millis);
                    int hours = c.get(Calendar.HOUR_OF_DAY);
                    String appName = getAppName(app.getPackageName());
                    long timeUsed = app.getTotalTimeInForeground();
                    long timeUsedAppInSeconds = TimeUnit.MILLISECONDS.toSeconds(timeUsed);

                    if (appName == null || timeUsedAppInSeconds == 0) {
                        continue;
                    }
                    mApTotalAppsUsed++;
                    mAppUsedHashSet.add(app.getPackageName());
                    if (hours > 6 && hours <= 12) {
                        //Apps used between 6AM-12AM
                        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.APP_USAGE_STATS,
                                AnalyticsConstants.Action.TIME_6AM_TO_12PM,
                                appName,
                                String.valueOf(timeUsedAppInSeconds),
                                false,
                                null);
                    } else if (hours > 12 && hours <= 18) {
                        //Apps used between 12AM-6PM
                        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.APP_USAGE_STATS,
                                AnalyticsConstants.Action.TIME_12PM_TO_6PM,
                                appName,
                                String.valueOf(timeUsedAppInSeconds),
                                false,
                                null);
                    } else if (hours > 18 && hours <= 24) {
                        //Apps used between 6PM-24PM
                        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.APP_USAGE_STATS,
                                AnalyticsConstants.Action.TIME_6PM_TO_12AM,
                                appName,
                                String.valueOf(timeUsedAppInSeconds),
                                false,
                                null);

                    } else if (hours > 0 && hours <= 6) {
                        //Apps used between 0AM-6AM
                        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.APP_USAGE_STATS,
                                AnalyticsConstants.Action.TIME_12AM_TO_6AM,
                                appName,
                                String.valueOf(timeUsedAppInSeconds),
                                false,
                                null);
                    }
                }
            }

            //Total apps used in a day
            if (mAppUsedHashSet.size() > 0) {
                new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.APP_USAGE_STATS,
                        AnalyticsConstants.Action.APPS_USED_PER_DAY,
                        null,
                        String.valueOf(mAppUsedHashSet.size()),
                        false,
                        null);
            }


        }
    }

    private String getAppName(String packageName) {
        PackageManager packageManager = getApplicationContext().getPackageManager();
        String appName = null;
        try {
            appName = (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception f) {
            f.printStackTrace();
        }
        return appName;
    }

    public List<UsageStats> getUsageStatistics(int intervalType) {
        // Get the app statistics since one day ago from the current time.
        List<UsageStats> queryUsageStats = mUsageStatsManager
                .queryUsageStats(intervalType, System.currentTimeMillis() - 86400000,
                        System.currentTimeMillis());

        if (queryUsageStats.size() == 0) {
            Log.d(TAG, "The user may not allow the access to apps usage. ");


        }
        return queryUsageStats;
    }

    private String[] pathsReceived = {"/WhatsApp/Media/WhatsApp Documents/",
            "/Hike/Media/hike Others/",
            "/Telegram/Telegram Documents/", "/Downloads/", "/SHAREit/files/"};

    private void analyticsFileReceived() {
        int filesReceived = 0;
        long filesReceivedSize = 0;
        for (String parentPath : pathsReceived) {
            File directory = new File(Environment.getExternalStorageDirectory().getPath() + parentPath);
            File[] files = directory.listFiles();
            if (files == null) {
                continue;
            }
            Log.d("Files", "Size: " + files.length);
            for (int i = 0; i < files.length; i++) {
                if (files[i].isFile() && System.currentTimeMillis() - files[i].lastModified() < 86400000) {
                    filesReceived++;
                    filesReceivedSize += files[i].length();
                }

            }
        }
        Log.d("FILES_RECEIVED", "analyticsFileRecieved: " + filesReceived);
        new AnalyticsController(getApplicationContext()).sendAnalytics(
                AnalyticsConstants.Category.FILE_USAGE_STATS,
                AnalyticsConstants.Action.FILE_RECEIVED,
                null,
                String.valueOf(filesReceived),
                false,
                null);

        new AnalyticsController(getApplicationContext()).sendAnalytics(
                AnalyticsConstants.Category.FILE_USAGE_STATS,
                AnalyticsConstants.Action.FILE_RECEIVED_SIZE,
                null,
                String.valueOf(filesReceivedSize / 1000000),
                false,
                null);
    }

    private String[] paths = {"/WhatsApp/Media/WhatsApp Images/",
            "/WhatsApp/Media/WhatsApp Video/",
            "/Hike/Media/hike Images/",
            "/Telegram/Telegram Images/",
            "/viber/media/Viber Images/",
            "/Android/data/jp.naver.line.android/storage/mo/", "/Snapchat/", "/SHAREit/pictures"};

    private void analyticsMediaRecieved() {
        int imagesReceived = 0;
        long imageRecievedSize = 0;
        for (String parentPath : paths) {
            File directory = new File(Environment.getExternalStorageDirectory().getPath() + parentPath);
            File[] files = directory.listFiles();
            if (files == null) {
                continue;
            }
            Log.d("Files", "Size: " + files.length);
            for (int i = 0; i < files.length; i++) {
                if (files[i].isFile() && System.currentTimeMillis() - files[i].lastModified() < 86400000) {
                    imagesReceived++;
                    imageRecievedSize += files[i].length();
                }

            }
        }
        Log.d("FILES_RECEIVED", "analyticsFileRecieved: " + imagesReceived);
        new AnalyticsController(getApplicationContext()).sendAnalytics(
                AnalyticsConstants.Category.FILE_USAGE_STATS,
                AnalyticsConstants.Action.MEDIA_RECIEVED,
                null,
                String.valueOf(imagesReceived),
                false,
                null);

        new AnalyticsController(getApplicationContext()).sendAnalytics(
                AnalyticsConstants.Category.FILE_USAGE_STATS,
                AnalyticsConstants.Action.MEDIA_RECIEVED_SIZE,
                null,
                String.valueOf(imageRecievedSize / 1000000),
                false,
                null);

    }

    private void analyticsFileCreated() {


        int filesCreated = 0;
        long fileCreatedSize = 0;
        Uri uri;
        String photo_Date, photoSize, photoPath;
        Cursor cursor;
        int column_date_added, column_size_added, column_name;

        uri = MediaStore.Files.getContentUri("external");
        long currentTime = System.currentTimeMillis();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");


        Date currentDate = new Date(currentTime);
        simpleDateFormat.format(currentDate);


        String orderBy = MediaStore.Files.FileColumns.DATE_ADDED + " DESC";
        String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                + MediaStore.Files.FileColumns.MEDIA_TYPE_NONE;

        cursor = getContentResolver().query(uri, null, selection,
                null, orderBy);


        assert cursor != null;
        column_date_added = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATE_ADDED);
        column_size_added = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.SIZE);
        column_name = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA);


        while (cursor.moveToNext()) {

            photo_Date = cursor.getString(column_date_added);
            photoSize = cursor.getString(column_size_added);
            photoPath = cursor.getString(column_name);

            if (photoPath.contains("Android") || photo_Date == null || photoSize == null) {
                continue;
            }

            Date photoDate = new Date(Long.parseLong(photo_Date.trim()));
            simpleDateFormat.format(photoDate);

            if (printDifference(currentDate, photoDate)) {
                filesCreated++;
                photoSize = photoSize.trim();
                fileCreatedSize += Long.parseLong(photoSize);
            }


            Log.e("FILES1", cursor.getString(column_date_added));
            Log.e("FILES1DATE", photo_Date);

        }

        Log.e("FILES1DATE1", "filesModifiedInADay " + filesCreated);
        new AnalyticsController(getApplicationContext()).sendAnalytics(
                AnalyticsConstants.Category.FILE_USAGE_STATS,
                AnalyticsConstants.Action.FILE_CREATED,
                null,
                String.valueOf(filesCreated),
                false,
                null);


        new AnalyticsController(getApplicationContext()).sendAnalytics(
                AnalyticsConstants.Category.FILE_USAGE_STATS,
                AnalyticsConstants.Action.FILE_CREATED_SIZE,
                null,
                String.valueOf(fileCreatedSize / 1000000),
                false,
                null);
    }

    /**
     * The {@link Comparator} to sort a collection of {@link UsageStats} sorted by the timestamp
     * last time the app was used in the descendant order.
     */
    private static class LastTimeLaunchedComparatorDesc implements Comparator<UsageStats> {

        @Override
        public int compare(UsageStats left, UsageStats right) {
            return Long.compare(right.getLastTimeUsed(), left.getLastTimeUsed());
        }
    }

    /**
     * Match signature of application to identify that if it is signed by system
     * or not.
     *
     * @param packageName package of application. Can not be blank.
     * @return <code>true</code> if application is signed by system certificate,
     * otherwise <code>false</code>
     */
    public boolean isSystemApp(String packageName) {
        try {
            // Get packageinfo for target application
            PackageInfo targetPkgInfo = packageManager.getPackageInfo(
                    packageName, PackageManager.GET_SIGNATURES);
            // Get packageinfo for system package
            PackageInfo sys = packageManager.getPackageInfo(
                    SYSTEM_PACKAGE_NAME, PackageManager.GET_SIGNATURES);
            // Match both packageinfo for there signatures
            return (targetPkgInfo != null && targetPkgInfo.signatures != null && sys.signatures[0]
                    .equals(targetPkgInfo.signatures[0]));
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /**
     * Check if application is preloaded. It also check if the application is
     * signed by system certificate or not.
     *
     * @param packageName package name of application. Can not be null.
     * @return <code>true</code> if package is preloaded and system.
     */
    public boolean isAppPreLoaded(String packageName) {
        if (packageName == null) {
            throw new IllegalArgumentException("Package name can not be null");
        }
        try {
            ApplicationInfo ai = packageManager.getApplicationInfo(
                    packageName, 0);
            // First check if it is preloaded.
            // If yes then check if it is System app or not.
            if (ai != null
                    && (ai.flags & (ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP)) != 0) {
                // Check if signature matches
                if (isSystemApp(packageName) == true) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean isUserInstalledApplication(PackageInfo app) {
        return (app.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 1;
    }

    private long listCombineAppsDateModified(String pkgName) {
        File externalPath = Environment.getExternalStorageDirectory();
        File internal, external;
        File selected;
        String path = externalPath.getName() + "/Android/data/";
        internal = new File("/data/data/" + pkgName);
        selected = internal;
        try {
            external = new File(path + pkgName);
            if (external.lastModified() > internal.lastModified()) {
                selected = external;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return selected.lastModified();
    }

    public long getApkSize(String packageName) throws PackageManager.NameNotFoundException {
        return new File(packageManager.getApplicationInfo(packageName, 0).publicSourceDir).length();
    }
}
