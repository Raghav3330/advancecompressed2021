package com.spaceup;

import android.os.Build;
import androidx.multidex.MultiDexApplication;
import android.util.Log;

import com.spaceup.accessibility.AccessibilityAutomation;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;

/**
 * Created by SAJAL on 11/3/2016.
 */
public class App extends MultiDexApplication {
    private static final String TAG = App.class.getSimpleName();
    private static App sInstance;

    public static App getInstance() {
        return sInstance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/brandon.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());
        Log.d(TAG, "onCreate Finished");
        sInstance = this;
        RemoteConfig.initialize(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable e) {
                    handleUncaughtException(thread, e);
                }
            });
        }
    }

    public void handleUncaughtException(Thread thread, Throwable e) {
        e.printStackTrace(); // not all Android versions will print the stack trace automatically

            try {
                AccessibilityAutomation.getSharedInstance().disableSelf();
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            System.exit(1); // kill off the crashed app
        }





}