package com.spaceup.services;

import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.spaceup.App;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.models.DataModel;
import com.spaceup.uninstall.activities.AppInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.spaceup.apkgenerator.constants.Constants.WHITE_LIST_FILE_ASSET_NAME;
import static com.spaceup.apkgenerator.constants.Constants.WHITE_LIST_FILE_PATH;

/**
 * Created by Sajal Jain on 25-01-2017.
 */

public class RecommendationService extends Service {


    private List<AppInfo> mUserInstalled;
    private Set<String> mFinanceApps;

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("recommend", "onStartCommand");
        Log.d("ALARM_RECEIVER", "RecommendationService Onstart");

        get_Apps(getApplicationContext());

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void get_Apps(final Context ctx) {
        class load_Apps extends AsyncTask<Context, Void, List<AppInfo>> {
            AppDBHelper db = AppDBHelper.getInstance(ctx);
            String TAG = "recommend";

            @Override
            protected void onPostExecute(List<AppInfo> recommendedApps) {
                //super.onPostExecute(aVoid);

                mUserInstalled = recommendedApps;

                //Write to 5 Apps to Top_FIVE_DB
                try {
                    AppDBHelper.getInstance(getApplicationContext()).cleanDb();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < mUserInstalled.size(); i++) {
                    if (mUserInstalled.get(i).getCompressed_status() == -1 && !mUserInstalled.get(i).getPkgName().contains(".stash") && !mUserInstalled.get(i).getPkgName().contains("sample.example.dhruv.sample_apk") && !mUserInstalled.get(i).getPkgName().contains(".spaceup")) {

                        try {
                            Log.d("TOP_5_APPS", mUserInstalled.get(i).getPkgName());
                            AppDBHelper.getInstance(getApplicationContext()).addTop5Application(mUserInstalled.get(i));
                            Log.d(TAG, "LISTING TOP 5 APPS " + mUserInstalled.get(i).getPkgName());


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.d(TAG, "LISTING TOP 5 OTHER " + mUserInstalled.get(i).getPkgName());
                    }
                }

                // Send Broadcast to 5minute POPUP

                Intent babyStashInstallationEvent = new Intent("com.times.DbUpdated");
                babyStashInstallationEvent.putExtra("status", "true");
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(babyStashInstallationEvent);
                Log.d("ALARM_RECEIVER", "RecommendationService Broadcast sent");

                RecommendationService.this.stopSelf();
            }

            // returns list of Finance apps from the financeApps
            private Set<String> getFinanceAppsList() {
                mFinanceApps = new LinkedHashSet<>();
                String line;
                try {
                    InputStream is;
                    File file = new File(Environment.getExternalStorageDirectory().getPath() + Constants.STASH + WHITE_LIST_FILE_PATH);
                    if (file.exists()) {
                        is = new FileInputStream(file);
                    } else {
                        is = App.getInstance().getAssets().open(WHITE_LIST_FILE_ASSET_NAME);
                    }
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    line = reader.readLine();

                    while (line != null) {
                        mFinanceApps.add(line);
                        line = reader.readLine();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return mFinanceApps;
            }


            @Override
            protected List<AppInfo> doInBackground(Context... ctx) {
                List<AppInfo> recommendedList = null;

                if (Build.VERSION.SDK_INT >= 21 && UtilityMethods.getInstance().isHavingUsageStatsPermission(ctx[0])) {
                    recommendedList = foregroundAndDataBasedHeuristics();

                } else {
                    recommendedList = listByCache();
                }

                return recommendedList;
            }


            private List<AppInfo> listByCache() {

                List<AppInfo> userInstalled = new ArrayList<AppInfo>();

                Set<String> financeApps = getFinanceAppsList();
                List<AppInfo> appListDb = db.getAppDetails();
                /*
                Dividing List into three parts in accordance with the Priority
                Keeping Priority of Apps as
                1) Apps below threshold size
                2) Apps above Threshold Size
                3) Finanace Apps
                */
                List<AppInfo> financeAppsInDb = new ArrayList<>(); //Apps on phone falling under Finance category
                List<AppInfo> appsAbvSize = new ArrayList<>(); //Apps list above threshold size of data
                List<AppInfo> appsBelowSize = new ArrayList<>(); //Apps list below threshold size of data

                for (int i = 0; i < appListDb.size(); i++) {
                    if (financeApps.contains(appListDb.get(i).getPkgName())) {
                        financeAppsInDb.add(appListDb.get(i));
                        Log.d("financeApp", appListDb.get(i).getAppName() + " " + appListDb.get(i).getPkgName());
                    } else if (appListDb.get(i).getSize() > 10 * 1024 * 1024)
                        appsAbvSize.add(appListDb.get(i));
                    else
                        appsBelowSize.add(appListDb.get(i));
                }

                Collections.sort(appsAbvSize, new Comparator<AppInfo>() {
                    long crntTime = (System.currentTimeMillis());

                    @Override
                    public int compare(AppInfo o1, AppInfo o2) {


                            long timeFactor1 = crntTime - o1.getLastModifiedDate();
                        long timeFactor2 = crntTime - o2.getLastModifiedDate();

                        // deprioritizing apps which are older than 2 months
                        if (timeFactor1 > 2 * 30 * 24 * 60 * 60 * 1000) {
                            timeFactor1 = 24 * 60 * 60 * 1000;
                        }
                        if (timeFactor2 > 2 * 30 * 24 * 60 * 60 * 1000) {
                            timeFactor2 = 24 * 60 * 60 * 1000;
                        }

                        long cacheFactor1 = o1.getSize();
                        // long cacheFactor2 = o2.getSize();
                        //
                        Log.d("mychck ", "" + (crntTime - o1.getLastModifiedDate()) + "---" + cacheFactor1 + "---" + o1.getAppName());

                        if (timeFactor1 > timeFactor2)
                            return -1;
                        else if (timeFactor1 < timeFactor2)
                            return 1;
                        else
                            return 0;
                    }
                });

                Collections.sort(appsBelowSize, new Comparator<AppInfo>() {
                    long crntTime = (System.currentTimeMillis());

                    @Override
                    public int compare(AppInfo o1, AppInfo o2) {
                        long timeFactor1 = crntTime - o1.getLastModifiedDate();
                        long timeFactor2 = crntTime - o2.getLastModifiedDate();

                        // deprioritizing apps which are older than 2 months
                        if (timeFactor1 > 2 * 30 * 24 * 60 * 60 * 1000) {
                            timeFactor1 = 24 * 60 * 60 * 1000;
                        }
                        if (timeFactor2 > 2 * 30 * 24 * 60 * 60 * 1000) {
                            timeFactor2 = 24 * 60 * 60 * 1000;
                        }

                        long cacheFactor1 = o1.getSize();
                        // long cacheFactor2 = o2.getSize();

                        Log.d("mychck ", "" + (crntTime - o1.getLastModifiedDate()) + "---" + cacheFactor1 + "---" + o1.getAppName());

                        if (timeFactor1 > timeFactor2)
                            return -1;
                        else if (timeFactor1 < timeFactor2)
                            return 1;
                        else
                            return 0;
                    }
                });


                userInstalled.addAll(appsBelowSize);
                userInstalled.addAll(appsAbvSize);
                userInstalled.addAll(financeAppsInDb);

                return userInstalled;
            }


            private List<AppInfo> foregroundAndDataBasedHeuristics() {
                Log.d("Scanning", "I am here2");
                //list recommended by app usage
                List<AppInfo> recommendedList = new ArrayList<>();
                //app list from db
                List<AppInfo> listFromDb = db.getAppDetails();

                Calendar beginCal, endCal;
                beginCal = Calendar.getInstance();
                beginCal.add(Calendar.MONTH, -1);
                List<UsageStats> usageStatsList;
                Log.d(TAG, "Making list of apps on weekly basis for a month");
                endCal = Calendar.getInstance();

                usageStatsList =
                        getUsageStatistics(UsageStatsManager.INTERVAL_WEEKLY, beginCal, endCal);

                // list with pkg name and app name
                //List<AppInfo> userApps = getUserInstalledApplication();
                List<AppInfo> userApps = new ArrayList<>();
                for (AppInfo app : listFromDb) {

                    if (!app.getPkgName().contains(".stash") && app.getCompressed_status() != -2) {
                        //app does not contains .stash and not a system app
                        userApps.add(app);
                    }
                }

                //weekly list total size and total foreground time
                List<Mem> weekylylist = new ArrayList<>();
                //apps with zero foreground time
                List<AppInfo> remainingApps = new ArrayList<>();

                //monthly list total size and total foreground time
                List<Mem> monthlylist = new ArrayList<>();

                //list sorted by application size (REDUNDANT BUT REQUIRED FOR NOW)
                List<Mem> remaininglist = new ArrayList<>();
                HashMap<String, Node> weekly = new HashMap<>();
                for (UsageStats stats : usageStatsList) {
                    if (weekly.containsKey(stats.getPackageName())) {
                        Node n = weekly.get(stats.getPackageName());
                        n.count += 1;
                        n.lastTimeUsed.add(stats.getLastTimeStamp());
                        n.foregroundTime.add(stats.getTotalTimeInForeground());
                        weekly.put(stats.getPackageName(), n);
                    } else {
                        Node n = new Node();
                        n.count = 1;
                        n.lastTimeUsed = new ArrayList<>();
                        n.lastTimeUsed.add(stats.getLastTimeStamp());
                        n.foregroundTime = new ArrayList<>();
                        n.foregroundTime.add(stats.getTotalTimeInForeground());
                        weekly.put(stats.getPackageName(), n);
                    }
                }


                //calc total foreground time of user apps
                for (AppInfo app : userApps) {

                    if (weekly.containsKey(app.getPkgName())) {
                        Mem obj = new Mem();
                        Node n = weekly.get(app.getPkgName());
                        long totalTimeInForeground = 0;
                        for (Long timeInstance : n.foregroundTime) {
                            totalTimeInForeground = totalTimeInForeground + timeInstance;
                        }

                        app.setTotalTimeInForeground(totalTimeInForeground);
                        obj.packageName = app.getPkgName();
                        obj.time = app.getTotalTimeInForeground();
                        weekylylist.add(obj);
                        //update foreground time and last time used in db
                        db.dataUpdateForegroundTime(app.getPkgName(), app.getTotalTimeInForeground(), n.lastTimeUsed.get(n.lastTimeUsed.size() - 1));
                    }
                }


                //Log.d(TAG, userApps.size() + " " + weekylylist.size());
                //calculating foreground time and last time stamp for those apps which are not found in weekly data query
                if (userApps.size() - weekylylist.size() > 0) {
                    //calculate data on monthly basis
                    Log.d(TAG, "6 months data on monthly basis");
                    beginCal = Calendar.getInstance();
                    beginCal.add(Calendar.MONTH, -6);
                    endCal = Calendar.getInstance();
                    usageStatsList = getUsageStatistics(UsageStatsManager.INTERVAL_YEARLY, beginCal, endCal);

                    HashMap<String, Node> monthly = new HashMap<>();
                    for (UsageStats stats : usageStatsList) {
                        if (monthly.containsKey(stats.getPackageName())) {
                            Node n = monthly.get(stats.getPackageName());
                            n.count += 1;
                            n.lastTimeUsed.add(stats.getLastTimeStamp());
                            n.foregroundTime.add(stats.getTotalTimeInForeground());
                            monthly.put(stats.getPackageName(), n);
                        } else {
                            Node n = new Node();
                            n.count = 1;
                            n.lastTimeUsed = new ArrayList<>();
                            n.lastTimeUsed.add(stats.getLastTimeStamp());
                            n.foregroundTime = new ArrayList<>();
                            n.foregroundTime.add(stats.getTotalTimeInForeground());
                            monthly.put(stats.getPackageName(), n);
                        }
                    }
                    //apps not in weekly list calc their total foreground time of user apps and last time used
                    for (AppInfo app : userApps) {
                        if (monthly.containsKey(app.getPkgName()) && !weekly.containsKey(app.getPkgName())) {
                            Mem obj = new Mem();
                            Node n = monthly.get(app.getPkgName());
                            long totalTimeInForeground = 0;
                            for (Long timeInstance : n.foregroundTime) {
                                totalTimeInForeground = totalTimeInForeground + timeInstance;
                            }
                            app.setTotalTimeInForeground(totalTimeInForeground);
                            obj.packageName = app.getPkgName();
                            obj.time = app.getTotalTimeInForeground();
                            monthlylist.add(obj);
                            db.dataUpdateForegroundTime(app.getPkgName(), app.getTotalTimeInForeground(), n.lastTimeUsed.get(n.lastTimeUsed.size() - 1));
                        }
                    }

                    //apps not found in weekly list and monthly list, adding them to remaning list
                    if (userApps.size() - monthlylist.size() > 0) {
                        for (AppInfo app : userApps) {
                            if (!monthly.containsKey(app.getPkgName())) {
                                remainingApps.add(app);
                            }
                        }
                    }
                }

                //fetching data size of remaning apps
                AppDBHelper db = AppDBHelper.getInstance(getApplicationContext());
                for (AppInfo app : remainingApps) {
                    //object for recommended list
                    AppInfo ele = new AppInfo();
                    Mem obj = new Mem();
                    obj.packageName = app.getPkgName();
                    DataModel data = db.getDataSize(app.getPkgName());
                    obj.size = data.getDataSize() + data.getApkSize() + data.getCacheSize() + data.getCodeSize();
                    remaininglist.add(obj);
                }


                //get updated list from db
                listFromDb = db.getAppDetails();
                //getting updated data from db in a hash map
                HashMap<String, AppInfo> updatedDb = new HashMap<>();
                for (AppInfo app : listFromDb) {
                    if (!updatedDb.containsKey(app.getPkgName())) {
                        updatedDb.put(app.getPkgName(), app);
                    }
                }

                Log.d("Scanning", "I am here jadoo" + updatedDb.size());


                Collections.sort(remaininglist, new Comparator<Mem>() {
                    @Override
                    public int compare(Mem o1, Mem o2) {
                        return Long.compare(o1.size, o2.size);
                    }
                });


                Log.d(TAG, "first");
                Set<String> finance = getFinanceAppsList();
                List<AppInfo> financeAppsInDb = new ArrayList<>();

                UtilityMethods methods = UtilityMethods.getInstance();
                for (Mem app : remaininglist) {
                    if (!finance.contains(app.packageName)) {
                        Log.d(TAG, "Package name " + app.packageName);
                        Log.d(TAG, "size " + methods.getSizeinMB(app.size));
                        if (updatedDb.containsKey(app.packageName)) {
                            recommendedList.add(updatedDb.get(app.packageName));
                        }
                    } else {
                        if (updatedDb.containsKey(app.packageName)) {
                            financeAppsInDb.add(updatedDb.get(app.packageName));
                        }
                    }
                }


                Collections.sort(monthlylist, new Comparator<Mem>() {
                    @Override
                    public int compare(Mem o1, Mem o2) {
                        return Long.compare(o1.time, o2.time);
                    }
                });


                Log.d(TAG, "second");
                //to be show in middle
                for (Mem app : monthlylist) {

                    if (!finance.contains(app.packageName)) {


                        Log.d(TAG, "Package name " + app.packageName);

                        String t = String.format("%d min, %d sec",
                                TimeUnit.MILLISECONDS.toMinutes(app.time),
                                TimeUnit.MILLISECONDS.toSeconds(app.time) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(app.time))
                        );

                        if (updatedDb.containsKey(app.packageName)) {
                            recommendedList.add(updatedDb.get(app.packageName));
                        }

                        Log.d(TAG, "time in foreground " + t);
                    } else {
                        if (updatedDb.containsKey(app.packageName)) {
                            financeAppsInDb.add(updatedDb.get(app.packageName));
                        }
                    }
                }


                Collections.sort(weekylylist, new Comparator<Mem>() {
                    @Override
                    public int compare(Mem o1, Mem o2) {
                        return Long.compare(o1.time, o2.time);
                    }
                });


                //to be shown down
                Log.d(TAG, "third");

                for (Mem app : weekylylist) {
                    if (!finance.contains(app.packageName)) {

                        Log.d(TAG, "Package name " + app.packageName);

                        String t = String.format("%d min, %d sec",
                                TimeUnit.MILLISECONDS.toMinutes(app.time),
                                TimeUnit.MILLISECONDS.toSeconds(app.time) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(app.time))
                        );

                        if (updatedDb.containsKey(app.packageName)) {
                            recommendedList.add(updatedDb.get(app.packageName));
                        }

                        Log.d(TAG, "time in foreground " + t);
                    } else {
                        if (updatedDb.containsKey(app.packageName)) {
                            financeAppsInDb.add(updatedDb.get(app.packageName));
                        }
                    }
                }

                Log.d(TAG, "Total number of apps " + userApps.size());
                Log.d(TAG, "weekly apps list " + weekylylist.size());
                Log.d(TAG, "monthly apps list " + monthlylist.size());
                Log.d(TAG, "remaining apps list " + remainingApps.size());

                recommendedList.addAll(financeAppsInDb);

                return recommendedList;
            }


            private List<UsageStats> getUsageStatistics(int intervalType, Calendar beginCal, Calendar endCal) {

                UsageStatsManager mUsageStatsManager = (UsageStatsManager) getApplicationContext()
                        .getSystemService(Context.USAGE_STATS_SERVICE); //Context.USAGE_STATS_SERVICE

                List<UsageStats> queryUsageStats = mUsageStatsManager
                        .queryUsageStats(intervalType, beginCal.getTimeInMillis(), endCal.getTimeInMillis());

                return queryUsageStats;
            }

            private List<AppInfo> getUserInstalledApplication() {
                int flags = PackageManager.GET_META_DATA |
                        PackageManager.GET_SHARED_LIBRARY_FILES;
                PackageManager pm = getPackageManager();

                final List<ApplicationInfo> applications = pm.getInstalledApplications(flags);
                List<AppInfo> userInstalled = new ArrayList<>();

                for (ApplicationInfo app : applications) {
                    if ((app.flags & ApplicationInfo.FLAG_SYSTEM) != 1) {
                        AppInfo apkDetails = new AppInfo();
                        apkDetails.setPkgName(app.packageName);
                        apkDetails.setAppName(app.loadLabel(pm).toString());
                        //AppInfo apkDetails = new AppInfo(app.loadLabel(pm).toString(), app.packageName, 1, 18432, false);
                        userInstalled.add(apkDetails);
                    }
                }
                return userInstalled;
            }

            class Mem {
                long size;
                long time;
                String packageName;
            }

            class Node {
                int count;
                List<Long> lastTimeUsed;
                List<Long> foregroundTime;
            }
        }

        load_Apps a = new load_Apps();
        Context[] myTaskParams = {ctx};
        a.execute(myTaskParams);
    }

}
