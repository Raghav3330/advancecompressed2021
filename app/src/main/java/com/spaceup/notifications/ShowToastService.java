package com.spaceup.notifications;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;


/**
 * Created by Sajal Jain on 21-01-2017.
 */

public class ShowToastService extends IntentService {
    Handler mHandler;


    public ShowToastService() {

        super(ShowToastService.class.getName());

    }

    @Override
    public void onCreate() {

        super.onCreate();
        mHandler = new Handler();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(ShowToastService.this, "Uncompression In Progress!!!", Toast.LENGTH_LONG).show();
            }
        });
    }
}
