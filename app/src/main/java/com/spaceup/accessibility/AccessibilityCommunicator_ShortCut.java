package com.spaceup.accessibility;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.spaceup.Activities.FinalResultScreen;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.Asycs.ApkBackupAndUnInstallGenerator_ShortCut;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.cache.LogCleaner.Cache_Clean.RamCleaner;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.models.AppsCompressedModel;
import com.spaceup.models.DataModel;
import com.spaceup.uninstall.activities.AppInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dhruv on 10/12/16.
 */

public class AccessibilityCommunicator_ShortCut extends Service {

    private ArrayList<String> toCompressAppList, toCompressAppName;
    private String TAG = "Accessibility";
    private int allAppsUninstalled = 0;
    private String state;
    // declaring list of uncompressed apps:- Used in sequentially uninstalling - installing apps
    BroadcastReceiver accessibilityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("com.times.eventAppRemovedShortcut")) {
                if (intent.getExtras().getBoolean("get_packagename")) {
                    if (!state.equals("single_compress")) {
                        AccessibilityAutomation.getSharedInstance().updateAnimation();
                    }
                    Log.d("APPCompressorShortCut", "Accessibility Communicator allAppsUninstalled count " + allAppsUninstalled);
                    List<AppInfo> appInDb = AppDBHelper.getInstance(getApplicationContext()).getAppDetails();
                    for (AppInfo app : appInDb) {
                        if (UtilityMethods.isNotEmptyCollection(toCompressAppName) &&
                                UtilityMethods.isNotEmptyCollection(toCompressAppList) &&
                                app.getPkgName().equals(toCompressAppList.get(allAppsUninstalled))) {
                            long size;
                            size = app.getSize() + app.getCacheSize() + app.getApkSize() + app.getCodeSize();

                            // Analytics
                            Bundle mGAParams;
                            mGAParams = new Bundle();
                            mGAParams.putString("category", AnalyticsConstants.Category.COMPRESS);
                            mGAParams.putString("action", AnalyticsConstants.Action.COMPRESSED_PKG);
                            mGAParams.putString("label", toCompressAppList.get(allAppsUninstalled) + "");
                            new AnalyticsHandler().logGAEvent(mGAParams);
                            Log.d("chckpkg", "onReceive: " + toCompressAppList.get(allAppsUninstalled));

                            AppDBHelper.getInstance(getApplicationContext()).addToShorcutTable(toCompressAppName.get(allAppsUninstalled), toCompressAppList.get(allAppsUninstalled), size);
                            AppDBHelper.getInstance(getApplicationContext()).appQuickCompressed(toCompressAppList.get(allAppsUninstalled));
                            UtilityMethods.getInstance().addToJSONFile(app.getPkgName(), size, app.getLastModifiedDate(), Constants.QUICK_COMPRESSED);
                            Log.d("shortcutfile", "onReceive: adding to file");

                            Log.d("APPCompressorShortCutSI", "ADDING TO SHORTCUT TB " + toCompressAppList.get(allAppsUninstalled) + " Size " + size);
                        }
                    }
                    allAppsUninstalled++;

                    if (UtilityMethods.isNotEmptyCollection(toCompressAppList) && allAppsUninstalled == toCompressAppList.size()) {
                        Log.d(TAG, "ALL APPS UNINSTALLED");
                        AppDBHelper.getInstance(context).deleteCompressedAppData();
                        if (state.equals("single_compress")) {
                            AccessibilityAutomation.getSharedInstance().removeSingleAppUI();
                        } else if (state.equals("outer_shortcut")) {
                            Log.d("TAG", "Performing OUTER SHORTCUT");

                            AccessibilityAutomation.getSharedInstance().removeOverLayOuter();
                        } else {
                            Log.d("TAG", "Performing NORMAL SHORTCUT");

                            AccessibilityAutomation.getSharedInstance().removeOverLay(false, false);
                        }
                        AccessibilityAutomation.getSharedInstance().allowReading(false);

                        Log.d("APPCompressorShortCut", "Accessibility Communicator STOPPED");
                        UtilityMethods.getInstance().addShortcut(getApplicationContext());
                        AccessibilityCommunicator_ShortCut.this.stopSelf();
                    } else {
                        if (!AccessibilityAutomation.getSharedInstance().isAbortFlag()) {
                            Log.d("APPCompressorShortCut", "Accessibility Communicator Calling next backup" + allAppsUninstalled);
                            ApkBackupAndUnInstallGenerator_ShortCut backup = new ApkBackupAndUnInstallGenerator_ShortCut(getApplicationContext(), toCompressAppList.get(allAppsUninstalled));
                            backup.run();
                        } else {
                            long size = 0;
                            if (UtilityMethods.isNotEmptyCollection(toCompressAppList)) {
                                for (int i = 0; i < allAppsUninstalled && i < toCompressAppList.size(); i++) {
                                    DataModel data = AppDBHelper.getInstance(context).getDataSize(toCompressAppList.get(i));
                                    Log.d("AppDBHelper", "package -->" + (toCompressAppList.get(i)));
                                    Log.d("AppDBHelper", "getApkSize -->" + data.getApkSize());
                                    Log.d("AppDBHelper", "getCodeSize -->" + data.getCodeSize());
                                    Log.d("AppDBHelper", "getCacheSize -->" + data.getCacheSize());
                                    Log.d("AppDBHelper", "getDataSize -->" + data.getDataSize());
                                    size += data.getApkSize() + data.getCodeSize() + data.getCacheSize() + data.getDataSize();

                                }

                                Log.d("APPCompressorShortCut", "Accessibility Communicator Process Aborted" + allAppsUninstalled);
                                Intent new_Activity = new Intent(getApplication(), FinalResultScreen.class);
                                new_Activity.putExtra("no_of_apps_compressed", "" + allAppsUninstalled);
                                new_Activity.putExtra("size_saved", "" + size);
                                new_Activity.putExtra("process_abort", true);
                                new_Activity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(new_Activity);

                                Intent finish_uninstaller_intent = new Intent("com.times.finishUnInstallerActivity");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(finish_uninstaller_intent);


                            }
                            AppDBHelper.getInstance(context).deleteCompressedAppData();

                            AccessibilityAutomation.getSharedInstance().removeOverLay(true, false);


                            AccessibilityAutomation.getSharedInstance().allowReading(false);

                            Log.d("APPCompressorShortCut", "Accessibility Communicator STOPPED");
                            UtilityMethods.getInstance().addShortcut(getApplicationContext());

                            AccessibilityCommunicator_ShortCut.this.stopSelf();
                        }

                    }

                }

            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Communicator onStartCalled");
        Log.d(TAG, "is ON");
        Log.d("APPCompressorShortCut", "Accessibility Communicator ON_CREATE");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (null == PrefManager.getInstance(getApplicationContext()).getINSTALLED_APP_TRACKER()) {
            if (AccessibilityAutomation.getSharedInstance() != null)
                AccessibilityAutomation.getSharedInstance().fromActivity(true);

            if (intent != null && intent.getBooleanExtra("non_sticky_intent", false)) {
                Log.d(TAG, "START_NOT_STICKY called");

                return START_NOT_STICKY;
            } else if (intent != null && intent.getStringArrayListExtra("compressList") != null && intent.getStringArrayListExtra("compressList").size() > 0) {

                if (AccessibilityAutomation.isCleanRam()) {
                    // cleanRam();    Commented because of lag user were experiencing
                }

                toCompressAppList = intent.getStringArrayListExtra("compressList");
                toCompressAppName = intent.getStringArrayListExtra("compressName");

                initializeCompressedAppDB(toCompressAppList);


                if (toCompressAppList != null && toCompressAppList.size() > 0) {
                    if (AccessibilityAutomation.getSharedInstance() != null) {
                        AccessibilityAutomation.getSharedInstance().initializeAppsToCompress((ArrayList<String>) toCompressAppList.clone(), (ArrayList<String>) toCompressAppName.clone());
                    }
                    allAppsUninstalled = 0;
                    Log.d("APPCompressorShortCut", "Accessibility Communicator allAppsUninstalled =0 ");

                    IntentFilter accessibilityReceiverIntentFilter = new IntentFilter();

                    accessibilityReceiverIntentFilter.addAction("com.times.eventAppRemovedShortcut");   //Broadcast Listner
                    LocalBroadcastManager.getInstance(this).registerReceiver(accessibilityReceiver,
                            accessibilityReceiverIntentFilter);

                    state = intent.getExtras().getString("state");

                    if (AccessibilityAutomation.getSharedInstance() != null) {
                        if (state.equals("single_compress")) {


                            // initiate overlay when single app compress
                            Log.d(TAG, "State Single App Compression");
                            AccessibilityAutomation.getSharedInstance().overlay_singleApp();
                        } else {
                            AccessibilityAutomation.getSharedInstance().overlay(true, true);
                        }
                        AccessibilityAutomation.getSharedInstance().allowReading(true);
                        Log.d("APPCompressorShortCut", "Accessibility CommunicatorStarting for 1st App ");

                        ApkBackupAndUnInstallGenerator_ShortCut backup = new ApkBackupAndUnInstallGenerator_ShortCut(getApplicationContext(), toCompressAppList.get(0));
                        backup.run();

                    }
                }
                return START_STICKY;
            }
        } else {
            Toast.makeText(getApplicationContext(), "Uncompression in progress !!!", Toast.LENGTH_SHORT).show();
            stopSelf();
            return START_NOT_STICKY;
        }
        return START_STICKY;
    }

    private void cleanRam() {
        List<AppInfo> appList;
        ArrayList<String> temp_packages;
        temp_packages = new ArrayList<>();
        appList = AppDBHelper.getInstance(this).getAppDetails();
        for (AppInfo applist : appList) {
            temp_packages.add(applist.getPkgName());
        }

        RamCleaner ramCleaner = new RamCleaner(this, temp_packages);
        ramCleaner.execute();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Communicator", "onDestroy called");
        Log.d("APPCompressorShortCut", "Destroyed");

        if (AccessibilityAutomation.getSharedInstance() != null) {
            AccessibilityAutomation.getSharedInstance().fromActivity(false);
        }
        LocalBroadcastManager.getInstance(AccessibilityCommunicator_ShortCut.this).unregisterReceiver(accessibilityReceiver);


    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    void initializeCompressedAppDB(ArrayList<String> compressedAppArrayList) {
        AppDBHelper.getInstance(this).deleteCompressedAppData();

        int x = 0;
        for (String app_package : compressedAppArrayList) {
            AppsCompressedModel acm = new AppsCompressedModel();
            acm.setAppName(toCompressAppName.get(x));
            acm.setFolder("");
            acm.setPackageName(app_package);
            acm.setInstall_status("0");
            acm.setUninstall_status("0");
            AppDBHelper.getInstance(this).addCompressedApp(acm);
            x++;
        }
    }

}