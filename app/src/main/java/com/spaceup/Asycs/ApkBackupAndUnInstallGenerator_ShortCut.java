package com.spaceup.Asycs;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.util.Log;

import com.spaceup.accessibility.ActionFailHandler_Shortcut;
import com.spaceup.apkgenerator.constants.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by shashank.tiwari on 10/12/16.
 */

public class ApkBackupAndUnInstallGenerator_ShortCut extends Thread {

    Context context;
    String packageName;
    String dummyApkPath;
    String TAG = "Accessibility";


    public ApkBackupAndUnInstallGenerator_ShortCut(Context context, String packageName) {
        this.context = context;
        this.packageName = packageName;

    }

    @Override
    public void run() {
        super.run();
        load_apk();     // Extract APK
    }

    public void load_apk() {

        extractapk(packageName);
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    public void extractapk(String packagename) {
        Log.d(TAG, "APK BACKUP STARTED FOR APP" + packagename);
        PackageManager packageManager = context.getPackageManager();
        try {
            File file = new File(packageManager.getApplicationInfo(packagename, PackageManager.GET_META_DATA).publicSourceDir);
            File output = new File(Environment.getExternalStorageDirectory().getPath() + Constants.STASH + "/db/" + packagename + ".stash");
            try {
                output.createNewFile();
                FileOutputStream fos;
                try {

                    InputStream assetFile = new FileInputStream(file);
                    fos = new FileOutputStream(output);
                    copyFile(assetFile, fos);
                    fos.close();
                    assetFile.close();
                    // Send an instance to  for uninstalling the app

                    ActionFailHandler_Shortcut action = new ActionFailHandler_Shortcut(context);
                    action.startUninstall(packagename);


                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
