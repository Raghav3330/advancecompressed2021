package com.spaceup.notifications;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.spaceup.Utility.DeleteFromRecyclerBin;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.data.version_one.AppDBHelper;

/**
 * Created by Dhruv Kaushal on 16-12-2016.
 */

public class NotificationService_7PM extends IntentService {

    public NotificationService_7PM() {

        super(NotificationService_7PM.class.getName());

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("ALARM_RECEIVER2PM", "STARTED_ALARM_RECEIVER_2PM");


    }

    @Override
    protected void onHandleIntent(Intent intent) {


        double available_internal_memory = UtilityMethods.getInstance().getAvailableInternalMemorySize();
        long totalInternalMemorySize = UtilityMethods.getInstance().getTotalInternalMemorySize();
        if (available_internal_memory > (totalInternalMemorySize * 0.80)) {
            //SHOW TOAST
            Log.d("less", "notify");
            //Toast.makeText(this, "NOTIFY", Toast.LENGTH_SHORT).show();
        }

        Log.d("ALARM_RECEIVER2PM", "STARTED_ALARM_RECEIVER_7PM");
        if (PrefManager.getInstance(getApplicationContext()).show_noti_two(AppDBHelper.getInstance(getApplicationContext()).total_compressed_Apps())) {
            NotificationHandler.getInstance(getApplicationContext()).two_app_installed_notification(getApplicationContext());
            Log.d("ALARM_RECEIVER2PM", "STARTED_ALARM_RECEIVER_7PM SHOWNOTI");
        } else {
            Log.d("ALARM_RECEIVER2PM", "STARTED_ALARM_RECEIVER_7PM DONT_SHOWNOTI");
        }
    }


}
