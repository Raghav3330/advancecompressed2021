package com.spaceup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spaceup.Activities.ManageApps;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.uninstall.activities.AppInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class popup_bottom_play_store extends Activity {
    public List<AppInfo> appList;
    ArrayList<String> compressList, compressListSize;
    ImageView appOneI, appTwoI, appThreeI;
    long totalSize = 0;
    private Bundle mGAParams;
    private JSONObject mApsalarTracking;
    String appName, appPackageName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_play_store);
        Calendar calendar = Calendar.getInstance();

        mGAParams = new Bundle();
        mGAParams.putString("category", AnalyticsConstants.Category.NOTIFICATION);
        mGAParams.putString("action", AnalyticsConstants.Action.PLAY_STORE_NOTIFICATION);
        mGAParams.putString("label", AnalyticsConstants.Label.CLICKED);
        mGAParams.putLong("value", 0l);
        new AnalyticsHandler().logGAEvent(mGAParams);
        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
// Analytics end
        try {
            appName = getIntent().getExtras().getString("name");
            appPackageName = getIntent().getExtras().getString("packageName");

            TextView title = (TextView) findViewById(R.id.title_text);
            title.setText(appName + "  is already installed!");


        } catch (Exception e) {
            e.printStackTrace();
        }


        TextView name = (TextView) findViewById(R.id.app_name);
        name.setText(appName + " is compressed. You can find compressed apps folder on homescreen.");
        //Apsalar Analytics START
        mApsalarTracking = new JSONObject();
        try {
            mApsalarTracking.put(AnalyticsConstants.Action.PLAY_STORE_NOTIFICATION, 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(AnalyticsConstants.APSALARTAG, "NOTIFICATION: " + mApsalarTracking.toString());
        new AnalyticsHandler().logApsalarJsonEvent(AnalyticsConstants.Key.NOTIFICATION, mApsalarTracking);
        //Apsalar Analytics END


        ImageView close = (ImageView) findViewById(R.id.imageView31);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.outer_clicker);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView saveButton = (TextView) findViewById(R.id.SaveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // open shortcu folder and box
                Intent intent = new Intent(popup_bottom_play_store.this, ManageApps.class);
                intent.putExtra("name", appName);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("packageName", appPackageName);
                startActivity(intent);
                finish();

            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

}
