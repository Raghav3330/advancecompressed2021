package com.spaceup.Adapter;

/**
 * Created by shashank.tiwari on 16/03/17.
 */


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.spaceup.R;
import com.spaceup.View.GridItem;
import com.spaceup.View.GridRecyclerItem;
import com.spaceup.models.ImageModel;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Handler.Callback {

    public final int JUNK_HEADER = 0, OTHER_JUNK_HEADER = 4, OTHER_JUNK_RECYCLER_ITEM = 1, JUNK_RECYCLER_ITEM = 5, OTHER_RECYCLER_ITEM = 6, IGNORE_HEADER = 2, NO_IMAGE = 3;
    public int junkSelectedCount, ignoreSelectedCount, otherJunkSelectedCount;
    public boolean hideOtherJunk = true, hideIgnoredJunk = true;
    Drawable selectedDrawable;
    private List<ImageModel> junkList, ignoredList;
    private ArrayList<String> trashImages;
    private Context context;
    private int junkListSize, ignoreListSize, gridSize;
    //private ThreadPoolExecutor executor;
    private Handler handler, junkHandler;
    private boolean junkSelected, otherJunkSelected, ignoreSelected;
    //TextView selectedCount;


    public RecyclerViewAdapter(Context context, List<ImageModel> junkList, List<ImageModel> ignoredList, ArrayList<String> trashImages, Handler junkHandler) {

        this.junkList = junkList;
        this.ignoredList = ignoredList;
        this.context = context;
        ignoreListSize = ignoredList.size();
        junkListSize = junkList.size();
        this.trashImages = trashImages;

        this.junkHandler = junkHandler;
        selectedDrawable = context.getResources().getDrawable(R.drawable.junk_image_background_selected_drawable);
        handler = new Handler(this);
    }

    @Override
    public boolean handleMessage(Message message) {
        if (message.arg1 == 1 && message.arg2 == 1) {
            junkSelected = true;
        } else if (message.arg1 == 1 && message.arg2 == 2) {
            otherJunkSelected = true;
        } else if (message.arg1 == 1 && message.arg2 == 3) {
            ignoreSelected = true;
        } else if (message.arg1 == -1 && message.arg2 == 1) {
            junkSelected = false;
        } else if (message.arg1 == -1 && message.arg2 == 2) {
            otherJunkSelected = false;
        } else if (message.arg1 == -1 && message.arg2 == 3) {
            ignoreSelected = false;
        }

        sendJunkSelectedCount(junkSelectedCount + ignoreSelectedCount);
        notifyDataSetChanged();

        return false;
    }

    public void setJunkSelection(boolean add)
    {
        new listSelected(add, 1, 0, handler).execute();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case JUNK_HEADER:

                View junkView = LayoutInflater.from(parent.getContext()).inflate(R.layout.junk_header, parent, false);
                JunkImageHeader junkHolder = new JunkImageHeader(junkView);
                return junkHolder;

            case OTHER_JUNK_HEADER:
                View ignoreView = LayoutInflater.from(parent.getContext()).inflate(R.layout.other_junk_header, parent, false);
                OtherJunkHeader ignoreHolder = new OtherJunkHeader(ignoreView);
                return ignoreHolder;

            case IGNORE_HEADER:
                View otherJunkView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ignore_header, parent, false);
                IgnoreListHeader otherJunkHolder = new IgnoreListHeader(otherJunkView);
                return otherJunkHolder;

            case JUNK_RECYCLER_ITEM:
                View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item, parent, false);
                JunkRecyclerViewHolders rcv = new JunkRecyclerViewHolders(layoutView);
                return rcv;

            case OTHER_JUNK_RECYCLER_ITEM:
                View otherJunkLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item, parent, false);
                OtherJunkRecyclerViewHolders rcv1 = new OtherJunkRecyclerViewHolders(otherJunkLayoutView);
                return rcv1;

            case OTHER_RECYCLER_ITEM:
                View otherLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item, parent, false);
                OtherRecyclerViewHolders rcv2 = new OtherRecyclerViewHolders(otherLayoutView);
                return rcv2;

            case NO_IMAGE:

                View noImageView = LayoutInflater.from(parent.getContext()).inflate(R.layout.no_image_layout, parent, false);
                NoImage noImage = new NoImage(noImageView);
                return noImage;
            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof JunkRecyclerViewHolders) {
            JunkRecyclerViewHolders viewHolder = (JunkRecyclerViewHolders) holder;
            Uri uri = null;
            try {
                if (position <= junkList.size()) {
                    uri = Uri.fromFile(new File(junkList.get(position - 1).getPath()));
                    boolean checked = junkList.get(position - 1).isChecked();
                    if (checked) {
                        viewHolder.selector.setVisibility(View.VISIBLE);


                    } else {
                        viewHolder.selector.setVisibility(View.GONE);

                    }
                    viewHolder.checkBox.setChecked(checked);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Picasso.with(context).load(uri)
                    .resize(120, 120).centerCrop().error(R.mipmap.ic_launcher).into(viewHolder.imageView);
        } else if (holder instanceof OtherJunkRecyclerViewHolders) {
            OtherJunkRecyclerViewHolders viewHolder = (OtherJunkRecyclerViewHolders) holder;
            Uri uri = null;
            try {

                if (position <= junkList.size() + 1) {
                    uri = Uri.fromFile(new File(junkList.get(position - 2).getPath()));
                    boolean checked = junkList.get(position - 2).isChecked();
                    if (checked) {
                        viewHolder.selector.setVisibility(View.VISIBLE);


                    } else {
                        viewHolder.selector.setVisibility(View.GONE);

                    }
                    viewHolder.checkBox.setChecked(checked);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            Picasso.with(context).load(uri)
                    .resize(120, 120).centerCrop().error(R.mipmap.ic_launcher).into(viewHolder.imageView);

        } else if (holder instanceof OtherRecyclerViewHolders) {
            OtherRecyclerViewHolders viewHolder = (OtherRecyclerViewHolders) holder;
            Uri uri = null;
            if (!hideOtherJunk) {
                if (position > junkList.size() + 2) {
                    try {
                        uri = Uri.fromFile(new File(ignoredList.get(position - (junkList.size() + 3)).getPath()));
                        boolean checked = ignoredList.get(position - (junkList.size() + 3)).isChecked();
                        if (checked) {
                            viewHolder.selector.setVisibility(View.VISIBLE);

                        } else {
                            viewHolder.selector.setVisibility(View.GONE);

                        }
                        viewHolder.checkBox.setChecked(checked);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } else {
                if (junkList.size() >= 18) {
                    try {
                        uri = Uri.fromFile(new File(ignoredList.get(position - 21).getPath()));
                        boolean checked = ignoredList.get(position - 21).isChecked();
                        if (checked) {
                            viewHolder.selector.setVisibility(View.VISIBLE);

                        } else {
                            viewHolder.selector.setVisibility(View.GONE);

                        }
                        viewHolder.checkBox.setChecked(checked);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                    try {
                        uri = Uri.fromFile(new File(ignoredList.get(position - (junkList.size() + 3)).getPath()));
                        boolean checked = ignoredList.get(position - (junkList.size() + 3)).isChecked();
                        if (checked) {
                            viewHolder.selector.setVisibility(View.VISIBLE);

                        } else {
                            viewHolder.selector.setVisibility(View.GONE);

                        }
                        viewHolder.checkBox.setChecked(checked);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            Picasso.with(context).load(uri)
                    .resize(120, 120).centerCrop().error(R.mipmap.ic_launcher).into(viewHolder.imageView);
        } else if (holder instanceof JunkImageHeader) {
            JunkImageHeader viewHolder = (JunkImageHeader) holder;
            if (junkListSize >= 18)
                viewHolder.junkCount.setText("(" + 18 + ")");
            else
                viewHolder.junkCount.setText("(" + junkList.size() + ")");
            viewHolder.junkSelectedCount.setText("" + junkSelectedCount);
            viewHolder.junkSelector.setChecked(junkSelected);
        } else if (holder instanceof OtherJunkHeader) {
            OtherJunkHeader viewHolder = (OtherJunkHeader) holder;
            if (junkListSize >= 18)
                viewHolder.ignoredCount.setText("(" + (junkList.size() - 18) + ")");
            else
                viewHolder.ignoredCount.setText("(" + 0 + ")");

            if(hideOtherJunk)
                viewHolder.down.setRotation(180);
            else
                viewHolder.down.setRotation(0);
            viewHolder.ignoreSelectedCount.setText("" + otherJunkSelectedCount);
            viewHolder.ignoreSelector.setChecked(otherJunkSelected);
        } else if (holder instanceof IgnoreListHeader) {
            IgnoreListHeader viewHolder = (IgnoreListHeader) holder;
            if(hideIgnoredJunk)
                viewHolder.down.setRotation(180);
            else
                viewHolder.down.setRotation(0);
            viewHolder.ignoredCount.setText("(" + ignoredList.size() + ")");
            viewHolder.ignoreSelectedCount.setText("" + ignoreSelectedCount);
            viewHolder.ignoreSelector.setChecked(ignoreSelected);
        }


    }

    @Override
    public int getItemCount() {
        junkListSize = this.junkList.size();
        ignoreListSize = this.ignoredList.size();
        if (junkListSize == 0)
            return 1;
        else if (junkListSize >= 18) {
            if (hideIgnoredJunk && hideOtherJunk)
                return 18 + 2 + 1;
            else if (hideIgnoredJunk && !hideOtherJunk)
                return junkListSize + 2 + 1;
            else if (!hideIgnoredJunk && hideOtherJunk)
                return 18 + 2 + 1 + ignoreListSize;
            else /*if (!hideIgnoredJunk && !hideOtherJunk)*/
                return junkListSize + ignoreListSize + 2 + 1;
        } else /*if(junkListSize<18)*/ {
            if (hideIgnoredJunk && hideOtherJunk)
                return junkListSize + 2 + 1;
            else if (hideIgnoredJunk && !hideOtherJunk)
                return junkListSize + 2 + 1;
            else if (!hideIgnoredJunk && hideOtherJunk)
                return junkListSize + 2 + 1 + ignoreListSize;
            else /*if (!hideIgnoredJunk && !hideOtherJunk)*/
                return junkListSize + ignoreListSize + 2 + 1;
        }

    }

    @Override
    public int getItemViewType(int position) {

        if (junkListSize == 0)
            return NO_IMAGE;
        else if (junkListSize >= 18) {
            if (!hideIgnoredJunk && !hideOtherJunk) {
                if (position == 0) {
                    return JUNK_HEADER;
                } else if (position > 0 && position <= 18) {
                    return JUNK_RECYCLER_ITEM;
                } else if (position == 19) {
                    return OTHER_JUNK_HEADER;
                } else if (position > 19 && position < junkListSize + 2) {
                    return OTHER_JUNK_RECYCLER_ITEM;
                } else if (position == junkListSize + 2) {
                    return IGNORE_HEADER;
                } else if (position > junkListSize + 2) {
                    return OTHER_RECYCLER_ITEM;
                }
            } else if (!hideIgnoredJunk && hideOtherJunk) {
                if (position == 0)
                    return JUNK_HEADER;
                else if (position > 0 && position <= 18)
                    return JUNK_RECYCLER_ITEM;
                else if (position == 19)
                    return OTHER_JUNK_HEADER;
                else if (position == 20)
                    return IGNORE_HEADER;
                else if (position > 20)
                    return OTHER_RECYCLER_ITEM;
            } else if (hideIgnoredJunk && !hideOtherJunk) {
                if (position == 0)
                    return JUNK_HEADER;
                else if (position > 0 && position <= 18)
                    return JUNK_RECYCLER_ITEM;
                else if (position == 19)
                    return OTHER_JUNK_HEADER;
                else if (position > 19 && position < junkListSize + 2)
                    return OTHER_JUNK_RECYCLER_ITEM;
                else if (position == junkListSize + 2)
                    return IGNORE_HEADER;
            } else if (hideIgnoredJunk && hideOtherJunk) {
                if (position == 0)
                    return JUNK_HEADER;
                else if (position > 0 && position <= 18)
                    return JUNK_RECYCLER_ITEM;
                else if (position == 19)
                    return OTHER_JUNK_HEADER;
                else if (position == 20)
                    return IGNORE_HEADER;

            }
        } else {
            if (!hideIgnoredJunk) {
                if (position == 0) {
                    return JUNK_HEADER;
                } else if (position > 0 && position <= junkListSize) {
                    return JUNK_RECYCLER_ITEM;
                } else if (position == junkListSize + 1) {
                    return OTHER_JUNK_HEADER;
                } else if (position == junkListSize + 1 + 1) {
                    return IGNORE_HEADER;
                } else if (position > junkListSize + 2) {
                    return OTHER_RECYCLER_ITEM;
                }
            } else if (hideIgnoredJunk) {
                if (position == 0)
                    return JUNK_HEADER;
                else if (position > 0 && position <= junkListSize)
                    return JUNK_RECYCLER_ITEM;
                else if (position == junkListSize + 1)
                    return OTHER_JUNK_HEADER;
                else if (position == junkListSize + 2)
                    return IGNORE_HEADER;

            }
        }

        return OTHER_RECYCLER_ITEM;
    }

    void sendJunkSelectedCount(int count) {
        Message msg = new Message();
        msg.arg1 = count;
        junkHandler.sendMessage(msg);
    }

    public class JunkImageHeader extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView junkSelectedCount, junkCount;
        public CheckBox junkSelector;
        public View selector;


        public JunkImageHeader(View view) {
            super(view);
            junkCount = (TextView) view.findViewById(R.id.junk_count);
            junkSelectedCount = (TextView) view.findViewById(R.id.junk_selected_count);
            junkSelector = (CheckBox) view.findViewById(R.id.selected);
            selector = view.findViewById(R.id.selector);
            //view.setOnClickListener(this);
            selector.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            boolean add;
            add = !junkSelector.isChecked();
            new listSelected(add, 1, getPosition(), handler).execute();
        }
    }

    public class OtherJunkHeader extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView ignoreSelectedCount, ignoredCount;
        public CheckBox ignoreSelector;
        public View selector;
        public ImageView down;

        public OtherJunkHeader(View view) {
            super(view);
            ignoredCount = (TextView) view.findViewById(R.id.ignored_count);
            ignoreSelectedCount = (TextView) view.findViewById(R.id.ignored_selected_count);
            ignoreSelector = (CheckBox) view.findViewById(R.id.selected);
            selector = view.findViewById(R.id.selector);
            down = (ImageView) view.findViewById(R.id.chevron_down);
            view.setOnClickListener(this);
            selector.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            boolean add;
            switch (view.getId()) {
                case R.id.other_junk_header:
                    hideOtherJunk = !hideOtherJunk;
                    notifyDataSetChanged();
                    break;
                case R.id.selector:
                    add = !ignoreSelector.isChecked();
                    new listSelected(add, 2, getPosition(), handler).execute();
                    break;
            }
        }
    }

    public class IgnoreListHeader extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView ignoreSelectedCount, ignoredCount;
        public CheckBox ignoreSelector;
        public View selector;
        public ImageView down;

        public IgnoreListHeader(View view) {
            super(view);
            ignoredCount = (TextView) view.findViewById(R.id.ignored_count);
            ignoreSelectedCount = (TextView) view.findViewById(R.id.ignored_selected_count);
            ignoreSelector = (CheckBox) view.findViewById(R.id.selected);
            selector = view.findViewById(R.id.selector);
            down = (ImageView) view.findViewById(R.id.chevron_down);
            view.setOnClickListener(this);
            selector.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            boolean add;
            switch (view.getId()) {
                case R.id.ignore_header:
                    hideIgnoredJunk = !hideIgnoredJunk;
                    notifyDataSetChanged();
                    break;

                case R.id.selector:

                    add = !ignoreSelector.isChecked();

                    new listSelected(add, 3, getPosition(), handler).execute();
                    break;
            }
        }
    }

    public class NoImage extends RecyclerView.ViewHolder implements View.OnClickListener {

        public NoImage(View view) {
            super(view);

            view.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {

        }
    }

    public class JunkRecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

        //public TextView countryName;
        public ImageView /*image,*/selector;
        public CheckBox checkBox;
        public GridItem imageView;
        public GridRecyclerItem background;

        public JunkRecyclerViewHolders(View view) {
            super(view);
            // image = (ImageView) view.findViewById(R.id.whatsapp_image);
            imageView = (GridItem) view.findViewById(R.id.whatsapp_image_grid);
            selector = (ImageView) view.findViewById(R.id.selector);
            checkBox = (CheckBox) view.findViewById(R.id.selected);
            background = (GridRecyclerItem) view.findViewById(R.id.grid_recycleritem);
            //checkBox.setButtonDrawable(R.drawable.checkbox_drawable);
            imageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getPosition();
            if (position <= 18) {
                boolean checked = !junkList.get(position - 1).isChecked();
                junkList.get(position - 1).setChecked(checked);

                if (checked) {
                    trashImages.add(junkList.get(position - 1).getPath());
                    junkSelectedCount++;

                } else {
                    trashImages.remove(junkList.get(position - 1).getPath());
                    junkSelectedCount--;
                }
                notifyItemChanged(0);
                notifyItemChanged(position);

            }/* else {
                boolean checked = !ignoredList.get(position - (junkList.size() + 2)).isChecked();
                ignoredList.get(position - (junkList.size() + 2)).setChecked(checked);
                if (checked) {
                    trashImages.add(ignoredList.get(position - (junkList.size() + 2)).getPath());
                    ignoreSelectedCount++;
                } else {
                    trashImages.remove(ignoredList.get(position - (junkList.size() + 2)).getPath());
                    ignoreSelectedCount--;
                }
                notifyItemChanged(junkList.size() + 1);
                notifyItemChanged(position);
            }*/
           /* selector.setBackgroundResource(R.drawable.junk_image_background_selected_drawable);
            checkBox.setChecked(true);
            sendJunkSelectedCount(junkSelectedCount + ignoreSelectedCount);*/
        }
    }

    public class OtherJunkRecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

        //public TextView countryName;
        public ImageView /*image,*/selector;
        public CheckBox checkBox;
        public GridItem imageView;
        public GridRecyclerItem background;

        public OtherJunkRecyclerViewHolders(View view) {
            super(view);
            // image = (ImageView) view.findViewById(R.id.whatsapp_image);
            imageView = (GridItem) view.findViewById(R.id.whatsapp_image_grid);
            selector = (ImageView) view.findViewById(R.id.selector);
            checkBox = (CheckBox) view.findViewById(R.id.selected);
            background = (GridRecyclerItem) view.findViewById(R.id.grid_recycleritem);
            //checkBox.setButtonDrawable(R.drawable.checkbox_drawable);
            imageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getPosition();
            if (position > 19 && position <= junkList.size() + 1) {
                boolean checked = !junkList.get(position - 2).isChecked();
                junkList.get(position - 2).setChecked(checked);

                if (checked) {
                    trashImages.add(junkList.get(position - 2).getPath());
                    junkSelectedCount++;

                } else {
                    trashImages.remove(junkList.get(position - 2).getPath());
                    junkSelectedCount--;
                }
                notifyItemChanged(19);
                notifyItemChanged(position);

            }
        }
    }

    public class OtherRecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

        //public TextView countryName;
        public ImageView /*image,*/selector;
        public CheckBox checkBox;
        public GridItem imageView;
        public GridRecyclerItem background;

        public OtherRecyclerViewHolders(View view) {
            super(view);
            // image = (ImageView) view.findViewById(R.id.whatsapp_image);
            imageView = (GridItem) view.findViewById(R.id.whatsapp_image_grid);
            selector = (ImageView) view.findViewById(R.id.selector);
            checkBox = (CheckBox) view.findViewById(R.id.selected);
            background = (GridRecyclerItem) view.findViewById(R.id.grid_recycleritem);
            //checkBox.setButtonDrawable(R.drawable.checkbox_drawable);
            imageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getPosition();

            if (!hideOtherJunk /*|| junkList.size()<=18*/) {
                boolean checked = !ignoredList.get(position - (junkList.size() + 3)).isChecked();
                ignoredList.get(position - (junkList.size() + 3)).setChecked(checked);
                if (checked) {
                    trashImages.add(ignoredList.get(position - (junkList.size() + 3)).getPath());
                    ignoreSelectedCount++;
                } else {
                    trashImages.remove(ignoredList.get(position - (junkList.size() + 3)).getPath());
                    ignoreSelectedCount--;
                }
                notifyItemChanged(junkList.size() + 2);
                notifyItemChanged(position);
            } else {
                if (junkList.size() >= 18) {
                    boolean checked = !ignoredList.get(position - (18 + 3)).isChecked();
                    ignoredList.get(position - (18 + 3)).setChecked(checked);
                    if (checked) {
                        trashImages.add(ignoredList.get(position - (18 + 3)).getPath());
                        ignoreSelectedCount++;
                    } else {
                        trashImages.remove(ignoredList.get(position - (18 + 3)).getPath());
                        ignoreSelectedCount--;
                    }
                    notifyItemChanged(18 + 2);
                    notifyItemChanged(position);
                } else {
                    boolean checked = !ignoredList.get(position - (junkList.size() + 3)).isChecked();
                    ignoredList.get(position - (junkList.size() + 3)).setChecked(checked);
                    if (checked) {
                        trashImages.add(ignoredList.get(position - (junkList.size() + 3)).getPath());
                        ignoreSelectedCount++;
                    } else {
                        trashImages.remove(ignoredList.get(position - (junkList.size() + 3)).getPath());
                        ignoreSelectedCount--;
                    }
                    notifyItemChanged(junkList.size() + 2);
                    notifyItemChanged(position);
                }
            }
           /* selector.setBackgroundResource(R.drawable.junk_image_background_selected_drawable);
            checkBox.setChecked(true);
            sendJunkSelectedCount(junkSelectedCount + ignoreSelectedCount);*/
        }
    }

    public class listSelected extends AsyncTask<String, Void, Message> {
        boolean add;
        int junk;
        Handler handler;
        int position;

        listSelected(boolean add, int junk, int position, Handler handler) {
            this.add = add;
            this.junk = junk;
            this.handler = handler;
            this.position = position;
        }


        @Override
        protected Message doInBackground(String... strings) {

            int i = 0;
            if (add) {
                if (junk == 1) {


                    for (ImageModel img : junkList) {
                        trashImages.remove(img.getPath());
                        trashImages.add(img.getPath());
                        img.setChecked(true);
                        i++;
                        if (i == 18 || i == junkList.size())
                            break;
                    }
                    if (junkList.size() > 18)
                        junkSelectedCount = 18;
                    else
                        junkSelectedCount = junkList.size();
                } else if (junk == 2) {

                    for (i = 18; i < junkList.size(); i++) {
                        trashImages.remove(junkList.get(i).getPath());
                        trashImages.add(junkList.get(i).getPath());
                        junkList.get(i).setChecked(true);
                    }
                    if (junkList.size() > 18)
                        otherJunkSelectedCount = junkList.size() - 18;
                    else
                        otherJunkSelectedCount = 0;

                } else if (junk == 3) {
                    for (ImageModel img : ignoredList) {
                        trashImages.remove(img.getPath());
                        trashImages.add(img.getPath());
                        img.setChecked(true);
                    }
                    ignoreSelectedCount = ignoredList.size();
                }
                //trashImages.add("");
            } else {
                if (junk == 1) {

                    for (ImageModel img : junkList) {
                        trashImages.remove(img.getPath());
                        img.setChecked(false);
                        i++;
                        if (i == 18 || i == junkList.size())
                            break;
                    }
                    junkSelectedCount = 0;


                } else if (junk == 2) {

                    for (i = 18; i < junkList.size(); i++) {
                        trashImages.remove(junkList.get(i).getPath());
                        junkList.get(i).setChecked(false);
                    }
                    otherJunkSelectedCount = 0;


                } else if (junk == 3) {
                    for (ImageModel img : ignoredList) {
                        trashImages.remove(img.getPath());
                        img.setChecked(false);
                    }
                    ignoreSelectedCount = 0;

                }
                trashImages.remove("");
            }

            Message message = new Message();
            if (add)
                message.arg1 = 1;
            else
                message.arg1 = -1;

            message.arg2 = junk;


            return message;
        }

        @Override
        protected void onPostExecute(Message message) {
            super.onPostExecute(message);
            handler.dispatchMessage(message);
        }


    }

}