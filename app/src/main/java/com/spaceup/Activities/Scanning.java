package com.spaceup.Activities;

/**
 * Created by shashanktiwari on 30/11/16.
 */

import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.App;
import com.spaceup.BuildConfig;
import com.spaceup.R;
import com.spaceup.RemoteConfig;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.models.DataModel;
import com.spaceup.uninstall.activities.AppInfo;
import com.spaceup.uninstall.activities.UninstallerActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;


import static com.spaceup.apkgenerator.constants.Constants.WHITE_LIST_FILE_ASSET_NAME;
import static com.spaceup.apkgenerator.constants.Constants.WHITE_LIST_FILE_PATH;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class Scanning extends AppCompatActivity {
    public List<AppInfo> userInstalled;
    public static int width, height, top;
    public static int bottom, view_height, top_margin, indicator_height;
    public static Set<String> sFinanceApps;
    private ImageView scanning;
    private TextView scanning_text;
    //checks number of iteration by the glowing bar
    private int x = 0;
    private int mTheme;
    private RelativeLayout upper_layout;
    private MediaPlayer mMediaPlayer;

    private Bundle mGAParams;
    private JSONObject mApsalarTracking;
    public static int sFinanceAppCount = 0;
    private SurfaceHolder mActiveSurface;
    private String mUriScanning;

    private SurfaceHolder mFirstSurface;
    private Uri mVideoUri;
    private ViewGroup mContainer;

    private ImageView imgFreeApp = null;
    private TextView txtFreeApp = null;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scanning);

//        mAdView = new AdView(this);
//        mAdView.setAdSize(AdSize.BANNER);
//        RelativeLayout layout = (RelativeLayout) findViewById(R.id.add_view_rel);
//        if (BuildConfig.DEBUG) {
////            mAdView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");
//            mAdView.setAdUnitId(UtilityMethods.getInstance().getAdmobID(UtilityMethods.SCANNING_SCREEN));
//        } else {
//            mAdView.setAdUnitId(UtilityMethods.getInstance().getAdmobID(UtilityMethods.SCANNING_SCREEN));
//            Log.d("SCANNING : TESING ADS  ",UtilityMethods.getInstance().getAdmobID(UtilityMethods.SCANNING_SCREEN));
//        }
//        AdRequest adRequest = new AdRequest.Builder().build();
//
//        mAdView.loadAd(adRequest);
//        if( RemoteConfig.getBoolean(R.string.show_banner_ads) && UtilityMethods.getInstance().isNewUser(getApplicationContext())) {
//
//            layout.addView(mAdView);
//        }
        try {
            if (getIntent().getExtras().getString("title") != null && getIntent().getExtras().getString("title").equals("TWO_APPS_INSTALLED")) {
                Log.d("LOG", "NOTIFICATION_TWO_APPS_INSTALLED");

                mGAParams = new Bundle();
                mGAParams.putString("category", AnalyticsConstants.Category.NOTIFICATION);
                mGAParams.putString("action", AnalyticsConstants.Action.TWO_APP_INSTALLED);
                mGAParams.putString("label", AnalyticsConstants.Label.CLICKED);
                mGAParams.putLong("value", 1l);
                new AnalyticsHandler().logGAEvent(mGAParams);
                Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                // Analytics end


                //Apsalar Analytics START
                mApsalarTracking = new JSONObject();
                try {
                    mApsalarTracking.put(AnalyticsConstants.Action.TWO_APP_INSTALLED, 1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d(AnalyticsConstants.APSALARTAG, "NOTIFICATION: " + mApsalarTracking.toString());
                new AnalyticsHandler().logApsalarJsonEvent(AnalyticsConstants.Key.NOTIFICATION, mApsalarTracking);
                //Apsalar Analytics END

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mTheme = getIntent().getIntExtra("theme", PrefManager.getInstance(Scanning.this).getTheme());
        //reset theme if started from notification intent
        PrefManager.getInstance(Scanning.this).setTheme(mTheme);

        if (mTheme == 0) {
            mUriScanning = "android.resource://" + getPackageName() + "/" + R.raw.scanning;

            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.HOMESCREEN, AnalyticsConstants.Action.CONVERSION_TO_SCAN_BLUE, AnalyticsConstants.Label.SCAN_CLICKED, null, false, null);

        } else {
            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.HOMESCREEN, AnalyticsConstants.Action.CONVERSION_TO_SCAN_RED, AnalyticsConstants.Label.SCAN_CLICKED, null, false, null);
            mUriScanning = "android.resource://" + getPackageName() + "/" + R.raw.scanning;

        }
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        } else if (Build.VERSION.SDK_INT >= 19) {
            setStatusBarTranslucent(true);
        }

        //userInstalled = new ArrayList<>();


        userInstalled = new ArrayList<>();
        upper_layout = (RelativeLayout) findViewById(R.id.upper_layout);

        scanning = (ImageView) findViewById(R.id.scanning_indicator);
        scanning_text = (TextView) findViewById(R.id.scanning_text);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scanning_text.setText("Scanning Background Processes");
            }
        }, 1000);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scanning_text.setText("Scanning Apps");
            }
        }, 3000);

        get_Apps(Scanning.this);

//        GifView gifView1 = (GifView) findViewById(R.id.gif1);
//        gifView1.setVisibility(View.VISIBLE);
//        gifView1.play();
//        gifView1.setGifResource(R.drawable.scanning4);
//


        SurfaceView first = (SurfaceView) findViewById(R.id.firstSurface);
        first.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {

                mFirstSurface = surfaceHolder;
                String uri = mUriScanning;
                mVideoUri = Uri.parse(uri);

                if (mVideoUri != null) {
                    mMediaPlayer = MediaPlayer.create(getApplicationContext(),
                            mVideoUri, mFirstSurface);
                    try {
                        mMediaPlayer.setLooping(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mActiveSurface = mFirstSurface;
                    mMediaPlayer.start();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            }
        });

        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.SCANNING);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            mMediaPlayer.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (mTheme == 0) {
            //blue
            Resources res = getResources(); //resource handle
            Drawable drawable = res.getDrawable(R.drawable.blue_background); //new Image that was added to the res folder
            upper_layout.setBackground(drawable);

        } else {
            //red
            Resources res = getResources(); //resource handle

        }

    }

    int updateCounter = 0, downCounter = 0;

    @Override
    public void onBackPressed() {
        Bundle paramBundle = new Bundle();
        paramBundle.putString(AnalyticsConstants.Params.DUMMY, "");
        new AnalyticsHandler().logEvent(AnalyticsConstants.Event.SCAN_BACK_PRESSED, paramBundle);
        //super.onBackPressed();
    }


    public void get_Apps(final Context ctx) {
        class load_Apps extends AsyncTask<Context, Void, List<AppInfo>> {
            AppDBHelper db = AppDBHelper.getInstance(Scanning.this);
            String TAG = "RecommendAsync";

            @Override
            protected void onPostExecute(List<AppInfo> recommendedApps) {
                //super.onPostExecute(aVoid);
                userInstalled = recommendedApps;


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            mMediaPlayer.setLooping(false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Intent reviewScreen = new Intent(Scanning.this, UninstallerActivity.class);
                        reviewScreen.putParcelableArrayListExtra("userAppList", (ArrayList<? extends Parcelable>) userInstalled);
                        startActivity(reviewScreen);
                        finish();
                    }
                }, 3000);
                //TODO make a handler send data to main screen

            }

            // returns list of Finance apps from the financeApps
            private Set<String> getFinanceAppsList() {
                sFinanceApps = new LinkedHashSet<>();
                String line;
                try {
                    InputStream is;
                    File file = new File(Environment.getExternalStorageDirectory().getPath() + Constants.STASH + WHITE_LIST_FILE_PATH);
                    if (file.exists()) {
                        is = new FileInputStream(file);
                    } else {
                        is = App.getInstance().getAssets().open(WHITE_LIST_FILE_ASSET_NAME);
                    }
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    line = reader.readLine();

                    while (line != null) {
                        sFinanceApps.add(line);
                        line = reader.readLine();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return sFinanceApps;
            }


            @Override
            protected List<AppInfo> doInBackground(Context... ctx) {
                List<AppInfo> recommendedList = null;


                if (Build.VERSION.SDK_INT >= 21 && UtilityMethods.getInstance().isHavingUsageStatsPermission(ctx[0])) {
                    recommendedList = foregroundAndDataBasedHeuristics();

                } else {
                    recommendedList = listByCache();
                }


                //Write to 5 Apps to Top_FIVE_DB
                try {
                    AppDBHelper.getInstance(getApplicationContext()).cleanDb();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < recommendedList.size(); i++) {
                    if (recommendedList.get(i).getCompressed_status() == -1 && !recommendedList.get(i).getPkgName().contains(".stash") && !recommendedList.get(i).getPkgName().contains("sample.example.dhruv.sample_apk")) {

                        try {
                            AppDBHelper.getInstance(getApplicationContext()).addTop5Application(recommendedList.get(i));
                            Log.d(TAG, "LISTING TOP 5 APPS " + recommendedList.get(i).getPkgName());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.d(TAG, "LISTING TOP 5 OTHER " + recommendedList.get(i).getPkgName());
                    }

                }


                return recommendedList;
            }


            private List<AppInfo> listByCache() {

                List<AppInfo> userInstalled = new ArrayList<AppInfo>();

                Set<String> financeApps = getFinanceAppsList();
                List<AppInfo> appListDb = db.getAppDetails();
                /*
                Dividing List into three parts in accordance with the Priority
                Keeping Priority of Apps as
                1) Apps below threshold size
                2) Apps above Threshold Size
                3) Finanace Apps
                */
                List<AppInfo> financeAppsInDb = new ArrayList<>(); //Apps on phone falling under Finance category
                List<AppInfo> appsAbvSize = new ArrayList<>(); //Apps list above threshold size of data
                List<AppInfo> appsBelowSize = new ArrayList<>(); //Apps list below threshold size of data

                for (int i = 0; i < appListDb.size(); i++) {
                    if (financeApps.contains(appListDb.get(i).getPkgName())) {
                        sFinanceAppCount++;
                        financeAppsInDb.add(appListDb.get(i));
                        Log.d("financeApp", appListDb.get(i).getAppName() + " " + appListDb.get(i).getPkgName());
                    } else if (appListDb.get(i).getSize() > 10 * 1024 * 1024)
                        appsAbvSize.add(appListDb.get(i));
                    else
                        appsBelowSize.add(appListDb.get(i));
                }

                Collections.sort(appsAbvSize, new Comparator<AppInfo>() {
                    long crntTime = (System.currentTimeMillis());

                    @Override
                    public int compare(AppInfo o1, AppInfo o2) {


                        long timeFactor1 = crntTime - o1.getLastModifiedDate();
                        long timeFactor2 = crntTime - o2.getLastModifiedDate();

                        // deprioritizing apps which are older than 2 months
                        if (timeFactor1 > 2 * 30 * 24 * 60 * 60 * 1000) {
                            timeFactor1 = 24 * 60 * 60 * 1000;
                        }
                        if (timeFactor2 > 2 * 30 * 24 * 60 * 60 * 1000) {
                            timeFactor2 = 24 * 60 * 60 * 1000;
                        }

                        long cacheFactor1 = o1.getSize();
                        // long cacheFactor2 = o2.getSize();
                        //
                        Log.d("mychck ", "" + (crntTime - o1.getLastModifiedDate()) + "---" + cacheFactor1 + "---" + o1.getAppName());

                        if (timeFactor1 > timeFactor2)
                            return -1;
                        else if (timeFactor1 < timeFactor2)
                            return 1;
                        else
                            return 0;
                    }
                });

                Collections.sort(appsBelowSize, new Comparator<AppInfo>() {
                    long crntTime = (System.currentTimeMillis());

                    @Override
                    public int compare(AppInfo o1, AppInfo o2) {
                        long timeFactor1 = crntTime - o1.getLastModifiedDate();
                        long timeFactor2 = crntTime - o2.getLastModifiedDate();

                        // deprioritizing apps which are older than 2 months
                        if (timeFactor1 > 2 * 30 * 24 * 60 * 60 * 1000) {
                            timeFactor1 = 24 * 60 * 60 * 1000;
                        }
                        if (timeFactor2 > 2 * 30 * 24 * 60 * 60 * 1000) {
                            timeFactor2 = 24 * 60 * 60 * 1000;
                        }

                        long cacheFactor1 = o1.getSize();
                        // long cacheFactor2 = o2.getSize();

                        Log.d("mychck ", "" + (crntTime - o1.getLastModifiedDate()) + "---" + cacheFactor1 + "---" + o1.getAppName());

                        if (timeFactor1 > timeFactor2)
                            return -1;
                        else if (timeFactor1 < timeFactor2)
                            return 1;
                        else
                            return 0;
                    }
                });


                userInstalled.addAll(appsBelowSize);
                userInstalled.addAll(appsAbvSize);
                userInstalled.addAll(financeAppsInDb);


                return userInstalled;
            }

            private List<AppInfo> foregroundAndDataBasedHeuristics() {
                Log.d("Scanning", "I am here2");
                //list recommended by app usage
                List<AppInfo> recommendedList = new ArrayList<>();
                //app list from db
                List<AppInfo> listFromDb = db.getAppDetails();

                Calendar beginCal, endCal;
                beginCal = Calendar.getInstance();
                beginCal.add(Calendar.MONTH, -1);
                List<UsageStats> usageStatsList;
                Log.d(TAG, "Making list of apps on weekly basis for a month");
                endCal = Calendar.getInstance();

                usageStatsList =
                        getUsageStatistics(UsageStatsManager.INTERVAL_WEEKLY, beginCal, endCal);

                // list with pkg name and app name
                //List<AppInfo> userApps = getUserInstalledApplication();
                List<AppInfo> userApps = new ArrayList<>();
                for (AppInfo app : listFromDb) {

                    if (!app.getPkgName().contains(".stash") && app.getCompressed_status() != -2) {
                        //app does not contains .stash and not a system app
                        ApplicationInfo applicationInfo = null;
                        try {
                            applicationInfo = getApplicationContext().getPackageManager().getApplicationInfo(app.getPkgName(), 0);
                            if ((applicationInfo.flags & ApplicationInfo.FLAG_IS_GAME) != ApplicationInfo.FLAG_IS_GAME) {

                                userApps.add(app);
                                Log.d("GAMES NOT", ApplicationInfo.FLAG_IS_GAME + " package Name " + app.getPkgName());

                            } else {
                                Log.d("GAMES ", ApplicationInfo.FLAG_IS_GAME + " package Name " + app.getPkgName());
                            }
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }


                    }
                }

                //weekly list total size and total foreground time
                List<Mem> weekylylist = new ArrayList<>();
                //apps with zero foreground time
                List<AppInfo> remainingApps = new ArrayList<>();

                //monthly list total size and total foreground time
                List<Mem> monthlylist = new ArrayList<>();

                //list sorted by application size (REDUNDANT BUT REQUIRED FOR NOW)
                List<Mem> remaininglist = new ArrayList<>();
                HashMap<String, Node> weekly = new HashMap<>();
                for (UsageStats stats : usageStatsList) {
                    if (weekly.containsKey(stats.getPackageName())) {
                        Node n = weekly.get(stats.getPackageName());
                        n.count += 1;
                        n.lastTimeUsed.add(stats.getLastTimeStamp());
                        n.foregroundTime.add(stats.getTotalTimeInForeground());
                        weekly.put(stats.getPackageName(), n);
                    } else {
                        Node n = new Node();
                        n.count = 1;
                        n.lastTimeUsed = new ArrayList<>();
                        n.lastTimeUsed.add(stats.getLastTimeStamp());
                        n.foregroundTime = new ArrayList<>();
                        n.foregroundTime.add(stats.getTotalTimeInForeground());
                        weekly.put(stats.getPackageName(), n);
                    }
                }


                //calc total foreground time of user apps
                for (AppInfo app : userApps) {

                    if (weekly.containsKey(app.getPkgName())) {
                        Mem obj = new Mem();
                        Node n = weekly.get(app.getPkgName());
                        long totalTimeInForeground = 0;
                        for (Long timeInstance : n.foregroundTime) {
                            totalTimeInForeground = totalTimeInForeground + timeInstance;
                        }

                        app.setTotalTimeInForeground(totalTimeInForeground);
                        obj.packageName = app.getPkgName();
                        obj.time = app.getTotalTimeInForeground();
                        weekylylist.add(obj);
                        //update foreground time and last time used in db
                        db.dataUpdateForegroundTime(app.getPkgName(), app.getTotalTimeInForeground(), n.lastTimeUsed.get(n.lastTimeUsed.size() - 1));
                    }
                }


                //Log.d(TAG, userApps.size() + " " + weekylylist.size());
                //calculating foreground time and last time stamp for those apps which are not found in weekly data query
                if (userApps.size() - weekylylist.size() > 0) {
                    //calculate data on monthly basis
                    Log.d(TAG, "6 months data on monthly basis");
                    beginCal = Calendar.getInstance();
                    beginCal.add(Calendar.MONTH, -6);
                    endCal = Calendar.getInstance();
                    usageStatsList = getUsageStatistics(UsageStatsManager.INTERVAL_YEARLY, beginCal, endCal);

                    HashMap<String, Node> monthly = new HashMap<>();
                    for (UsageStats stats : usageStatsList) {
                        if (monthly.containsKey(stats.getPackageName())) {
                            Node n = monthly.get(stats.getPackageName());
                            n.count += 1;
                            n.lastTimeUsed.add(stats.getLastTimeStamp());
                            n.foregroundTime.add(stats.getTotalTimeInForeground());
                            monthly.put(stats.getPackageName(), n);
                        } else {
                            Node n = new Node();
                            n.count = 1;
                            n.lastTimeUsed = new ArrayList<>();
                            n.lastTimeUsed.add(stats.getLastTimeStamp());
                            n.foregroundTime = new ArrayList<>();
                            n.foregroundTime.add(stats.getTotalTimeInForeground());
                            monthly.put(stats.getPackageName(), n);
                        }
                    }
                    //apps not in weekly list calc their total foreground time of user apps and last time used
                    for (AppInfo app : userApps) {
                        if (monthly.containsKey(app.getPkgName()) && !weekly.containsKey(app.getPkgName())) {
                            Mem obj = new Mem();
                            Node n = monthly.get(app.getPkgName());
                            long totalTimeInForeground = 0;
                            for (Long timeInstance : n.foregroundTime) {
                                totalTimeInForeground = totalTimeInForeground + timeInstance;
                            }
                            app.setTotalTimeInForeground(totalTimeInForeground);
                            obj.packageName = app.getPkgName();
                            obj.time = app.getTotalTimeInForeground();
                            monthlylist.add(obj);
                            db.dataUpdateForegroundTime(app.getPkgName(), app.getTotalTimeInForeground(), n.lastTimeUsed.get(n.lastTimeUsed.size() - 1));
                        }
                    }

                    //apps not found in weekly list and monthly list, adding them to remaning list
                    if (userApps.size() - monthlylist.size() > 0) {
                        for (AppInfo app : userApps) {
                            if (!monthly.containsKey(app.getPkgName())) {
                                remainingApps.add(app);
                            }
                        }
                    }
                }

                //fetching data size of remaning apps
                AppDBHelper db = AppDBHelper.getInstance(Scanning.this);
                for (AppInfo app : remainingApps) {
                    //object for recommended list
                    AppInfo ele = new AppInfo();
                    Mem obj = new Mem();
                    obj.packageName = app.getPkgName();
                    DataModel data = db.getDataSize(app.getPkgName());
                    obj.size = data.getDataSize() + data.getApkSize() + data.getCacheSize() + data.getCodeSize();
                    remaininglist.add(obj);
                }


                //get updated list from db
                listFromDb = db.getAppDetails();
                //getting updated data from db in a hash map
                HashMap<String, AppInfo> updatedDb = new HashMap<>();
                for (AppInfo app : listFromDb) {
                    if (!updatedDb.containsKey(app.getPkgName())) {
                        updatedDb.put(app.getPkgName(), app);
                    }
                }

                Log.d("Scanning", "I am here jadoo" + updatedDb.size());


                Collections.sort(remaininglist, new Comparator<Mem>() {
                    @Override
                    public int compare(Mem o1, Mem o2) {
                        return Long.compare(o1.size, o2.size);
                    }
                });


                Log.d(TAG, "first");
                Set<String> finance = getFinanceAppsList();
                List<AppInfo> financeAppsInDb = new ArrayList<>();

                UtilityMethods methods = UtilityMethods.getInstance();
                for (Mem app : remaininglist) {
                    if (!finance.contains(app.packageName)) {
                        Log.d(TAG, "Package name 1 " + app.packageName);
                        Log.d(TAG, "size " + methods.getSizeinMB(app.size));
                        if (updatedDb.containsKey(app.packageName)) {
                            recommendedList.add(updatedDb.get(app.packageName));
                        }
                    } else {
                        Log.d(TAG, "foregroundAndDataBasedHeuristics: BCBCBCBBCBCBC");
                        sFinanceAppCount++;
                        if (updatedDb.containsKey(app.packageName)) {
                            financeAppsInDb.add(updatedDb.get(app.packageName));
                        }
                    }
                }


                Collections.sort(monthlylist, new Comparator<Mem>() {
                    @Override
                    public int compare(Mem o1, Mem o2) {
                        return Long.compare(o1.time, o2.time);
                    }
                });


                Log.d(TAG, "second");
                //to be show in middle
                for (Mem app : monthlylist) {

                    if (!finance.contains(app.packageName)) {


                        Log.d(TAG, "Package name 2 " + app.packageName);

                        String t = String.format(Locale.ENGLISH, "%d min, %d sec",
                                TimeUnit.MILLISECONDS.toMinutes(app.time),
                                TimeUnit.MILLISECONDS.toSeconds(app.time) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(app.time))
                        );

                        if (updatedDb.containsKey(app.packageName)) {
                            recommendedList.add(updatedDb.get(app.packageName));
                        }

                        Log.d(TAG, "time in foreground " + t);
                    } else {
                        sFinanceAppCount++;

                        if (updatedDb.containsKey(app.packageName)) {
                            financeAppsInDb.add(updatedDb.get(app.packageName));

                        }
                    }
                }


                Collections.sort(weekylylist, new Comparator<Mem>() {
                    @Override
                    public int compare(Mem o1, Mem o2) {
                        return Long.compare(o1.time, o2.time);
                    }
                });


                //to be shown down
                Log.d(TAG, "third");

                for (Mem app : weekylylist) {
                    if (!finance.contains(app.packageName)) {

                        Log.d(TAG, "Package name 3 " + app.packageName);

                        String t = String.format(Locale.ENGLISH, "%d min, %d sec",
                                TimeUnit.MILLISECONDS.toMinutes(app.time),
                                TimeUnit.MILLISECONDS.toSeconds(app.time) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(app.time))
                        );

                        if (updatedDb.containsKey(app.packageName)) {
                            recommendedList.add(updatedDb.get(app.packageName));
                        }

                        Log.d(TAG, "time in foreground " + t);
                    } else {
                        sFinanceAppCount++;

                        if (updatedDb.containsKey(app.packageName)) {
                            financeAppsInDb.add(updatedDb.get(app.packageName));
                        }
                    }
                }

                Log.d(TAG, "Total number of apps " + userApps.size());
                Log.d(TAG, "weekly apps list " + weekylylist.size());
                Log.d(TAG, "monthly apps list " + monthlylist.size());
                Log.d(TAG, "remaining apps list " + remainingApps.size());

                recommendedList.addAll(financeAppsInDb);

                return recommendedList;
            }

            private List<UsageStats> getUsageStatistics(int intervalType, Calendar beginCal, Calendar endCal) {

                UsageStatsManager mUsageStatsManager = (UsageStatsManager) getApplicationContext()
                        .getSystemService(Context.USAGE_STATS_SERVICE); //Context.USAGE_STATS_SERVICE

                List<UsageStats> queryUsageStats = mUsageStatsManager != null ? mUsageStatsManager
                        .queryUsageStats(intervalType, beginCal.getTimeInMillis(), endCal.getTimeInMillis()) : null;

                assert queryUsageStats != null;
                if (queryUsageStats.size() == 0) {
                    Log.d(TAG, "The user may not allow the access to apps usage.");
//                    Toast.makeText(getApplicationContext(),getString(R.string.explanation_access_to_appusage_is_not_enabled),Toast.LENGTH_LONG).show();

                    startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));

                }
                return queryUsageStats;
            }

            private List<AppInfo> getUserInstalledApplication() {
                int flags = PackageManager.GET_META_DATA |
                        PackageManager.GET_SHARED_LIBRARY_FILES;
                PackageManager pm = getPackageManager();

                final List<ApplicationInfo> applications = pm.getInstalledApplications(flags);
                List<AppInfo> userInstalled = new ArrayList<>();

                for (ApplicationInfo app : applications) {
                    if ((app.flags & ApplicationInfo.FLAG_SYSTEM) != 1) {
                        AppInfo apkDetails = new AppInfo();
                        apkDetails.setPkgName(app.packageName);
                        apkDetails.setAppName(app.loadLabel(pm).toString());
                        //AppInfo apkDetails = new AppInfo(app.loadLabel(pm).toString(), app.packageName, 1, 18432, false);
                        userInstalled.add(apkDetails);
                    }
                }
                return userInstalled;
            }

            class Mem {
                long size;
                long time;
                String packageName;
            }

            class Node {
                int count;
                List<Long> lastTimeUsed;
                List<Long> foregroundTime;
            }
        }

        load_Apps a = new load_Apps();
        Context[] myTaskParams = {ctx};
        a.execute(myTaskParams);

    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    public class MVAccelerateDecelerateInterpolator implements Interpolator {

        // easeInOutQuint
        public float getInterpolation(float t) {
            /*float x = t*2.0f;
            if (t<0.5f) return 0.5f*x*x*x*x*x;
            x = (t-0.5f)*2-1;*/
            Log.d("accelerator", "" + t);

            return t * t * t;
        }
    }

    public class MVAccelerateInterpolator implements Interpolator {

        int a, b, c, d;
        Random random;

        // easeInOutQuint
        public float getInterpolation(float t) {

            double e = 3;
            double time = (double) t;
           /* if(Math.pow(e,t));
            return a*(1-t*t*t)+3*b*(1-t*t)*t+3*c*(1-t)*t*t+d*t*t*t;*/
            return (float) Math.pow(e, time);
        }
    }

}