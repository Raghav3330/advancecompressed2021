package com.spaceup.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import com.spaceup.Alarm.AlarmService_Controller;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by dhurv on 22-01-2017.
 */
public class AutoStartUp extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {


        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            // Starting 4 days Alarm
            PrefManager prefManager = PrefManager.getInstance(getApplicationContext());
            prefManager.set_is_FIRSTNOTIFICATION(false, "NotificationService_4days");
            new AlarmService_Controller(getApplicationContext()).every4days();

        }

    }

}
