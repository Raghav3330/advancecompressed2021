package com.spaceup.Utility;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.textservice.SentenceSuggestionsInfo;
import android.view.textservice.SpellCheckerSession;
import android.view.textservice.SuggestionsInfo;
import android.view.textservice.TextInfo;
import android.view.textservice.TextServicesManager;

import com.spaceup.DataInerfaceSuggestionCount;
import com.spaceup.SpellCheckActivity;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dhurv on 16-03-2017.
 */

public class SpellCheckerUtility{
    public boolean isStringAlpha(String aString){
        if( aString.length() <2){
            return false;
        }
        Log.d("SPELL_CHECK",aString);
        int charCount=0;
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!:";
        if(aString.length() == 0) return false;//zero length string ain't alpha
        for(int i=0;i<aString.length();i++){
            for(int j=0;j<alphabet.length();j++){
                if(aString.substring(i,i+1).equals(alphabet.substring(j,j+1))
                        || aString.substring(i,i+1).equals(alphabet.substring(j,j+1).toLowerCase()))
                    charCount++;
            }
            if(charCount != (i+1)){
                System.out.println("\n**Invalid input! Enter alpha values**\n");
                return false;
            }
        }
        return true;
    }
}
