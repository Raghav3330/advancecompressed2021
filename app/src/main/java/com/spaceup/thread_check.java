package com.spaceup;


import android.app.Dialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

public class thread_check extends AppCompatActivity {

    private Dialog dialogDeleting;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thread_check);
        dialogDeleting = new Dialog(thread_check.this, R.style.Dialog1);
        dialogDeleting.setContentView(R.layout.image_trash_deleting_progress);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialogDeleting.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.gravity = Gravity.BOTTOM;
        lp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        window.setAttributes(lp);
        try {

            dialogDeleting.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
