package com.spaceup;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.RemoteException;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityAutomation;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.models.DataModel;
import com.spaceup.notifications.NotificationHandler;
import com.spaceup.uninstall.activities.AppInfo;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import static com.spaceup.accessibility.AccessibilityAutomation.installingFlag;

/**
 * Created by dhruv on 30-11-2016.
 */
public class YourReceiver extends BroadcastReceiver {

    private final String path = Environment.getExternalStorageDirectory().getPath()
            + Constants.APK_DB + "/" + Constants.STASH_DB_FILE + ".json";
    private final String TAG = "Accessibility";
    private final CountDownTimer mAppUpdateTimer = new CountDownTimer(60000, 10000) {

        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            UtilityMethods.setAppUpdateGoingOn(false);
        }
    };
    private String versionName = null;

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.d(TAG, "YOUR RECEIVER ACTION LOL ");

        String packageName = intent.getData().getEncodedSchemeSpecificPart();
        if (packageName.equals("com.stash.junkcleaner")) {

            PrefManager.getInstance(context).putBoolean(PrefManager.GLOBAL_TURNOFF_BUBBLE_PERMANENT, true);
            new AnalyticsController(context).sendAnalytics(AnalyticsConstants.Category.STASH_REF,
                    AnalyticsConstants.Action.APP_INSTALLED,
                    null,
                    null,
                    false,
                    null);

        }
        Bundle b = intent.getExtras();
        int uid = b.getInt(Intent.EXTRA_UID);
        Log.d(TAG, "YOUR RECEIVER ACTION" + uid);

        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_PACKAGE_REPLACED)) {
            if (RemoteConfig.getBoolean(R.string.app_upgrade_check)) {
                mAppUpdateTimer.cancel();
                mAppUpdateTimer.start();
                UtilityMethods.setAppUpdateGoingOn(true);
            }
        }

        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_PACKAGE_REPLACED) || intent.getAction().equalsIgnoreCase(Intent.ACTION_PACKAGE_REMOVED)) {
            String appToRemove = packageName;
            if (appToRemove.contains(Constants.APK_EXTENSION)) {
                //support previous dummy apps
                // doing this to handle removal of dummy apps :
                appToRemove = appToRemove.replace(".stash", "");
            }
            AppDBHelper.getInstance(context).appUninstalled(appToRemove);
            UtilityMethods.getInstance().removeFromJSONFile(appToRemove);
            if (packageName.contains(".stash")) {

                if (AccessibilityAutomation.getSharedInstance() != null && AccessibilityAutomation.getSharedInstance().getCleaningFlag()) {
                    Intent i = new Intent("com.times.doneApkGenerator");
                    i.putExtra("status", "UnInstalled");
                    LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(i);

                    Intent uninstallIntent = new Intent("com.times.accessibilityReceiver");
                    uninstallIntent.putExtra("compress_single_app", true);
                    LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(uninstallIntent);

                    AccessibilityAutomation.getSharedInstance().deactivateCleaningFlag();
                } else {
                    try {
                        AccessibilityAutomation.getSharedInstance().getActionFailHandlerReactivate().setUninstall_handler(false, null, false, context);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    Intent babyStashInstallationEvent = new Intent("com.times.eventDummyAppRemoved");
                    babyStashInstallationEvent.putExtra("get_packagename", true);
                    babyStashInstallationEvent.putExtra("package_removed", packageName.replace(".stash", ""));
                    LocalBroadcastManager.getInstance(context).sendBroadcast(babyStashInstallationEvent);

                    try {
                        AccessibilityAutomation.getSharedInstance().flick();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {

                        //AppDBHelper.getInstance(context).dummyAppRemoved(packageName);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    Log.d("YourReceiver", "Dummy app removed" + packageName);
                }
            } else {

                Log.d("AccessibilityReactivate", "1eventDummyAppRemovedNew 1 ");

                Intent babyStashInstallationEvent = new Intent("com.times.eventAppRemovedShortcut");
                babyStashInstallationEvent.putExtra("get_packagename", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(babyStashInstallationEvent);


                // New Process V18 Reactivate Activity
                Log.d("AccessibilityReactivate", "eventDummyAppRemovedNew 2 ");
               /* try {
                    AccessibilityAutomation.getSharedInstance().getActionFailHandlerReactivate().setUninstall_handler(false, null, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                Intent dummyAppUninstallEvent = new Intent(Constants.Receiver.DUMMY_INSTALL_RECEIVER);
                dummyAppUninstallEvent.putExtra("get_packagename", true);
                dummyAppUninstallEvent.putExtra("package_removed", packageName);
                LocalBroadcastManager.getInstance(context).sendBroadcast(dummyAppUninstallEvent);

                try {
                    AccessibilityAutomation.getSharedInstance().flick();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (AppDBHelper.getInstance(context).getUninstallFlag(packageName) == 0) {
                    Log.d("Accessibility1", "Package Removed Broadcast Read" + packageName);

                    ContentValues values = new ContentValues();
                    values.put("uninstall_status", "1");
                    int updatedApp = AppDBHelper.getInstance(context).updateCompressedApp(values, packageName);
                    if (updatedApp == 1) {

                        try {
                            if (AccessibilityAutomation.getSharedInstance() != null) {
                                AccessibilityAutomation.getSharedInstance().deactivateUninstallFlag();
                                AccessibilityAutomation.getSharedInstance().getActionFailHandler().refreshUninstallCounter();
                                AccessibilityAutomation.getSharedInstance().getActionFailHandler().setUninstall_handler(false, null, false);
                                AccessibilityAutomation.getSharedInstance().flick();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        UtilityMethods.getInstance().throwInstallEvent(context, packageName);


                    }
                }


                Intent appRemoved = new Intent("com.times.eventAppRemoved");
                appRemoved.putExtra("get_packagename", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(appRemoved);
                // You just lost an app ! POPUP
                // Send call back to Accessibility_SHORTCUT
                if (!UtilityMethods.getInstance().isAccessibilityServiceRunning1(context)) {

                    if (!isPackageExisted(packageName, context)) {
                        // show popup

                        NotificationHandler.getInstance(context).custom_sad_noti(context);

                    }
                }

                //AppDBHelper.getInstance(context).appUninstalled(packageName);

            }

        } else {
            Log.d(TAG, "ASSET_LINE ABOVE" + packageName);
            // Remove the apk from Shortcut Db

            if (packageName.contains(".stash")) {


                Calendar d = Calendar.getInstance();
                PrefManager.getInstance(context).setLastCompressTimeStamp(d.getTimeInMillis());
                Log.d("Theme", "BUG TestActivity time set in last compress time stamp " + d.getTimeInMillis());

                if (installingFlag) {

                    String originalPkgName = packageName.replace(".stash", "");
                    if (AppDBHelper.getInstance(context).getInstallationFlag(originalPkgName) == 0) {

                        Log.d("Accessibility1", "Package Installed Broadcast Read" + packageName);

                        try {
                            installingFlag = false;
                            AccessibilityAutomation.getSharedInstance().flick();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    /*try {

                                        AccessibilityAutomation.getSharedInstance().getActionFailHandler().setInstall_handle(false, null, false);
                                        AccessibilityAutomation.getSharedInstance().getActionFailHandler().refreshInstallCounter();
                                        AccessibilityAutomation.getSharedInstance().throwUninstallEvent();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }*/
                                }
                            }, 2000);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                Log.d("AccessibilityY", "dummy app installed" + packageName);
                try {
                    Log.d("YourReceiver", "dummy app installed" + packageName);
                    setDataSize(context, packageName);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                String originalPkgName = packageName.replace(".stash", "");
                Log.d("file", "REDUCED PACKAGE NAME IS " + originalPkgName);
                Log.d("file", "start file operation");
                File dbfile = new File(path);

                DataModel dm = AppDBHelper.getInstance(context).getDataSize(originalPkgName);
                int count;
                long size = 0;
                int new_count = 0;
                long size_to_added = dm.getCodeSize() + dm.getCacheSize() + dm.getApkSize() + dm.getDataSize();
                if (dbfile.exists()) {
                    Log.d("file", "file already exists");
                    //reading file

                    JSONParser parser = new JSONParser();
                    JSONArray apps = null;
                    JSONObject jsonObject;
                    try {
                        Log.d("file", "inside try catch");
                        Object obj = parser.parse(new FileReader(path));
                        Log.d("file", "path object is initialized");
                        jsonObject = (JSONObject) obj;
                        count = (int) ((long) jsonObject.get("count"));
                        size = (long) jsonObject.get("size");
                        Log.d("file ", "old count " + count);
                        Log.d("file", "old size" + size);

                        //making changes
                        new_count = count + 1;

                        //reading json array of apps so that we can append new app
                        apps = (JSONArray) jsonObject.get("apps");
                        if (null == apps) {
                            apps = new JSONArray();
                        }
                        JSONObject app = new JSONObject();
                        app.put("pkgname", originalPkgName);
                        app.put("size", size_to_added);
                        apps.add(app);
                        size_to_added = size + size_to_added;

                        //obj.put("apps",applist);

                        JSONArray msg = (JSONArray) jsonObject.get("apps");
                        Iterator<JSONObject> iterator = msg.iterator();
                        while (iterator.hasNext()) {
                            JSONObject ap = iterator.next();
                            //System.out.println(iterator.next());
                            Log.d("file", "package name " + ap.get("pkgname") + " size is" + ap.get("size"));
                        }
                        Log.d("file ", "new count " + new_count);
                        Log.d("file", "new size" + size_to_added);
                        //add count
                        // add size
                    } catch (Exception e) {
                        count = 0;
                        size = 0;
                        Log.d("file", Log.getStackTraceString(e));
                        e.printStackTrace();
                    }
                    //write
                    JSONObject obj = new JSONObject();
                    obj.put("count", new_count);
                    obj.put("size", size_to_added);

                    try {
                        obj.put("apps", apps);
                        FileWriter fw = new FileWriter(path);
                        fw.write(obj.toJSONString());
                        fw.flush();
                        fw.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.d("file", Log.getStackTraceString(e));
                    }
                } else {

                    Log.d("file", "new file made");

                    Log.d("file ", "new count " + 1);
                    Log.d("file", "new size" + size_to_added);

                    //for first time
                    JSONObject obj = new JSONObject();
                    obj.put("count", 1);
                    obj.put("size", size_to_added);
                    //list of apps : with pkgname and size
                    JSONArray applist = new JSONArray();
                    JSONObject app = new JSONObject();
                    app.put("pkgname", originalPkgName);
                    app.put("size", size_to_added);
                    applist.add(app);
                    obj.put("apps", applist);
                    try {
                        FileWriter fw = new FileWriter(path);
                        fw.write(obj.toJSONString());
                        fw.flush();
                        fw.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.d("file", Log.getStackTraceString(e));
                    }
                }

            } else {
                //to launch the installed app after reactivating

                versionName = UtilityMethods.getInstance().getCurrentAppVersionName(context, packageName);
                if (versionName != null && versionName.equalsIgnoreCase("1.stash")) {
                    try {
                        Intent babyStashInstallationEvent = new Intent("com.times.convertToAdvance");
                        babyStashInstallationEvent.putExtra("get_packagename", packageName);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(babyStashInstallationEvent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Bundle mGAParams;
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.COMPRESS);
                    mGAParams.putString("action", AnalyticsConstants.Action.COMPRESSED_PKG);
                    mGAParams.putString("label", packageName + "");
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d("chckpkg", "onReceive: " + packageName);


                    DataModel dm = AppDBHelper.getInstance(context).getDataSize(packageName);
                    long size_to_added = dm.getCodeSize() + dm.getCacheSize() + dm.getApkSize() + dm.getDataSize();
                    UtilityMethods.getInstance().addToJSONFile(packageName, size_to_added, dm.getLastModifiedDate(), Constants.COMPRESSED);

                    if (AppDBHelper.getInstance(context).getInstallationFlag(packageName) == 0) {
                        installingFlag = false;
                        ContentValues values = new ContentValues();
                        values.put("install_status", "1");
                        int updatedApp = AppDBHelper.getInstance(context).updateCompressedApp(values, packageName);

                        if (updatedApp == 1) {

                            try {

                                AccessibilityAutomation.getSharedInstance().getActionFailHandler().setInstall_handle(false, null, false);
                                AccessibilityAutomation.getSharedInstance().getActionFailHandler().refreshInstallCounter();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            UtilityMethods.getInstance().startNextCompression(context);
                        }

                    }
                }

                Log.d("INSTALLED_TRACKER_1", "OPEN APPS FROM RECEIVER" + packageName);
                Intent i1 = new Intent("com.times.accessibilityREACTIVATE1");
                i1.putExtra("packageName_of_apps_uninstalled", packageName);
                LocalBroadcastManager.getInstance(context).sendBroadcast(i1);
                try {
                    Log.d("YourReceiver", "new package installed " + packageName);
                    setDataSize(context, packageName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public long[] setDataSize(final Context context, final String pkgName) throws
            InvocationTargetException, IllegalAccessException, InterruptedException {
        final long dataSize[] = new long[3];
        Method mGetPackageSizeInfoMethod = null;
        try {
            mGetPackageSizeInfoMethod = context.getPackageManager().getClass().getMethod(
                    "getPackageSizeInfo", String.class, IPackageStatsObserver.class);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        final PackageManager pm = context.getPackageManager();
        try {
            mGetPackageSizeInfoMethod.invoke(pm, pkgName,
                    new IPackageStatsObserver.Stub() {
                        @Override
                        public void onGetStatsCompleted(PackageStats pStats,
                                                        boolean succeeded) throws RemoteException {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                synchronized (dataSize) {
                                    dataSize[0] = pStats.dataSize;
                                    dataSize[1] = pStats.cacheSize;
                                    dataSize[2] = pStats.codeSize;

                                    AppInfo obj = new AppInfo();
                                    ApplicationInfo ai;
                                    try {
                                        ai = pm.getApplicationInfo(pkgName, 0);
                                    } catch (final PackageManager.NameNotFoundException e) {
                                        ai = null;
                                    }
                                    final String applicationName = (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
                                    obj.setPkgName(pkgName);
                                    obj.setAppName(applicationName);
                                    obj.setSize(dataSize[0]);
                                    obj.setCacheSize(dataSize[1]);
                                    obj.setCodeSize(dataSize[2]);
                                    Log.d("YourReceiver", "DATA UPDATED" + dataSize[0] + " " + dataSize[1] + " " + dataSize[2]);
                                    try {
                                        obj.setApkSize(getApkSize(pm, pkgName));
                                    } catch (PackageManager.NameNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                    //user installed apps
                                    obj.setCompressed_status(Constants.UNCOMPRESSED);



                                    if (AppDBHelper.getInstance(context).appExists(pkgName)) {
                                        Log.d("YourReceiver", "updating" + pkgName);
                                        //update database
                                        //TODO :sajal remove this .stash or condition from here
                                        if (pkgName.contains(".stash") || Constants.DUMMY_APP_VERSION_NAME.equalsIgnoreCase(UtilityMethods.getInstance().getCurrentAppVersionName(context, pkgName))) {
                                            //dummy app updated, install status 2
                                            //TODO : this can be optimized
                                            AppDBHelper.getInstance(context).appCompressed(pkgName);
                                        } else {
                                            //update data of existing apps in DB
                                            AppDBHelper.getInstance(context).updateApplication(obj);
                                        }
                                    } else {
                                        Log.d("YourReceiver", "adding " + pkgName);
                                        //new package was installed by user
                                        //TODO :sajal remove this .stash or condition from here
                                        if (pkgName.contains(".stash") || Constants.DUMMY_APP_VERSION_NAME.equalsIgnoreCase(UtilityMethods.getInstance().getCurrentAppVersionName(context, pkgName))) {
                                            //This case will not happen from database version 2 : because installed apks will not be .stash apps anymore but we have kept this for now, CAN BE REMOVED FROM HERE
                                            obj.setCompressed_status(Constants.COMPRESSED);
                                        }
                                        Calendar calobj = Calendar.getInstance();
                                        obj.setLastModifiedDate(calobj.getTimeInMillis());
                                        AppDBHelper.getInstance(context).addApplication(obj);
                                    }


                                }
                            }
                        }
                    });
        } catch (NullPointerException e) {
            Log.d(TAG, e.getMessage(), e);
        } catch (Exception j) {
            j.printStackTrace();

            AppInfo obj = new AppInfo();
            ApplicationInfo ai;

            try {
                ai = pm.getApplicationInfo(pkgName, 0);
            } catch (final PackageManager.NameNotFoundException e) {
                ai = null;
            }
            final String applicationName = (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");

            obj.setPkgName(pkgName);
            obj.setAppName(applicationName);
            obj.setSize(0);
            obj.setCacheSize(0);
            obj.setCodeSize(0);

            //ADD to db in case reflection fails

            if (AppDBHelper.getInstance(context).appExists(pkgName)) {
                Log.d("YourReceiver", "updating" + pkgName);
                //update database
                //TODO :sajal remove this .stash or condition from here
                if (pkgName.contains(".stash") || Constants.DUMMY_APP_VERSION_NAME.equalsIgnoreCase(UtilityMethods.getInstance().getCurrentAppVersionName(context, pkgName))) {
                    //dummy app updated, install status 2
                    //TODO : this can be optimized
                    AppDBHelper.getInstance(context).appCompressed(pkgName);
                } else {
                    //update data of existing apps in DB
                    AppDBHelper.getInstance(context).updateApplication(obj);
                }
            } else {
                Log.d("YourReceiver", "adding " + pkgName);
                //new package was installed by user
                //TODO :sajal remove this .stash or condition from here
                if (pkgName.contains(".stash") || Constants.DUMMY_APP_VERSION_NAME.equalsIgnoreCase(UtilityMethods.getInstance().getCurrentAppVersionName(context, pkgName))) {
                    //This case will not happen from database version 2 : because installed apks will not be .stash apps anymore but we have kept this for now, CAN BE REMOVED FROM HERE
                    obj.setCompressed_status(Constants.COMPRESSED);
                }
                Calendar calobj = Calendar.getInstance();
                obj.setLastModifiedDate(calobj.getTimeInMillis());
                AppDBHelper.getInstance(context).addApplication(obj);
            }
        }
        return dataSize;
    }

    public long getApkSize(PackageManager packageManager, String packageName) throws PackageManager.NameNotFoundException {
        return new File(packageManager.getApplicationInfo(packageName, 0).publicSourceDir).length();
    }

    public boolean isPackageExisted(String targetPackage, Context context) {
        List<ApplicationInfo> packages;
        PackageManager pm;

        pm = context.getPackageManager();
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            if (packageInfo.packageName.equals(targetPackage))
                return true;
        }
        return false;
    }


}