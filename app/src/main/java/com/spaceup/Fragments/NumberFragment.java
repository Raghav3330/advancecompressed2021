package com.spaceup.Fragments;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spaceup.Activities.ManageApps;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.R;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;


/**
 * Created by Sajal on 27-12-2016
 */
public class NumberFragment extends Fragment {

    public NumberFragment() {

    }

    public static String getSizeinMB(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "");

        // Considering No Decimal places in case of MB
        if (pre.equals("M"))
            return String.format("%.0f %sB", bytes / Math.pow(unit, exp), pre);

        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.numberfragment, container, false);
        TextView numberOfApps = (TextView) view.findViewById(R.id.no_of_apps_uncompressed);
        TextView appStorageUsed = (TextView) view.findViewById(R.id.app_storage_used);
        TextView noOfAppsCompressed = (TextView) view.findViewById(R.id.no_of_apps_compressed);
        TextView appStorageSaved = (TextView) view.findViewById(R.id.app_storage_saved);
        TextView appsNcTxt = (TextView) view.findViewById(R.id.apps_nc_txt);
        TextView appsCompTxt = (TextView) view.findViewById(R.id.apps_comp_txt);
        RelativeLayout appsDataLayout = (RelativeLayout) view.findViewById(R.id.apps_data_layout);
        appsDataLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), ManageApps.class));
            }
        });

        int totalApps;
        long totalAppSize, compressedApps, compressedAppSize;


        totalApps = Integer.parseInt(getArguments().getString("total_apps"));
        totalAppSize = Long.parseLong(getArguments().getString("total_app_size"));
        compressedApps = Long.parseLong(getArguments().getString("compressed_apps"));
        compressedAppSize = Long.parseLong(getArguments().getString("compressed_app_size"));

        numberOfApps.setText("" + totalApps);
        new AnalyticsController(getActivity()).sendAnalytics(AnalyticsConstants.Category.HOMESCREEN, AnalyticsConstants.Action.TOTAL_APPS, AnalyticsConstants.Label.STORAGE_4GB, String.valueOf(totalApps), true, PrefManager.TOTAL_APPS_PREF);

        appStorageUsed.setText(getSizeinMB(totalAppSize, false) + " Used");
        noOfAppsCompressed.setText(PrefManager.getInstance(getContext()).getInt(PrefManager.TOTAL_APPS_UNINSTALLED) + "");
        long appSizeSaved = (PrefManager.getInstance(getContext()).getLong(PrefManager.TOTAL_SPACE_SAVED));
        appStorageSaved.setText(UtilityMethods.getSizeinMBOnly((long) (appSizeSaved)) + " Saved");

        // 3 digit case: Aligning Info box in Center
        if (totalApps > 99) {
            Resources r = getResources();
            float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0.47f, r.getDisplayMetrics());

            // Changing Margin to align in b/w
            float leftMarginValue = 100 * px;
            Log.d("checkValue", " " + leftMarginValue + " ");
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) appsDataLayout.getLayoutParams();
            params.leftMargin = (int) leftMarginValue;
            appsDataLayout.requestLayout(); // important

            // Changing Text to adjust width in 3 digit cases
            appsNcTxt.setText("Apps Not\nUninstalled");
            appsNcTxt.setTextSize(10);
            appsCompTxt.setTextSize(10);
        }

        return view;
    }

}
