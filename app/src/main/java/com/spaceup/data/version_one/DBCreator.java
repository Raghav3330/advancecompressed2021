package com.spaceup.data.version_one;

/**
 * Created by Sajal Jain on 01-12-2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class DBCreator extends SQLiteOpenHelper {

    private static final String TAG = "DbUpdate";
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 4;


    // Database Name
    private static final String DATABASE_NAME = "Stash";


    // Contacts table name
    private static final String TABLE_APP_DETAILS = "AppDetails";

    private static final String APP_NAME = "AppName";
    private static final String PKG_NAME = "PkgName";
    private static final String COMPRESSED_STATUS = "CompressedStatus";
    private static final String COMPRESSED_COUNT = "CompressedCount";
    private static final String UNCOMPRESSED_COUNT = "UncompressedCount";
    private static final String INSTALL_COUNT = "InstallCount";
    private static final String UNINSTALL_COUNT = "UninstallCount";

    private static final String USAGE_COUNT = "UsageCount";
    private static final String APK_SIZE = "ApkSize";
    private static final String CODE_SIZE = "CodeSize";
    private static final String CACHE_SIZE = "CacheSize";
    private static final String DATA_SIZE = "DataSize";
    private static final String PRIORITY_SCORE = "PriorityScore";
    private static final String TIME = "Time";
    private static final String CATERGORY = "Category";
    private static final String MODIFIED_DATE = "ModifiedDate";
    private static final String EXTRACT = "Extract";

    private static final String COMPRESSION_TABLE = "CompressedAppDetails";   //compressed apps TABLE
    private static final String TOP_FIVE_TABLE = "TopFiveApps";   //Top 5 apps TABLE to store Top 5 apps in Listing Algorith
    private static final String SHORTCUT_TB = "Shortcut";   //Top 5 apps TABLE to store Top 5 apps in Listing Algorith
    public static final String IMAGES_TB = "Images";


    public static final String ACTION_STATUS = "ActionStatus";
    public static final String RESPONSE_STATUS = "ResponseStatus";
    public static final String PATH = "Path";
    public static final String TRASH_PATH = "TrashPath";


    private static final String COMPRESSED_APP_NAME = "AppName";
    private static final String COMPRESSED_PKG_NAME = "PkgName";
    private static final String COMPRESSED_APP_PATH = "DummyAppPath";
    private static final String COMPRESSED_APP_FOLDER = "folder";
    private static final String COMPRESSED_INSTALL_STATUS = "install_status";
    private static final String COMPRESSED_UNINSTALL_STATUS = "uninstall_status";


    //The Android's default system path of your application database.
    private static String DB_PATH;
    private static String DB_NAME = "Stash.sqlite";
    private static DBCreator dbCreator;
    private SQLiteDatabase db;
    private Context mContext;


    // constructor
    public DBCreator(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
        mContext = context;

    }

    public static synchronized DBCreator getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (dbCreator == null) {
            dbCreator = new DBCreator(context.getApplicationContext());
        }
        return dbCreator;
    }


    @Override
    public synchronized void close() {
        if (db != null)
            db.close();
        super.close();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate: of database ");

        String CREATE_APP_TABLE = "CREATE TABLE " + TABLE_APP_DETAILS + "("
                + PKG_NAME + " TEXT PRIMARY KEY," +
                APP_NAME + " TEXT," +
                //+ PKG_NAME + " TEXT ," + APP_NAME + " TEXT," + USAGE_COUNT + " TEXT,"
                APK_SIZE + " TEXT," +
                CODE_SIZE + " TEXT," +
                CACHE_SIZE + " TEXT," +
                DATA_SIZE + " TEXT," +
                PRIORITY_SCORE + " TEXT," +
                TIME + " TEXT," +
                MODIFIED_DATE + " TEXT," +
                CATERGORY + " TEXT," +
                //SPECIAL_FLAG + " TEXT," +
                EXTRACT + " TEXT," +
                USAGE_COUNT + " TEXT," +
                COMPRESSED_STATUS + " NUMBER," +
                COMPRESSED_COUNT + " NUMBER," +
                UNCOMPRESSED_COUNT + " NUMBER," +
                INSTALL_COUNT + " NUMBER," +
                UNINSTALL_COUNT + " NUMBER" +
                ")";
        db.execSQL(CREATE_APP_TABLE);

        String COMPRESSED_APP_TABLE = "CREATE TABLE " + COMPRESSION_TABLE + "("
                + COMPRESSED_PKG_NAME + " TEXT PRIMARY KEY," +
                COMPRESSED_APP_NAME + " TEXT," +
                COMPRESSED_INSTALL_STATUS + " TEXT," +
                COMPRESSED_UNINSTALL_STATUS + " TEXT," +
                COMPRESSED_APP_PATH + " TEXT," +
                COMPRESSED_APP_FOLDER + " TEXT)";
        db.execSQL(COMPRESSED_APP_TABLE);

        String TOP_FIVE = "CREATE TABLE " + TOP_FIVE_TABLE + "("
                + PKG_NAME + " TEXT PRIMARY KEY," +
                APP_NAME + " TEXT," +
                APK_SIZE + " TEXT)";
        db.execSQL(TOP_FIVE);

        String SHORTCUT = "CREATE TABLE IF NOT EXISTS " + SHORTCUT_TB + "("
                + PKG_NAME + " TEXT PRIMARY KEY," +
                APP_NAME + " TEXT," +
                APK_SIZE + " TEXT)";
        db.execSQL(SHORTCUT);


        String IMAGES_QUERY = "CREATE TABLE IF NOT EXISTS " + IMAGES_TB + "("
                + PATH + " TEXT PRIMARY KEY," +
                RESPONSE_STATUS + " TEXT," +
                ACTION_STATUS + " TEXT," +
                TRASH_PATH + " TEXT)";
        db.execSQL(IMAGES_QUERY);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("Seq", "I am here from upgrade");
        Log.d(TAG, "onUpgrade: " + oldVersion);
        Log.d(TAG, "onUpgrade: " + newVersion);

        updateDatabase(oldVersion, db);


    }

    private void updateDatabase(int oldVersion, SQLiteDatabase db) {
        switch (oldVersion) {
            case 1:
                updateDatabaseVersion1(db);

            case 2:
                updateDatabaseVersion2(db);
            case 3:
                updateDatabaseVersion3(db);
            default:
                Log.d(TAG, "updateDatabase: default " + oldVersion);
                break;
        }
    }


    private void updateDatabaseVersion1(SQLiteDatabase db) {
        // ShortCut Table
        String SHORTCUT = "CREATE TABLE IF NOT EXISTS " + SHORTCUT_TB + "("
                + PKG_NAME + " TEXT PRIMARY KEY," +
                APP_NAME + " TEXT," +
                APK_SIZE + " TEXT)";
        db.execSQL(SHORTCUT);

    }

    private void updateDatabaseVersion2(SQLiteDatabase db) {
        Log.d(TAG, "Database update called VERSION 2");

        //hash map constaing package name to size, value coming from JSON file
        HashMap<String, Long> pkgToSize = jsonReformat(db);

        try {
            removeStashExtensionFromDB(db, pkgToSize);
        } catch (Exception e) {
            Log.d(TAG, "updateDatabaseVersion2: " + Log.getStackTraceString(e));
        }
        syncJSON();
    }

    /*
    * Images table is introduced in this version
    * */
    private void updateDatabaseVersion3(SQLiteDatabase db) {
        Log.d(TAG, "Database update called VERSION 3");
        String IMAGES_QUERY = "CREATE TABLE IF NOT EXISTS " + IMAGES_TB + "("
                + PATH + " TEXT PRIMARY KEY," +
                RESPONSE_STATUS + " TEXT," +
                ACTION_STATUS + " TEXT," +
                TRASH_PATH + " TEXT)";
        db.execSQL(IMAGES_QUERY);
    }

    /*
    * removes useless entries from JSON FILE
    * */
    private void syncJSON() {
        //residual json file exits in sd card
        JSONParser parser = new JSONParser();
        UtilityMethods utils = UtilityMethods.getInstance();
        try {
            Object obj = parser.parse(new FileReader(Constants.RESIDUAL_FILE));
            JSONObject jsonObject = (JSONObject) obj;
            //reading json array of apps so that we can append new app
            JSONArray appsList = (JSONArray) jsonObject.get("apps");
            Iterator<JSONObject> iterator = appsList.iterator();

            while (iterator.hasNext()) {
                JSONObject ap = iterator.next();
                String pkgName = (String) ap.get("pkgname");
                //package doesn't exits in package manager
                String APKFilePath = "";
                APKFilePath = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + pkgName + Constants.APK_EXTENSION;

                //package is installed
                if ((long) ap.get("status") == Constants.COMPRESSED && !Constants.DUMMY_APP_VERSION_NAME.equalsIgnoreCase(UtilityMethods.getInstance().getCurrentAppVersionName(mContext, pkgName)) && !utils.isPackageInstalled(pkgName + ".stash", mContext.getPackageManager())) {
                    //App is installed on device and it is not dummy app of any kind namely version code and .stash
                    //removing staled entries from JSON file
                    Log.d(TAG, "App is installed on device and it is not dummy app, so removing from JSON");
                    UtilityMethods.getInstance().removeFromJSONFile(pkgName);
                } else if ((long) ap.get("status") == Constants.QUICK_COMPRESSED && !new File(APKFilePath).exists()) {
                    UtilityMethods.getInstance().removeFromJSONFile(pkgName);
                }
            }
        } catch (Exception e) {
            Log.d(TAG, Log.getStackTraceString(e));
            e.printStackTrace();
        }
    }


    private void removeStashExtensionFromDB(SQLiteDatabase db, HashMap<String, Long> pkgToSize) {


        List<String> removeApps = new ArrayList<>();
        List<String> updateApps = new ArrayList<>();
        String uninstalledDummyQuery, installedDummyQuery, quickCompressedQuery, appExitsQuery;

        //get list of .stash apps and uninstalled
        uninstalledDummyQuery = "SELECT  " + PKG_NAME + " FROM " + TABLE_APP_DETAILS + " where " + COMPRESSED_STATUS + " = " + "-3";
        Log.d(TAG, "removeStashExtensionFromDB: uninstalledDummyQuery " + uninstalledDummyQuery);
        Cursor cursor = db.rawQuery(uninstalledDummyQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Log.d(TAG, "removeStashExtensionFromDB: adding package to remove apps list " + cursor.getString(0));
                removeApps.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        cursor.close();

        //removing (uninstalled dummy app) from db
        for (String pkgName : removeApps) {
            db.delete(TABLE_APP_DETAILS, PKG_NAME + "='" + pkgName + "'", null);
            Log.d(TAG, "removeStashExtensionFromDB: remove package " + pkgName + " from db ");
        }

        //get list of .stash apps and installed
        installedDummyQuery = "SELECT  " + PKG_NAME + " FROM " + TABLE_APP_DETAILS + " where " + COMPRESSED_STATUS + " = " + "2";
        Log.d(TAG, "removeStashExtensionFromDB: installedDummyQuery " + installedDummyQuery);
        cursor = db.rawQuery(installedDummyQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Log.d(TAG, "removeStashExtensionFromDB: adding package to update apps list " + cursor.getString(0));
                updateApps.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        cursor.close();

        //removing (.stash apps & uninstalled app from db) and update apps
        boolean appExitsInDb;
        for (String pkgName : updateApps) {
            appExitsInDb = false;

            //update normal package name
            pkgName = pkgName.replace(".stash", "");

            //SELECT  * FROM AppDetails where PkgName = 'com.whatsapp'
            appExitsQuery = "SELECT  * FROM " + TABLE_APP_DETAILS + " where " + PKG_NAME + "=" + "'" + pkgName + "'";
            Log.d(TAG, "removeStashExtensionFromDB: check if package name exits" + appExitsQuery);

            try {
                cursor = db.rawQuery(appExitsQuery, null);
                if (cursor.getCount() > 0) {
                    appExitsInDb = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
            }


            if (appExitsInDb) {

                ContentValues values = new ContentValues();
                values.put(COMPRESSED_STATUS, Constants.COMPRESSED);
                //update 'com.whatsapp' status to compress
                db.update(TABLE_APP_DETAILS, values, PKG_NAME + "=" + "'" + pkgName + "'", null);
                Log.d(TAG, "removeStashExtensionFromDB: update original package " + pkgName + " in db ");

                //remove .stash apps from db
                pkgName = pkgName + Constants.APK_EXTENSION;   //com.flipkart.stash    removing from db
                db.delete(TABLE_APP_DETAILS, PKG_NAME + "='" + pkgName + "'", null);
                Log.d(TAG, "removeStashExtensionFromDB: remove .stash installed " + pkgName + " from db ");

            } else {
                //handling case of clear data in previous run

                long prevSize = 0;


                if (!UtilityMethods.isEmptyMap(pkgToSize) && pkgToSize.size() != 0 && pkgToSize.containsKey(pkgName)) {
                    prevSize = pkgToSize.get(pkgName);
                }


                //just update existing .stash pkg name to normal package name and mark apk as compressed
                ContentValues values = new ContentValues();
                values.put(PKG_NAME, pkgName); //com.whatsapp
                values.put(COMPRESSED_STATUS, Constants.COMPRESSED);
                if (prevSize != 0) {
                    //taking prev calculated size from JSON file
                    values.put(DATA_SIZE, prevSize);
                    values.put(CACHE_SIZE, 0);
                    values.put(CODE_SIZE, 0);
                    values.put(APK_SIZE, 0);
                }

                pkgName = pkgName + Constants.APK_EXTENSION;

                //updating com.whatsapp.stash to com.whatsapp
                db.update(TABLE_APP_DETAILS, values, PKG_NAME + "=" + "'" + pkgName + "'", null);
                Log.d(TAG, "removeStashExtensionFromDB: update original package " + pkgName + " in db ");

            }


        }

        //getting apps from shortcut table and update in App Details table
        quickCompressedQuery = "SELECT " + PKG_NAME + " FROM " + SHORTCUT_TB;
        cursor = db.rawQuery(quickCompressedQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Log.d(TAG, "removeStashExtensionFromDB: update status to quick compressed app " + cursor.getString(0));
                ContentValues values = new ContentValues();
                values.put(COMPRESSED_STATUS, Constants.QUICK_COMPRESSED);
                db.update(TABLE_APP_DETAILS, values, PKG_NAME + "=" + "'" + cursor.getString(0) + "'", null);
            } while (cursor.moveToNext());
        }
        cursor.close();


    }

    /*
    * old
    * {"size":69578015,"apps":[{"size":5729128,"pkgname":"com.afwsamples.testdpc.stash"}] ,"count":1}
    * update format
    * {"size":69578015,"apps":[{"size":5729128,"pkgname":"com.afwsamples.testdpc" ,"status" : Constants.QUICK_COMPRESSED ,"last_modified_date" : 324312451}] ,"count":1}
    * */
    private HashMap<String, Long> jsonReformat(SQLiteDatabase db) {
        //key is package name and value is size.
        HashMap<String, Long> pkgToSize = new HashMap<>();
        File file = new File(Constants.RESIDUAL_FILE);

        //number of compressed apps should not be zero && compressed_app_size should be zero then only check from JSON file
        if (file.exists()) {
            Log.d(Constants.JSON_TAG, "file already exists");
            //reading file
            JSONParser parser = new JSONParser();
            JSONObject oldJsonObj;//init old file data
            JSONArray oldAppList; //contains list of OLD compressed appS
            JSONObject oldApp; //contains details of compressed app
            long oldCount = 0; //count of apps
            long oldTotalSize = 0; //total size of apps
            String oldPkgName; //package name of old application
            long oldAppSize;

            JSONObject updateJsonObj = new JSONObject(); //init new file data
            JSONArray updateAppList = new JSONArray(); //contains list of UPDATED compressed appS
            JSONObject updateApp;
            long lastDateModified;


            try {
                Log.d(Constants.JSON_TAG, "opening residual json file");
                Object residualFile = parser.parse(new FileReader(Constants.RESIDUAL_FILE));

                Log.d(Constants.JSON_TAG, "old JSON initialized");
                oldJsonObj = (JSONObject) residualFile;

                oldCount = (int) ((long) oldJsonObj.get("count"));
                oldTotalSize = (long) oldJsonObj.get("size");
                Log.d(Constants.JSON_TAG, "old count " + oldCount);
                Log.d(Constants.JSON_TAG, "old size" + oldTotalSize);

                //reading json array of apps so that we can append new app
                oldAppList = (JSONArray) oldJsonObj.get("apps");
                if (null == oldAppList) {
                    oldAppList = new JSONArray();
                }
                Iterator<JSONObject> iterator = oldAppList.iterator();
                while (iterator.hasNext()) {
                    oldApp = iterator.next();
                    oldPkgName = (String) oldApp.get("pkgname");
                    oldAppSize = (long) oldApp.get("size");
                    Log.d(Constants.JSON_TAG, "old app package name " + oldPkgName + " size is" + oldAppSize);

                    updateApp = new JSONObject();

                    if (oldPkgName.contains(".stash")) {
                        //quick compressed app
                        oldPkgName = oldPkgName.replace(".stash", "");
                        updateApp.put(Constants.STATUS, Constants.QUICK_COMPRESSED);
                    } else {
                        //advance compressed apps
                        updateApp.put(Constants.STATUS, Constants.COMPRESSED);

                    }


                    pkgToSize.put(oldPkgName, oldAppSize);
                    Log.d(Constants.JSON_TAG, "adding to hash map key " + oldPkgName + " value " + oldAppSize);

                    updateApp.put(Constants.SIZE, oldAppSize);
                    updateApp.put(Constants.PKGNAME, oldPkgName);
                    lastDateModified = getLastModifiedDate(oldPkgName, db);
                    updateApp.put(Constants.LAST_MODIFIED_DATE, lastDateModified);


                    updateAppList.add(updateApp);
                    Log.d(Constants.JSON_TAG, "adding to new json " + updateAppList.size());
                    oldAppSize = 0;
                    oldPkgName = null;
                    lastDateModified = 0;
                }

            } catch (Exception e) {
                Log.d(Constants.JSON_TAG, Log.getStackTraceString(e));
            }

            //write
            updateJsonObj.put("count", oldCount);
            updateJsonObj.put("size", oldTotalSize);
            updateJsonObj.put(Constants.JSON_VERSION, 1);
            try {
                updateJsonObj.put("apps", updateAppList);
                FileWriter fw = new FileWriter(Constants.RESIDUAL_FILE);
                fw.write(updateJsonObj.toJSONString());
                fw.flush();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("file", Log.getStackTraceString(e));
            } catch (Exception e) {
                Log.d(TAG, Log.getStackTraceString(e));
                e.printStackTrace();
            }

        }

        return pkgToSize;

    }

    public synchronized long getLastModifiedDate(String pkgName, SQLiteDatabase db) {

        long lastModifiedDate = System.currentTimeMillis();
        String selectQuery = "SELECT " + MODIFIED_DATE + " FROM " + TABLE_APP_DETAILS + " WHERE " + PKG_NAME + "=" + "'" + pkgName + "'";
        Log.d(Constants.JSON_TAG, "getLastModifiedDate: select query is " + selectQuery);
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                lastModifiedDate = Long.parseLong(cursor.getString(0));
                Log.d(Constants.JSON_TAG, "getLastModifiedDate: last modified date is " + lastModifiedDate);
            }
            cursor.close();
        } catch (Exception e) {
            Log.d(Constants.JSON_TAG, Log.getStackTraceString(e));
        }
        return lastModifiedDate;
    }

   /* private void updateDatabaseVersion3(SQLiteDatabase db) {
        Log.d(TAG, "Database update called VERSION 3");

    }

    private void updateDatabaseVersion4(SQLiteDatabase db) {
        Log.d(TAG, "Database update called VERSION 4");

    }*/

}
