package com.spaceup.notifications;

import android.app.IntentService;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

//import static com.facebook.GraphRequest.TAG;

/**
 * Created by Dhruv Kaushal on 08-12-2017.
 */

public class StashInstalledCheckService extends IntentService {

    public StashInstalledCheckService() {

        super(StashInstalledCheckService.class.getName());

    }

    @Override
    public void onCreate() {
        super.onCreate();

    }


    @Override
    protected void onHandleIntent(Intent intent) {
        boolean install = UtilityMethods.isPackageExisted(getApplicationContext(), "com.stash.junkcleaner");
        if( install ){
            PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.STASH_INSTALLED, true);
        }
        else{PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.STASH_INSTALLED, false);

        }
    }
}
