package com.spaceup;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Environment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.apkgenerator.utilities.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * Created by dhurv on 28-11-2016.
 */

public class PREPARING_ASSETS extends Thread {
    Context context;
    String TAG = "PREPARING_ASSET";
    long t1;
    int no_of_folders;

    public PREPARING_ASSETS(Context context, int no_of_folders) {
        this.context = context;
        this.no_of_folders = no_of_folders;
    }

    @Override
    public void run() {

        preparing_Assets();
    }

    private void preparing_Assets() {
//        t1 = System.currentTimeMillis();
//        Log.d(TAG, "PREPARING APK ASSETS");
//        File f = new File(Environment.getExternalStorageDirectory() + Constants.STASH);
//        if (!f.isDirectory()) {
//            f.mkdirs();
//            // Make folder Stash on external directory
//        }
//        File ff = new File(Environment.getExternalStorageDirectory() + Constants.STASH, ".nomedia");
//        try {
//            if (!ff.exists()) {
//                ff.createNewFile();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        // Make folder Stash on external directory
//        File db = new File(Environment.getExternalStorageDirectory() + Constants.STASH + "/db");
//        if (!db.isDirectory()) {
//            db.mkdirs();
//            // Make folder Stash on external directory
//        }
//// Make folder Stash on external directory
//        db = new File(Environment.getExternalStorageDirectory() + Constants.STASH + "/WhatsappDump");
//        if (!db.isDirectory()) {
//            db.mkdirs();
//            // Make folder Stash on external directory
//        }
//        //prepare TestKey
//        String filepath = Environment.getExternalStorageDirectory() + Constants.STASH + "/TestKeyStore.jks";
//        File key = new File(filepath);
//        try {
//            key.createNewFile();
//            FileOutputStream fos = null;
//            try {
//                AssetManager am = context.getAssets();
//                InputStream assetFile = am.open("utils/TestKeyStore.jks");
//                fos = new FileOutputStream(filepath);
//                copyFile(assetFile, fos);
//                fos.close();
//                assetFile.close();
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//        // prepare item.xml file
//        String filepath_item = Environment.getExternalStorageDirectory() + Constants.STASH + "/item.xml";
//        File item = new File(filepath_item);
//        try {
//            item.createNewFile();
//            FileOutputStream fos;
//            try {
//                AssetManager am = context.getAssets();
//                InputStream assetFile = am.open("utils/sample_item.xml");
//                fos = new FileOutputStream(filepath_item);
//                copyFile(assetFile, fos);
//                fos.close();
//                assetFile.close();
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//        // prepare 5 test folders
//        File dir = new File(Constants.UNZIP);
//        deleteRecursive(dir);
//        /** Extract each zip foldeer to all hcjb folders  */
//        for (int i = 1; i < no_of_folders; i++) {
//            AssetManager am = context.getAssets();
//
//            try {
//                File file = new File(Constants.UNZIP + "/hcjb" + i);
//
//                if (!file.exists()) {
//                    FileUtils.unZip(am.open("utils/backupapk.zip"), Constants.UNZIP + "/hcjb" + i);
//                }
//            } catch (IOException e) {
//
//                e.printStackTrace();
//            }
//
//        }
//
//
//        Log.d(TAG, "DONE PREPARING APK ASSETS in" + (System.currentTimeMillis() - t1) + " miilis");
//        // Copy Install.png file
//
//
//        try {
//            AssetManager am = context.getAssets();
//
//            File file = new File(Environment.getExternalStorageDirectory().getPath() + Constants.STASH + "/install.pic");
//            if (!file.exists()) {
//                key.createNewFile();
//                FileOutputStream fos = null;
//                fos = new FileOutputStream(file);
//                copyFile(am.open("utils/install.png"), fos);
//                fos.close();
//
//            }
//        } catch (IOException e) {
//
//            e.printStackTrace();
//        }


        /**
         *
         * Send broadcast preparing_done
         *
         */


        Intent intent = new Intent("preparing_done");
        // add data
        intent.putExtra("message", "done");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            File[] files = fileOrDirectory.listFiles();
            if (files != null && files.length > 0) {
                for (File child : files) {
                    deleteRecursive(child);
                }
            }
        }
        fileOrDirectory.delete();
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }


}