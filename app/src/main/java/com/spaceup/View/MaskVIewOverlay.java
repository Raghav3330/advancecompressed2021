package com.spaceup.View;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ImageView;


import com.spaceup.Animations.ScanningOverlay;

/**
 * Created by shashanktiwari on 23/11/16.
 */

public class MaskVIewOverlay extends ImageView {

    private Paint paint = null;

    private Rect mRect = new Rect();

    public MaskVIewOverlay(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        invalidate();
        return true;
    }

    protected void onDraw(Canvas canvas) {
        if (paint == null) {
            Bitmap original = Bitmap.createBitmap(
                    getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
            Canvas originalCanvas = new Canvas(original);
            super.onDraw(originalCanvas);    // ImageView

            Shader shader = new BitmapShader(original,
                    Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

            paint = new Paint();
            paint.setShader(shader);
        }

        canvas.drawColor(Color.TRANSPARENT);
        Log.d("BottomOverlay", "" + ScanningOverlay.width+""+ ScanningOverlay.bottom);
        try {
            mRect.right = ScanningOverlay.width;
        } catch (Exception e) {


            e.printStackTrace();
        }
        try {
            mRect.bottom = ScanningOverlay.bottom;
        } catch (Exception e) {


            e.printStackTrace();
        }
        canvas.drawRect(mRect, paint);
    }

}