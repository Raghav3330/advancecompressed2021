package com.spaceup.accessibility.permission;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.R;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityAutomation;

import java.util.List;

import static android.view.accessibility.AccessibilityNodeInfo.ACTION_CLICK;
import static android.view.accessibility.AccessibilityNodeInfo.ACTION_SCROLL_FORWARD;

/**
 * Created by dhurv on 20-03-2017.
 */

public class AppUsagePemissionHandler {
    private static AppUsagePemissionHandler methods = null;
    private boolean PERMISSION_GRANTED = false;
    private boolean SPACEUP_CLICKED = false;

    private Context context;

    public static AppUsagePemissionHandler getInstance() {
        if (methods == null) {
            methods = new AppUsagePemissionHandler();
        }
        return methods;
    }

    public void launchIntent(Context context, Intent intentActivty) {
        if (AccessibilityAutomation.getSharedInstance() == null) {
            return;
        }
        new AnalyticsController(context).sendAnalytics(
                AnalyticsConstants.Category.PERMISSION,
                AnalyticsConstants.Action.APP_USAGE,
                AnalyticsConstants.Label.FIRED,
                null,
                false,
                null);
        AccessibilityAutomation.getSharedInstance().overLayScanning(intentActivty);
        PERMISSION_GRANTED = false;
        SPACEUP_CLICKED = false;
        this.context = context;
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
        if (intent.resolveActivity(packageManager) != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Log.d("component", "Intent available to handle action" + intent.resolveActivity(packageManager));
            try {
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        AccessibilityAutomation.getSharedInstance().startAccessiblityUsageStats();
    }

    public void takePermissionfromAccessibility(final AccessibilityNodeInfo pNodeInfo) {

        Log.d("STATUS_PERMISSION", PERMISSION_GRANTED + " " + SPACEUP_CLICKED);
        if (pNodeInfo == null || PERMISSION_GRANTED)
            return;

        // Step 1 find SpaceUp and Click  | Step 2 Click Permit


        //Scroll the page
        gainPermission(pNodeInfo);


    }

    private void gainPermission(final AccessibilityNodeInfo pNodeInfo) {

        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scroll(pNodeInfo);
            }
        }, 1000);
        List<AccessibilityNodeInfo> list;
        list = pNodeInfo.findAccessibilityNodeInfosByText(context.getResources().getString(R.string.app_name));
        if (pNodeInfo.toString().contains("android.widget.ListView") && UtilityMethods.isNotEmptyCollection(list)) {
            for (final AccessibilityNodeInfo node : list) {
                // One second Delay
                if (!SPACEUP_CLICKED)
                    performEvent(node, 1);

            }
        }


        //For some phone there is Pemit Usage Access
        list = pNodeInfo.findAccessibilityNodeInfosByText("Permit");
        if (UtilityMethods.isNotEmptyCollection(list)) {
            for (final AccessibilityNodeInfo node : list) {
                // One second Delay
                performEvent(node, 2);


            }
        }
        //Other  phone there is Allow app usage

        if (!PERMISSION_GRANTED) {
            list = pNodeInfo.findAccessibilityNodeInfosByText("Allow");
            if (UtilityMethods.isNotEmptyCollection(list)) {
                for (final AccessibilityNodeInfo node : list) {

                    performEvent(node, 2);


                }
            }
        }

        if (!PERMISSION_GRANTED) {
            list = pNodeInfo.findAccessibilityNodeInfosByText("OK");
            if (UtilityMethods.isNotEmptyCollection(list)) {
                for (final AccessibilityNodeInfo node : list) {

                    performEvent(node, 2);


                }
            }
        }
    }


    private void performEvent(AccessibilityNodeInfo node, int event) {

        while (node != null) {

            if (node.isClickable()) {
                node.performAction(ACTION_CLICK);
                if (event == 2) {
                    PERMISSION_GRANTED = true;

                    AccessibilityAutomation.getSharedInstance().globalActionBack();
                    //press back button 2 times
                    final AccessibilityNodeInfo finalNode = node;
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AccessibilityAutomation.getSharedInstance().globalActionBack();
                            AccessibilityAutomation.getSharedInstance().removeScanningOverLay();
                            new AnalyticsController(context).sendAnalytics(
                                    AnalyticsConstants.Category.PERMISSION,
                                    AnalyticsConstants.Action.APP_USAGE,
                                    AnalyticsConstants.Label.TAKEN,
                                    null,
                                    false,
                                    null);
                            // Above method will also open UninstallerActivity
                        }
                    }, 500);


                } else {
                    SPACEUP_CLICKED = true;
                }
                break;
            }
            node = node.getParent();

        }

    }

    private void scroll(AccessibilityNodeInfo node) {

        while (node != null) {

            if (node.isScrollable()) {

                node.performAction(ACTION_SCROLL_FORWARD);

                break;
            }
            node = node.getParent();

        }

    }
}
