package com.spaceup.View;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by shashank.tiwari on 27/03/17.
 */

public class GridItem extends ImageView {

    public GridItem(Context context) {
        super(context);
    }

    public GridItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GridItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec); // This is the key that will make the height equivalent to its width
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
    }
}