package com.spaceup.uninstall.adapters;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spaceup.R;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.uninstall.activities.AppInfo;
import com.spaceup.uninstall.activities.DataTransferInterface;

import java.util.List;

public class OptimizeListAdapter extends RecyclerView.Adapter<OptimizeListAdapter.ViewHolder> {

    private static final int TYPE_HEAD = 0;
    private static final int TYPE_LIST = 1;
    private Context context;
    private List<AppInfo> optimizeList;
    private DataTransferInterface dtInterface;
    private int mWarningTheme, lowUsageThreshold;

    public OptimizeListAdapter(Context context, List<AppInfo> optimizeList, DataTransferInterface dtInterface, int warningTheme, int lowUsageThreshold) {
        this.context = context;
        this.lowUsageThreshold = lowUsageThreshold;
        this.optimizeList = optimizeList;
        this.dtInterface = dtInterface;
        this.mWarningTheme = warningTheme;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_LIST) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.deactivate_app_row, parent, false);
            return new ViewHolder(itemView, viewType);
        } else if (viewType == TYPE_HEAD) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.header_layout, parent, false);
            return new ViewHolder(itemView, viewType);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (holder.view_type == TYPE_LIST) {
            final AppInfo app = optimizeList.get(position - 1);
            try {
                Drawable icon = context.getPackageManager().getApplicationIcon(app.getPkgName());
                holder.appIcon.setImageDrawable(icon);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }


            String appName = app.getAppName();
            if (appName.length() >= 18) {
                appName = appName.substring(0, 17);
                appName = appName + "...";
            }

            holder.appName.setText(appName);

            long size = app.getSize() + app.getCacheSize() + app.getCodeSize() + app.getApkSize();
            holder.size.setText(getSizeinMB(size));
            if (app.isChecked()) {
                holder.checkbox.setChecked(true);
            } else {
                holder.checkbox.setChecked(false);
            }

            holder.checkbox.setClickable(false);
            holder.row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (holder.checkbox.isChecked()) {
                        app.setChecked(false);
                        holder.checkbox.setChecked(false);
                        dtInterface.setValues(app, 1, position);

                    } else {

                        app.setChecked(true);
                        holder.checkbox.setChecked(true);
                        dtInterface.setValues(app, 2, position);

                    }
                }
            });

            //here we fixed 5 because only 5 elements are pre-selected
            if (position <= lowUsageThreshold) {
                holder.usageCount.setText("Low usage");
                int color = Color.parseColor("#23c795"); //The color GREEN
                holder.usageCount.setTextColor(color);
                holder.labelcolor.setVisibility(View.GONE);

            } else if (position > lowUsageThreshold) {
                holder.usageCount.setText("Medium usage");
                int color = Color.parseColor("#ffb96a"); //The color YELLOW
                holder.usageCount.setTextColor(color);
                holder.labelcolor.setVisibility(View.GONE);
            }

        } else if (holder.view_type == TYPE_HEAD) {

            if (optimizeList.size() == 0) {
                holder.no_apps_found_message_card.setVisibility(View.VISIBLE);
                holder.warning_view.setVisibility(View.GONE);
            } else {
                if (mWarningTheme == Constants.WARNING_THEME_RED) {
                    holder.warning_view.findViewById(R.id.warning_layout).setBackgroundColor(context.getResources().getColor(R.color.warning_background_red));
                    ((ImageView) holder.warning_view.findViewById(R.id.icon_warning)).setImageDrawable(context.getResources().getDrawable(R.drawable.uninstall_warning_white));
                    ((TextView) holder.warning_view.findViewById(R.id.textView7)).setTextColor(context.getResources().getColor(R.color.white));
                }
            }

        }


    }

    public List<AppInfo> getList() {
        return optimizeList;
    }

    @Override
    public int getItemCount() {
        //adding one for header
        return optimizeList.size() + 1;
    }

    String getSizeinMB(long size) {
        if (size > 1024) {
            size = size / 1024;
            if (size > 1024) {
                size = size / 1024;
                if (size > 1024) {
                    size = size / 1024;
                    return size + "GB";
                }
                return (size) + " MB";
            }
            return size + " KB";

        }
        return size + " Bytes";
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEAD;
        }
        return TYPE_LIST;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        //variable for list
        public ImageView appIcon, labelcolor;
        public TextView appName, size, usageCount;
        public CheckBox checkbox;
        public RelativeLayout row;
        int view_type;
        //variable for head-section
        CardView no_apps_found_message_card;
        RelativeLayout warning_view;

        public ViewHolder(View view, int viewType) {
            super(view);
            if (viewType == TYPE_LIST) {
                appIcon = (ImageView) view.findViewById(R.id.icon);
                appName = (TextView) view.findViewById(R.id.app_name);
                usageCount = (TextView) view.findViewById(R.id.usage);
                size = (TextView) view.findViewById(R.id.size);
                checkbox = (CheckBox) view.findViewById(R.id.check_box);
                row = (RelativeLayout) view.findViewById(R.id.row);
                labelcolor = (ImageView) view.findViewById(R.id.labelcolor);
                view_type = 1;
            } else if (viewType == TYPE_HEAD) {
                view_type = 0;
                no_apps_found_message_card = (CardView) view.findViewById(R.id.no_apps_found_message_card);
                warning_view = (RelativeLayout) view.findViewById(R.id.warning_view);
            }


        }
    }
}