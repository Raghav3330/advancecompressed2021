package com.spaceup.Runnable;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityAutomation;

import static com.spaceup.apkgenerator.constants.Constants.REQUEST_TURN_ON;

/**
 * Created by Times on 24-02-2017.
 */

public class AccRunnable implements Runnable {
    private volatile boolean exit = false;
    int trialCount = 0;
    int waitCount = 0;
    int removeBannerTimer = 0;

    View overlayLayout;
    WindowManager windowManager;
    Activity activityContext;
    UtilityMethods methods;

    public AccRunnable(WindowManager windowManager, View overlayLayout, Activity uninstallerActivity) {
        this.windowManager = windowManager;
        this.overlayLayout = overlayLayout;
        this.activityContext = uninstallerActivity;
        methods = UtilityMethods.getInstance();
        REQUEST_TURN_ON = true;
        Log.d("backdebug", "openAccessibilityHelper: inside function requestTurnOn :" + REQUEST_TURN_ON);
    }


    @Override
    public void run() {
        do {
            try {
                Thread.sleep(1000);
                //show dialog for 8 seconds only
                if (removeBannerTimer >= 7) {
                    try {
                        windowManager.removeView(overlayLayout);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                removeBannerTimer++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.d("backdebug", "inside do-while loop " + removeBannerTimer);
            if (waitCount >= 20) {
                Log.d("backdebug", "count exceeding now stopping thread");
                Log.d("backdebug", "calling stop from thread");
                stop();
                break;


            }
            waitCount++;
        }
        while ((AccessibilityAutomation.getSharedInstance() == null && !methods.isAccessibilityServiceRunning(activityContext)) && !exit);

        while (null != AccessibilityAutomation.getSharedInstance() && !activityContext.getPackageName().equals(AccessibilityAutomation.getSharedInstance().currentVisiblePackage)) {
            activityContext.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("backdebug", "generating flick " + trialCount);
                    AccessibilityAutomation.allowSpecialBack();

                }
            });
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //2 times create flick
            if (trialCount == 1) {
                break;
            }
            trialCount++;
        }

        if (methods.isAccessibilityServiceRunning(activityContext)) {
            activityContext.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        windowManager.removeView(overlayLayout);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //onActivityResult(55,0,null);

                    activityContext.finishActivity(55);
                    Log.d("backdebug", "calling finish 55");
                }
            });
        }


    }//end of run methods


    public void stop() {
        Log.d("backdebug", "stopping thread");

        exit = true;
        REQUEST_TURN_ON = false;
        trialCount = 0;
        waitCount = 0;
        removeBannerTimer = 0;
    }
}