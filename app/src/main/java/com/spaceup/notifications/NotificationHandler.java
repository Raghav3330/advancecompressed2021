package com.spaceup.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.spaceup.Activities.Scanning;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.OuterUninstallerLayout;
import com.spaceup.R;
import com.spaceup.TakeAccessibilityfromNoti;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.test.DBPrintService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;
import static android.content.Context.NOTIFICATION_SERVICE;


public class NotificationHandler {
    // Notification handler singleton
    private static NotificationHandler nHandler;
    private static NotificationManager mNotificationManager;
    private Bundle mGAParams;
    JSONObject mApsalarTracking;

    private final int JUNK_NOTIFICATION_ID = 1001;

    private NotificationHandler(Context pContext) {
        mNotificationManager =
                (NotificationManager) pContext.getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
    }


    /**
     * Singleton pattern implementation
     *
     * @return
     */
    public static NotificationHandler getInstance(Context context) {
        if (nHandler == null) {
            synchronized (NotificationHandler.class) {
                if (nHandler == null) {
                    nHandler = new NotificationHandler(context);
                }
            }
        }

        return nHandler;
    }

    public void two_app_installed_notification(Context context) {
        // Analytics Start
        Calendar calendar = Calendar.getInstance();

        mGAParams = new Bundle();
        mGAParams.putString("category", AnalyticsConstants.Category.NOTIFICATION);
        mGAParams.putString("action", AnalyticsConstants.Action.TWO_APP_INSTALLED);
        mGAParams.putString("label", AnalyticsConstants.Label.CLICKED);
        mGAParams.putLong("value", 0l);
        new AnalyticsHandler().logGAEvent(mGAParams);
        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
        // Analytics end
        //Apsalar Analytics START
        mApsalarTracking = new JSONObject();
        try {
            mApsalarTracking.put(AnalyticsConstants.Action.TWO_APP_INSTALLED, 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(AnalyticsConstants.APSALARTAG, "NOTIFICATION: " + mApsalarTracking.toString());
        new AnalyticsHandler().logApsalarJsonEvent(AnalyticsConstants.Key.NOTIFICATION, mApsalarTracking);
        //Apsalar Analytics END


        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.my_custom_notification);

        // Set Notification Title
        String strtitle = "TWO_APPS_INSTALLED";
        // Set Notification Text
        String strtext = "set Notification Text";

        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(context, Scanning.class);
        // Send data to NotificationView Class
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.drawable.logo)
                // Set Ticker Message
                .setTicker("Apps wasting storage")
                // Dismiss Notification
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Set RemoteViews into Notification
                .setContent(remoteViews);

        // Locate and set the Image into customnotificationtext.xml ImageViews
        remoteViews.setImageViewResource(R.id.uncompress_img, R.drawable.logo);
        //Locate and set the Text into customnotificationtext.xml TextViews
     /*   remoteViews.setTextViewText(R.id.title,getString(R.string.customnotificationtitle));
        remoteViews.setTextViewText(R.id.text,getString(R.string.customnotificationtext));
      */  // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(0, builder.build());


    }

    public void four_days_Notification(Context context) {
        // Analytics Start
        Calendar calendar = Calendar.getInstance();

        mGAParams = new Bundle();
        mGAParams.putString("category", AnalyticsConstants.Category.NOTIFICATION);
        mGAParams.putString("action", AnalyticsConstants.Action.TOP_NOTIFICATION);
        mGAParams.putString("label", AnalyticsConstants.Label.CLICKED);
        mGAParams.putLong("value", 0l);
        new AnalyticsHandler().logGAEvent(mGAParams);
        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
        // Analytics end


        //Apsalar Analytics START
        mApsalarTracking = new JSONObject();
        try {
            mApsalarTracking.put(AnalyticsConstants.Action.TOP_NOTIFICATION, 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(AnalyticsConstants.APSALARTAG, "NOTIFICATION: " + mApsalarTracking.toString());
        new AnalyticsHandler().logApsalarJsonEvent(AnalyticsConstants.Key.NOTIFICATION, mApsalarTracking);
        //Apsalar Analytics END

        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.my_custom_notification);

        // Set Notification Title
        String strtitle = "UPPER_NOTIFICATION";
        // Set Notification Text
        String strtext = "set Notification Text";

        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(context, OuterUninstallerLayout.class);
        // Send data to NotificationView Class
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.drawable.logo_noti)
                // Set Ticker Message
                .setTicker("Apps wasting storage")
                // Dismiss Notontext.getString(R.string.customnotificationticker)ification
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Set RemoteViews into Notification
                .setContent(remoteViews);

        // Locate and set the Image into customnotificationtext.xml ImageViews
        remoteViews.setImageViewResource(R.id.uncompress_img, R.drawable.logo);
        //Locate and set the Text into customnotificationtext.xml TextViews
     /*   remoteViews.setTextViewText(R.id.title,getString(R.string.customnotificationtitle));
        remoteViews.setTextViewText(R.id.text,getString(R.string.customnotificationtext));
      */  // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(0, builder.build());


    }

    public void custom_sad_noti(Context context) {
        // Using RemoteViews to bind custom layouts into Notification
        // Analytics Start
        Calendar calendar = Calendar.getInstance();

        mGAParams = new Bundle();
        mGAParams.putString("category", AnalyticsConstants.Category.NOTIFICATION);
        mGAParams.putString("action", AnalyticsConstants.Action.APP_LOST_NOTIFICATION);
        new AnalyticsHandler().logGAEvent(mGAParams);
        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
        // Analytics end


        //Apsalar Analytics START
        mApsalarTracking = new JSONObject();
        try {
            mApsalarTracking.put(AnalyticsConstants.Action.APP_LOST_NOTIFICATION, 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(AnalyticsConstants.APSALARTAG, "NOTIFICATION: " + mApsalarTracking.toString());
        new AnalyticsHandler().logApsalarJsonEvent(AnalyticsConstants.Key.NOTIFICATION, mApsalarTracking);
        //Apsalar Analytics END

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.my_custom_notification_sad);


        // Set Notification Title
        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(context, TakeAccessibilityfromNoti.class);
        // Send data to NotificationView Class

        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.drawable.logo_noti)
                // Set Ticker Message
                .setTicker("You just lost an app")
                // Dismiss Notification
                .setAutoCancel(true)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Set RemoteViews into Notification
                .setContent(remoteViews);

        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationmanager.notify(0, builder.build());


    }

    /**
     * Show a determinate and undeterminate progress notification
     *
     * @param context, activity context
     */
    public void createProgressNotification(final Context context, String pkgName) {
        Log.d("Notification", "createProgressNotification");
        // used to update the progress notification
        final int progresID = 69;

        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.notification_uncompressing);


        try {
            String APKFilePath = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + pkgName + ".stash";
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageArchiveInfo(APKFilePath, 0);
            // Logo of the app from DIR....
            pi.applicationInfo.sourceDir = APKFilePath;
            pi.applicationInfo.publicSourceDir = APKFilePath;
            Drawable APKicon = pi.applicationInfo.loadIcon(pm);
            Bitmap bitmap = ((BitmapDrawable) APKicon).getBitmap();
            remoteViews.setImageViewBitmap(R.id.uncompress_img, bitmap);
        } catch (Exception e) {
            remoteViews.setImageViewResource(R.id.uncompress_img, R.drawable.logo);
            e.printStackTrace();
        }


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Log.d("Notification", "createProgressNotification: " + pkgName);
        String first = "Uncompressing " + AppDBHelper.getInstance(context).getAppName(pkgName + ".stash");


        Log.d("Notification", "first string is " + first);

        //String totalString = first;

        if (first.length() >= 22) {
            first = first.substring(0, 22);
            first = first + "...";
        }

        Intent notificationIntent = new Intent(context, ShowToastService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, notificationIntent, 0);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.drawable.logo_noti)
                // Set Ticker Message
                .setTicker("Uncompressing")

                .setSound(defaultSoundUri)
                // Set PendingIntent into Notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(false)
                // Set RemoteViews into Notification
                .setContent(remoteViews);

        remoteViews.setTextViewText(R.id.title, first);

        Notification n = builder.build();
        n.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
        mNotificationManager.notify(progresID, n);

    }


    public void updateProgressNotification(final Context context, String package_name) {

        // used to update the progress notification
        final int progresID = 69;


        // Using RemoteViews to bind custom layouts into Notification

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.notification_do_in_bkg);

        Intent LaunchIntent = context.getPackageManager().getLaunchIntentForPackage(package_name);
        LaunchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, LaunchIntent, 0);

        try {
            Drawable x = context.getPackageManager().getApplicationIcon(package_name);
            Bitmap bitmap = ((BitmapDrawable) x).getBitmap();
            remoteViews.setImageViewBitmap(R.id.uncompress_img, bitmap);
        } catch (PackageManager.NameNotFoundException e) {
            remoteViews.setImageViewResource(R.id.uncompress_img, R.drawable.logo);
            e.printStackTrace();
        }
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        String totalString = UtilityMethods.getInstance().getApplicationName(context, package_name) + " Uncompressed";
        if (totalString.length() >= 22) {
            totalString = totalString.substring(0, 22);
            totalString = totalString + "...";
        }


        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.drawable.logo_noti)
                // Set Ticker Message
                .setTicker("App Uncompressed")
                // Dismiss Notification
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Set RemoteViews into Notification
                .setContent(remoteViews);


        remoteViews.setTextViewText(R.id.title, totalString);
        remoteViews.setTextViewText(R.id.text, "TIP: Compress again after use");
        remoteViews.setTextViewText(R.id.imagenotiright, "Open");
        Notification n = builder.build();
        // Build Notification with Notification Manager
        mNotificationManager.notify(progresID, n);
    }

    public void createTestNotification(Context context) {

        Intent notificationIntent = new Intent(context, DBPrintService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, notificationIntent, 0);

        // Building the notification
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.logo_noti) // notification icon
                .setContentTitle("Test") // main title of the notification
                .setContentText("Click to print db") // notification text
                .setAutoCancel(false)
                .setContentIntent(pendingIntent); // notification intent

        // mId allows you to update the notification later on.
        mNotificationManager.notify(10, mBuilder.build());
    }


}
