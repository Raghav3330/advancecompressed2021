package com.spaceup.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by shashanktiwari on 06/01/17.
 */

public class StateRetrieveModel implements Serializable {
    String uninstalRemainingPackage;
    ArrayList<String> compressedAppList;
    ArrayList<String> uncompressedAppList;
    HashMap dummyApkPathList;
    ArrayList<String> uncompressedAppNameList;

    public HashMap getDummyApkPathList() {
        return dummyApkPathList;
    }

    public void setDummyApkPathList(HashMap dummyApkPathList) {
        this.dummyApkPathList = dummyApkPathList;
    }

    public ArrayList<String> getCompressedAppList() {
        return compressedAppList;
    }

    public void setCompressedAppList(ArrayList<String> compressedAppList) {
        this.compressedAppList = compressedAppList;
    }

    public ArrayList<String> getUncompressedAppNameList() {

        return uncompressedAppNameList;
    }

    public void setUncompressedAppNameList(ArrayList<String> uncompressedAppNameList) {
        this.uncompressedAppNameList = uncompressedAppNameList;
    }

    public String getUninstalRemainingPackage() {

        return uninstalRemainingPackage;
    }

    public void setUninstalRemainingPackage(String uninstalRemainingPackage) {
        this.uninstalRemainingPackage = uninstalRemainingPackage;
    }

    public ArrayList<String> getUncompressedAppList() {
        return uncompressedAppList;
    }

    public void setUncompressedAppList(ArrayList<String> uncompressedAppList) {
        this.uncompressedAppList = uncompressedAppList;
    }
}
