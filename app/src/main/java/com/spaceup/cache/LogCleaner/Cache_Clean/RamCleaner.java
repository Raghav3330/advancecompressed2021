package com.spaceup.cache.LogCleaner.Cache_Clean;

import android.app.ActivityManager;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.spaceup.Utility.UtilityMethods;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static android.content.Context.ACTIVITY_SERVICE;

/**
 * Created by dhurv on 16-01-2017.
 */

public class RamCleaner extends AsyncTask<Void, Void, Void> {
    private List<String> mAppsList;
    private Context mContext;
    private final String TAG = RamCleaner.class.getSimpleName();

    public RamCleaner(Context pContext, ArrayList<String> pAppsList) {
        this.mAppsList = pAppsList;
        this.mContext = pContext;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (UtilityMethods.isNotEmptyCollection(mAppsList)) {
            ActivityManager am = (ActivityManager) mContext.getSystemService(ACTIVITY_SERVICE);
            List<ActivityManager.RunningServiceInfo> runningServiceInfoList = am.getRunningServices(Integer.MAX_VALUE);

            Set<String> packageSet = new HashSet<>();
            packageSet.addAll(mAppsList);
            for (ActivityManager.RunningServiceInfo runningServiceInfo : runningServiceInfoList) {
                if (runningServiceInfo.foreground || (runningServiceInfo.flags & ActivityManager.RunningServiceInfo.FLAG_SYSTEM_PROCESS) >= 1) {
                    packageSet.remove(runningServiceInfo.service.getPackageName());
                }
            }
            for (String packageName : packageSet) {
                try {
                    am.killBackgroundProcesses(packageName);
                    Log.d(TAG, "RAM BOOSTED FOR " + packageName);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }
}