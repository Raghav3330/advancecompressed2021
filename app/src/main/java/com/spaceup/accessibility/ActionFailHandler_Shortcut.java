package com.spaceup.accessibility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.CountDownTimer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import java.util.List;

/**
 * Created by Dhruv Kaushal on 10/02/17.
 */

public class ActionFailHandler_Shortcut {
    public static final int DIFFERENCE_TIME = 7000;   // Ten second difference
    CountDownTimer singleAppUninstallTimer;
    int singleAppRetryCount = 0;
    Context context;
    BroadcastReceiver accessibilityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("com.times.eventAppRemovedShortcut")) {
                if (intent.getExtras().getBoolean("get_packagename")) {
                    singleAppUninstallTimer.cancel();
                    Log.d("timer", "Restarting Timeout Cancel as apk installed");
                    LocalBroadcastManager.getInstance(context).unregisterReceiver(accessibilityReceiver);

                }
            }
        }
    };

    public ActionFailHandler_Shortcut(Context context) {
        this.context = context;
        singleAppRetryCount = 0;
        IntentFilter accessibilityReceiverIntentFilter = new IntentFilter();

        accessibilityReceiverIntentFilter.addAction("com.times.eventAppRemovedShortcut");   //Broadcast Listner
        LocalBroadcastManager.getInstance(context).registerReceiver(accessibilityReceiver,
                accessibilityReceiverIntentFilter);


    }

    public void startUninstall(final String packageName) {

        if (singleAppRetryCount > 2) {
            Log.d("timer", "Restarting Timeout Skipping");
            //start for next app
            try {
                singleAppUninstallTimer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }

            // fake uninstall was successful
            Intent babyStashInstallationEvent = new Intent("com.times.eventAppRemovedShortcut");
            babyStashInstallationEvent.putExtra("get_packagename", true);
            LocalBroadcastManager.getInstance(context).sendBroadcast(babyStashInstallationEvent);
        } else {
            Log.d("timer", "Retrying for " + singleAppRetryCount);

            singleAppRetryCount++;

            if(isPackageExisted(packageName,context)) {
                Uri packageUri = Uri.parse("package:" + packageName);
                Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageUri);
                uninstallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                context.startActivity(uninstallIntent);
            }

            try {
                singleAppUninstallTimer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
            startActionHandlerTimerOneApp(packageName);
        }
    }

    private void startActionHandlerTimerOneApp(final String packageName) {
        singleAppUninstallTimer = new CountDownTimer(DIFFERENCE_TIME, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {

                startUninstall(packageName);

            }
        };
        singleAppUninstallTimer.start();

    }
    public boolean isPackageExisted(String targetPackage,Context context){
        List<ApplicationInfo> packages;
        PackageManager pm;

        pm = context.getPackageManager();
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            if(packageInfo.packageName.equals(targetPackage))
                return true;
        }
        return false;
    }
}
