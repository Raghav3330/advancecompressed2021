package com.spaceup.models;

import com.spaceup.apkgenerator.constants.Constants;

/**
 * Created by Times on 23-03-2017.
 */

public class ImageModel {
    //path of image
    private String mPath;
    private int mResponseStatus;
    private int mActionStatus;
    private String mTrashPath;
    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getPath() {
        return mPath;
    }


    //default value for ImageModel
    public ImageModel() {
        mResponseStatus = Constants.NOT_SCANNED;
        mActionStatus = Constants.NOT_JUNK;
        mTrashPath = "";
    }

    public void setPath(String mPath) {
        this.mPath = mPath;
    }

    public int getResponseStatus() {
        return mResponseStatus;
    }

    public void setResponseStatus(int mResponseStatus) {
        this.mResponseStatus = mResponseStatus;
    }

    public int getActionStatus() {
        return mActionStatus;
    }

    public void setActionStatus(int mActionStatus) {
        this.mActionStatus = mActionStatus;
    }

    public String getTrashPath() {
        return mTrashPath;
    }

    public void setTrashPath(String mTrashPath) {
        this.mTrashPath = mTrashPath;
    }

    @Override
    public int hashCode() {
        if (mPath != null) {
            return mPath.hashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(mPath !=null && ((ImageModel) obj).mPath != null){
            return mPath.equals(((ImageModel) obj).mPath);
        }
        return false;
    }
}
