package com.spaceup.AppShortcut;
/**
 * Created by dhurv on 25-01-2017.
 */

import android.app.Activity;
import android.app.Dialog;
import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.spaceup.Activities.Scanning;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.R;
import com.spaceup.Runnable.AccRunnable;
import com.spaceup.Splash_Screen;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityCommunicator;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.models.DataModel;
import com.spaceup.uninstall.activities.AppInfo;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.spaceup.accessibility.AccessibilityAutomation.SHOW_DO_IN_BKG_BTN_INTENT;

import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan;
import io.github.inflationx.calligraphy3.TypefaceUtils;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class ShortCutFolder extends Activity implements DataInterface {
    public static int MOVE_TO_MENU = 0;
    public static boolean requestTurnOn = false;
    public ArrayList<String> toCompressAppList = new ArrayList<>();
    public ArrayList<String> toCompressAppName = new ArrayList<>();
    List<ItemObject> allItems = new ArrayList<>();
    private long mAppsize = 0;
    RecyclerView rView, rView1;
    int previous_count = 0;
    int divider_pos = 1, divider_mid;
    Dialog permissionDialog;
    private final String path = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + "/" + Constants.STASH_DB_FILE + ".json";
    Button accessibility_permission_btn;
    Button unknown_permission_btn;
    ImageView acc_tick, unknown_tick, cancel_btn;
    View overlayLayout;
    WindowManager windowManager;
    AccRunnable accRunnable;
    UnknownRunnable unknownRunnable;
    Thread accCheckThread, unknownThread;
    Dialog dialog_unknown;
    View oView1;
    WindowManager wm1;
    RecyclerViewAdapter rcAdapter = null;
    Bitmap image;
    Bundle mGAParams;
    int resultCodetracker;
    Dialog mAdvance;
    private int currentSize = 0;
    long compressed_apps = 0;
    long compressed_app_size = 0;
    long total_apps = 0;
    long total_app_size = 0;
    long size_of_system_apps = 0;
    // declaring list of uncompressed apps:- Used in sequentially uninstalling - installing apps
    BroadcastReceiver accessibilityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("com.times.convertToAdvance")) {
                if (intent.getExtras().getString("get_packagename") != null) {
                    String packageName = intent.getExtras().getString("get_packagename");
                    try {
                        AppDBHelper.getInstance(context).deleteFromShortcut(packageName.substring(0, packageName.lastIndexOf(".stash")));
                        Log.d("shortcutfile", "onReceive: Remove to file");
                        UtilityMethods.getInstance().removeFromJSONFile(packageName);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
    private int mStartLoc;
    private GridLayoutManager lLayout;
    BroadcastReceiver getAccessibilityReceiver_reactivate = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SHOW_DO_IN_BKG_BTN_INTENT)) {
                Log.d("SHORTCUT_FOLDER", "RECYCLER_UPDATED");
                getAllItemList();
            }
            LocalBroadcastManager.getInstance(context).unregisterReceiver(getAccessibilityReceiver_reactivate);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Runnable widgetUpdater = new Runnable() {
            @Override
            public void run() {
                JSONParser parser = new JSONParser();
                JSONArray apps = null;
                try {
                    Object obj = parser.parse(new FileReader(Constants.RESIDUAL_FILE));
                    JSONObject jsonObject = (JSONObject) obj;

                    //reading json array of apps so that we can append new app
                    apps = (JSONArray) jsonObject.get("apps");
                    if (null == apps) {
                        apps = new JSONArray();
                    }
                    JSONArray msg = (JSONArray) jsonObject.get("apps");
                    Iterator<JSONObject> iterator = msg.iterator();
                    while (iterator.hasNext()) {
                        JSONObject ap = iterator.next();
                        //System.out.println(iterator.next());


                        String pkgWithStash = (String) ap.get("pkgname");
                        if (pkgWithStash.contains(".stash")) {

                            String pkgWithOutStash = pkgWithStash.substring(0, pkgWithStash.lastIndexOf(".stash"));
                            Log.d("widgetUpdater", "run: " + pkgWithOutStash);

                            long size = (long) ap.get("size");
                            String appName = "";
                            try {
                                String APKFilePath = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + pkgWithStash;
                                Log.d("widgetUpdater", "run: " + APKFilePath);
                                PackageManager pm = getPackageManager();
                                try {
                                    //APKFilePath = APKFilePath.substring(0, APKFilePath.lastIndexOf(".stash"));
                                    PackageInfo pi = pm.getPackageArchiveInfo(APKFilePath, 0);
                                    pi.applicationInfo.sourceDir = APKFilePath;
                                    pi.applicationInfo.publicSourceDir = APKFilePath;
                                    appName = (String) pi.applicationInfo.loadLabel(pm);
                                } catch (Exception e) {
                                    Log.d("widgetUpdater", Log.getStackTraceString(e));
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                Log.d("widgetUpdater", Log.getStackTraceString(e));
                                e.printStackTrace();
                            }
                            Log.d("widgetUpdater", "package name " + pkgWithOutStash + " size is" + size + " app name " + pkgWithOutStash);
                            AppDBHelper.getInstance(ShortCutFolder.this).addToShorcutTable(appName, pkgWithOutStash, size);

                        }//end of if
                    }
                } catch (Exception e) {
                    Log.d("widgetUpdater", Log.getStackTraceString(e));
                    e.printStackTrace();
                }
            }
        };
        if (AppDBHelper.getInstance(ShortCutFolder.this).getShorcutApps().size() == 0) {
            Log.d("widgetUpdater", "start from splash screeen");
//            Thread widgetUpdaterThread = new Thread(widgetUpdater);
//            widgetUpdaterThread.start();
            widgetUpdater.run();
        }


        try {
            //from launcher icon
            mStartLoc = 1;
            getIntent().getCategories().toString();
        } catch (Exception e) {
            //from outside application
            mStartLoc = 2;
            e.printStackTrace();
        }

        if (mStartLoc == 1) {
            //from laucher

        }
        //from outside laucher (shortcut)
        else if (mStartLoc == 2) {


            new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.SHORTCUT);

        }

        MOVE_TO_MENU = 0;
        setContentView(R.layout.activity_short_cut_folder_other);
        try {
            if (dialog_unknown.isShowing()) {
                if (UtilityMethods.getInstance().isHavingUnkownSourcePermission(getApplicationContext())) {
                    dialog_unknown.dismiss();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (permissionDialog.isShowing()) {
                setDialogBox();
                if (UtilityMethods.getInstance().isAccessibilityServiceRunning(ShortCutFolder.this) && UtilityMethods.getInstance().isHavingUnkownSourcePermission(ShortCutFolder.this)) {
                    permissionDialog.cancel();
                    //uninstallNew();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(ShortCutFolder.this).unregisterReceiver(accessibilityReceiver);
        MOVE_TO_MENU = 0;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("NOTIFY_DATASET_CHANGED", "true");
        getAllItemList();

        //  IntentFilter refrestRecycler = new IntentFilter();
        // refrestRecycler.addAction(SHOW_DO_IN_BKG_BTN_INTENT);
        // LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(getAccessibilityReceiver_reactivate, refrestRecycler);

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void getAllItemList() {
        class LoadApps extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                List<AppInfo> appListDb = AppDBHelper.getInstance(ShortCutFolder.this).getAdvanceCompressedApps();

                allItems.add(new ItemObject("", null, ""));
                allItems.add(new ItemObject("", null, ""));

                // Listing Installed Dummy APK

                List<AppInfo> appListDbShortCut = AppDBHelper.getInstance(ShortCutFolder.this).getShorcutApps();
                // Listing Installed Dummy APK
                for (AppInfo appInfo : appListDbShortCut) {
                    Drawable icon = null;
                    try {
                        Log.d("ShortCutFolder", appInfo.getAppName() + " " + appInfo.getPkgName());

                        //TODO optimise here
                        try {

                            String APKFilePath = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + appInfo.getPkgName() + ".stash";
                            PackageManager pm = getPackageManager();
                            PackageInfo pi = pm.getPackageArchiveInfo(APKFilePath, 0);

                            // Logo of the app from DIR....
                            pi.applicationInfo.sourceDir = APKFilePath;
                            pi.applicationInfo.publicSourceDir = APKFilePath;
                            //
                            Drawable APKicon = pi.applicationInfo.loadIcon(pm);
                            allItems.add(new ItemObject(appInfo.getAppName(), UtilityMethods.getInstance().drawableToBitmap(APKicon), appInfo.getPkgName()));
                            toCompressAppList.add(appInfo.getPkgName());
                            toCompressAppName.add(appInfo.getAppName());
                            mAppsize += appInfo.getApkSize();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                divider_mid = allItems.size();
                allItems.add(new ItemObject("", null, ""));

                for (AppInfo appInfo : appListDb) {
                    try {
                        Log.d("ShortCutFolder", appInfo.getAppName() + " " + appInfo.getPkgName() + (appInfo.getCodeSize() + appInfo.getSize() + appInfo.getCacheSize() + appInfo.getApkSize()));


                        String APKFilePath = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + appInfo.getPkgName() + ".stash";
                        PackageManager pm = getPackageManager();
                        Drawable APKicon = null;
                        try {
                            APKFilePath = APKFilePath.substring(0, APKFilePath.lastIndexOf(".stash"));
                            PackageInfo pi = pm.getPackageArchiveInfo(APKFilePath, 0);

                            // Logo of the app from DIR....
                            pi.applicationInfo.sourceDir = APKFilePath;
                            pi.applicationInfo.publicSourceDir = APKFilePath;
                            //
                            APKicon = pi.applicationInfo.loadIcon(pm);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        //TODO optimise here
                        if (appInfo.getPkgName().contains(".stash") && appInfo.getCompressed_status() == 2 && !appInfo.getPkgName().equals("com.spaceup")) {

                            String realPkgName = appInfo.getPkgName().substring(0, appInfo.getPkgName().length() - 6);
                            Log.d("SHORTCUT_PKG_NAME", realPkgName);
                            DataModel data = AppDBHelper.getInstance(ShortCutFolder.this).getDataSize(realPkgName);
                            mAppsize += (appInfo.getCodeSize() + appInfo.getSize() + appInfo.getCacheSize() + appInfo.getApkSize());
                            Log.d("SHORTCUT_PKG_NAME_SIZE", "" + (data.getCodeSize() + data.getApkSize() + data.getDataSize() + data.getCacheSize()));

                            allItems.add(new ItemObject(appInfo.getAppName(), UtilityMethods.getInstance().drawableToBitmap(APKicon), appInfo.getPkgName()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                return null;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mAppsize = 0;
                ProgressBar loading = (ProgressBar) findViewById(R.id.progressBar2);
                loading.setVisibility(View.VISIBLE);
                currentSize = allItems.size();
                allItems.clear();
                toCompressAppList.clear();
                toCompressAppName.clear();
            }

            @Override
            protected void onPostExecute(Void a) {
                super.onPostExecute(a);
                ProgressBar loading = (ProgressBar) findViewById(R.id.progressBar2);
                loading.setVisibility(View.INVISIBLE);
                if (rcAdapter == null) {
                    rView = (RecyclerView) findViewById(R.id.recycler_view);
                    rcAdapter = new RecyclerViewAdapter(ShortCutFolder.this, allItems, divider_mid, ShortCutFolder.this, mStartLoc);
                    lLayout = new GridLayoutManager(ShortCutFolder.this, 3);
                    lLayout.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            if (position == divider_pos || position == divider_mid) {
                                return 3;
                            } else {
                                return 1;
                            }
                        }
                    });
                    rView.setHasFixedSize(true);
                    rView.setLayoutManager(lLayout);
                    rView.setAdapter(rcAdapter);
                }
                try {
                    rcAdapter.setdata(allItems, divider_mid);
                    rcAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (allItems.size() == 3) {
                    // Show no apps found popup
                    RelativeLayout noAppsLayout = (RelativeLayout) findViewById(R.id.no_apps_found_shortcut);
                    noAppsLayout.setVisibility(View.VISIBLE);
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) noAppsLayout.getLayoutParams();
                    lp.topMargin = getResources().getDimensionPixelSize(R.dimen.dp_size_115);
                    noAppsLayout.setLayoutParams(lp);
                    TextView startCompressingText = (TextView) noAppsLayout.findViewById(R.id.textView28);
                    startCompressingText.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = null;
                            if (PrefManager.getInstance(ShortCutFolder.this).isFirstRun()) {
                                intent = new Intent(ShortCutFolder.this, Splash_Screen.class);
                            } else {
                                intent = new Intent(ShortCutFolder.this, Scanning.class);
                            }
                            ShortCutFolder.this.startActivity(intent);
                            ShortCutFolder.this.finish();
                        }
                    });

                    RelativeLayout bg = (RelativeLayout) findViewById(R.id.shortcut_folder_bg);
                    bg.setBackgroundColor(getResources().getColor(R.color.text_color));
                    RelativeLayout header = (RelativeLayout) findViewById(R.id.upper_layout);
                    header.setVisibility(View.GONE);

                } else {
                    // set header values
                    RelativeLayout bg = (RelativeLayout) findViewById(R.id.wallpaper);
                    WallpaperManager wallpaperManager = WallpaperManager.getInstance(ShortCutFolder.this);
                    Drawable wallpaperDrawable = wallpaperManager.getDrawable();
                    bg.setBackgroundDrawable(wallpaperDrawable);

                    compressed_apps = 0;
                    compressed_app_size = 0;
                    total_apps = 0;
                    total_app_size = 0;
                    size_of_system_apps = 0;
                    List<AppInfo> appInDb = AppDBHelper.getInstance(ShortCutFolder.this).getAppDetails();
                    //list of quick compressed apps
                    List<AppInfo> quickCompressedApps = AppDBHelper.getInstance(ShortCutFolder.this).getShorcutApps();

                    for (AppInfo app : appInDb) {
                        Log.d("DBug", "Package Name " + app.getPkgName());

                        //check that application status is compressed and package name is not com.stash i.e. we are excluding our own application
                        if (app.getCompressed_status() == 2) {
                            Log.d("analysis", "Package Name " + app.getPkgName() + app.getCompressed_status());
                            compressed_apps++;

                            String realPkgName = app.getPkgName().substring(0, app.getPkgName().length() - 6);
                            DataModel data = AppDBHelper.getInstance(ShortCutFolder.this).getDataSize(realPkgName);

                            compressed_app_size += data.getCodeSize() + data.getApkSize() + data.getDataSize() + data.getCacheSize();
                            Log.d("analysis", "Package Name " + app.getPkgName() + " data size " + (data.getCodeSize() + data.getApkSize() + data.getDataSize() + data.getCacheSize()));

                            //compressed_app_size += app.getCodeSize() + app.getSize() + app.getCacheSize();
                        } else if (app.getCompressed_status() == -1 && !app.getPkgName().equalsIgnoreCase("sample.example.dhruv.sample_apk")) {
                            //counting uncompressed apps and not considering sample.example.dhruv.sample_apk

                            total_apps++;
                            total_app_size += app.getCodeSize() + app.getSize() + app.getCacheSize() + app.getApkSize();
                        } else if (app.getCompressed_status() == -2) {
                            size_of_system_apps += app.getCodeSize() + app.getSize() + app.getCacheSize() + app.getApkSize();
                        }
                        //TODO sajal : remove data of sample.example.dhruv.sample_apk from total size
                        //we are adding size of system and user apps so that meter rises
                        //total_app_size += app.getCodeSize() + app.getSize() + app.getCacheSize() + app.getApkSize();
                    }
                    for (AppInfo app : quickCompressedApps) {
                        compressed_apps++;
                        compressed_app_size += app.getApkSize();
                        Log.d("compresssize", "Update: " + compressed_app_size);
                    }


                    double available_internal_memory = UtilityMethods.getInstance().getAvailableInternalMemorySize();

                    File file = new File(path);


                    Log.d("analysis", "Update: compressed_apps " + compressed_apps + "compressed_app_size " + compressed_app_size + "file exits " + file.exists());
                    //number of compressed apps should not be zero && compressed_app_size should be zero then only check from JSON file
                    if (compressed_apps != 0 && compressed_app_size == 0 && file.exists()) {

                        JSONParser parser = new JSONParser();
                        try {
                            Object obj = parser.parse(new FileReader(path));
                            JSONObject jsonObject = (JSONObject) obj;
                            //compressed_apps = Long.parseLong((String)jsonObject.get("count"));

                            Log.d("file", "old count compressed_apps " + compressed_apps);
                            Log.d("file", "old size compressed_app_size " + compressed_app_size);


                            long number_of_app = (long) jsonObject.get("count");

                            Log.d("analysis", "Update: number_of_app " + number_of_app);
                            //if(number_of_app == compressed_apps){
                            compressed_app_size = (long) jsonObject.get("size");
                            Log.d("analysis", "Update: compressed_app_size " + compressed_app_size);
                            //}

                            Log.d("file", "new count compressed_apps " + compressed_apps);
                            Log.d("file", "new size compressed_app_size " + compressed_app_size);

                        } catch (Exception e) {
                            Log.d("file", Log.getStackTraceString(e));
                            e.printStackTrace();
                        }
                    }

                    TextView totalAppsCompresses = (TextView) findViewById(R.id.textView12);
                    totalAppsCompresses.setText("" + compressed_apps);
                    TextView sizeTotalCompressedApps = (TextView) findViewById(R.id.textView13);
                    sizeTotalCompressedApps.setText("" + UtilityMethods.getInstance().getSizeinMBNew(compressed_app_size));
                    TextView sizeTotalCompressedAppsChar = (TextView) findViewById(R.id.charSize);
                    sizeTotalCompressedAppsChar.setText("" + UtilityMethods.getInstance().getSize_charecter(compressed_app_size));

                }
            }
        }
        new LoadApps().execute();

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Shortcut", "finish");
    }

    @Override
    public void setValues(boolean status) {
        convertALLpopup();
        Log.d("Converting", "Starting called " + resultCodetracker);

    }

    private void convertALLpopup() {
        Log.d("CLICKED", "CLICKED_LIST" + toCompressAppList.toString());
        mAdvance = new Dialog(ShortCutFolder.this, R.style.Dialog1);
        mAdvance.setContentView(R.layout.popup_into_advance);
        try {
            TextView total = (TextView) mAdvance.findViewById(R.id.textView44);
            total.setText(toCompressAppList.size() + " Apps");
            ImageView icon = (ImageView) mAdvance.findViewById(R.id.imageView6);
            String APKFilePath = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + toCompressAppList.get(0) + ".stash";
            PackageManager pm = getPackageManager();
            PackageInfo pi = pm.getPackageArchiveInfo(APKFilePath, 0);

            // Logo of the app from DIR....
            pi.applicationInfo.sourceDir = APKFilePath;
            pi.applicationInfo.publicSourceDir = APKFilePath;

            Drawable APKicon = pi.applicationInfo.loadIcon(pm);
            icon.setImageDrawable(APKicon);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ImageView later = (ImageView) mAdvance.findViewById(R.id.imageView7);
        later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdvance.dismiss();
            }
        });
        TextView advance = (TextView) mAdvance.findViewById(R.id.textView281);
        advance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (toCompressAppList.size() > 0) {

                    startAdvcanceProcess();
                } else {
                    Toast.makeText(getApplicationContext(), "No apps to convert!", Toast.LENGTH_LONG).show();
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = mAdvance.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        mAdvance.show();
    }

    public void startAdvcanceProcess() {
        int permission_counter = 0;
        if (!UtilityMethods.getInstance().isAccessibilityServiceRunning(ShortCutFolder.this)) {
            permission_counter++;
        }
        if (!UtilityMethods.getInstance().isHavingUnkownSourcePermission(ShortCutFolder.this)) {
            permission_counter++;
        }

        if (!UtilityMethods.getInstance().isAccessibilityServiceRunning(ShortCutFolder.this) || !UtilityMethods.getInstance().isHavingUnkownSourcePermission(ShortCutFolder.this)) {
            permissionDialog = new Dialog(ShortCutFolder.this, R.style.Dialog);
            permissionDialog.setContentView(R.layout.bothpermission);
            accessibility_permission_btn = (Button) permissionDialog.findViewById(R.id.accessibility_permission_btn);
            unknown_permission_btn = (Button) permissionDialog.findViewById(R.id.unknown_permission_btn);
            acc_tick = (ImageView) permissionDialog.findViewById(R.id.accessibility_permission_tick);
            unknown_tick = (ImageView) permissionDialog.findViewById(R.id.unknown_permission_tick);
            cancel_btn = (ImageView) permissionDialog.findViewById(R.id.cancel);
            cancel_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    permissionDialog.cancel();
                }
            });
            setDialogBox();
            accessibility_permission_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openAccessibilityHelper();
                }
            });

            unknown_permission_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    resultCodetracker = 44;
                    unknown_bottom_helper();
                }
            });

            if (permission_counter == 1) {
                if (!UtilityMethods.getInstance().isAccessibilityServiceRunning(ShortCutFolder.this)) {
                    openAccessibilityHelper();
                }
                if (!UtilityMethods.getInstance().isHavingUnkownSourcePermission(ShortCutFolder.this)) {
                    resultCodetracker = 44;
                    unknown_bottom_helper();
                }
            }

            permissionDialog.setCanceledOnTouchOutside(false);

            WindowManager.LayoutParams lp = permissionDialog.getWindow().getAttributes();
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            lp.gravity = Gravity.CENTER_VERTICAL;

            permissionDialog.getWindow().setAttributes(lp);
            permissionDialog.show();
        } else if (UtilityMethods.getInstance().isAccessibilityServiceRunning(ShortCutFolder.this) && UtilityMethods.getInstance().isHavingUnkownSourcePermission(ShortCutFolder.this)) {
            try {
                mAdvance.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            convertAll();

        }
    }

    private void convertAll() {

        IntentFilter accessibilityReceiverIntentFilter = new IntentFilter();

        accessibilityReceiverIntentFilter.addAction("com.times.convertToAdvance");   //Broadcast Listner
        LocalBroadcastManager.getInstance(this).registerReceiver(accessibilityReceiver,
                accessibilityReceiverIntentFilter);

        MOVE_TO_MENU = 1;
        Log.d("Converting", "Starting");

        Intent intent = new Intent(ShortCutFolder.this, AccessibilityCommunicator.class);
        intent.putStringArrayListExtra("compressList", toCompressAppList);
        intent.putStringArrayListExtra("compressName", toCompressAppName);
        intent.putExtra("state", "multiple_app");
        startService(intent);


    }

    public void setDialogBox() {
        if (UtilityMethods.getInstance().isAccessibilityServiceRunning(ShortCutFolder.this)) {
            acc_tick.setVisibility(View.VISIBLE);
            accessibility_permission_btn.setVisibility(View.GONE);
        } else {
            acc_tick.setVisibility(View.GONE);
            accessibility_permission_btn.setVisibility(View.VISIBLE);
        }
        if (UtilityMethods.getInstance().isHavingUnkownSourcePermission(ShortCutFolder.this)) {
            unknown_tick.setVisibility(View.VISIBLE);
            unknown_permission_btn.setVisibility(View.GONE);
        } else {
            unknown_tick.setVisibility(View.GONE);
            unknown_permission_btn.setVisibility(View.VISIBLE);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 55:

                if (null != accRunnable) {
                    Log.d("backdebug", "stopping acc runnable");
                    accRunnable.stop();
                }
                Log.d("backdebug", "inside finish 55 ");

                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {

                }
                //setup ui of dialog box
                setDialogBox();
                if (UtilityMethods.getInstance().isAccessibilityServiceRunning(ShortCutFolder.this) && UtilityMethods.getInstance().isHavingUnkownSourcePermission(ShortCutFolder.this)) {
                    permissionDialog.cancel();
                    // Toast.makeText(this, "Start Compression 1", Toast.LENGTH_SHORT).show();
                    //unCompressionPopup(myString,source);

                }
                break;
            case 33:
                unknownRunnable.stop();
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                setDialogBox();
                if (UtilityMethods.getInstance().isAccessibilityServiceRunning(ShortCutFolder.this) && UtilityMethods.getInstance().isHavingUnkownSourcePermission(ShortCutFolder.this)) {
                    permissionDialog.cancel();

                }
            case 44:
                Log.d("Converting", "Starting1");

                unknownRunnable.stop();
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                setDialogBox();
                if (UtilityMethods.getInstance().isAccessibilityServiceRunning(ShortCutFolder.this) && UtilityMethods.getInstance().isHavingUnkownSourcePermission(ShortCutFolder.this)) {
                    permissionDialog.cancel();
                    Log.d("Converting", "Starting2");

                    convertAll();

                }
                break;
            default:
                //Toast.makeText(this, "default", Toast.LENGTH_SHORT).show();

        }
    }

    public void openAccessibilityHelper() {
        try {
            windowManager.removeView(overlayLayout);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        //startActivity(intent);
        requestTurnOn = true;

        startActivityForResult(intent, 55);

        LayoutInflater accessibilityHelper = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        overlayLayout = accessibilityHelper.inflate(R.layout.accessibility_permission, null);
        WindowManager.LayoutParams params1;
        TextView span_text_view = (TextView) overlayLayout.findViewById(R.id.span_text_view);
        TextView permission_helper_text = (TextView) overlayLayout.findViewById(R.id.permission_helper_text);
        String first = "";
        String second = "\uD83C\uDF1F SpaceUp \uD83C\uDF1F";
        final String totalString = first + second;
        Spannable spanText = new SpannableString(totalString);
        //set color to white
        spanText.setSpan(new ForegroundColorSpan(getResources()
                .getColor(R.color.spaceup_black)), first.length(), first.length() + second.length(), 0);
        //set text to bradon medium
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon_medium.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spanText.setSpan(typefaceSpan, first.length(), first.length() + second.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        span_text_view.setText(spanText, TextView.BufferType.SPANNABLE);

        String f = "Give permission to ";
        String s = "save upto 1.2 GB";
        final String t = f + s;
        Spannable spannableText = new SpannableString(t);
        //set color to white
        spannableText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ShortCutFolder.this, R.color.text_color)), f.length(), f.length() + s.length(), 0);
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spannableText.setSpan(typefaceSpan, f.length(), f.length() + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        permission_helper_text.setText(spannableText, TextView.BufferType.SPANNABLE);

        ImageView cancel = (ImageView) overlayLayout.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    windowManager.removeView(overlayLayout);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        params1 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);
        //params1.gravity = Gravity.TOP;
        params1.gravity = Gravity.BOTTOM;
        params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;


        windowManager = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        try {
            windowManager.addView(overlayLayout, params1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        accRunnable = new AccRunnable(windowManager, overlayLayout, this);

        accCheckThread = new Thread(accRunnable, "acc");
        accCheckThread.start();
        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.PERMISSION_ACCESSIBILITY);

    }

    private void unknown_bottom_helper() {
        Intent unknownSource = new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS);
        startActivityForResult(unknownSource, resultCodetracker);
        LayoutInflater accessibilityHelper = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        oView1 = accessibilityHelper.inflate(R.layout.unknown_permission, null);
        WindowManager.LayoutParams params1;

        TextView permission_helper_text = (TextView) oView1.findViewById(R.id.permission_helper_text);
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon_medium.ttf"));
        CalligraphyTypefaceSpan calligraphyRegular = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon.ttf"));
        String f = "Give permission for ";
        String s = "upto 90% compression";
        final String t = f + s;
        Spannable spannableText = new SpannableString(t);
        //set color to white
        spannableText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ShortCutFolder.this, R.color.text_color)), f.length(), f.length() + s.length(), 0);
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spannableText.setSpan(calligraphyRegular, 0, f.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableText.setSpan(typefaceSpan, f.length(), f.length() + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        permission_helper_text.setText(spannableText, TextView.BufferType.SPANNABLE);


        ImageView cancel = (ImageView) oView1.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        params1 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);

        params1.gravity = Gravity.TOP;
        params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        wm1 = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        try{wm1.addView(oView1, params1);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        unknownRunnable = new UnknownRunnable();
        unknownThread = new Thread(unknownRunnable, "unknown");
        unknownThread.start();
        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.PERMISSION_UNKNOWN_SOURCE);
    }

    class UnknownRunnable implements Runnable {
        int count = 0;
        private volatile boolean exit = false;

        @Override
        public void run() {


            do {
                try {
                    Thread.sleep(1000);
                    //show dialog for 8 seconds only
                    if (count > 8) {
                        try {
                            windowManager.removeView(overlayLayout);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    count++;
                } catch (Exception e) {

                }
            }
            while (!UtilityMethods.getInstance().isHavingUnkownSourcePermission(ShortCutFolder.this) && !exit);


            if (UtilityMethods.getInstance().isHavingUnkownSourcePermission(ShortCutFolder.this)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //inform onActivityResult if called not to start scanning
                        //start_scanning = true;

                        try {
                            windowManager.removeView(overlayLayout);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //onActivityResult(55,0,null);
                        Log.d("Converting", "Starting finish " + resultCodetracker);

                        finishActivity(resultCodetracker);
                    }
                });
            }

        }

        public void stop() {
            Log.d("TAG", "stop: thread");
            exit = true;
        }


    }


}