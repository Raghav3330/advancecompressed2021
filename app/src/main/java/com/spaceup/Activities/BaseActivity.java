package com.spaceup.Activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by shashanktiwari on 06/02/17.
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
