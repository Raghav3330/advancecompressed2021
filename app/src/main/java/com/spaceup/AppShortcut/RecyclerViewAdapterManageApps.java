package com.spaceup.AppShortcut;

/**
 * Created by dhurv on 25-01-2017.
 */

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.App;
import com.spaceup.R;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.UncompressingActivity;

import java.util.List;

/**
 * Created by dhurv on 11-01-2017.
 */

public class RecyclerViewAdapterManageApps extends RecyclerView.Adapter<RecyclerViewHolders> {

    private static final String SHORTCUT_NAME = "Compressed Apps";
    Bundle mGAParams;
    int INVISIBLE = -1;
    private int manageAppFlag;
    private List<ItemObject> itemList;
    private Activity context;
    private int divider_mid;
    private DataInterface dtInterface;
    String source;
    private boolean mClickedDoNotClean = false;
    public final int TYPE_STASH_CARD = 10, TYPE_BLANK = 11;

    public RecyclerViewAdapterManageApps(Activity context, List<ItemObject> itemList, int divider_mid, DataInterface dtInterface, int manageAppFlag) {
        this.itemList = itemList;
        this.context = context;
        this.divider_mid = divider_mid;
        this.dtInterface = dtInterface;
        this.manageAppFlag = manageAppFlag;

    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (!(drawable instanceof BitmapDrawable)) {
        } else {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerViewHolders rcv;
        if (viewType == 1) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.divider_manage_apps, null);
            if (manageAppFlag == 0) {
                RelativeLayout divider = (RelativeLayout) layoutView.findViewById(R.id.background_divider);
                divider.setBackgroundColor(context.getResources().getColor(R.color.divider_shortcut));

                TextView convert_text = (TextView) layoutView.findViewById(R.id.convert_text);
                convert_text.setTextColor(context.getResources().getColor(R.color.app_text_color));
                TextView coverHeading = (TextView) layoutView.findViewById(R.id.textView20);
                coverHeading.setTextColor(context.getResources().getColor(R.color.app_text_color));

                LinearLayout divider1 = (LinearLayout) layoutView.findViewById(R.id.linearLayout2);
                divider1.setBackgroundColor(context.getResources().getColor(R.color.divider_shortcut));
            }
            rcv = new RecyclerViewHolders(layoutView);

            return rcv;
        } else if (viewType == 2) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.divider_mid_manage_apps, null);
            if (manageAppFlag == 0) {
                TextView coverHeading = (TextView) layoutView.findViewById(R.id.textView20);
                coverHeading.setTextColor(context.getResources().getColor(R.color.app_text_color));
                LinearLayout divider = (LinearLayout) layoutView.findViewById(R.id.linearLayout2);
                divider.setBackgroundColor(context.getResources().getColor(R.color.divider_shortcut));
            }
            rcv = new RecyclerViewHolders(layoutView);
            return rcv;
        } else if (viewType == 0) {
            if (manageAppFlag == 0) {
                Log.d("VIEW_HOLDER", "-1");
                View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.create_shortcut_card, null);
                rcv = new RecyclerViewHolders(layoutView);
                return rcv;
            } else {
                Log.d("VIEW_HOLDER", "-1");
                View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.create_shortcut_card_line, null);
                rcv = new RecyclerViewHolders(layoutView);
                return rcv;
            }

        } else if (viewType == INVISIBLE) {

            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.create_shortcut_card_line, null);
            rcv = new RecyclerViewHolders(layoutView);
            return rcv;
        } else if (viewType == TYPE_STASH_CARD) {
            View stashCard = LayoutInflater.from(parent.getContext()).inflate(R.layout.shortcut_install, null);
            TextView installStash = (TextView) stashCard.findViewById(R.id.select_button);
            installStash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Uri uri = Uri.parse(context.getString(R.string.market_app_url_stash));
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    try {
                        context.startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Log.d("AppUpgradeActivity", "play store redirection failed ");
                    }

                    new AnalyticsController(context).sendAnalytics(AnalyticsConstants.Category.STASH_REF,
                            AnalyticsConstants.Action.POPUP_MANAGEAPPS,
                            AnalyticsConstants.Label.CLICKED,
                            null,
                            false,
                            null);
                }
            });
            rcv = new RecyclerViewHolders(stashCard);
            return rcv;

        } else {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_item, null);
            if (manageAppFlag == 0) {
                TextView text = (TextView) layoutView.findViewById(R.id.country_name);
                text.setTextColor(context.getResources().getColor(R.color.app_text_color));

            }
            rcv = new RecyclerViewHolders(layoutView);
            return rcv;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, int position) {
        if (position == 1) {
            try {

                holder.convertAdvcane.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        mGAParams = new Bundle();
                        if (manageAppFlag == 0) {
                            //from manage apps icon

                        } else if (manageAppFlag == 1) {
                            //from laucher

                        }
                        //from outside laucher (shortcut)
                        else if (manageAppFlag == 2) {

                        }


//                        mGAParams = new Bundle();
//                        mGAParams.putString("category",AnalyticsConstants.Category.MANAGE_APPS);
//                        mGAParams.putString("action",AnalyticsConstants.Action.INAPP);
//                        mGAParams.putString("label",AnalyticsConstants.Label.CONVERT);
//                        new AnalyticsHandler().logGAEvent(mGAParams);
//                        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());


                        // send call back to ShortCut Activity using an Interface
                        dtInterface.setValues(true);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (position == 0) {
            // Shortcut view

        }
        if (position == itemList.size() - 1) {
            try {
                holder.shortcut_created.setVisibility(View.GONE);
                Log.d("SHORTCUT_CREATED new ", "" + isShortcutCreated());
                holder.create_shortcut_Button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        createShortcut();
                        holder.create_shortcut_Button.setVisibility(View.GONE);
                        holder.shortcut_created.setVisibility(View.VISIBLE);


                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        try {
           /* if( position < divider_mid)
                holder.relLayout.setBackgroundColor(context.getResources().getColor(R.color.shortcut_grey));
            else*/

            if (position < divider_mid) {
                holder.badge.setVisibility(View.INVISIBLE);
            } else if (position > divider_mid) {
                // Added by Akshit (bcz badges not coming in advanced comp. apps from all launchers)
                holder.badge.setVisibility(View.VISIBLE);
            }


            Bitmap appPic = itemList.get(position).getPhoto();
            appPic = UtilityMethods.getInstance().toGrayscale(appPic);
            holder.countryName.setText(itemList.get(position).getName());
            holder.countryPhoto.setImageBitmap(appPic);
            final int mposition = position;

            holder.relLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    source = itemList.get(mposition).getPackageName();

                    UtilityMethods.sendUnCompressSourceAnalytics(manageAppFlag, 0);
                    // Analytics end


                    if (mposition > divider_mid) {
                        Intent i = new Intent(context, UncompressingActivity.class);
                        PackageManager packageManager = context.getPackageManager();

                        if (UtilityMethods.getInstance().isPackageInstalled(source, packageManager)
                                && Constants.DUMMY_APP_VERSION_NAME.equalsIgnoreCase(UtilityMethods.getInstance().getCurrentAppVersionName(context, source))) {
                            i.putExtra("process_type", "new_dummy");
                            Log.d("ADAPTER_RECYCLER", "NEW");
                            i.putExtra("app_location", Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + source + ".stash");


                        } else {
                            Log.d("ADAPTER_RECYCLER", "OLD");
                            source = source + ".stash";
                            i.putExtra("app_location", Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + source);


                        }

                        i.putExtra("confirm_popup", false);
                        i.putExtra("source", source);
                        i.putExtra("type", "dummy");
                        Log.d("ADAPTER_RECYCLER", source);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        context.startActivity(i);
                    } else if (mposition < divider_mid) {
                        Intent i = new Intent(context, UncompressingActivity.class);
                        i.putExtra("app_location", Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + source + ".stash");
                        i.putExtra("source", source + ".stash");
                        i.putExtra("type", "shortcut");
                        i.putExtra("confirm_popup", false);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        Log.d("SHORTCUT TAG", "app_location " + Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + source + ".stash" + "  ");
                        Log.d("SHORTCUT TAG", "source " + source + ".stash");

                        context.startActivity(i);
                    }
                   /* Toast.makeText(context,"Package"+itemList.get(position).getPackageName(),Toast.LENGTH_SHORT).show();
                    Intent i = new Intent("com.stash.reactivatee_Activity");
                    i.putExtra("app_location",  Environment.getExternalStorageDirectory().getPath()+Constants.APK_DB + itemList.get(position).getPackageName() );
                    i.putExtra("source",itemList.get(position).getPackageName());
                    context.startActivity(i);*/
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 3; //Default is 1


        if (position == 1) {
            if (divider_mid == 2) {
                viewType = -1;          // Handling if no Shortcut Apps Hide
            } else {
                viewType = 1; // Divider Layout separating Shortcut and  Dummy apps

            }

        } else if (position == divider_mid) {
            if (divider_mid == itemList.size() - 1) {
                viewType = -1;          // Handling if no Shortcut Apps Hide
            } else {
                viewType = 2;
            }
        } else if (position == itemList.size() - 1) {
            viewType = 0;
        } else if (position == 0) {
            if (!mClickedDoNotClean && !UtilityMethods.isPackageExisted(context, "com.stash.junkcleaner")) {
                viewType = TYPE_STASH_CARD;
            } else {
                viewType = INVISIBLE;

            }
        }
        return viewType;
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    private boolean isShortcutCreated() {
        return PrefManager.getInstance(App.getInstance()).getBoolean(PrefManager.KEY_SHORTCUT_CREATED);
    }

    private void createShortcut() {
        if (!android.os.Build.MANUFACTURER.equalsIgnoreCase("xiaomi")) {
            UtilityMethods.getInstance().addShortcut(context);
            PrefManager.getInstance(App.getInstance()).putBoolean(PrefManager.KEY_SHORTCUT_CREATED, true);
        }
    }


}
