package com.spaceup.Utility;

import android.content.Context;
import android.util.Log;

import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;

import java.io.File;

/**
 * Created by dhurv on 25-03-2017.
 */

public class DeleteFromRecyclerBin {
    Context context;

    public DeleteFromRecyclerBin(Context context) {
        this.context = context;
    }

    public void deleFiles() {
        // Traverse folder WhatsAppDump and Delete the contents if Name of Folder is greater that 10 days
        String path = Constants.RECYCLE_BIN_FOLDER;
        Log.d("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
        Log.d("Files", "Size: " + files.length);

        if (files != null && files.length > 0) {

            for (int i = 0; i < files.length; i++) {
                // extract name of directory
                try {
                    long mDirName = Long.parseLong(files[i].getName());
                    if ((System.currentTimeMillis() - mDirName) > (10 * 24 * 60 * 60 * 1000)) {
                        deleteRecursive(files[i]);
                        Log.d("FilesJunk", "FileName  Deleting" + files[i].getName() + " Current Millis" + System.currentTimeMillis());

                    } else {
                        Log.d("FilesJunk", "FileName Not Deleting" + files[i].getName() + " Current Millis" + System.currentTimeMillis());

                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
        AppDBHelper.getInstance(context).destroyImage(AppDBHelper.getInstance(context).getRestorePath(fileOrDirectory.getPath()));
        Log.d("FilesJunkDB", "FileName DELETING DB STATUS " + AppDBHelper.getInstance(context).getRestorePath(fileOrDirectory.getPath()));


    }
}
