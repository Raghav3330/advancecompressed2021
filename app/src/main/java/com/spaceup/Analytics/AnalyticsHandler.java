package com.spaceup.Analytics;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.apsalar.sdk.Apsalar;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.spaceup.App;
import com.spaceup.BuildConfig;
import com.spaceup.R;
import com.spaceup.Utility.PrefManager;

import org.json.JSONObject;

import java.util.Calendar;

import static com.spaceup.Analytics.AnalyticsConstants.Params.CAMPAIGN_SOURCE;
import static com.spaceup.Utility.PrefManager.KEY_CAMPAIGN_SOURCE;

/**
 * Created by ankit.agrawal on 19/01/17.
 */
public class AnalyticsHandler {

    private static boolean sDebugAnalytics = BuildConfig.DEBUG; // Set true to turn off the Analytics
    private Tracker mTracker;

    public AnalyticsHandler() {

    }

    // Session is initialized in OnCreate of Splash_screen.java
    public void apsalarInitialization(Activity activity) {
        //API key til_dev
        //Secret BqbgZPUX
        if (!sDebugAnalytics) {
            Apsalar.setFBAppId("776534965849143");
            Apsalar.startSession(activity, "timesinternet", "NmecPMtU");
        } else {
            Apsalar.setFBAppId("776534965849143");
            Apsalar.startSession(activity, "til_dev", "BqbgZPUX");
        }
    }

    public void logApsalarEvent(String pEvent) {
        Apsalar.event(pEvent);
    }

    public void logEvent(String event, Bundle param) {


        if (!sDebugAnalytics && param != null) {
            if (!param.containsKey(CAMPAIGN_SOURCE)) {
                String campaignSource = PrefManager.getInstance(App.getInstance()).getString(KEY_CAMPAIGN_SOURCE);
                if (campaignSource != null) {
                    param.putString(CAMPAIGN_SOURCE, PrefManager.getInstance(App.getInstance()).getString(KEY_CAMPAIGN_SOURCE));
                }
            }
            FirebaseAnalytics.getInstance(App.getInstance()).logEvent(event, param);
        }
    }


    public void startTimeStamp() {

        Calendar ts = Calendar.getInstance();
        PrefManager.getInstance(App.getInstance()).setTimeStamp(ts.getTimeInMillis());
    }

    public long timePeriod() {
        Calendar ts = Calendar.getInstance();
        return ts.getTimeInMillis() - PrefManager.getInstance(App.getInstance()).getTimeStamp();
    }


    /**
     * Gets the default {@link Tracker} for this
     *
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(App.getInstance().getApplicationContext());
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
            mTracker.enableAutoActivityTracking(false);
            // Enable Display Features.
            mTracker.enableAdvertisingIdCollection(true);
        }
        return mTracker;


    }


    public void logGAEvent(Bundle pParams) {
        try {
            if (!sDebugAnalytics) {

                Log.d("Analytics", "logGAEvent: " + sDebugAnalytics);
                Tracker tracker = getDefaultTracker();
                HitBuilders.EventBuilder eventBuilder = new HitBuilders.EventBuilder();

                int custom_flag = 0;
                int custom_index = 0;
                int custom_metric_value = 0;
                String custom_value = "";

                for (String key : pParams.keySet()) {
                    if (key == "category")
                        eventBuilder.setCategory(pParams.getString("category"));
                    else if (key == "action")
                        eventBuilder.setAction(pParams.getString("action"));
                    else if (key == "label")
                        eventBuilder.setLabel(pParams.getString("label"));
                    else if (key == "value")
                        eventBuilder.setValue(pParams.getLong("value"));
                    else if (key == "cd") {
                        custom_index = pParams.getInt("cd");
                        custom_value = pParams.getString("cd_value");
                        custom_flag = 1;
//                    Log.d(AnalyticsConstants.GATAG, "index " + cd_index + " value " + cd_value);

                    } else if (key == "cm") {
                        custom_index = pParams.getInt("cm");
                        custom_metric_value = pParams.getInt("cm_value");
                        custom_flag = 2;
//                    Log.d(AnalyticsConstants.GATAG, "index " + cd_index + " value " + cd_value);

                    }
                }

                if (custom_flag == 1) {
                    tracker.send(eventBuilder.setCustomDimension(custom_index, custom_value).build());
                } else if (custom_flag == 2) {
                    tracker.send(eventBuilder.setCustomMetric(custom_index, custom_metric_value).build());
                } else {
                    tracker.send(eventBuilder.build());
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
//        Log.d(AnalyticsConstants.GATAG, "logGAEvent: category" + pParams.getString("category"));
//        Log.d(AnalyticsConstants.GATAG, "logGAEvent: action" + pParams.getString("action"));
//        Log.d(AnalyticsConstants.GATAG, "logGAEvent: label" + label);
//        Log.d(AnalyticsConstants.GATAG, "logGAEvent: value" + value);
    }


    public void logGAScreen(String screenName) {
        try {
            if (!sDebugAnalytics) {

                // Get tracker.
                Tracker t = getDefaultTracker();
                // Set screen name.
                t.setScreenName(screenName);
                // Send a screen view.
                t.send(new HitBuilders.ScreenViewBuilder().build());
            }
            Log.d("GAScreen", "logGAScreen: " + screenName);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        //Log.d(AnalyticsConstants.GATAG, "logGAEvent: action" + pParams.getString("action"));
        //Log.d(AnalyticsConstants.GATAG, "logGAEvent: label" + label);
        //Log.d(AnalyticsConstants.GATAG, "logGAEvent: value" + value);

    }

    public void logGAScreenCustomDim(String screenName, int cd_index, String cd_value) {
        try {
            if (!sDebugAnalytics) {

                // Get tracker.
                Tracker t = getDefaultTracker();
                // Set screen name.
                t.setScreenName(screenName);
                // Send a screen view.
                t.send(new HitBuilders.ScreenViewBuilder().setCustomDimension(cd_index, cd_value).build());
            }
            Log.d("GAScreen", "logGAScreen: " + screenName);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void logApsalarJsonEvent(String key ,JSONObject jsonValue){

        // Record the event with Apsalar
        Apsalar.eventJSON(key, jsonValue);


    }



}


// CODE TO PUT FOR ANALYTICS
/*
   Bundle paramBundle = new Bundle();
   paramBundle.putString("param1", "val1");
   paramBundle.putString("param2", "val2");
   paramBundle.putString("param3", "val3");
    new AnalyticsHandler(getApplicationContext()).logEvent("test", paramBundle);
*/


/* GA TEMPLATE
* mGAParams = new Bundle();
        mGAParams.putString("category",AnalyticsConstants.Category.COMPRESS);
        mGAParams.putString("action",AnalyticsConstants.Action.RESULT);
        mGAParams.putString("label",AnalyticsConstants.Label.APPS_COMPRESSED);
        mGAParams.putLong("value",Long.parseLong(intent.getStringExtra("no_of_apps_compressed")));
        new AnalyticsHandler().logGAEvent(mGAParams);
        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
*
* */

//new AnalyticsHandler().logGAScreen(mGAParams);