package com.spaceup.uninstall.activities;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import java.util.Comparator;

/**
 * Created by Sajal on 9/21/2016.
 */
public class AppInfo implements Parcelable {
    private String appName, pkgName;
    private long apkSize;
    // size is the data size
    private long size;
    private long codeSize;
    private long cacheSize;
    private int compressed_status;
    private int compressed_count;
    private int unCompressed_count;
    private int usageCount;
    private boolean checked = false;
    private long lastModifiedDate;
    private long index;
    private long totalTimeInForeground;
    //for those apps which are compress, non compressed
    private int specialFlag;
    //differentiators for those apps which are not listed in usage stats
    private int flag;
    private String category, location;

    public AppInfo() {
    }

    public AppInfo(String appName, String pkgName, int compressed_status, int size, boolean checked) {
        setAppName(appName);
        setSize(size);
        setCompressed_status(compressed_status);
        setChecked(checked);
        setPkgName(pkgName);
    }

    public static Comparator<AppInfo> Descending = new Comparator<AppInfo>() {
        @Override
        public int compare(AppInfo o1, AppInfo o2) {
            return (int) (o2.getApkSize() - o1.getApkSize());
        }
    };

    public int getCompressed_count() {
        return compressed_count;
    }

    public void setCompressed_count(int compressed_count) {
        this.compressed_count = compressed_count;
    }

    public int getUnCompressed_count() {
        return unCompressed_count;
    }

    public void setUnCompressed_count(int unCompressed_count) {
        this.unCompressed_count = unCompressed_count;
    }

    public int getUsageCount() {
        return usageCount;
    }

    public void setUsageCount(int usageCount) {
        this.usageCount = usageCount;
    }

    public int getSpecialFlag() {
        return specialFlag;
    }

    public void setSpecialFlag(int specialFlag) {
        this.specialFlag = specialFlag;
    }

    public long getApkSize() {
        return apkSize;
    }

    public void setApkSize(long apkSize) {
        this.apkSize = apkSize;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public long getCodeSize() {
        return codeSize;
    }

    public void setCodeSize(long codeSize) {
        this.codeSize = codeSize;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public long getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(long lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public long getCacheSize() {
        return cacheSize;
    }

    public void setCacheSize(long cacheSize) {
        this.cacheSize = cacheSize;
    }


    public long getTotalTimeInForeground() {
        return totalTimeInForeground;
    }

    public void setTotalTimeInForeground(long totalTimeInForeground) {
        this.totalTimeInForeground = totalTimeInForeground;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getPkgName() {
        return pkgName;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }


    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

  /*  public int getUsageCount() {
        return usageCount;
    }

    public void setUsageCount(int usageCount) {
        this.usageCount = usageCount;
    }*/

    public int getCompressed_status() {

        return compressed_status;
    }

    public void setCompressed_status(int compressed_status) {
        this.compressed_status = compressed_status;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }



    protected AppInfo(Parcel in) {
        appName=in.readString();
        pkgName=in.readString();
        category=in.readString();
        location=in.readString();
        apkSize = in.readLong();
        size = in.readLong();
        codeSize = in.readLong();
        cacheSize = in.readLong();
        compressed_status = in.readInt();
        compressed_count = in.readInt();
        unCompressed_count = in.readInt();
        usageCount = in.readInt();
        checked = in.readByte() != 0x00;
        lastModifiedDate = in.readLong();
        index = in.readLong();
        totalTimeInForeground = in.readLong();
        specialFlag = in.readInt();
        flag = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(appName);
        dest.writeString(pkgName);
        dest.writeString(category);
        dest.writeString(location);
        dest.writeLong(apkSize);
        dest.writeLong(size);
        dest.writeLong(codeSize);
        dest.writeLong(cacheSize);
        dest.writeInt(compressed_status);
        dest.writeInt(compressed_count);
        dest.writeInt(unCompressed_count);
        dest.writeInt(usageCount);
        dest.writeByte((byte) (checked ? 0x01 : 0x00));
        dest.writeLong(lastModifiedDate);
        dest.writeLong(index);
        dest.writeLong(totalTimeInForeground);
        dest.writeInt(specialFlag);
        dest.writeInt(flag);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AppInfo> CREATOR = new Parcelable.Creator<AppInfo>() {
        @Override
        public AppInfo createFromParcel(Parcel in) {
            return new AppInfo(in);
        }

        @Override
        public AppInfo[] newArray(int size) {
            return new AppInfo[size];
        }
    };
}