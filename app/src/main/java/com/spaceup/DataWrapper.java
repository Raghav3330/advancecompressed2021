package com.spaceup;

import com.spaceup.uninstall.activities.AppInfo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dhruv on 11/22/2016.
 */
public class DataWrapper implements Serializable {

    private List<AppInfo> parliaments;

    public DataWrapper(List<AppInfo> data) {
        this.parliaments = data;
    }

    public List<AppInfo> getParliaments() {
        return this.parliaments;
    }

}