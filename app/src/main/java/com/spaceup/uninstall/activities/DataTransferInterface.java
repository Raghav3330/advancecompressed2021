package com.spaceup.uninstall.activities;

/**
 * Created by  on 10-11-2016.
 */

public interface DataTransferInterface {
    void setValues(AppInfo updatedList, int action, int pos);

    void setUnknownTracker(boolean status);

}

