package com.spaceup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.spaceup.Activities.Scanning;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class YouJustLostAnApp_POPUP_gained extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_user_uninstalled_app_granted);

        TextView textView = (TextView) findViewById(R.id.textView11);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(YouJustLostAnApp_POPUP_gained.this, Scanning.class);
                startActivity(intent);
            }
        });
        ImageView image = (ImageView) findViewById(R.id.imageView31);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        TextView later = (TextView) findViewById(R.id.textView28);
        later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
