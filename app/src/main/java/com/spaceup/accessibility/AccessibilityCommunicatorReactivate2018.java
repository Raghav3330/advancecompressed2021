package com.spaceup.accessibility;

import static androidx.core.content.FileProvider.getUriForFile;

import android.app.ActivityManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

import androidx.core.content.FileProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.spaceup.R;
import com.spaceup.RemoteConfig;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;

import java.io.File;

/**
 * Created by Dhruv kaushal on 10/12/18.
 */

public class AccessibilityCommunicatorReactivate2018 extends Service {

    private static final long APP_SIZE_CONSTANT = 2;
    private static final long TIME_CONSTANT = 8;
    private String path;
    private String TAG = "CommunicatorReactivateFiltered";
    private long RAM_CONSTANT = 2;


    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final int MAX_RETRY_COUNT_OF_UNINSTALL_EVENT = 3;
    private final int MAX_RETRY_COUNT_OF_INSTALL_EVENT = 3;
    private int mCurrentInstallCount = 0;
    private int mCurrentUnInstallCount = 0;
    private long mTimeToWaitForUninstallEach = 8000; // 8 second wait to start again
    private float mTimeToWaitForInstallEach = 14000f; // 14 second wait to start again
    private String mPackageName;
    private boolean mIsUninstalled, mIsInstalled;

    private long totalRamMemorySize() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        assert activityManager != null;
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.totalMem / 1048576L;
        return availableMegs;
    }

    private void decideTimeOutTimeBasedOnRam(String path) {
        float appSize = (new File(path).length() / (1000 * 1000));
        float ramSize = (float) (Math.ceil(totalRamMemorySize() / 1000f));

        if (ramSize <= 0) {
            ramSize = 1;
        }
        mTimeToWaitForInstallEach = Math.max(RemoteConfig.getInt(R.string.uncompression_min_time), Math.min(15000, 1000 * ((appSize / APP_SIZE_CONSTANT * TIME_CONSTANT * (RAM_CONSTANT / ramSize)))));
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        Log.d(TAG, "onHANDLE: CALLED");
        Bundle b1 = intent.getExtras();
        assert b1 != null;
        path = b1.getString("path");
        mPackageName = b1.getString("source");
        final String type = b1.getString("type");     // Shortcut or
        if (AccessibilityAutomation.getSharedInstance() != null) {

            AccessibilityAutomation.getSharedInstance().fromActivity(true);
            AccessibilityAutomation.getSharedInstance().overlayDecompression(mPackageName);
            AccessibilityAutomation.getSharedInstance().allowReading(true);
        }

                Log.d(TAG, "onHANDLE: STARTING_TASK");

                Log.d("OnHANDLE: STARTING_TASK", mPackageName.toString());


                decideTimeOutTimeBasedOnRam(path);
                startTimerUninstall(mPackageName);
                throwUninstallEvent(mPackageName);

        //Listen for App Uninstalled Broadcast
        IntentFilter listenForUninstall = new IntentFilter(Constants.Receiver.APP_UNINSTALLED_BROADCAST);
        LocalBroadcastManager.getInstance(this).registerReceiver(mAppUninstalledReceiver, listenForUninstall);
        return START_NOT_STICKY;
    }


    private Runnable mRunnableUninstall;
    private Handler mTimerInstall = new Handler();

    private void startTimerUninstall(final String mPackageName) {
        Log.d(TAG, "startTimerUninstall: ");

        mRunnableUninstall = new Runnable() {
            @Override
            public void run() {
                if (!mIsUninstalled) {
                    throwUninstallEvent(mPackageName);
                    mTimerInstall.postDelayed(mRunnableUninstall, mTimeToWaitForUninstallEach);
                }
            }
        };

        mTimerInstall.postDelayed(mRunnableUninstall, mTimeToWaitForUninstallEach);

    }

    private void throwUninstallEvent(String pPackageName) {
        Log.d(TAG, "throwUninstallEvent : ");
        if (mCurrentUnInstallCount > MAX_RETRY_COUNT_OF_UNINSTALL_EVENT) {
            mTimerInstall.removeCallbacks(mRunnableUninstall);
            FirebaseCrashlytics.getInstance().log(Constants.CUSTOM_CRASH.UNINSTALL_RETRY_OCCUR + " " + mPackageName);
            Log.d(TAG, "throwUninstallEvent : TIMEOUT");

            stopSelf();
            return;
        }
        mCurrentUnInstallCount++;
        //try uninstall the app and throw analytics with package
        if (UtilityMethods.getInstance().isPackageInstalled(pPackageName, getApplicationContext().getPackageManager())) {
            Uri packageUri = Uri.parse("package:" + pPackageName);
            Log.d("throwUninstallEvent: ",pPackageName);
            Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageUri);
            uninstallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            uninstallIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(uninstallIntent);
            Log.d(TAG, "throwUninstallEvent: THROWN EVENT");

        } else {
            //FAIL CONDITION
            //means app have been uninstall already throw broadcast  Constants.Receiver.APP_UNINSTALLED_BROADCAST
            Log.d(TAG, "throwUninstallEvent: THROWN BUT APP NOT FOUND");
            mIsUninstalled = true;
            mTimerInstall.removeCallbacks(mRunnableUninstall);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    Intent i1 = new Intent(Constants.Receiver.APP_UNINSTALLED_BROADCAST);
                    i1.putExtra(Constants.PACKAGE_NAME, mPackageName);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i1);
                }
            }, 1000);
        }
    }

    // GET UNINSTALL EVENT BELOW AND START FOR INSTALLATION
    BroadcastReceiver mAppUninstalledReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() == null) {
                return;

            }
            if (mPackageName.equals(intent.getExtras().getString(Constants.PACKAGE_NAME))) {
                Log.d(TAG, "throwUninstallEvent:mAppUninstalledReceiver");
                mIsUninstalled = true;
                //Listen for App Uninstalled Broadcast
                if (!mIsInstalled) {
                    IntentFilter listenForUninstall = new IntentFilter(Constants.Receiver.APP_INSTALLED_BROADCAST);
                    LocalBroadcastManager.getInstance(context).registerReceiver(mAppInstalledReceiver, listenForUninstall);
                    throwInstallEvent(mPackageName);
                    startTimerInstall(mPackageName);
                } else {
                    stopSelf();
                }
            }

        }
    };

    private Runnable mRunnableInstall;
    private Handler mHandlerInstall = new Handler();


    private void startTimerInstall(final String mPackageName) {
        mRunnableInstall = new Runnable() {
            @Override
            public void run() {
                if (!mIsInstalled) {
                    throwInstallEvent(mPackageName);
                    mHandlerInstall.postDelayed(mRunnableInstall, Math.round(mTimeToWaitForInstallEach));
                }
            }
        };
        mHandlerInstall.postDelayed(mRunnableInstall, Math.round(mTimeToWaitForInstallEach));


    }

    private void throwInstallEvent(String pPackageName) {
        Log.d(TAG, "throwInstall Event: " + mCurrentInstallCount);
        if (mCurrentInstallCount > MAX_RETRY_COUNT_OF_INSTALL_EVENT) {
            FirebaseCrashlytics.getInstance().log(Constants.CUSTOM_CRASH.INSTALL_RETRY_OCCUR + " " + mPackageName);
            Log.d(TAG, "throwInstall Event: TIMEOUT");

            stopSelf();
            return;
        }
        mCurrentInstallCount++;
        if (!UtilityMethods.getInstance().isPackageInstalled(pPackageName, getApplicationContext().getPackageManager())) {
            //try Install the app
            File f = new File(path);
            if (f.exists()) {
                Log.d("Location -- RK ", "path " + path);
//                    final Uri path1 = Uri.fromFile(f);
                  final Uri path1 = getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".fileprovider",f);
                Log.d("Install Decomp - RK ",path1.toString());


                UtilityMethods.getInstance().throwInstallPopup(path1, getApplicationContext());

                // Copied and pasted code from THROWINSTALLPOPUP in UTILITY METHODS

//                String packageName = null;
//                if (UtilityMethods.isPackageExisted(getApplicationContext(), "com.google.android.packageinstaller")) {
//                    packageName = "com.google.android.packageinstaller";
//                }
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                if (packageName != null) {
//                    i.setPackage("com.google.android.packageinstaller");
//                }
//                i.setDataAndType(path1, "application/vnd.android.package-archive");
//                Log.d("checking by RK : ",path1.toString());
//                i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                i.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//                try {
//                    startActivity(i);
//                } catch (Exception y) {
//                    y.printStackTrace();
//                    Log.d("Checking log by RK : ",y.getMessage().toString());
//
//                }

            } else {
                //Analytics File DELETED
                //This case should not occur
                Log.d(TAG, "throwInstall Event: INSTALL PACKAGE NOT FOUND!");

                FirebaseCrashlytics.getInstance().log(Constants.CUSTOM_CRASH.INSTALL_FILE_NOT_FOUND + " " + mPackageName);
                stopSelf();
            }


        } else {

            //Package Already Installed
            //Cancel Timer and Send Installed Broadcast
            //Show App Boosted Screen
            Log.d(TAG, "throwInstall Event -- RK : ALREADY INSTALLED");
            mHandlerInstall.removeCallbacks(mRunnableInstall);
            stopSelf();
            mIsInstalled = true;

        }

    }

    BroadcastReceiver mAppInstalledReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mPackageName.equals(intent.getExtras().getString(Constants.PACKAGE_NAME))) {
                Log.d(TAG, "onReceive: APP INSTALLED " + mPackageName);
                mIsInstalled = true;
                stopSelf();
            }
        }
    };

    public void onDestroy() {
        Log.d(TAG, "onDestroy: CALLED");
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mAppInstalledReceiver);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mAppUninstalledReceiver);
        if (AccessibilityAutomation.getSharedInstance() != null) {
            AccessibilityAutomation.getSharedInstance().fromActivity(false);
            AccessibilityAutomation.getSharedInstance().removeDecompressionOverLay();

        }
        mTimerInstall.removeCallbacks(mRunnableUninstall);
        mHandlerInstall.removeCallbacks(mRunnableInstall);
        super.onDestroy();

    }

}
