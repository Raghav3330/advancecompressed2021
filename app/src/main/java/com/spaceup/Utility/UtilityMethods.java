package com.spaceup.Utility;

import android.app.Activity;
import android.app.AppOpsManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.os.Vibrator;
import android.provider.Settings;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.Analytics.ImagesAnalyticsConstants;
import com.spaceup.AppShortcut.ShortCutFolder_Launcher;
import com.spaceup.R;
import com.spaceup.RemoteConfig;
import com.spaceup.accessibility.AccessibilityAutomation;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static android.content.Context.CONNECTIVITY_SERVICE;
import static androidx.core.content.FileProvider.getUriForFile;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Sajal Jain on 22-12-2016.
 * <p>
 * This class can be improvised to singleton
 */

public class UtilityMethods {

    public static final int HOME_SCREEN = 0;
    public static final int SCANNING_SCREEN = 1;
    public static final int REVIEW_SCREEN = 2;
    public static final int COMPRESSING_SCREEN = 3;
    public static final int RESULT_SCREEN = 4;
    public static final int GET_STARTED = 5;
    private static int sRingState = 0;
    private static UtilityMethods methods = null;

    private static boolean sIsAppUpdateGoingOn = false;

    private UtilityMethods() {

    }

    public static UtilityMethods getInstance() {
        if (methods == null) {
            methods = new UtilityMethods();
        }
        return methods;
    }

    public static boolean isPackageExisted(Context context, String targetPackage) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(targetPackage, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    public static boolean isAccessibilityServiceEnabled(Context context, Class<?> accessibilityService) {
        ComponentName expectedComponentName = new ComponentName(context, accessibilityService);

        String enabledServicesSetting = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        if (enabledServicesSetting == null)
            return false;

        TextUtils.SimpleStringSplitter colonSplitter = new TextUtils.SimpleStringSplitter(':');
        colonSplitter.setString(enabledServicesSetting);

        while (colonSplitter.hasNext()) {
            String componentNameString = colonSplitter.next();
            ComponentName enabledService = ComponentName.unflattenFromString(componentNameString);

            if (enabledService != null && enabledService.equals(expectedComponentName))
                return true;
        }

        return false;
    }

    public static String getSizeinMBOnly(long prev_size) {
        int unit = 1000;
        if (prev_size > unit) {
            prev_size = prev_size / unit;
            if (prev_size > unit) {
                prev_size = prev_size / unit;

                return prev_size + "MB";
            }
            return prev_size + "KB";

        }
        return prev_size + "B";
    }

    public static String getSizeinMBZeroDecimal(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "");
        return String.format("%.0f %sB", bytes / Math.pow(unit, exp), pre);
    }

    public static String getSizeinMBZeroDecimalChar(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "");
        return String.format("%.0f", bytes / Math.pow(unit, exp), pre);
    }

    public static boolean isAppUpdateGoingOn() {
        return sIsAppUpdateGoingOn;
    }

    public static void setAppUpdateGoingOn(boolean pAppUpdateGoingOn) {
        sIsAppUpdateGoingOn = pAppUpdateGoingOn;
    }

    public static boolean isEmptyMap(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    public static boolean isEmptyCollection(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNotEmptyCollection(Collection<?> collection) {
        return collection != null && (!collection.isEmpty());
    }

    public static String getCurrentAppVersionName(Context context, String packageName) {
        String versionName = null;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

    public static boolean checkInternet() {
        try {
            InetAddress ipAddr = InetAddress.getByName("www.google.com");
            return !ipAddr.equals("");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getNetworkType(Context context) {
        TelephonyManager mTelephonyManager = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = mTelephonyManager.getNetworkType();
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return ImagesAnalyticsConstants.CustomDimVal.TWO_G;
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                /**
                 From this link https://goo.gl/R2HOjR ..NETWORK_TYPE_EVDO_0 & NETWORK_TYPE_EVDO_A
                 EV-DO is an evolution of the CDMA2000 (IS-2000) standard that supports high data rates.

                 Where CDMA2000 https://goo.gl/1y10WI .CDMA2000 is a family of 3G[1] mobile technology standards for sending voice,
                 data, and signaling data between mobile phones and cell sites.
                 */
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                //Log.d("Type", "3g");
                //For 3g HSDPA , HSPAP(HSPA+) are main  networktype which are under 3g Network
                //But from other constants also it will 3g like HSPA,HSDPA etc which are in 3g case.
                //Some cases are added after  testing(real) in device with 3g enable data
                //and speed also matters to decide 3g network type
                //http://goo.gl/bhtVT
                return ImagesAnalyticsConstants.CustomDimVal.THREE_G;
            case TelephonyManager.NETWORK_TYPE_LTE:
                //No specification for the 4g but from wiki
                //I found(LTE (Long-Term Evolution, commonly marketed as 4G LTE))
                //https://goo.gl/9t7yrR
                return ImagesAnalyticsConstants.CustomDimVal.FOUR_G;
            default:
                return ImagesAnalyticsConstants.CustomDimVal.WIFI;
        }
    }

    public static boolean isInternetAvailable() {
        if (checkInternet()) {

            Bundle mGAParams;
            ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(CONNECTIVITY_SERVICE);
            boolean isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
            if (isWifi) {
                String networkType = getNetworkType(getApplicationContext());
                if (!PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.HAS_SENT_INTERNET_ANALYTICS)) {
                    mGAParams = new Bundle();
                    mGAParams.putString("category", ImagesAnalyticsConstants.Category.INTERNET_STATUS);
                    mGAParams.putString("action", ImagesAnalyticsConstants.CustomDimVal.WIFI);
                    mGAParams.putInt("cd", ImagesAnalyticsConstants.CustomDim.INTERNET_TYPE);
                    mGAParams.putString("cd_value", ImagesAnalyticsConstants.CustomDimVal.WIFI);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.HAS_SENT_INTERNET_ANALYTICS, true);
                }
            } else {
                String networkType = getNetworkType(getApplicationContext());
                if (!PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.HAS_SENT_INTERNET_ANALYTICS)) {
                    mGAParams = new Bundle();
                    mGAParams.putString("category", ImagesAnalyticsConstants.Category.INTERNET_STATUS);
                    mGAParams.putString("action", networkType);
                    mGAParams.putInt("cd", ImagesAnalyticsConstants.CustomDim.INTERNET_TYPE);
                    mGAParams.putString("cd_value", networkType);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.HAS_SENT_INTERNET_ANALYTICS, true);
                }
            }


            return true;
        } else {
            return false;
        }
    }

    public static boolean isNotNullOrEmpty(String s) {
        return s != null && !s.isEmpty();
    }

    public boolean isPackageExisted(String targetPackage, Context context) {
        List<ApplicationInfo> packages;
        PackageManager pm;

        pm = context.getPackageManager();
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            if (packageInfo.packageName.equals(targetPackage))
                return true;
        }
        return false;
    }

    public void handleSound(boolean off, Context context) {
        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        if (off) {   //turn off ringing/sound
            Log.d("UtilityMethods", "Audio Off" + String.valueOf(sRingState));
            //get the current ringer mode
            sRingState = audio.getRingerMode();

            Log.d("UtilityMethods", String.valueOf(sRingState));
            if (sRingState != AudioManager.RINGER_MODE_SILENT) {
//                audio.setRingerMode(AudioManager.RINGER_MODE_SILENT);//turn off
            }
            disableVibration(context);
        } else {
            Log.d("UtilityMethods", "Audio ON" + String.valueOf(sRingState));
            //restore previous state
//            audio.setRingerMode(sRingState);
            enableVibration(context);
        }
    }

    private void disableVibration(Context pContext) {
        // Start with 1000ms delay -> Vibrate for 0ms
        Log.d("UtilityMethods", "Vib start");
        long pattern[] = {1000, 0};
        Vibrator vib = (Vibrator) pContext.getSystemService(Context.VIBRATOR_SERVICE);
        // Repeat from pos. 0
        vib.vibrate(pattern, 0);
    }

    private void enableVibration(Context pContext) {
        Log.d("UtilityMethods", "Vib stop");
        Vibrator vib = (Vibrator) pContext.getSystemService(Context.VIBRATOR_SERVICE);
        try {
            vib.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    /*
     * Usage Stats Permission
     *
     */
    public String getSize_charecter(long prev_size) {
        if (prev_size > 1024) {
            prev_size = prev_size / 1024;
            if (prev_size > 1024) {
                prev_size = prev_size / 1024;
                if (prev_size > 1024) {
                    prev_size = prev_size / 1024;
                    return "GB";
                }
                return "MB";
            }
            return "KB";

        }
        return "B";
    }

    public String getSize_numberResult(long bytes, boolean si) {

        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + "";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "");
        return String.format("%.1f", bytes / Math.pow(unit, exp));

    }
    public String getSize_number(long bytes, boolean si) {

        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + "";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "");
        return String.format("%.0f", bytes / Math.pow(unit, exp));

    }

    public Bitmap toGrayscale(Bitmap bmpOriginal) {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    public boolean isHavingUsageStatsPermission(Context context) {
        if (Build.VERSION.SDK_INT >= 21) {
            if (Build.MANUFACTURER != null && ("HUAWEI".equals(Build.MANUFACTURER) || Build.MANUFACTURER.contains("LG"))) {
                return true;
            }
            try {
                PackageManager packageManager = context.getPackageManager();
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 0);
                AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
                int mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, applicationInfo.uid, applicationInfo.packageName);
                return (mode == AppOpsManager.MODE_ALLOWED);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                //double check usage stat's permission by query the usage stats manager
                UsageStatsManager mUsageStatsManager;
                Calendar beginCal;
                beginCal = Calendar.getInstance();
                beginCal.add(Calendar.MONTH, -1);
                mUsageStatsManager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
                List<UsageStats> queryUsageStats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_WEEKLY, beginCal.getTimeInMillis(), System.currentTimeMillis());
                return 0 != queryUsageStats.size();
            } catch (Exception e) {
                e.printStackTrace();
                return true;
            }
        }

        return false;
    }

    /**
     * Unknown Sources Permission
     */
    public boolean isHavingUnkownSourcePermission(Context context) {
        boolean isNonPlayStoreAppAllowed;
        try {

            isNonPlayStoreAppAllowed = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.INSTALL_NON_MARKET_APPS) == 1;

        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            isNonPlayStoreAppAllowed = false;
        }
        return isNonPlayStoreAppAllowed;
    }

    /**
     * Accessibility Service Running
     */
    public boolean isAccessibilityServiceRunning(Activity activity) {

        Context context = activity.getApplicationContext();
        ComponentName expectedComponentName = new ComponentName(context, AccessibilityAutomation.class);
        String enabledServicesSetting = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        if (enabledServicesSetting == null)
            return false;
        TextUtils.SimpleStringSplitter colonSplitter = new TextUtils.SimpleStringSplitter(':');
        colonSplitter.setString(enabledServicesSetting);
        while (colonSplitter.hasNext()) {
            String componentNameString = colonSplitter.next();
            ComponentName enabledService = ComponentName.unflattenFromString(componentNameString);

            if (enabledService != null && enabledService.equals(expectedComponentName)) {
                Log.d("AccessibilityService", "true");
                return true;
            }

        }
        return false;
    }

    public boolean isAccessibilityServiceRunning1(Context context) {


        ComponentName expectedComponentName = new ComponentName(context, AccessibilityAutomation.class);
        String enabledServicesSetting = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        if (enabledServicesSetting == null)
            return false;
        TextUtils.SimpleStringSplitter colonSplitter = new TextUtils.SimpleStringSplitter(':');
        colonSplitter.setString(enabledServicesSetting);
        while (colonSplitter.hasNext()) {
            String componentNameString = colonSplitter.next();
            ComponentName enabledService = ComponentName.unflattenFromString(componentNameString);

            if (enabledService != null && enabledService.equals(expectedComponentName))
                return true;
        }
        return false;
    }

    public String getSizeinMB(long bytes) {
        int unit = 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = ("KMGTPE").charAt(exp - 1) + "";

        if (pre.equals("M"))
            return String.format("%.0f %sB", bytes / Math.pow(unit, exp), pre);

        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    public String getSizeinMBNew(long bytes) {
        int unit = 1024;
        if (bytes < unit) return bytes + "";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = ("KMGTPE").charAt(exp - 1) + "";

        if (pre.equals("M"))
            return String.format("%.0f%s", bytes / Math.pow(unit, exp), "");

        return String.format("%.1f%s", bytes / Math.pow(unit, exp), "");
    }

    public void addShortcut(Context context) {
        //Adding shortcut for MainActivity
        //on Home screen
        if (!android.os.Build.MANUFACTURER.equalsIgnoreCase("xiaomi")) {
            Intent shortcutIntent = new Intent(context,
                    ShortCutFolder_Launcher.class);

            shortcutIntent.setAction(Intent.ACTION_MAIN);

            Intent addIntent = new Intent();
            addIntent
                    .putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "Compressed Apps");
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                    Intent.ShortcutIconResource.fromContext(context,
                            R.drawable.folder_icon));
            addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            addIntent.putExtra("duplicate", false);

            context.sendBroadcast(addIntent);
        }

    }

    public String getApplicationName(Context context, String pkgname) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(pkgname, 0);
            String applicationName = (String) (applicationInfo != null ? context.getPackageManager().getApplicationLabel(applicationInfo) : "App");
            return applicationName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "App";
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    /**
     * This method calculates the total internal memory of device, this size is excluding OS in long
     *
     * @return a long value to represent total internal memory of device
     */
    public long getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return (totalBlocks * blockSize);
    }

    /**
     * This method calculates the Avaiable Memory
     *
     * @return a long value to represent Avaiable Memory
     */
    public double getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return (double) (availableBlocks * blockSize)/* / (double) (1073741824)*/;
    }

    public void addToJSONFile(String pPkgName, long pSizeToAdd, long pLastDateModified, int pCompressStatus) {
        int count;
        long size = 0;
        int new_count = 0;
        File dbfile = new File(Constants.RESIDUAL_FILE);

        if (dbfile.exists()) {
            Log.d("file", "file already exists");
            //reading file

            JSONParser parser = new JSONParser();
            JSONArray apps = null;
            JSONObject jsonObject;
            try {
                Log.d("file", "inside try catch");
                Object obj = parser.parse(new FileReader(Constants.RESIDUAL_FILE));
                Log.d("file", "path object is initialized");
                jsonObject = (JSONObject) obj;
                count = (int) ((long) jsonObject.get("count"));
                size = (long) jsonObject.get("size");
                Log.d("file ", "old count " + count);
                Log.d("file", "old size" + size);

                //making changes
                new_count = count + 1;

                //reading json array of apps so that we can append new app
                apps = (JSONArray) jsonObject.get("apps");
                if (null == apps) {
                    apps = new JSONArray();
                }
                JSONObject app = new JSONObject();
                app.put(Constants.PKGNAME, pPkgName);
                app.put(Constants.SIZE, pSizeToAdd);
                app.put(Constants.STATUS, pCompressStatus);
                app.put(Constants.LAST_MODIFIED_DATE, pLastDateModified);
                apps.add(app);

                pSizeToAdd = size + pSizeToAdd;

                //obj.put("apps",applist);

                JSONArray msg = (JSONArray) jsonObject.get("apps");
                Iterator<JSONObject> iterator = msg.iterator();
                while (iterator.hasNext()) {
                    JSONObject ap = iterator.next();
                    //System.out.println(iterator.next());
                    Log.d("file", "package name " + ap.get("pkgname") + " size is" + ap.get("size"));
                }
                Log.d("file ", "new count " + new_count);
                Log.d("file", "new size" + pSizeToAdd);
                //add count
                // add size
            } catch (Exception e) {
                Log.d("file", Log.getStackTraceString(e));
                e.printStackTrace();
            }
            //write
            JSONObject obj = new JSONObject();
            obj.put("count", new_count);
            obj.put("size", pSizeToAdd);
            obj.put(Constants.JSON_VERSION, 1);

            try {
                obj.put("apps", apps);
                FileWriter fw = new FileWriter(Constants.RESIDUAL_FILE);
                fw.write(obj.toJSONString());
                fw.flush();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("file", Log.getStackTraceString(e));
            }
        } else {

            Log.d("file", "new file made");

            Log.d("file ", "new count " + 1);
            Log.d("file", "new size" + pSizeToAdd);

            //for first time
            JSONObject obj = new JSONObject();
            obj.put("count", 1);
            obj.put("size", pSizeToAdd);
            obj.put(Constants.JSON_VERSION, 1);
            //list of apps : with pkgname and size
            JSONArray applist = new JSONArray();

            JSONObject app = new JSONObject();
            app.put(Constants.PKGNAME, pPkgName);
            app.put(Constants.SIZE, pSizeToAdd);
            app.put(Constants.STATUS, pCompressStatus);
            app.put(Constants.LAST_MODIFIED_DATE, pLastDateModified);
            applist.add(app);

            obj.put("apps", applist);
            try {
                FileWriter fw = new FileWriter(Constants.RESIDUAL_FILE);
                fw.write(obj.toJSONString());
                fw.flush();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("file", Log.getStackTraceString(e));
            }
        }
    }

    public void removeFromJSONFile(String originalPkgName) {

        Log.d("REMOVE_FROM_JSON", "trying to removeFromJSONFile: " + originalPkgName);

        File file = new File(Constants.RESIDUAL_FILE);
        Log.d("file", "Original package name " + originalPkgName);
        Log.d("file", "start file operation");
        JSONObject jsonObject;
        JSONArray apps = null;
        int rm_app_index = -1;

        long total_size = 0;
        if (file.exists()) {
            //reading file and update
            JSONParser parser = new JSONParser();
            try {
                Object obj = parser.parse(new FileReader(Constants.RESIDUAL_FILE));
                jsonObject = (JSONObject) obj;

                apps = (JSONArray) jsonObject.get("apps");

                Iterator<JSONObject> iterator = apps.iterator();
                int index = 0;

                while (iterator.hasNext()) {
                    JSONObject ap = iterator.next();
                    //System.out.println(iterator.next());
                    if (ap.get("pkgname").equals(originalPkgName)) {
                        rm_app_index = index;
                        //putting this break so that we remove only the previous entered package name
                        break;
                    } else
                        total_size = total_size + (Long) ap.get("size");
                    index++;
                }

            } catch (Exception e) {
                Log.d("file", Log.getStackTraceString(e));
                e.printStackTrace();
            }

            //write
            if (rm_app_index != -1) {
                apps.remove(rm_app_index);
                JSONObject obj = new JSONObject();
                obj.put("count", apps.size());
                obj.put("size", total_size);
                obj.put(Constants.JSON_VERSION, 1);
                try {
                    obj.put("apps", apps);
                    FileWriter fw = new FileWriter(Constants.RESIDUAL_FILE);
                    fw.write(obj.toJSONString());
                    fw.flush();
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("file", Log.getStackTraceString(e));
                }
            }
        }
    }

    public void startNextCompression(Context context) {
        Intent uninstallIntent = new Intent("com.times.accessibilityReceiver");
        uninstallIntent.putExtra("compress_single_app", true);
        LocalBroadcastManager.getInstance(context).sendBroadcast(uninstallIntent);

        Intent i = new Intent("com.times.doneApkGenerator");
        i.putExtra("status", "UnInstalled");
        LocalBroadcastManager.getInstance(context).sendBroadcast(i);
    }

    public boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("DbUpdate", Log.getStackTraceString(e));
            return false;
        }
    }

    public boolean isPackageInstallerBusy(Context context) {
        if (Build.VERSION.SDK_INT >= 21 && RemoteConfig.getBoolean(R.string.consider_installer_busy)) {
            PackageInstaller packageInstaller = context.getPackageManager().getPackageInstaller();
            List<PackageInstaller.SessionInfo> list = packageInstaller.getAllSessions();

            return list.size() != 0;

            /*for (PackageInstaller.SessionInfo info : list) {
                Log.d("PackageInstaller", info.getAppPackageName());
            }*/
        } else
            return false;
    }

    public void throwInstallEvent(Context context, String packageName) {


        String dummyApkPath = AppDBHelper.getInstance(context).getDummyPath(packageName);
        File f = null;

        if (dummyApkPath != null && !"".equalsIgnoreCase(dummyApkPath))
            f = new File(dummyApkPath);

        if (f != null && f.exists()) {
//            Uri path_uri = Uri.fromFile(f);
            Uri path_uri = getUriForFile(getApplicationContext(),getApplicationContext().getPackageName() + ".fileprovider",f);

//            UtilityMethods.getInstance().throwInstallPopup(path_uri, context);

            try {
                Log.d("Compression", "Install intent thrown for " + packageName + " path: " + dummyApkPath);
                AccessibilityAutomation.getSharedInstance().getActionFailHandler().setInstall_handle(true, dummyApkPath, false);
                AccessibilityAutomation.getSharedInstance().getActionFailHandler().refreshInstallCounter();

            } catch (Exception y) {
                y.printStackTrace();
            }
        } else {
            Log.d("Compression", "File do not exists, starting next compression");
            UtilityMethods.getInstance().startNextCompression(context);

        }


    }

    public static void oneAppCompleteSound() {
        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.one_step);
        mp.start();

    }

    public static void playFinishSound() {

        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.process_complete);
        mp.start();
    }

    public void throwInstallPopup(Uri path1, Context context) {
        String packageName = null;
        if (isPackageExisted(context, "com.google.android.packageinstaller")) {
            packageName = "com.google.android.packageinstaller";
        }
        Intent i = new Intent(Intent.ACTION_VIEW);
        if (packageName != null) {
            i.setPackage("com.google.android.packageinstaller");
        }
        i.setDataAndType(path1, "application/vnd.android.package-archive");
        Log.d("checking by RK : ",path1.toString());
        i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        i.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            context.startActivity(i);
        } catch (Exception y) {
            y.printStackTrace();
            Log.d("Checking by RK : ",y.getMessage().toString());

        }
    }

    static Bundle mGAParams;

    public static void sendUnCompressSourceAnalytics(int manageAppFlag, int dummyFlag) {
        int inapp = 0, menu_launcher = 0, dummy = 0, shortcut = 0;
        // Analytics Start
        if (manageAppFlag == 0) {
            inapp = 1;

        } else if (manageAppFlag == 1) {
            menu_launcher = 1;

        } else if (manageAppFlag == 2) {
            shortcut = 1;

        } else if (dummyFlag == 1) {
            dummy = 1;
        }
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.UNCOMPRESS,
                AnalyticsConstants.Action.SRC,
                AnalyticsConstants.Label.INAPP,
                String.valueOf(inapp),
                false,
                null);

        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.UNCOMPRESS,
                AnalyticsConstants.Action.SRC,
                AnalyticsConstants.Label.SHORTCUT,
                String.valueOf(shortcut),
                false,
                null);
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.UNCOMPRESS,
                AnalyticsConstants.Action.SRC,
                AnalyticsConstants.Label.MENU_LAUNCHER,
                String.valueOf(menu_launcher),
                false,
                null);
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.UNCOMPRESS,
                AnalyticsConstants.Action.SRC,
                AnalyticsConstants.Label.DUMMY,
                String.valueOf(dummy),
                false,
                null);

    }

    public String getActionRefferalResult() {
        String experimentVal = RemoteConfig.getString(R.string.stash_insall_test);
        if( experimentVal.equals("test1")){
            return AnalyticsConstants.Action.POPUP_RESULT_SCREEN_OLD;
        }
        else{
            return AnalyticsConstants.Action.POPUP_RESULT_SCREEN_NEW;
        }


    }
    public String getActionRefferalPopup() {
        String experimentVal = RemoteConfig.getString(R.string.stash_insall_test);
        if( experimentVal.equals("test1")){
            return AnalyticsConstants.Action.POPUP_WHATSAPP_OLD;
        }
        else{
            return AnalyticsConstants.Action.POPUP_WHATSAPP_NEW;
        }


    }

    public boolean isNewUser(Context context) {

        return PrefManager.getInstance(context).getBoolean(PrefManager.IS_NEW_USER);
    }
    public String getAdmobID(int source){

        String advertsId = RemoteConfig.getString(R.string.advert_ids);
        String[] output = advertsId.split(",");

        try {
            switch (source){
                case HOME_SCREEN:
                    Log.d( "getAdmobID: ","i am here ");
                    return output[0];
                case SCANNING_SCREEN:
                    return output[1];
                case REVIEW_SCREEN:
                    return output[2];
                case COMPRESSING_SCREEN:
                    return output[3];
                case RESULT_SCREEN:
                    return output[4];
                case GET_STARTED:
                    return output[5];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";


    }
}