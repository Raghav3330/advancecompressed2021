package com.spaceup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.spaceup.apkgenerator.constants.Constants;

/**
 * Created by dhurv on 25-01-2018.
 */

public class UnCompressionReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String packageName = intent.getData().getEncodedSchemeSpecificPart();

        if( "android.intent.action.PACKAGE_REMOVED".equals(intent.getAction())){
            Intent i1 = new Intent(Constants.Receiver.APP_UNINSTALLED_BROADCAST);
            i1.putExtra(Constants.PACKAGE_NAME, packageName);
            LocalBroadcastManager.getInstance(context).sendBroadcast(i1);

        }
        if( "android.intent.action.PACKAGE_ADDED".equals(intent.getAction())){
            Intent i1 = new Intent(Constants.Receiver.APP_INSTALLED_BROADCAST);
            i1.putExtra(Constants.PACKAGE_NAME, packageName);
            LocalBroadcastManager.getInstance(context).sendBroadcast(i1);

        }
    }
}
