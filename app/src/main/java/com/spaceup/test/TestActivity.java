package com.spaceup.test;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.spaceup.R;
import com.spaceup.accessibility.AccessibilityCommunicatorReactivate2018;

import java.io.IOException;

public class TestActivity extends AppCompatActivity {

    String packageName = "app.olaplay.com.olaplay";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Intent i = new Intent(this, AccessibilityCommunicatorReactivate2018.class);
        i.putExtra("path", packageName);
        i.putExtra("source", packageName);
        i.putExtra("type", packageName);
        startService(i);
       // uninstallPackage(getApplicationContext(), "com.jiit.noida.jiitconnect.webkiosk");

    }
    public boolean uninstallPackage(Context context, String packageName) {
        PackageManager packageManger = context.getPackageManager();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            PackageInstaller packageInstaller = packageManger.getPackageInstaller();
            PackageInstaller.SessionParams params = new PackageInstaller.SessionParams(
                    PackageInstaller.SessionParams.MODE_FULL_INSTALL);
            params.setAppPackageName(packageName);
            int sessionId = 0;
            try {
                sessionId = packageInstaller.createSession(params);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            packageInstaller.uninstall(packageName, PendingIntent.getBroadcast(context, sessionId,
                    new Intent("android.intent.action.MAIN"), 0).getIntentSender());
            return true;
        }
        System.err.println("old sdk");
        return false;
    }



}