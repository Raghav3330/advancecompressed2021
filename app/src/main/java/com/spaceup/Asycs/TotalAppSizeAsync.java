package com.spaceup.Asycs;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.RemoteException;
import android.os.TransactionTooLargeException;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.services.DBService;
import com.spaceup.uninstall.activities.AppInfo;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * Created by shashanktiwari on 01/12/16.
 */
public class TotalAppSizeAsync extends AsyncTask<Void, Void, List<AppInfo>> {

    public static String CUSTOM_INTENT = "update.complete";
    private int total_apps, app_size_response_increment;
    private PackageManager packageManager;
    private boolean size_calculated;
    private long start;
    private AppDBHelper _db;
    private String TAG = "TotalAppSizeAsync";
    private Context appContext;

    public TotalAppSizeAsync(Context ctx) {

        appContext = ctx.getApplicationContext();
        _db = AppDBHelper.getInstance(appContext);
        packageManager = ctx.getPackageManager();
    }

    @Override
    protected List<AppInfo> doInBackground(Void... params) {

        Log.d("Seq", "I am here getTotalAppsList doInBackground()");
        try {
            return getTotalAppsList();
        } catch (InterruptedException | TransactionTooLargeException | PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            Log.d("Thread Halted", "");
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<AppInfo> result) {

        PrefManager.getInstance(appContext).setTotalAsyncStatus(1);

        long time = System.currentTimeMillis() - start;
        Log.d("Timetaken", "" + (time / 1000));

        /*if (UtilityMethods.isNotEmptyCollection(result)) {
            synchronized (result) {
                Iterator<AppInfo> iterator = result.iterator();
                while (iterator.hasNext()) {

                    AppInfo res = iterator.next();
                    _db.addApplication(res);
                }
            }
        }*/
        Intent it = new Intent(CUSTOM_INTENT);
        Log.d("ThreadErr", "broadcast send");
        LocalBroadcastManager.getInstance(appContext).sendBroadcast(it);

        try {
            appContext.stopService(new Intent(appContext, DBService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private List<AppInfo> getTotalAppsList() throws InterruptedException, TransactionTooLargeException, PackageManager.NameNotFoundException {
        Log.d("Seq", "I am here getTotalAppsList function");


        List<AppInfo> dbApps = _db.getAppDetails();
        //if user coming back to our application and residual JSON file exists in db

        //TODO sajal : make first run


        if (new File(Constants.RESIDUAL_FILE).exists() && PrefManager.getInstance(appContext).isFirstRun()) {
            //case of reinsallation after few days
            Log.d(Constants.JSON_TAG, "first run is encountered");
            JSONParser parser = new JSONParser();
            try {
                Object residualFile = parser.parse(new FileReader(Constants.RESIDUAL_FILE));
                Log.d(Constants.JSON_TAG, "old JSON reading ");
                JSONObject oldJsonObj = (JSONObject) residualFile;

                if (null == oldJsonObj.get(Constants.JSON_VERSION)) {
                    jsonReformat();
                }


            } catch (IOException e) {
                Log.d(Constants.JSON_TAG, Log.getStackTraceString(e));
                e.printStackTrace();
            } catch (Exception e) {
                Log.d(Constants.JSON_TAG, Log.getStackTraceString(e));
            }


        }

        if (new File(Constants.RESIDUAL_FILE).exists()
                && AppDBHelper.getInstance(appContext).getShorcutApps().size() == 0 &&
                dbApps.size() == 0) {
            backUpFromJSON();
        }

        List<AppInfo> userApps = Collections.synchronizedList(new ArrayList<AppInfo>());
        //List<PackageInfo> allApps = packageManager.getInstalledPackages(0);
        List<ApplicationInfo> allApps = packageManager.getInstalledApplications(0);
        HashSet<String> appsInDB = _db.listAllPackageInDB();

        for (ApplicationInfo appInfo : allApps) {
            if (!appsInDB.contains(appInfo.packageName) && !appInfo.packageName.contains(Constants.APK_EXTENSION)) {
                total_apps++;
                String applicationPackage = appInfo.packageName;
                ApplicationInfo packageInfo = packageManager.getApplicationInfo(applicationPackage, 0);
                appSizeAnother(packageManager, packageInfo, userApps);
            }

            //CAUTION : donot change if else flow
            //app exists in db
            //do package still exits in package manager, if it does not add to update list to mark it uninstalled
        }

        if (total_apps == 0) {
            size_calculated = true;
        }
        while (!size_calculated) {
            Thread.sleep(100);
        }

        //Add application to db
        if (UtilityMethods.isNotEmptyCollection(userApps)) {
            synchronized (userApps) {
                for (AppInfo res : userApps) {
                    if (!Constants.CURRENT_PKG_NAME.equals(res.getPkgName())) {
                        _db.addApplication(res);
                    }
                }
            }
        }

        //THIS WILL BE USEFULL WHEN YOUR_RECEIVER.java DIDN'T worked example when the user reinstall app after 1 month or so
        dbApps = _db.getAppDetails();
        for (AppInfo appInfo : dbApps) {
            if (!isPackageInstalled(appInfo.getPkgName(), packageManager)) {
                //package is not installed
                File backUpApkFile = new File(Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + appInfo.getPkgName() + Constants.APK_EXTENSION);
                if (!((appInfo.getCompressed_status() == Constants.QUICK_COMPRESSED && backUpApkFile.exists())
                        ||
                        (appInfo.getCompressed_status() == Constants.COMPRESSED && isPackageInstalled(appInfo.getPkgName() + Constants.APK_EXTENSION, packageManager))
                ) // not condition
                        ) {
                    //[CASE OF QUICK COMPRESSION ]if compress status is quick and back file exists, then don't change anything in db else make changes
                    //    OR
                    //[CASE OF REFORMATTING DB ]if compress status is compressed and .stash is installed, then don't change anything in db

                    _db.appUninstalled(appInfo.getPkgName());
                }
            }
        }


        return userApps;

    }

    /*
    * old
    * {"size":69578015,"apps":[{"size":5729128,"pkgname":"com.afwsamples.testdpc.stash"}] ,"count":1}
    * update format
    * {"size":69578015,"apps":[{"size":5729128,"pkgname":"com.afwsamples.testdpc" ,"status" : Constants.QUICK_COMPRESSED ,"last_modified_date" : 324312451}] ,"count":1}
    * */
    private void jsonReformat() {

        File file = new File(Constants.RESIDUAL_FILE);

        //number of compressed apps should not be zero && compressed_app_size should be zero then only check from JSON file
        if (file.exists()) {
            Log.d(Constants.JSON_TAG, "file already exists");
            //reading file
            JSONParser parser = new JSONParser();
            JSONObject oldJsonObj;//init old file data
            JSONArray oldAppList; //contains list of OLD compressed appS
            JSONObject oldApp; //contains details of compressed app
            long oldCount = 0; //count of apps
            long oldTotalSize = 0; //total size of apps
            String oldPkgName; //package name of old application
            long oldAppSize;

            JSONObject updateJsonObj = new JSONObject(); //init new file data
            JSONArray updateAppList = new JSONArray(); //contains list of UPDATED compressed appS
            JSONObject updateApp;
            long lastDateModified;


            try {
                Log.d(Constants.JSON_TAG, "opening residual json file");
                Object residualFile = parser.parse(new FileReader(Constants.RESIDUAL_FILE));

                Log.d(Constants.JSON_TAG, "old JSON initialized");
                oldJsonObj = (JSONObject) residualFile;

                oldCount = (int) ((long) oldJsonObj.get("count"));
                oldTotalSize = (long) oldJsonObj.get("size");
                Log.d(Constants.JSON_TAG, "old count " + oldCount);
                Log.d(Constants.JSON_TAG, "old size" + oldTotalSize);

                //reading json array of apps so that we can append new app
                oldAppList = (JSONArray) oldJsonObj.get("apps");
                if (null == oldAppList) {
                    oldAppList = new JSONArray();
                }
                for (JSONObject anOldAppList : (Iterable<JSONObject>) oldAppList) {
                    oldApp = anOldAppList;
                    oldPkgName = (String) oldApp.get("pkgname");
                    oldAppSize = (long) oldApp.get("size");
                    Log.d(Constants.JSON_TAG, "old app package name " + oldPkgName + " size is" + oldAppSize);

                    updateApp = new JSONObject();

                    if (oldPkgName.contains(".stash")) {
                        //quick compressed app
                        oldPkgName = oldPkgName.replace(".stash", "");
                        updateApp.put(Constants.STATUS, Constants.QUICK_COMPRESSED);
                    } else {
                        //advance compressed apps
                        updateApp.put(Constants.STATUS, Constants.COMPRESSED);

                    }
                    updateApp.put(Constants.SIZE, oldAppSize);
                    updateApp.put(Constants.PKGNAME, oldPkgName);
                    lastDateModified = System.currentTimeMillis();
                    updateApp.put(Constants.LAST_MODIFIED_DATE, lastDateModified);


                    updateAppList.add(updateApp);
                    Log.d(Constants.JSON_TAG, "adding to new json " + updateAppList.size());
                    oldAppSize = 0;
                    oldPkgName = null;
                    lastDateModified = 0;
                }

            } catch (Exception e) {
                Log.d(Constants.JSON_TAG, Log.getStackTraceString(e));
            }

            //write
            updateJsonObj.put("count", oldCount);
            updateJsonObj.put("size", oldTotalSize);
            updateJsonObj.put(Constants.JSON_VERSION, 1);
            try {
                updateJsonObj.put("apps", updateAppList);
                FileWriter fw = new FileWriter(Constants.RESIDUAL_FILE);
                fw.write(updateJsonObj.toJSONString());
                fw.flush();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("file", Log.getStackTraceString(e));
            } catch (Exception e) {
                Log.d(TAG, Log.getStackTraceString(e));
                e.printStackTrace();
            }
        }

    }


    private void backUpFromJSON() {
        //residual json file exits in sd card
        //and (if no apps found in shortcut table or if no apps found in AppDetails table
        Log.d("widgetUpdater", "start from total async status");
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader(Constants.RESIDUAL_FILE));
            JSONObject jsonObject = (JSONObject) obj;
            //reading json array of apps so that we can append new app
            JSONArray appsList = (JSONArray) jsonObject.get("apps");

            for (JSONObject ap : (Iterable<JSONObject>) appsList) {
                String pkgName = (String) ap.get("pkgname");
                //useful when if user coming back to our application and residual JSON file exists in db
                //package is installed
                //isPackageInstalled(pkgName, packageManager)
                if (isPackageInstalled(pkgName, packageManager) && !Constants.DUMMY_APP_VERSION_NAME.equalsIgnoreCase(UtilityMethods.getInstance().getCurrentAppVersionName(appContext, pkgName))) {

                    //App is installed on device and it is not dummy app

                    //removing staled entries from JSON file
                    Log.d("widgetUpdater", "App is installed on device and it is not dummy app, so removing from JSON");

                    UtilityMethods.getInstance().removeFromJSONFile(pkgName);
                } else if ((isPackageInstalled(pkgName, packageManager)
                        && Constants.DUMMY_APP_VERSION_NAME.equalsIgnoreCase(UtilityMethods.getInstance().getCurrentAppVersionName(appContext, pkgName))) || isPackageInstalled(pkgName + ".stash", packageManager)) {

                    //Dummy app is installed
                    //JSON is having COMPRESSED STATUS
                    if ((long) ap.get("status") == Constants.COMPRESSED) {
                        long size = (long) ap.get("size");
                        String appName = "";
                        appName = UtilityMethods.getInstance().getApplicationName(appContext, pkgName);

                        if (appName == null || "App".equalsIgnoreCase(appName)) {
                            //take package name from .apk file
                            try {
                                String APKFilePath = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + pkgName + Constants.APK_EXTENSION;
                                Log.d("widgetUpdater", "run: " + APKFilePath);
                                PackageManager pm = appContext.getPackageManager();
                                ApplicationInfo pi = pm.getApplicationInfo(APKFilePath, 0);
                                pi.sourceDir = APKFilePath;
                                pi.publicSourceDir = APKFilePath;
                                appName = (String) pi.loadLabel(pm);
                            } catch (Exception e) {
                                Log.d("widgetUpdater", Log.getStackTraceString(e));
                                e.printStackTrace();
                            }
                        }

                        Log.d("widgetUpdater", "package name " + pkgName + " size is" + size + " app name " + appName);

                        //making AppInfo object from JSON file
                        AppInfo oldAppDetail = new AppInfo();
                        oldAppDetail.setPkgName(pkgName);
                        oldAppDetail.setAppName(appName);
                        oldAppDetail.setSize(size);
                        oldAppDetail.setCacheSize(0);
                        oldAppDetail.setCodeSize(0);
                        oldAppDetail.setApkSize(0);
                        oldAppDetail.setLastModifiedDate(listCombineAppsDateModified(pkgName));
                        oldAppDetail.setCompressed_status(Constants.COMPRESSED);
                        Log.d("widgetUpdater", "add App to COMPRESSED");

                        _db.addApplication(oldAppDetail);
                    }

                } else {
                    //package doesn't exits in package manager
                    String APKFilePath = "";
                    APKFilePath = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + pkgName + Constants.APK_EXTENSION;
                    //JSON STATUS is QUICK COMPRESSED and FilePath exists ......
                    if ((long) ap.get("status") == Constants.QUICK_COMPRESSED && new File(APKFilePath).exists()) {

                        //Dummy app is installed
                        long size = (long) ap.get("size");
                        String appName = "";

                        try {

                            Log.d("widgetUpdater", "run: " + APKFilePath);
                            PackageManager pm = appContext.getPackageManager();
                            ApplicationInfo pi = pm.getApplicationInfo(APKFilePath, 0);
                            pi.sourceDir = APKFilePath;
                            pi.publicSourceDir = APKFilePath;
                            appName = (String) pi.loadLabel(pm);

                        } catch (Exception e) {
                            Log.d("widgetUpdater", Log.getStackTraceString(e));
                            e.printStackTrace();
                        }
                        Log.d("widgetUpdater", "package name " + pkgName + " size is" + size + " app name " + appName);

                        //making AppInfo object from JSON file
                        AppInfo oldAppDetail = new AppInfo();
                        oldAppDetail.setPkgName(pkgName);
                        oldAppDetail.setAppName(appName);
                        oldAppDetail.setSize(size);
                        oldAppDetail.setCacheSize(0);
                        oldAppDetail.setCodeSize(0);
                        oldAppDetail.setApkSize(0);
                        oldAppDetail.setLastModifiedDate(listCombineAppsDateModified(pkgName));
                        oldAppDetail.setCompressed_status(Constants.QUICK_COMPRESSED);

                        //confirm if we can show App icon in Quick compress so we check if APK file exits
                        AppDBHelper.getInstance(appContext).addToShorcutTable(appName, pkgName, size);
                        _db.addApplication(oldAppDetail);
                        Log.d("widgetUpdater", "adding in Constants.QUICK_COMPRESSED");
                    } else {
                        //this does not match any of the cases so we are removing from JSON File
                        UtilityMethods.getInstance().removeFromJSONFile(pkgName);
                        Log.d("widgetUpdater", "we are removing from JSON File");
                    }
                }

            }

        } catch (Exception e) {
            Log.d("widgetUpdater", Log.getStackTraceString(e));
            e.printStackTrace();
        }
    }

    private boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, Log.getStackTraceString(e));
            return false;
        }
    }

    private void appSizeAnother(final PackageManager pm, final ApplicationInfo appInfo, final List<AppInfo> userApps) {
        Method getPackageSizeInfo = null;
        final String pkgName = appInfo.packageName;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            long appData = 100;
            long codeSize = 100;
            long cacheSize = 100;
            AppInfo obj = new AppInfo();
            obj.setPkgName(pkgName);
            obj.setAppName(appInfo.loadLabel(pm).toString());
            obj.setSize(appData);
            obj.setCacheSize(cacheSize);
            obj.setCodeSize(codeSize);
            try {
                obj.setApkSize(getApkSize(pkgName));
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            obj.setLastModifiedDate(listCombineAppsDateModified(pkgName));
            //user installed apps
            if (isUserInstalledApplication(appInfo)) {
                if (Constants.DUMMY_APP_VERSION_NAME.equalsIgnoreCase(UtilityMethods.getCurrentAppVersionName(appContext, obj.getPkgName()))) {
                    //remove com.spaceup  and change status of apps as compressed [DUMMY]
                    obj.setCompressed_status(Constants.COMPRESSED);
                } else {
                    //Apps are uncompressed by default
                    obj.setCompressed_status(Constants.UNCOMPRESSED);
                }
            } else {
                //System app
                obj.setCompressed_status(Constants.SYSTEM_APP);
            }
            userApps.add(obj);
            Log.d(TAG, pkgName + " App Name: " + appInfo.loadLabel(pm).toString());
            app_size_response_increment++;
            if (app_size_response_increment == total_apps) {
                size_calculated = true;
            }
            Log.d("TotalSize", "" + pkgName);

        } else {

            try {
                getPackageSizeInfo = pm.getClass().getMethod(
                        "getPackageSizeInfo", String.class, IPackageStatsObserver.class);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }


            try {
                // Log.d("Total App size",""+total_app_size);
                if (getPackageSizeInfo != null) {
                    getPackageSizeInfo.invoke(pm, pkgName, new IPackageStatsObserver.Stub() {
                        @Override
                        synchronized public void onGetStatsCompleted(PackageStats pStats, boolean succeeded)
                                throws RemoteException {
                            long appData = pStats.dataSize;
                            long codeSize = pStats.codeSize;
                            long cacheSize = pStats.cacheSize;
                            AppInfo obj = new AppInfo();
                            obj.setPkgName(pkgName);
                            obj.setAppName((String) appInfo.loadLabel(pm).toString());
                            obj.setSize(appData);
                            obj.setCacheSize(cacheSize);
                            obj.setCodeSize(codeSize);
                            try {
                                obj.setApkSize(getApkSize(pkgName));
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                            obj.setLastModifiedDate(listCombineAppsDateModified(pkgName));
                            //user installed apps
                            if (isUserInstalledApplication(appInfo)) {

                                if (Constants.DUMMY_APP_VERSION_NAME.equalsIgnoreCase(UtilityMethods.getCurrentAppVersionName(appContext, obj.getPkgName()))) {
                                    //remove com.spaceup  and change status of apps as compressed [DUMMY]
                                    obj.setCompressed_status(Constants.COMPRESSED);
                                } else {
                                    //Apps are uncompressed by default
                                    obj.setCompressed_status(Constants.UNCOMPRESSED);
                                }
                            } else {
                                //System app
                                obj.setCompressed_status(Constants.SYSTEM_APP);
                            }
                            userApps.add(obj);
                            Log.d(TAG, pkgName + " App Name: " + appInfo.loadLabel(pm).toString());
                            app_size_response_increment++;
                            if (app_size_response_increment == total_apps) {
                                size_calculated = true;
                            }
                            Log.d("TotalSize", "" + pkgName);
                        }
                    });
                }

            } catch (ReflectiveOperationException e) {
                e.printStackTrace();
                app_size_response_increment++;
                if (app_size_response_increment == total_apps)
                    size_calculated = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public long getApkSize(String packageName) throws PackageManager.NameNotFoundException {
        return new File(packageManager.getApplicationInfo(packageName, 0).publicSourceDir).length();
    }


    @Override
    protected void onPreExecute() {
        start = System.currentTimeMillis();
        Log.d("ThreadErr", "in total async");
    }

    @Override
    protected void onProgressUpdate(Void... values) {

    }

    private List<AppInfo> getUserInstalledApplication() {
        int flags = PackageManager.GET_META_DATA |
                PackageManager.GET_SHARED_LIBRARY_FILES;
        PackageManager pm = this.packageManager;

        final List<ApplicationInfo> applications = pm.getInstalledApplications(flags);
        List<AppInfo> userInstalled = new ArrayList<>();

        for (ApplicationInfo app : applications) {
            if ((app.flags & ApplicationInfo.FLAG_SYSTEM) != 1) {
                AppInfo apkDetails = new AppInfo(app.loadLabel(pm).toString(), app.packageName, 0, 0, false);
                userInstalled.add(apkDetails);
            }
        }
        return userInstalled;
    }

    private long listCombineAppsDateModified(String pkgName) {
        File externalPath = Environment.getExternalStorageDirectory();
        File internal, external;
        File selected;
        String path = externalPath.getName() + "/Android/data/";
        internal = new File("/data/data/" + pkgName);
        selected = internal;
        try {
            external = new File(path + pkgName);
            if (external.lastModified() > internal.lastModified()) {
                selected = external;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return selected.lastModified();
    }

    private boolean isUserInstalledApplication(ApplicationInfo app) {
        return (app.flags & ApplicationInfo.FLAG_SYSTEM) != 1;
    }
}
