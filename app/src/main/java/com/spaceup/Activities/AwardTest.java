package com.spaceup.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
//import com.google.android.gms.ads.reward.RewardItem;
//import com.google.android.gms.ads.reward.RewardedVideoAd;
//import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.spaceup.BuildConfig;
import com.spaceup.R;
import com.spaceup.RemoteConfig;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityAutomation;

import static com.spaceup.apkgenerator.constants.Constants.LOAD_ADVERTISEMENT;

import org.jetbrains.annotations.NotNull;

public class AwardTest extends AppCompatActivity {
    private static final String TAG = "AWARD TEST";
    private String mFeatureLanded = "Other";
    private RewardedAd mRewardedAd;
    private InterstitialAd mInterstitialAd;
    private com.facebook.ads.InterstitialAd adInterstitial;
    private boolean seenAdvert = false;

    boolean isDebug = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_award_test);

        if (BuildConfig.DEBUG) {
            isDebug = true;
        }

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(@NonNull @NotNull InitializationStatus initializationStatus) {
            }
        });

        AudienceNetworkAds.initialize(this);


        if(RemoteConfig.getString(R.string.ad_network).equals("google")) {
            if (showShowAds()) {
                loadAds();
            } else {
                userSeenAdvertSuccess();
            }
        }else if(RemoteConfig.getString(R.string.ad_network).equals("facebook")){
            loadFBAd();
        }
    }

    private boolean showShowAds() {

        long prevTime = PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.LAST_TIME_SEEN_AD);
        if (System.currentTimeMillis() - prevTime > 2 * 60 * 1000) {

            return true;
        }
        return true;
//        if (BuildConfig.DEBUG) {
//            Toast.makeText(this, "NOT SHOWING AD!", Toast.LENGTH_SHORT).show();
//        }
//        return false;
    }

    private void loadAds() {
        AdRequest adRequest = new AdRequest.Builder().build();
        Log.d("loadAds: ",UtilityMethods.getInstance().getAdmobID(UtilityMethods.COMPRESSING_SCREEN));
        RewardedAd.load(this, UtilityMethods.getInstance().getAdmobID(UtilityMethods.COMPRESSING_SCREEN),
                adRequest, new RewardedAdLoadCallback() {
                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error.
                        mRewardedAd = null;
                        Log.d("onAdFailedToLoad: ",loadAdError.getMessage().toString());
                        AdRequest adRequest = new AdRequest.Builder().build();
                        InterstitialAd.load(getApplicationContext(), UtilityMethods.getInstance().getAdmobID(UtilityMethods.REVIEW_SCREEN), adRequest,
                                new InterstitialAdLoadCallback() {
                                    @Override
                                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                                        // The mInterstitialAd reference will be null until
                                        // an ad is loaded.
                                        mInterstitialAd = interstitialAd;
                                        if (mInterstitialAd != null) {

                                            mInterstitialAd.show(AwardTest.this);
                                            mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                                                @Override
                                                public void onAdDismissedFullScreenContent() {
                                                    // Called when fullscreen content is dismissed.
                                                    Log.d("TAG", "The ad was dismissed.");
                                                    mRewardedAd = null;
                                                    String message = "SUCCESS";
                                                    Intent intent = new Intent();
                                                    intent.putExtra("MESSAGE", message);
                                                    setResult(LOAD_ADVERTISEMENT, intent);
                                                    finish();
                                                }

                                                @Override
                                                public void onAdFailedToShowFullScreenContent(AdError adError) {
                                                    // Called when fullscreen content failed to show.
                                                    Log.d("TAG", "The ad failed to show.");
                                                }

                                                @Override
                                                public void onAdShowedFullScreenContent() {
                                                    // Called when fullscreen content is shown.
                                                    // Make sure to set your reference to null so you don't
                                                    // show it a second time.
                                                    mInterstitialAd = null;
                                                    Log.d("TAG", "The ad was shown.");
                                                }
                                            });
                                        } else {
                                            Log.d("TAG", "The interstitial ad wasn't ready yet.");
                                        }
                                    }
                                    @Override
                                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                                        // Handle the error
                                        mInterstitialAd = null;
                                        Log.d("onAdFailedToLoad: ", loadAdError.getMessage().toString());
                                        mRewardedAd = null;
                                        String message = "FAILURE";
                                        Intent intent = new Intent();
                                        intent.putExtra("MESSAGE", message);
                                        setResult(LOAD_ADVERTISEMENT, intent);
                                        finish();
                                    }
                                });
                    }

                    @Override
                    public void onAdLoaded(@NonNull RewardedAd rewardedAd) {
                        mRewardedAd = rewardedAd;
                        Log.d(TAG, "Ad was loaded.");
                        showAds();
                    }
                });
    }

    private void showAds() {

        mRewardedAd.setFullScreenContentCallback(new FullScreenContentCallback() {
            @Override
            public void onAdShowedFullScreenContent() {
                // Called when ad is shown.
                Log.d(TAG, "Ad was shown.");
            }

            @Override
            public void onAdFailedToShowFullScreenContent(AdError adError) {
                // Called when ad fails to show.
                Log.d(TAG, "Ad failed to show.");
            }

            @Override
            public void onAdDismissedFullScreenContent() {
                // Called when ad is dismissed.
                // Set the ad reference to null so you don't show the ad a second time.
                Log.d(TAG, "Ad was dismissed.");
                mRewardedAd = null;
//                String message = "SUCCESS";
//                Intent intent = new Intent();
//                intent.putExtra("MESSAGE", message);
//                setResult(LOAD_ADVERTISEMENT, intent);
                finish();
            }
        });
        if (mRewardedAd != null) {
            Activity activityContext = this;
            mRewardedAd.show(activityContext, new OnUserEarnedRewardListener() {
                @Override
                public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                    // Handle the reward.
                    Log.d(TAG, "The user earned the reward.");
//                    seenAdvert = true;
//                    if (seenAdvert) {
                        userSeenAdvertSuccess();
//                    }
                }
            });
        } else {
            Log.d(TAG, "The rewarded ad wasn't ready yet.");
        }
    }
    private void loadFBAd() {
        adInterstitial = new com.facebook.ads.InterstitialAd(this, "600591120962050_601726497515179");
        InterstitialAdListener interstitialAdListener = new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback
                Log.d( "ad displayed.","I am here");
                userSeenAdvertSuccess();
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                // Interstitial dismissed callback
                Log.d( "ad dismissed.","I am here");
                adInterstitial = null;
                String message = "SUCCESS";
                Intent intent = new Intent();
                intent.putExtra("MESSAGE", message);
                setResult(LOAD_ADVERTISEMENT, intent);
                finish();
            }
            @Override
            public void onError(Ad ad, com.facebook.ads.AdError adError) {
                Log.d("AD ERROR ",adError.getErrorMessage().toString());

            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Interstitial ad is loaded and ready to be displayed
                Log.d( "ad is loaded ","i am here");
                // Show the ad
                adInterstitial.show();
                Log.d( "onAdLoaded: ",ad.getPlacementId());
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                Log.d( "ad clicked!","AD CLICKED");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d( "ad impression logged!","LOGGING IMPRESSION");
            }
        };

        // For auto play video ads, it's recommended to load the ad
        // at least 30 seconds before it is shown
        adInterstitial.loadAd(
                adInterstitial.buildLoadAdConfig()
                        .withAdListener(interstitialAdListener)
                        .build());
    }


    private void userSeenAdvertSuccess() {
        seenAdvert = true;
        String message ;
        if(seenAdvert){
            message = "SUCCESS";
        }else{
            message = "FAILURE";
        }
        Intent intent = new Intent();
        intent.putExtra("MESSAGE", message);
        setResult(LOAD_ADVERTISEMENT, intent);
        PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.LAST_TIME_SEEN_AD, System.currentTimeMillis());
        finish();//finishing activity
//        if (RemoteConfig.getBoolean(R.string.auto_back_after_advert) && AccessibilityAutomation.getSharedInstance() != null)
//            AccessibilityAutomation.getSharedInstance().globalActionBack();

    }
}


        //        if (showShowAds()) {
//            loadRewardedVideoAd();
//        } else {
//            userSeenAdvertSuccess();
//
//        }
//    }
//
//    private void userSeenAdvertSuccess() {
//        String message = "SUCCESS";
//        Intent intent = new Intent();
//        intent.putExtra("MESSAGE", message);
//        setResult(LOAD_ADVERTISEMENT, intent);
//        PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.LAST_TIME_SEEN_AD, System.currentTimeMillis());
//        finish();//finishing activity
//        if (RemoteConfig.getBoolean(R.string.auto_back_after_advert) && AccessibilityAutomation.getSharedInstance() != null)
//            AccessibilityAutomation.getSharedInstance().globalActionBack();
//
//    }
//
//    private boolean showShowAds() {
//        long prevTime = PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.LAST_TIME_SEEN_AD);
//        if (System.currentTimeMillis() - prevTime > 2 * 60 * 1000) {
//
//            return true;
//        }
//        if (BuildConfig.DEBUG) {
//            Toast.makeText(this, "NOT SHOWING AD!", Toast.LENGTH_SHORT).show();
//        }
//        return false;
//    }
//
//    private void loadRewardedVideoAd() {
//        String coinId = UtilityMethods.getInstance().getAdmobID(UtilityMethods.REVIEW_SCREEN);
//        if (isDebug) {
//            coinId = "ca-app-pub-3940256099942544/5224354917";
//        }
//
//        mRewardedVideoAd.loadAd(coinId,
//                new AdRequest.Builder().build());
//
//    }
//
//    @Override
//    public void onRewarded(RewardItem reward) {
//        seenAdvert = true;
//        if (seenAdvert) {
//            userSeenAdvertSuccess();
//
//
//        } else {
//            String message = "FAILURE";
//            Intent intent = new Intent();
//            intent.putExtra("MESSAGE", message);
//            setResult(LOAD_ADVERTISEMENT, intent);
//            finish();
//        }
//
//    }
//
//    @Override
//    public void onRewardedVideoAdLeftApplication() {
//
//    }
//
//    @Override
//    public void onRewardedVideoAdClosed() {
//
//        String message = "FAILURE";
//        Intent intent = new Intent();
//        intent.putExtra("MESSAGE", message);
//        setResult(LOAD_ADVERTISEMENT, intent);
//        finish();
//    }
//
//    @Override
//    public void onRewardedVideoAdFailedToLoad(int errorCode) {
//
//
//        userSeenAdvertSuccess();
//
//    }
//
//    @Override
//    public void onRewardedVideoAdLoaded() {
//
//        mRewardedVideoAd.show();
//
//
//    }
//
//    @Override
//    public void onRewardedVideoAdOpened() {
//
//    }
//
//    @Override
//    public void onRewardedVideoStarted() {
//    }