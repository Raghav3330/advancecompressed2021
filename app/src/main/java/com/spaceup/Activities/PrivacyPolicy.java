package com.spaceup.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.spaceup.R;
import com.spaceup.apkgenerator.constants.Constants;


public class PrivacyPolicy extends AppCompatActivity {
    String title = "Privacy", url = "http://spaceupapp.com/privacy.html";
    WebView myWebView;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy_layout);
        TextView page_title = findViewById(R.id.page_title);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(Constants.URL_PARAMETER) && bundle.containsKey(Constants.TITLE_PARAMETER)) {
            url = bundle.getString(Constants.URL_PARAMETER);
            title = bundle.getString(Constants.TITLE_PARAMETER);
        }

        page_title.setText(title);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        } else if (Build.VERSION.SDK_INT >= 19) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        myWebView = findViewById(R.id.privacy_webview);
//        myWebView.getSettings().setJavaScriptEnabled(false);
//        myWebView.loadUrl(url);
//        myWebView.setWebViewClient(new WebViewController());
         Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
         startActivity(intent);

    }


    public class WebViewController extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
