package com.spaceup.hosting;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by dhurv on 07-03-2017.
 */

public class SendFeedbackSpaceUpUninstall {
    private String emailId, message;
    private Context context;
    private boolean space_check, data_check, crash_check, battery_check;
    private boolean mLaunchFlag;

    public SendFeedbackSpaceUpUninstall(Context context, boolean space_check, boolean data_check, boolean crash_check, boolean battery_check, String message, boolean pLaunchFlag) {
        this.context = context;
        this.space_check = space_check;
        this.data_check = data_check;
        this.crash_check = crash_check;
        this.battery_check = battery_check;
        this.mLaunchFlag = pLaunchFlag;
        this.message = message;
    }
    public void send_to_server(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST,"http://spaceup.esy.es/send_feedback.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("FEEDBACK","SPACE_UP_FEED" + response);
                        if (!mLaunchFlag) {
                            Toast.makeText(context, "Thanks for your feedback!", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.print(error.getMessage());
                        Log.d("FEEDBACK","SPACE_UP_FEED ERROR");


                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                String formattedDate = df.format(c.getTime());String gmail = null;

                Pattern gmailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
                Account[] accounts = AccountManager.get(context).getAccounts();
                for (Account account : accounts) {
                    if (gmailPattern.matcher(account.name).matches()) {
                        gmail = account.name;
                    }
                }

                emailId = gmail;
                params.put("email", emailId);
                params.put("space_check", ""+space_check);
                params.put("data_check", ""+data_check);
                params.put("crash_check", ""+crash_check);
                params.put("battery_check", ""+battery_check);
                params.put("message", message);
                params.put("date", formattedDate);
                Log.d("RESPONSE_DATA",emailId + space_check + data_check + crash_check + battery_check + message + formattedDate);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }

}
