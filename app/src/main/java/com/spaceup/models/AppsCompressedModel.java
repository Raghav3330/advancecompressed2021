package com.spaceup.models;

/**
 * Created by shashank.tiwari on 14/12/16.
 */

public class AppsCompressedModel {

    String appName;
    String packageName;
    String dummyApkPath;
    String folder, install_status, uninstall_status;

    public String getDummyApkPath() {
        return dummyApkPath;
    }

    public void setDummyApkPath(String dummyApkPath) {
        this.dummyApkPath = dummyApkPath;
    }

    public String getInstall_status() {

        return install_status;
    }

    public void setInstall_status(String install_status) {
        this.install_status = install_status;
    }

    public String getUninstall_status() {
        return uninstall_status;
    }

    public void setUninstall_status(String uninstall_status) {
        this.uninstall_status = uninstall_status;
    }

    public String getPackageName() {

        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getAppName() {

        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }
}
