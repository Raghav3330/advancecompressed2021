package com.spaceup.Activities;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.spaceup.Fragments.NumberFragment;
import com.spaceup.Fragments.WarningFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sajal on 27-12-2016
 */
public class FragmentPageAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragments;

    public FragmentPageAdapter(FragmentManager fm, int total_apps, long total_app_size, long compressed_apps, long compressed_app_size) {

        super(fm);
        this.fragments = new ArrayList<Fragment>();
        NumberFragment numberFragment = new NumberFragment();
        Bundle data = new Bundle();//create bundle instance
        data.putString("total_apps", "" + total_apps);//put string to pass with a key value
        data.putString("total_app_size", "" + total_app_size);//put string to pass with a key value
        data.putString("compressed_apps", "" + compressed_apps);//put string to pass with a key value
        data.putString("compressed_app_size", "" + compressed_app_size);//put string to pass with a key value
        numberFragment.setArguments(data);
        fragments.add(numberFragment);
        fragments.add(new WarningFragment());
    }


    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }


}
