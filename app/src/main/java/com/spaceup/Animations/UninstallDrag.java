package com.spaceup.Animations;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.R;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityAutomation;
import com.spaceup.accessibility.AccessibilityCommunicator;
import com.spaceup.accessibility.AccessibilityCommunicator_ShortCut;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.uninstall.activities.AppInfo;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.WINDOW_SERVICE;
import static com.spaceup.AppShortcut.ShortCutFolder.MOVE_TO_MENU;
import static com.spaceup.accessibility.AccessibilityAutomation.repeat_uninstall;

/**
 * Created by Dhruv on 18-02-2017.
 */

public class UninstallDrag {

    private AccessibilityAutomation sSharedInstance;
    private Context context;
    private WindowManager wm1;
    private View oView1;
    private String appname;
    private String package_name;
    private String app_name;
    private long prev_size;
    private Drawable icon;
    private Bundle mGAParams;
    private AccessibilityNodeInfo nodeInfo;

    public UninstallDrag(String appname, AccessibilityAutomation sSharedInstance, Context context, AccessibilityNodeInfo nodeInfo) {

        this.sSharedInstance = sSharedInstance;
        this.context = context;
        this.appname = appname;
        this.nodeInfo = nodeInfo;
    }

    public void overlayDrag() {
        try {
            wm1.removeView(oView1);
            wm1 = null;
            oView1 = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        MOVE_TO_MENU = 0;
        CardView box;
        ImageView appIcon;
        View before, after;
        TextView previous_Size, previous_Text, finalSize;
        //extract app name logo and package name
        LayoutInflater inflater1 = (LayoutInflater) sSharedInstance.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        oView1 = inflater1.inflate(R.layout.accessibility_uninstall_message, null);
        TextView message = (TextView) oView1.findViewById(R.id.textView24);
        box = (CardView) oView1.findViewById(R.id.card_view);
        box.setOnClickListener(null); //prevent closing window on touching CardView
        message.setText("Compress " + appname);
        List<AppInfo> appInDb = AppDBHelper.getInstance(context).getAppDetails();
        for (AppInfo app : appInDb) {
            try {

                if (app.getAppName().equals(appname)) {
                    package_name = app.getPkgName();
                    app_name = app.getAppName();
                    before = oView1.findViewById(R.id.before_icon);
                    after = oView1.findViewById(R.id.after_icon);

                    previous_Text = (TextView) before.findViewById(R.id.textView26);
                    previous_Size = (TextView) before.findViewById(R.id.textView23);  // in Long
                    finalSize = (TextView) after.findViewById(R.id.textView23);  // in Long

                    prev_size = app.getSize() + app.getCacheSize() + app.getApkSize() + app.getCodeSize();
                    finalSize.setText("" + UtilityMethods.getInstance().getSizeinMB(prev_size / 10));
                    previous_Size.setText(UtilityMethods.getInstance().getSizeinMB(prev_size));
                    previous_Text.setText("Current");
                    icon = context.getPackageManager().getApplicationIcon(app.getPkgName());
                    appIcon = (ImageView) before.findViewById(R.id.imageView27);
                    appIcon.setImageDrawable(icon);
                    appIcon = (ImageView) after.findViewById(R.id.imageView27);
                    Bitmap tempBMP = UtilityMethods.getInstance().toGrayscale(UtilityMethods.getInstance().drawableToBitmap(icon));
                    appIcon.setImageBitmap(tempBMP);
                    try {
                        List<AccessibilityNodeInfo> list;
                        list = nodeInfo.findAccessibilityNodeInfosByText("Cancel");
                        Log.d("TAG", "CLICKED COMPRESSED" + list.toString());
                        if (list.toString().contains("android.widget.Button"))
                            for (AccessibilityNodeInfo node : list) {
                                node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
//TODO SAJAL

        WindowManager.LayoutParams params1;

        try {
            params1 = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSPARENT);
            params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

            wm1 = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
            try {
                wm1.addView(oView1, params1);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception j) {
            params1 = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSPARENT);
            params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
            wm1 = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
            try {
                wm1.addView(oView1, params1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        TextView uninstall = (TextView) oView1.findViewById(R.id.textView25);
        uninstall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                repeat_uninstall = 1;

                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();

                }
                Bundle paramBundle = new Bundle();
                paramBundle.putString(AnalyticsConstants.Params.PACKAGE_NAME, package_name);
                paramBundle.putString(AnalyticsConstants.Params.CANCEL_TYPE, "uninstall");
                paramBundle.putString(AnalyticsConstants.Params.STATUS, "cancel");
                Log.d(AnalyticsConstants.TAG, "Drag compress uninstall" + package_name);
                new AnalyticsHandler().logEvent(AnalyticsConstants.Event.DRAG_COMPRESS, paramBundle);
                // check for unknown permission
                // Analytics Start
                mGAParams = new Bundle();
                mGAParams.putString("category", AnalyticsConstants.Category.DRAG_COMPRESS);
                mGAParams.putString("action", AnalyticsConstants.Action.UNINSTALL);
                mGAParams.putString("label", package_name);
                new AnalyticsHandler().logGAEvent(mGAParams);
                Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                // Analytics end


                Uri packageUri1 = Uri.parse("package:" + package_name);
                Intent uninstallIntent1 = new Intent(Intent.ACTION_DELETE, packageUri1);
                uninstallIntent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    Log.d("DHRUV", "delete intent try block");
                    context.startActivity(uninstallIntent1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        final TextView compress = (TextView) oView1.findViewById(R.id.textView28);
        compress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bundle paramBundle = new Bundle();
                paramBundle.putString(AnalyticsConstants.Params.PACKAGE_NAME, package_name);
                paramBundle.putString(AnalyticsConstants.Params.CANCEL_TYPE, "null");
                paramBundle.putString(AnalyticsConstants.Params.STATUS, "init");
                Log.d(AnalyticsConstants.TAG, "Drag compress " + package_name);
                new AnalyticsHandler().logEvent(AnalyticsConstants.Event.DRAG_COMPRESS, paramBundle);


              /*  Intent intent=new Intent(UninstallerActivity.this,AccessibilityCommunicator.class);
                intent.putStringArrayListExtra("compressList",toCompressAppList);
                intent.putStringArrayListExtra("compressName",toCompressAppName);
                startService(intent);*/


                // Analytics Start
                mGAParams = new Bundle();
                mGAParams.putString("category", AnalyticsConstants.Category.DRAG_COMPRESS);
                mGAParams.putString("action", AnalyticsConstants.Action.COMPRESS);
                mGAParams.putString("label", package_name);
                mGAParams.putLong("value", 1l);
                Log.d("DRAG_CLASS" + getClass().getName(), package_name);

                new AnalyticsHandler().logGAEvent(mGAParams);
                Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                // Analytics end
                if (!UtilityMethods.getInstance().isHavingUnkownSourcePermission(context)) {
                    Intent intent = new Intent(context, AccessibilityCommunicator_ShortCut.class);
                    ArrayList<String> toCompressAppList = new ArrayList<>();
                    ArrayList<String> toCompressAppName = new ArrayList<>();
                    toCompressAppList.add(package_name);
                    toCompressAppName.add(app_name);
                    intent.putStringArrayListExtra("compressList", toCompressAppList);
                    intent.putStringArrayListExtra("compressName", toCompressAppName);
                    intent.putExtra("state", "single_compress");
                    context.startService(intent);
                } else {
                    Intent intent = new Intent(context, AccessibilityCommunicator.class);
                    ArrayList<String> toCompressAppList = new ArrayList<>();
                    ArrayList<String> toCompressAppName = new ArrayList<>();
                    toCompressAppList.add(package_name);
                    toCompressAppName.add(app_name);
                    intent.putStringArrayListExtra("compressList", toCompressAppList);
                    intent.putStringArrayListExtra("compressName", toCompressAppName);
                    intent.putExtra("state", "   ");
                    context.startService(intent);
                }
                Log.d("SINGLE_APPS_NAME", "" + package_name + " " + app_name + " " + appname);

            }
        });
        final ImageView cancel = (ImageView) oView1.findViewById(R.id.imageView31);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Analytics Start
                mGAParams = new Bundle();
                mGAParams.putString("category", AnalyticsConstants.Category.DRAG_COMPRESS);
                mGAParams.putString("action", AnalyticsConstants.Action.CANCEL);
                mGAParams.putString("label", package_name);
                new AnalyticsHandler().logGAEvent(mGAParams);
                Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                // Analytics end


                //Analytics
                Bundle paramBundle = new Bundle();
                paramBundle.putString(AnalyticsConstants.Params.PACKAGE_NAME, package_name);
                paramBundle.putString(AnalyticsConstants.Params.CANCEL_TYPE, "cross");
                paramBundle.putString(AnalyticsConstants.Params.STATUS, "cancel");
                Log.d(AnalyticsConstants.TAG, "Drag compress cancel" + package_name);

                new AnalyticsHandler().logEvent(AnalyticsConstants.Event.DRAG_COMPRESS, paramBundle);

                repeat_uninstall = 1;
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Uri packageUri1 = Uri.parse("package:" + package_name);
                Intent uninstallIntent1 = new Intent(Intent.ACTION_DELETE, packageUri1);
                uninstallIntent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    Log.d("DHRUV", "delete intent try block");
                    context.startActivity(uninstallIntent1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }
}
