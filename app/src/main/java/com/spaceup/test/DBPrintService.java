package com.spaceup.test;

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.models.ImageModel;
import com.spaceup.uninstall.activities.AppInfo;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Times on 01-03-2017.
 */

public class DBPrintService extends IntentService {
    Handler mHandler;


    public DBPrintService() {

        super(DBPrintService.class.getName());

    }

    @Override
    public void onCreate() {

        super.onCreate();
        mHandler = new Handler();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        //printAppDatabase();
        //printJSONFile();
        printImageDatabase();

    }


    private void printAppDatabase() {
        /*
         COMPRESS STATUS STATES
         * 1 = compressed apps
         * -1 = uncompressed apps
         * -2 = system apps
         * 2 = .stash apps installed
         * -3 = .stash apps uninstalled
         * -4 = app uninstalled*/


        List<AppInfo> appInDb = AppDBHelper.getInstance(this).getAppDetails();
        for (AppInfo app : appInDb) {


            switch (app.getCompressed_status()) {
                case Constants.COMPRESSED:
                    Log.d("PrintDB", " " + app.getPkgName() + "  " + app.getAppName() + " COMPRESSED");
                    Log.d("PrintDB", "ModifiedDate " + app.getLastModifiedDate());
                    Log.d("PrintDB", "ApkSize " + app.getApkSize());
                    Log.d("PrintDB", "CodeSize " + app.getCodeSize());
                    Log.d("PrintDB", "CacheSize " + app.getCacheSize());
                    Log.d("PrintDB", "DataSize " + app.getSize());
                    Log.d("PrintDB", "-------------------------------------------------------------------");
                    break;

                case Constants.UNCOMPRESSED:
                    Log.d("PrintDB", " " + app.getPkgName() + "  " + app.getAppName() + " UNCOMPRESSED APP");
                    Log.d("PrintDB", "ModifiedDate " + app.getLastModifiedDate());
                    Log.d("PrintDB", "ApkSize " + app.getApkSize());
                    Log.d("PrintDB", "CodeSize " + app.getCodeSize());
                    Log.d("PrintDB", "CacheSize " + app.getCacheSize());
                    Log.d("PrintDB", "DataSize " + app.getSize());
                    Log.d("PrintDB", "-------------------------------------------------------------------");
                    break;
                case Constants.SYSTEM_APP:
                    //Log.d("PrintDB", " " + app.getPkgName() + "  " + app.getAppName() + " SYSTEM APPS");
                    break;
                case 2:
                    Log.d("PrintDB", " " + app.getPkgName() + "  " + app.getAppName() + " .STASH APPS INSTALLED");
                    Log.d("PrintDB", "ModifiedDate " + app.getLastModifiedDate());
                    Log.d("PrintDB", "ApkSize " + app.getApkSize());
                    Log.d("PrintDB", "CodeSize " + app.getCodeSize());
                    Log.d("PrintDB", "CacheSize " + app.getCacheSize());
                    Log.d("PrintDB", "DataSize " + app.getSize());
                    Log.d("PrintDB", "-------------------------------------------------------------------");
                    break;
                case -3:
                    Log.d("PrintDB", " " + app.getPkgName() + "  " + app.getAppName() + " .STASH APPS UNINSTALLED");
                    Log.d("PrintDB", "ModifiedDate " + app.getLastModifiedDate());
                    Log.d("PrintDB", "ApkSize " + app.getApkSize());
                    Log.d("PrintDB", "CodeSize " + app.getCodeSize());
                    Log.d("PrintDB", "CacheSize " + app.getCacheSize());
                    Log.d("PrintDB", "DataSize " + app.getSize());
                    Log.d("PrintDB", "-------------------------------------------------------------------");
                    break;
                case Constants.APP_UNINSTALLED:
                    Log.d("PrintDB", " " + app.getPkgName() + "  " + app.getAppName() + " APPS UNINSTALLED REMOVED");
                    Log.d("PrintDB", "ModifiedDate " + app.getLastModifiedDate());
                    Log.d("PrintDB", "ApkSize " + app.getApkSize());
                    Log.d("PrintDB", "CodeSize " + app.getCodeSize());
                    Log.d("PrintDB", "CacheSize " + app.getCacheSize());
                    Log.d("PrintDB", "DataSize " + app.getSize());
                    Log.d("PrintDB", "-------------------------------------------------------------------");
                    break;
                case Constants.QUICK_COMPRESSED:
                    Log.d("PrintDB", " " + app.getPkgName() + "  " + app.getAppName() + " Quick Compressed App");
                    Log.d("PrintDB", "ModifiedDate " + app.getLastModifiedDate());
                    Log.d("PrintDB", "ApkSize " + app.getApkSize());
                    Log.d("PrintDB", "CodeSize " + app.getCodeSize());
                    Log.d("PrintDB", "CacheSize " + app.getCacheSize());
                    Log.d("PrintDB", "DataSize " + app.getSize());
                    Log.d("PrintDB", "-------------------------------------------------------------------");
                    break;
                default:
                    Log.d("PrintDB", " " + app.getPkgName() + "  " + app.getAppName() + " COMPRESSED_STATUS " + app.getCompressed_status());
                    Log.d("PrintDB", "ModifiedDate " + app.getLastModifiedDate());
                    Log.d("PrintDB", "ApkSize " + app.getApkSize());
                    Log.d("PrintDB", "CodeSize " + app.getCodeSize());
                    Log.d("PrintDB", "CacheSize " + app.getCacheSize());
                    Log.d("PrintDB", "DataSize " + app.getSize());
                    Log.d("PrintDB", "-------------------------------------------------------------------");


            }



          /*
            Log.d("PrintDB", "PriorityScore " + app.getIndex());
            Log.d("PrintDB", "Time " + app.getTotalTimeInForeground());
            Log.d("PrintDB", "Category " + app.getCategory());
            Log.d("PrintDB", "ModifiedDate " + app.getLastModifiedDate());
            Log.d("PrintDB", "Extract " + app.getLocation());*/
            //Log.d("PrintDB", "COMPRESSED_COUNT " + app.getCompressed_count());
            //Log.d("PrintDB", "UNCOMPRESSED_COUNT " + app.getUnCompressed_count());

        }

    }


    private void printJSONFile() {
        Log.d("PrintDB", "-------JSON FILE OPEN-------");

        String path = Environment.getExternalStorageDirectory().getPath() + Constants.APK_DB + "/" + Constants.STASH_DB_FILE + ".json";
        File file = new File(path);


        JSONObject jsonObject;
        JSONArray apps;

        if (file.exists()) {
            //reading file and update
            JSONParser parser = new JSONParser();
            try {
                Object obj = parser.parse(new FileReader(path));
                jsonObject = (JSONObject) obj;
                JSONArray msg = (JSONArray) jsonObject.get("apps");
                Iterator<JSONObject> iterator = msg.iterator();
                while (iterator.hasNext()) {
                    JSONObject ap = iterator.next();

                    Log.d("PrintDB", "Package name " + ap.get(Constants.PKGNAME));
                    Log.d("PrintDB", "Size " + ap.get(Constants.SIZE));
                    Log.d("PrintDB", "Status " + ap.get(Constants.STATUS));
                    Log.d("PrintDB", "Last modified date " + ap.get(Constants.LAST_MODIFIED_DATE));
                    Log.d("PrintDB", "-------------------------------------------------------------------");
                }
            } catch (Exception e) {

            }
        }
    }


    private void printImageDatabase() {
        List<ImageModel> imageList = AppDBHelper.getInstance(this).
                getAllImages();
        for (ImageModel img : imageList) {

            Log.d("PrintDB", "Image Path " + img.getPath());

            switch (img.getResponseStatus()) {
                case Constants.SCANNED:
                    Log.d("PrintDB", "Response Status Scanned");
                    break;
                case Constants.NOT_SCANNED:
                    Log.d("PrintDB", "Response Status Not Scanned");
                    break;
                case Constants.TRASH:
                    Log.d("PrintDB", "Response Status TRASH");
                    break;
                case Constants.RECOVERED:
                    Log.d("PrintDB", "Response Status Recovered");
                    break;
            }

            switch (img.getActionStatus()) {
                case Constants.JUNK:
                    Log.d("PrintDB", "Action Status Junk");
                    break;
                case Constants.NOT_JUNK:
                    Log.d("PrintDB", "Action Status Not junk");
                    break;
                case Constants.IGNORED:
                    Log.d("PrintDB", "Action Status Ignored");
                    break;
            }

            Log.d("PrintDB", "Image Trash Path " + img.getTrashPath());

            Log.d("PrintDB", "-------------------------------------------------------------------");
        }

        Log.d("PrintDB", "-------------------------------------------------------------------" + imageList.size());


    }

}

