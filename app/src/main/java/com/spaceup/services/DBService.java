package com.spaceup.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.spaceup.Asycs.TotalAppSizeAsync;
import com.spaceup.Utility.PrefManager;


/**
 * Created by Sajal Jain on 28-01-2017.
 */

public class DBService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.d("dbservice", "onCreate");
        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(null == intent ){
            Log.d("dbservice", "onStartCommand : starting it again");
        }
        Log.d("dbservice", "onStartCommand");
        PrefManager.getInstance(this).setTotalAsyncStatus(0);
        new TotalAppSizeAsync(getApplicationContext()).execute();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d("dbservice", "onDESTROY");

        super.onDestroy();
    }
}
