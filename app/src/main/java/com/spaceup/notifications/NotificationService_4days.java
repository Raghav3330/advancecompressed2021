package com.spaceup.notifications;

import android.app.ActivityManager;
import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.spaceup.R;
import com.spaceup.Utility.PrefManager;
import com.spaceup.accessibility.AccessibilityAutomation;
import com.spaceup.app_services.app_Service;
import com.spaceup.services.RecommendationService;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

/**
 * Created by Dhruv Kaushal on 16-12-2016.
 */

public class NotificationService_4days extends IntentService {
    BroadcastReceiver accessibilityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("com.times.DbUpdated")) {

                try {
                    if (intent.getExtras().containsKey("status") && intent.getExtras().getString("status").equals("true")) {

                        if (!isRunningInForeground() && !AccessibilityAutomation.getAllowed()) {

                            Intent intentService = new Intent(NotificationService_4days.this, app_Service.class);
                            intentService.putExtra("no_of_folders", "6");

                            startService(intentService);

                            PrefManager.getInstance(getApplicationContext()).set_Alternate_BOTTOM("pop_up");
                            NotificationHandler.getInstance(getApplicationContext()).four_days_Notification(getApplicationContext());
                        } else {
                            Log.d("ALARM_RECEIVER_4DAY", "NO COMPRESSED RUNNING IN BG");
                        }
                    }
                } catch (Exception e) {

                    Log.d(NotificationService.class.getName(), Log.getStackTraceString(e));
                } finally {
                    LocalBroadcastManager.getInstance(context).unregisterReceiver(accessibilityReceiver);
                }
            }
        }
    };

    public NotificationService_4days() {

        super(NotificationService_4days.class.getName());

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("ALARM_RECEIVER_4DAY", "STARTED_ALARM_ONCREATE");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("ALARM_RECEIVER_4DAY", "STARTED_ALARM_RECEIVER");


        PrefManager prefManager = PrefManager.getInstance(getApplicationContext());

        // Notification to be shown 4 day wise if .stash app are not found in db
        if (!prefManager.get_is_FIRSTNOTIFICATION("NotificationService_4days")) {
            IntentFilter accessibilityReceiverIntentFilter = new IntentFilter();
            accessibilityReceiverIntentFilter.addAction("com.times.DbUpdated");
            LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(accessibilityReceiver,
                    accessibilityReceiverIntentFilter);
            UpdateRecommendationDb();
        } else {
            prefManager.set_is_FIRSTNOTIFICATION(false, "NotificationService_4days");
        }
    }

    public void UpdateRecommendationDb() {
        Log.d("ALARM_RECEIVER", "Starting RecommendationService");
        Intent intent = new Intent(getApplicationContext(), RecommendationService.class);
        startService(intent);

    }

    protected boolean isRunningInForeground() {

        try {
            ActivityManager am = (ActivityManager) NotificationService_4days.this.getSystemService(ACTIVITY_SERVICE);
            // The first in the list of RunningTasks is always the foreground task.
            ActivityManager.RunningTaskInfo foregroundTaskInfo = am.getRunningTasks(1).get(0);
            String foregroundTaskPackageName = foregroundTaskInfo.topActivity.getPackageName();
            PackageManager pm = NotificationService_4days.this.getPackageManager();
            PackageInfo foregroundAppPackageInfo = pm.getPackageInfo(foregroundTaskPackageName, 0);
            String foregroundTaskAppName = foregroundAppPackageInfo.applicationInfo.loadLabel(pm).toString();
            if (foregroundTaskAppName.equals(getResources().getString(R.string.app_name))) {

                return true;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}