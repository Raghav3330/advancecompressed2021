package com.spaceup.uninstall.activities;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;

import com.facebook.ads.Ad;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.view.ViewCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.spaceup.Activities.AwardTest;
import com.spaceup.Activities.BaseActivity;
import com.spaceup.Activities.Scanning;
import com.spaceup.Alarm.AlarmService_Controller;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.Animations.ScanningOverlay;
import com.spaceup.ConfirmationPopupAdapter;
import com.spaceup.R;
import com.spaceup.RemoteConfig;
import com.spaceup.Runnable.AccRunnable;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityAutomation;
import com.spaceup.accessibility.AccessibilityCommunicator;
import com.spaceup.accessibility.AccessibilityCommunicator_ShortCut;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.app_services.app_Service;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.uninstall.adapters.IgnoreListAdapter;
import com.spaceup.uninstall.adapters.OptimizeListAdapter;
import com.spaceup.uninstall.adapters.QuickListAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


import static com.spaceup.AppShortcut.ShortCutFolder.MOVE_TO_MENU;
import static com.spaceup.apkgenerator.constants.Constants.LOAD_ADVERTISEMENT;
import static com.spaceup.apkgenerator.constants.Constants.WHITE_LIST_FILE_PATH;

import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan;
import io.github.inflationx.calligraphy3.TypefaceUtils;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class UninstallerActivity extends BaseActivity implements DataTransferInterface, View.OnClickListener, AppBarLayout.OnOffsetChangedListener {

    // Prevent Accessibility to perform click other then Stash
    public static int allowed = 0;                               // Prevent Accessibility to perform click other then Stash
    private final int suggested_apps = 10, selected_apps = 0;
    private final int OVERLAY_REQUEST_CODE = 300;
    //RAM Module
    private final float AVAILABLE_RAM_PROPORTION = 0.6f;
    public int total_apps_to_compress = 0;
    private String TAG = "UninstallerActivity";
    public ArrayList<String> toCompressAppList = new ArrayList<>();
    public ArrayList<String> toCompressAppName = new ArrayList<>();
    public ArrayList<AppInfo> compressAppList = new ArrayList<>();
    public ArrayList<AppInfo> confirmation_compressAppList = new ArrayList<>();
    private Dialog dialog_unknown;
    private Dialog tooManyAppsSelectedDialog;
    private TextView textView;
    //by default quick compress is set
    //private boolean mAdvanceCompression = false;
    private int activity_code = 0;
    private View overlayLayout;
    private WindowManager windowManager;
    private boolean backpress = true;

    private Thread accCheckThread, unknownThread;
    private AccRunnable accRunnable;
    private UnknownRunnable unknownRunnable;
    private TextView optimizeBtn;
    private LinearLayout selected_size_layout, popup_background, disabledLayout;
    // handles OnResume while awaiting Accessibility service
    private long suggested_apps_size = 0, selected_apps_size = 0;
    private int suggested_apps_count = 0, selected_apps_count = 0, upperSectionCount = 0, upper_forty_percent = 0, appSelected = 0;
    private UtilityMethods methods;
    private PrefManager pm;
    private PopupWindow popupMessage;
    private LinearLayout viewGroup;
    private InterstitialAd mInterstitialAd;

    private boolean globalUnknown, test = true;
    private CardView warning_card;


    private int dismissedOnce = 0;
    private View oView1;
    private WindowManager wm1;
    private BroadcastReceiver finishUnInstaller = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            UninstallerActivity.this.finish();

        }
    };
    private Bundle mGAParams;
    private RecyclerView optimizeAppsRecycler, ignoreAppsRecycler, quickcompress;
    private TextView txt_view_selected_apps, txt_app_selected, confirmation_optimize_btn;
    private ImageView back_btn, chevron_down;
    private RelativeLayout warningLayout, helpscreen;
    //    private RelativeLayout warning_message;
    //sajal's variable
    private LinearLayout ignoredAppsLayout;
    private CollapsingToolbarLayout collapse_toolbar;
    private OptimizeListAdapter optimizeAdapter;
    private QuickListAdapter quickAdapter;
    private IgnoreListAdapter ignoredAdapter;
    private List<AppInfo> applist = new ArrayList<>();
    private List<AppInfo> optimizeAppsList = new ArrayList<>();
    private List<AppInfo> ignoredAppsList = new ArrayList<>();

    private long mRamCleanSize;
    private ProgressBar hint_progress;
    private TextView mRamCleanSizeText, warning_text, warning_text1;
    private CheckBox mRamCleanCheckBox;
    private boolean mRamCleanSelected;

    private boolean mRamClean;
    private String mCompressionType;
    private String mFirstCompressionType;
    private CoordinatorLayout review_container;
    private ObjectAnimator circleSeekAnim;
    private com.facebook.ads.InterstitialAd adInterstitial;

    public static String getSizeinMB(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "");

        if (pre.equals("M"))
            return String.format("%.0f %sB", bytes / Math.pow(unit, exp), pre);

        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (globalUnknown && quickAdapter != null) {
            quickAdapter.notifyDataSetChanged();
        }
        if (UtilityMethods.getInstance().isHavingUnkownSourcePermission(getApplicationContext())) {
            try {
                dialog_unknown.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

    }

    private final CountDownTimer confirmationPopupCountDownTimer = new CountDownTimer(1000, 100) {

        int x = 0;

        @Override
        public void onTick(long millisUntilFinished) {

            switch (x) {
                case 0:
                    warning_text1.setText("Please \n ");
                    x++;
                    break;

                case 1:
                    warning_text1.setText("Please confirm \n ");
                    x++;
                    break;

                case 2:
                    warning_text1.setText("Please confirm the \n ");
                    x++;
                    break;

                case 3:
                    warning_text1.setText("Please confirm the apps \n");
                    x++;
                    break;

                case 4:
                    warning_text1.setText("Please confirm the apps you \n");
                    x++;
                    break;
                case 5:
                    warning_text1.setText("Please confirm the apps you \nwant ");
                    x++;
                    break;
                case 6:
                    warning_text1.setText("Please confirm the apps you \nwant to ");
                    x++;
                    break;
                case 7:
                    warning_text1.setText("Please confirm the apps you \nwant to uninstall");
                    x++;
                    break;

                default:
                    warning_text1.setText("Please confirm the apps you \nwant to uninstall");
                    x++;
                    break;

            }
        }

        @Override
        public void onFinish() {
            x = 0;
            warning_text1.setText("Please confirm the apps you \nwant to uninstall");
            disabledLayout.setVisibility(View.GONE);
        }
    };


    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Accessibility", "OnStart Called");
    }

    AppBarLayout appbar;
    LinearLayoutManager optimizeAppsLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AudienceNetworkAds.initialize(this);

//        if(RemoteConfig.getString(R.string.ad_network).equals("google")) {
//            AdRequest adRequest = new AdRequest.Builder().build();
//            InterstitialAd.load(getApplicationContext(), UtilityMethods.getInstance().getAdmobID(UtilityMethods.SCANNING_SCREEN), adRequest,
//                    new InterstitialAdLoadCallback() {
//                        @Override
//                        public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
//                            // The mInterstitialAd reference will be null until
//                            // an ad is loaded.
//                            mInterstitialAd = interstitialAd;
//                            if (mInterstitialAd != null) {
//
//                                mInterstitialAd.show(UninstallerActivity.this);
//                                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
//                                    @Override
//                                    public void onAdDismissedFullScreenContent() {
//                                        // Called when fullscreen content is dismissed.
//                                        Log.d("TAG", "The ad was dismissed.");
//                                    }
//
//                                    @Override
//                                    public void onAdFailedToShowFullScreenContent(AdError adError) {
//                                        // Called when fullscreen content failed to show.
//                                        Log.d("TAG", "The ad failed to show.");
//                                    }
//
//                                    @Override
//                                    public void onAdShowedFullScreenContent() {
//                                        // Called when fullscreen content is shown.
//                                        // Make sure to set your reference to null so you don't
//                                        // show it a second time.
//                                        mInterstitialAd = null;
//                                        Log.d("TAG", "The ad was shown.");
//                                    }
//                                });
//                            } else {
//                                Log.d("TAG", "The interstitial ad wasn't ready yet.");
//                            }
//                        }
//
//                        @Override
//                        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
//                            // Handle the error
//                            mInterstitialAd = null;
//                            Log.d("onAdFailedToLoad: ", loadAdError.getMessage().toString());
//                        }
//                    });
//        }else if(RemoteConfig.getString(R.string.ad_network).equals("facebook")){
//            loadFBAds();
//        }

        wm1 = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        /**
         * Analytics- How many users launched Uninstalling Activity
         */
        MOVE_TO_MENU = 0;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.uninstaller_v3);
        methods = UtilityMethods.getInstance();
        toCompressAppList.clear();
        init();
        init_noti();
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.REVIEW,
                AnalyticsConstants.Action.NORMAL_DROP,
                AnalyticsConstants.Label.FIRED,
                null,
                false,
                null);


        appbar = (AppBarLayout) findViewById(R.id.MyAppbar);
        appbar.addOnOffsetChangedListener(this);
        pm = PrefManager.getInstance(UninstallerActivity.this);
        optimizeAppsRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int firstVisiblePosition = optimizeAppsLayoutManager.findFirstCompletelyVisibleItemPosition();
                    if (firstVisiblePosition == 0) {
                        appbar.setExpanded(true, true);
                    }
                }
            }
        });


//        if (PrefManager.getInstance(getApplicationContext()).getPermissionFirst()) {
//            quickcompress.setVisibility(View.GONE);
//            PrefManager.getInstance(getApplicationContext()).setPermissionFirst();
//            Log.d("PERMISSION HANDLER", "INVISBLE");
//        }
//        if (UtilityMethods.getInstance().isHavingUnkownSourcePermission(getApplicationContext())) {
//            quickcompress.setVisibility(View.VISIBLE);
//        }
        if (UtilityMethods.getInstance().isHavingUnkownSourcePermission(getApplicationContext())) {
            globalUnknown = true;
        }


        //here we have got a sorted list of applications
        applist = getIntent().getParcelableArrayListExtra("userAppList");

        //atleast there are 10 apps minimum
        assert applist.size() >= 10 : " Not valid";
        if (applist == null) {
            return;
        }

        toCompressAppList.clear();
        toCompressAppName.clear();
        if (compressAppList != null)
            compressAppList.clear();

        boolean preSelect = RemoteConfig.getBoolean(R.string.preselect_apps);
        float upperMiddleIndex = 0f;
        int unCompressedApps = 0;
        for (AppInfo app : applist) {
            if (app.getCompressed_status() == Constants.UNCOMPRESSED) {
                unCompressedApps++;
            }
        }
        int financeApps = 0;
        if (Scanning.sFinanceAppCount == 0) {
            financeApps = ScanningOverlay.mFinanceAppCount;
        }

        optimizeBtn.setOnClickListener(null);
        int colorGrey = getResources().getColor(R.color.colorPrimaryDark); //The color GREY
        optimizeBtn.setBackgroundColor(colorGrey);
        optimizeBtn.setTextColor(getResources().getColor(R.color.colorPrimaryDark1));

        //TODO: financeApps with sajal help

        //unCompressedApps = unCompressedApps - financeApps;
        for (AppInfo app : applist) {
            //apps never compressed
            // and condition will exclude stash
            //show non compressed applications
            if (app.getCompressed_status() == Constants.UNCOMPRESSED) {
                //get top 10 apps and tick the checkbox
                float indexUpper = Math.round(unCompressedApps * 0.6f);
                upperMiddleIndex = Math.round(indexUpper * 0.4f);
                if (upperSectionCount < indexUpper) {
                    upperSectionCount++;

                    if (preSelect && appSelected < selected_apps) {
                        selected_apps_count++;
                        appSelected++;
                        total_apps_to_compress++;
                        app.setChecked(true);
                        toCompressAppList.add(app.getPkgName());
                        toCompressAppName.add(app.getAppName());
                        compressAppList.add(app);
                        selected_apps_size += app.getSize() + app.getCacheSize() + app.getCodeSize() + app.getApkSize();
                        mToolBar.setTitle(total_apps_to_compress + " Apps Selected (" + UtilityMethods.getSizeinMBOnly(selected_apps_size) + ")");
                        optimizeBtn.setText("UNINSTALL NOW (" + UtilityMethods.getSizeinMBOnly(selected_apps_size) + ")");

                    } else {
                        app.setChecked(false);
                    }
                    //comment this line to have null list
                    optimizeAppsList.add(app);
                    suggested_apps_size += app.getSize() + app.getCacheSize() + app.getCodeSize() + app.getApkSize();
                    suggested_apps_count++;
                } else {
                    app.setChecked(false);
                    optimizeAppsList.add(app);

                }
            }
        }



        //suggested apps will be 10, In future we will decided by a index value
        suggested_apps_count = optimizeAppsList.size();

        int warningTheme = RemoteConfig.getInt(R.string.warning_theme);
        optimizeAdapter = new

                OptimizeListAdapter(getApplicationContext(), optimizeAppsList, this, warningTheme, Math.round(upperMiddleIndex));
        Collections.sort(optimizeAppsList,AppInfo.Descending);
        optimizeAdapter.notifyDataSetChanged();

        //compression type from
        mCompressionType = RemoteConfig.getString(R.string.compression_type);
        Log.d("remote", "onCreate: mCompressionType " + mCompressionType);
        mFirstCompressionType = RemoteConfig.getString(R.string.first_compression_type);
        Log.d("remote", "onCreate: mFirstCompressionType " + mFirstCompressionType);
        quickAdapter = new

                QuickListAdapter(getApplicationContext(), optimizeAppsList, this, mCompressionType, mFirstCompressionType);


        ignoredAdapter = new

                IgnoreListAdapter(getApplicationContext(), ignoredAppsList, this);


        optimizeAppsLayoutManager = new

                LinearLayoutManager(this);

        RecyclerView.LayoutManager ignoreAppsLayoutManager = new LinearLayoutManager(getApplicationContext()) {
            @Override
            public boolean canScrollVertically() {

                return false;
            }
        };
        optimizeAppsRecycler.setLayoutManager(optimizeAppsLayoutManager);
        optimizeAppsRecycler.setHasFixedSize(true);
        optimizeAppsRecycler.setAdapter(optimizeAdapter);

        ignoreAppsRecycler.setLayoutManager(ignoreAppsLayoutManager);
        ignoreAppsRecycler.setHasFixedSize(true);

        //if we are unable to suggest any apps to compress
        if (optimizeAppsList.size() == 0) {

            Toast.makeText(this, "Unable to suggest any app!", Toast.LENGTH_SHORT).show();
        }

        IntentFilter accessibilityReceiverIntentFilter = new IntentFilter();
        accessibilityReceiverIntentFilter.addAction("com.times.finishUnInstallerActivity");
        LocalBroadcastManager.getInstance(this).

                registerReceiver(finishUnInstaller, accessibilityReceiverIntentFilter);

        //TODO to be tested
        ignoreAppsRecycler.setAdapter(ignoredAdapter);
        new

                AnalyticsHandler().

                logGAScreen(AnalyticsConstants.Screens.REVIEW);


        //Apsalar Analytics START
        JSONObject appsalarTracking = new JSONObject();
        try

        {
            appsalarTracking.put("review_test", 1);

        } catch (
                JSONException e)

        {
            e.printStackTrace();
        }
        Log.d(AnalyticsConstants.APSALARTAG, "REVIEW_TEST: " + appsalarTracking.toString());
        new

                AnalyticsHandler().

                logApsalarJsonEvent(AnalyticsConstants.Key.REVIEW_TEST, appsalarTracking);
        //Apsalar Analytics END


        //Hide the High Usage Apps By Default
        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate_chevron_down);
        chevron_down.startAnimation(rotation);
        rotation.setFillEnabled(true);
        rotation.setFillAfter(true);
        ignoreAppsRecycler.setVisibility(View.GONE);
        warningLayout.setVisibility(View.GONE);
        makeProgressBar(600, 300, 100);

    }

    private void init_noti() {
        AlarmService_Controller alarm = new AlarmService_Controller(getApplicationContext());
        alarm.Alarmat2PM();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void loadFBAds() {
        adInterstitial = new com.facebook.ads.InterstitialAd(this, "600591120962050_601833347504494");
        InterstitialAdListener interstitialAdListener = new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback
                Log.e( "ad displayed.","I am here");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                // Interstitial dismissed callback
                Log.e( "ad dismissed.","I am here");
            }
            @Override
            public void onError(Ad ad, com.facebook.ads.AdError adError) {
                Log.d("AD ERROR ",adError.getErrorMessage().toString());

            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Interstitial ad is loaded and ready to be displayed
                Log.d( "ad is loaded ","i am here");
                // Show the ad
                adInterstitial.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                Log.d( "ad clicked!","AD CLICKED");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d( "ad impression logged!","LOGGING IMPRESSION");
            }
        };

        // For auto play video ads, it's recommended to load the ad
        // at least 30 seconds before it is shown
        adInterstitial.loadAd(
                adInterstitial.buildLoadAdConfig()
                        .withAdListener(interstitialAdListener)
                        .build());
    }


    private void makeProgressBar(final int redprogress, final int greenProgress, final int blueBrogress) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                updateProgress(redBar, 0, redprogress);
                updateProgress(greenBar, 0, redprogress + greenProgress);
                updateProgress(blueBar, 0, redprogress + greenProgress + blueBrogress);

            }
        }, 600);
    }

    void updateProgress(ProgressBar progressBar, int from, int to) {
        ObjectAnimator circleSeekAnim = ObjectAnimator.ofInt(progressBar, "progress", from, to);
        circleSeekAnim.setDuration(1500);
        circleSeekAnim.setInterpolator(new DecelerateInterpolator());
        circleSeekAnim.start();

        circleSeekAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

    }

    private ProgressBar greenBar, blueBar, redBar;
    Toolbar mToolBar;

    private void init() {

        mToolBar = findViewById(R.id.toolbar);
        mToolBar.setTitle("0 Apps Selected");
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        card_toolbar = findViewById(R.id.card_toolbar);
        Intent intentService = new Intent(this, app_Service.class);
        intentService.putExtra("no_of_folders", "6");
        intentService.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startService(intentService);

        redBar = findViewById(R.id.red_bar);
        greenBar = findViewById(R.id.green_bar);
        blueBar = findViewById(R.id.blue_bar);

        review_container = (CoordinatorLayout) findViewById(R.id.review_container);
        popup_background = (LinearLayout) findViewById(R.id.popup_background);
        optimizeAppsRecycler = (RecyclerView) findViewById(R.id.list_optimize_apps);
        //  quickcompress = (RecyclerView) findViewById(R.id.list_optimize_apps1);
        // quickcompress.setNestedScrollingEnabled(false);
        ignoreAppsRecycler = (RecyclerView) findViewById(R.id.list_ignored_apps);
        optimizeAppsRecycler.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(optimizeAppsRecycler, false);
        ignoreAppsRecycler.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(ignoreAppsRecycler, false);
        optimizeBtn = (TextView) findViewById(R.id.optimize_btn);
        optimizeBtn.setOnClickListener(this);

        optimizeBtn.setBackgroundColor(getResources().getColor(R.color.colorBlue));
        int white = Color.parseColor("#ffffff"); //The color GREY
        optimizeBtn.setTextColor(white);


        txt_view_selected_apps = (TextView) findViewById(R.id.selected_apps);
        txt_app_selected = (TextView) findViewById(R.id.txt_app_selected);
        warning_text = (TextView) findViewById(R.id.warning_text);
        warning_card = (CardView) findViewById(R.id.warning_card);

        ignoredAppsLayout = (LinearLayout) findViewById(R.id.ignored_apps_layout);
        ignoredAppsLayout.setOnClickListener(this);
        warningLayout = (RelativeLayout) findViewById(R.id.warningLayout);
        warningLayout.setOnClickListener(this);

        selected_size_layout = (LinearLayout) findViewById(R.id.selected_size_layout);

        chevron_down = (ImageView) findViewById(R.id.chevron_down);
        collapse_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);

        mRamCleanSize = (long) (getOccupiedRamSize() * AVAILABLE_RAM_PROPORTION);
        AccessibilityAutomation.setRamCleanSize(mRamCleanSize);
        mRamCleanSizeText = (TextView) findViewById(R.id.ram_clean_size);
        mRamCleanSizeText.setText(String.valueOf(mRamCleanSize) + " MB");
        TextView tentative_size_saved = findViewById(R.id.tentative_size_saved);
        tentative_size_saved.setText(String.valueOf(UtilityMethods.getInstance().getSizeinMBZeroDecimalChar((mRamCleanSize * 1000000) + (Long.parseLong(AppDBHelper.getInstance(getApplicationContext()).totalCache()) + Long.parseLong(AppDBHelper.getInstance(getApplicationContext()).totalAppsSizes())), false)));
        TextView total_ram_size = findViewById(R.id.total_ram_size);
        total_ram_size.setText("RAM       :   " + String.valueOf(mRamCleanSize) + " MB");
        TextView total_cache_size = findViewById(R.id.total_cache_size);
        total_cache_size.setText("Cache     :   " + UtilityMethods.getInstance().getSizeinMBZeroDecimal(Long.parseLong(AppDBHelper.getInstance(getApplicationContext()).totalCache()), false));
        TextView total_apps = findViewById(R.id.total_apps);
        total_apps.setText("Apps       :   " + UtilityMethods.getInstance().getSizeinMBOnly(Long.parseLong(AppDBHelper.getInstance(getApplicationContext()).totalAppsSizes())));

        mRamCleanCheckBox = (CheckBox) findViewById(R.id.ram_clean_check);
        mRamClean = RemoteConfig.getBoolean(R.string.ram_clean);

        mRamCleanCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    mRamCleanSelected = true;
                } else {
                    mRamCleanSelected = false;

                }
                if (mRamCleanSelected) {
                    mToolBar.setTitle(total_apps_to_compress + " Apps Selected (" + UtilityMethods.getSizeinMBOnly(selected_apps_size + mRamCleanSize * 1024 * 1024) + ")");
                    setSupportActionBar(mToolBar);
                    optimizeBtn.setText("UNINSTALL NOW (" + UtilityMethods.getSizeinMBOnly(selected_apps_size) + ")");


                } else {
                    mToolBar.setTitle(total_apps_to_compress + " Apps Selected");
                    setSupportActionBar(mToolBar);
                    mToolBar.setTitle(total_apps_to_compress + " Apps Selected (" + UtilityMethods.getSizeinMBOnly(selected_apps_size) + ")");
                    optimizeBtn.setText("UNINSTALL NOW (" + UtilityMethods.getSizeinMBOnly(selected_apps_size) + ")");
                }
            }
        });
        if (mRamClean) {
            mRamCleanCheckBox.setChecked(true);
        } else {
            mRamCleanCheckBox.setChecked(false);

        }
    }

    private long getOccupiedRamSize() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        return (mi.totalMem - mi.availMem) / 1048576L;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Uninstall", "onDestroy: called by sajal");
        PrefManager.getInstance(getApplicationContext()).setisCompresssingFirst(false);
        Log.d("UNINSTALLER", "setisCompresssingFirst" + false);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(finishUnInstaller);
        PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.IS_WARNING_ACCEPTED_IN_SESSION, false);
        PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.SENT_FIRST_TIME_REVIEW_SCREEN, true);
        PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.SENT_FIRST_TIME_REVIEW_ACCESSIBILITY_EVENT, true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Unins", "onStop: called by sajal");
        Log.d("Accessibility", "OnStop Called");
    }

    @Override
    protected void onPause() {

        super.onPause();
        Log.d("DHRUV", "ON PAUSE");

    }

    public void showCustomAlert() {

        Context context = getApplicationContext();
        // Create layout inflator object to inflate toast.xml file
        LayoutInflater inflater = getLayoutInflater();

        // Call toast.xml file for toast layout
        View toastRoot = inflater.inflate(R.layout.activity_accessibity_hint, null);
        Toast toast = new Toast(context);
        // Set layout to toast
        toast.setView(toastRoot);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.FILL_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }


    @Override
    public void setValues(final AppInfo app, final int action, final int pos) {
        int index = -1;

        switch (action) {
            // action : unchecked item from optimized apps list

            case 5:


                if (optimizeAppsList.contains(app)) {
                    index = optimizeAppsList.indexOf(app);
                    optimizeAppsList.get(index).setChecked(false);
                    //adding +1 to comply with header position
                    optimizeAdapter.notifyItemChanged(index + 1);
                } else if (ignoredAppsList.contains(app)) {
                    index = ignoredAppsList.indexOf(app);
                    ignoredAppsList.get(index).setChecked(false);
                    ignoredAdapter.notifyItemChanged(index);
                } else {
                    Log.d("Index", "" + index + " app name " + app.getAppName() + " status " + app.isChecked());
                }
            case 1:
                // action : unchecked item from ignored list
            case 3:

                compressAppList.remove(app);
                total_apps_to_compress--;
                toCompressAppList.remove(app.getPkgName());
                toCompressAppName.remove(app.getAppName());
                // compressAppList.remove(app);
                selected_apps_count -= 1;
                selected_apps_size = selected_apps_size - (app.getApkSize() + app.getCacheSize() + app.getCodeSize() + app.getSize());
                if (selected_apps_count < 0) {
                    selected_apps_count = 0;
                } else if (selected_apps_size < 0) {
                    selected_apps_size = 0;
                }
                if (mRamCleanSelected) {
                    mToolBar.setTitle(total_apps_to_compress + " Apps Selected (" + UtilityMethods.getSizeinMBOnly(selected_apps_size + mRamCleanSize * 1024 * 1024) + ")");
                    setSupportActionBar(mToolBar);
                    optimizeBtn.setText("UNINSTALL NOW (" + UtilityMethods.getSizeinMBOnly(selected_apps_size) + ")");

                } else {

                    mToolBar.setTitle(total_apps_to_compress + " Apps Selected");
                    setSupportActionBar(mToolBar);
                    mToolBar.setTitle(total_apps_to_compress + " Apps Selected (" + UtilityMethods.getSizeinMBOnly(selected_apps_size) + ")");
                    optimizeBtn.setText("UNINSTALL NOW (" + UtilityMethods.getSizeinMBOnly(selected_apps_size) + ")");
                }
                if (total_apps_to_compress == 0) {
                    optimizeBtn.setOnClickListener(null);
                    int colorGrey = getResources().getColor(R.color.colorPrimaryDark); //The color GREY
                    optimizeBtn.setBackgroundColor(colorGrey);
                    optimizeBtn.setTextColor(getResources().getColor(R.color.colorPrimaryDark1));

                }


                break;
            // action : checked item from optimized apps list

            case 6:

                if (optimizeAppsList.contains(app)) {
                    index = optimizeAppsList.indexOf(app);
                    optimizeAppsList.get(index).setChecked(true);
                    //adding +1 to comply with header position
                    optimizeAdapter.notifyItemChanged(index + 1);
                } else if (ignoredAppsList.contains(app)) {
                    index = ignoredAppsList.indexOf(app);
                    ignoredAppsList.get(index).setChecked(true);
                    ignoredAdapter.notifyItemChanged(index);
                } else {
                    Log.d("Index", "" + index + " app name " + app.getAppName() + " status " + app.isChecked());
                }

            case 2:
                //action : checked an item from ignore list
            case 4:
                compressAppList.add(app);

                try {
                    if (compressAppList.size() == 1) {
                        try {
                            textView.setOnClickListener(this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        int white = Color.parseColor("#ffffff"); //The color GREY
                        try {
                            textView.setTextColor(white);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }


                total_apps_to_compress++;
                toCompressAppList.add(app.getPkgName());
                toCompressAppName.add(app.getAppName());
                //compressAppList.add(app);
                if (total_apps_to_compress > 0) {
                    optimizeBtn.setOnClickListener(this);
                    optimizeBtn.setBackgroundColor(getResources().getColor(R.color.colorBlue));
                    optimizeBtn.setTextColor(getResources().getColor(R.color.white));

                }

                selected_apps_count += 1;
                selected_apps_size = selected_apps_size + (app.getApkSize() + app.getCacheSize() + app.getCodeSize() + app.getSize());
                if (mRamCleanSelected) {
                    mToolBar.setTitle(total_apps_to_compress + " Apps Selected (" + UtilityMethods.getSizeinMBOnly(selected_apps_size + mRamCleanSize * 1024 * 1024) + ")");
                    setSupportActionBar(mToolBar);
                    optimizeBtn.setText("UNINSTALL NOW (" + UtilityMethods.getSizeinMBOnly(selected_apps_size) + ")");

                } else {

                    mToolBar.setTitle(total_apps_to_compress + " Apps Selected");
                    setSupportActionBar(mToolBar);
                    mToolBar.setTitle(total_apps_to_compress + " Apps Selected (" + UtilityMethods.getSizeinMBOnly(selected_apps_size) + ")");
                    optimizeBtn.setText("UNINSTALL NOW (" + UtilityMethods.getSizeinMBOnly(selected_apps_size) + ")");
                }


                if (toCompressAppList.size() == 8 && PrefManager.getInstance(UninstallerActivity.this).isTooManyAppsReminder()) {

                    if (null == tooManyAppsSelectedDialog) {
                        tooManyAppsSelectedDialog = new Dialog(this, R.style.Dialog);
                    }
                    tooManyAppsSelectedDialog.setContentView(R.layout.vulnerable_apps_dialog);
                    TextView dialogButtonOk = (TextView) tooManyAppsSelectedDialog.findViewById(R.id.dialogButtonOK);
                    CheckBox chkbox = (CheckBox) tooManyAppsSelectedDialog.findViewById(R.id.check_box);
                    chkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                          @Override
                                                          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                              Log.d("reminder", "onCheckedChanged: " + (isChecked));
                                                              PrefManager.getInstance(UninstallerActivity.this).setTooManyAppsReminder(isChecked);
                                                          }
                                                      }
                    );
                    // if button is clicked, close the custom dialog and item remains selected
                    dialogButtonOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            tooManyAppsSelectedDialog.dismiss();
                        }
                    });
                    TextView dialogButtonCancel = (TextView) tooManyAppsSelectedDialog.findViewById(R.id.dialogButtonCancel);
                    //if button is clicked, close the custom dialog
                    dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d("Steve", "onClick: " + pos);
                            app.setChecked(false);
                            //here we need to change only one adapter but because we have only one switch case
                            // for handling check events we cannot do so now therefore applying try catch to avoid
                            // index out of bound exception
                            try {
                                if (action == 4) {
                                    ignoredAdapter.notifyItemChanged(pos);
                                    setValues(app, 3, pos);
                                }
                                if (action == 2) {
                                    optimizeAdapter.notifyItemChanged(pos);
                                    setValues(app, 1, pos);
                                }
                            } catch (Exception e) {
                                Log.d("reminder", Log.getStackTraceString(e));
                            }
                            tooManyAppsSelectedDialog.dismiss();
                        }
                    });
                    WindowManager.LayoutParams lp = tooManyAppsSelectedDialog.getWindow().getAttributes();
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.gravity = Gravity.CENTER_VERTICAL;

                    tooManyAppsSelectedDialog.getWindow().setAttributes(lp);

                    tooManyAppsSelectedDialog.show();


                    return;

                }

                break;

        }


    }

    @Override
    public void setUnknownTracker(boolean status) {

        Log.d("UNKNOWN_TRACKER", "CLICKED" + status);
        globalUnknown = status;

        if (status && !UtilityMethods.getInstance().isHavingUnkownSourcePermission(getApplicationContext())) {
            // get Unknown Sources permission
            openUnknownHelper();

        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ignored_apps_layout:

                if (ignoreAppsRecycler.getVisibility() == View.GONE) {

                    Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate_chevron_up);
                    chevron_down.startAnimation(rotation);
                    rotation.setFillEnabled(true);
                    rotation.setFillAfter(true);

                    ignoreAppsRecycler.setVisibility(View.VISIBLE);
                    if (ignoredAppsList.size() > 0) {
                        warningLayout.setVisibility(View.VISIBLE);
                        int warningTheme = RemoteConfig.getInt(R.string.warning_theme);
                        if (warningTheme == Constants.WARNING_THEME_RED) {
                            warningLayout.findViewById(R.id.warning_layout).setBackgroundColor(getResources().getColor(R.color.warning_background_red));
                            ((ImageView) warningLayout.findViewById(R.id.icon_warning)).setImageDrawable(getResources().getDrawable(R.drawable.uninstall_warning_white));
                            ((TextView) warningLayout.findViewById(R.id.textView7)).setTextColor(getResources().getColor(R.color.white));
                        }
                    }
                } else if (ignoreAppsRecycler.getVisibility() == View.VISIBLE) {
                    Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate_chevron_down);
                    chevron_down.startAnimation(rotation);
                    rotation.setFillEnabled(true);
                    rotation.setFillAfter(true);
                    ignoreAppsRecycler.setVisibility(View.GONE);
                    warningLayout.setVisibility(View.GONE);
                }

                break;
            case R.id.optimize_btn:


                showPopup();


                break;
            case R.id.back_btn:


                onBackPressed();

                break;

            case R.id.helpscreen_timer:

                helpscreen.setVisibility(View.GONE);

                break;

            case R.id.confirmation_optimize_btn:
                if (needTOWatch) {
                    Intent video = new Intent(this, AwardTest.class);
                    startActivityForResult(video, 1234);
                    return;
                }
                starCompressionAfterAdvert();
                break;
            default:
        }
    }

    private void starCompressionAfterAdvert() {
        if (UtilityMethods.isAppUpdateGoingOn()) {
            Toast.makeText(this, this.getString(R.string.app_updating_msg), Toast.LENGTH_SHORT).show();
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                whiteListApps();
            }
        }).start();


        AccessibilityAutomation.setCleanRam(false);


        Bundle paramBundle = new Bundle();
        paramBundle.putString(AnalyticsConstants.Params.PERMISSION, "val1");
        //TODO
        paramBundle.putString(AnalyticsConstants.Params.RUN_COUNT, "" + PrefManager.getInstance(UninstallerActivity.this).getAppRunCount());
        new AnalyticsHandler().logEvent(AnalyticsConstants.Event.COMPRESS_PRESSED, paramBundle);

        if (methods.isHavingUnkownSourcePermission(getApplicationContext())) {


            // Analytics Start

            new AnalyticsController(getApplicationContext()).sendAnalytics(
                    AnalyticsConstants.Category.PERMISSION,
                    AnalyticsConstants.Action.UNKNOWN_SOURCES,
                    AnalyticsConstants.Label.EXIST,
                    null,
                    true, PrefManager.UNKNOW_SOURCE_EXIST);

        }


        if (!methods.isAccessibilityServiceRunning(UninstallerActivity.this)) {
            openAccessibilityHelper();

        } else {

            // Analytics Start
            mGAParams = new Bundle();
            mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
            mGAParams.putString("action", AnalyticsConstants.Action.ACCESSIBILITY + "_" + AnalyticsConstants.Label.EXIST);
            mGAParams.putString("label", AnalyticsConstants.Label.REVIEW_SCREEN);
            mGAParams.putLong("value", 1);
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

            if (globalUnknown && methods.isHavingUnkownSourcePermission(UninstallerActivity.this)) {

                //Perform Long Compression
                uninstallNew();
                //mAdvanceCompression = true;
                //showPopup();
            } else if (dismissedOnce == 1 || !methods.isHavingUnkownSourcePermission(UninstallerActivity.this)) {

                unknown_bottom_helper();
            } else {
                final Dialog dialog = new Dialog(UninstallerActivity.this, R.style.Dialog1);
                dialog.setContentView(R.layout.popup_turn_on_advance_compression);
                TextView later = (TextView) dialog.findViewById(R.id.textView28);
                later.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        shortCut();
                        //mAdvanceCompression = false;
                        //showPopup();
                        //Perform Quick Compression
                        dialog.cancel();
                    }
                });
                TextView advance = (TextView) dialog.findViewById(R.id.textView281);
                advance.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        globalUnknown = true;
                        activity_code = 1;
                        unknown_bottom_helper();
                        //Perform Quick Compression
                        dialog.cancel();
                    }
                });
                dialog.setCanceledOnTouchOutside(false);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                dialog.show();


            }

        }

        closeConfirmationPopup();

    }

    public void getPermissionDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.xiaomi_permission_layout, null);
        dialogBuilder.setView(dialogView);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        TextView enableOverlayPermission = (TextView) dialogView.findViewById(R.id.overlay_permission_btn);
        TextView enableAccessibilityPermission = (TextView) dialogView.findViewById(R.id.accessibility_permission_btn);

        enableOverlayPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent overlayIntent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
                overlayIntent.setClassName("com.miui.securitycenter", "com.miui.permcenter.permissions.AppPermissionsEditorActivity");
                overlayIntent.putExtra("extra_pkgname", getPackageName());
                try {
                    startActivityForResult(overlayIntent, OVERLAY_REQUEST_CODE);
                    alertDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        enableAccessibilityPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAccessibilityHelper();
                alertDialog.dismiss();
            }
        });

    }

    private void shortCut() {
        PrefManager.getInstance(getApplicationContext()).setisCompresssingFirst(true);
        Log.d("UNINSTALLER", "setisCompresssingFirst" + true);

        if (!UtilityMethods.isAccessibilityServiceEnabled(getApplicationContext(), AccessibilityAutomation.class)) {
            showCustomAlert();
            Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
            startActivity(intent);
            AccessibilityAutomation.getSharedInstance().allowSpecialBack();

        } else {
            //Analytics start
            //Shortcut Analytics
            int recom_apps_unselected = 0;
            int suggested_apps_selected = 0;
            int index = 0;
            for (AppInfo appInfo : optimizeAppsList) {
                if (index < selected_apps && !appInfo.isChecked()) {
                    recom_apps_unselected++;
                }
                if (appInfo.isChecked()) {
                    suggested_apps_selected++;
                }
                index++;
            }


            // Start TIme to calculate execution time of compression
            new AnalyticsHandler().startTimeStamp();
            mGAParams = new Bundle();
            mGAParams.putString("category", AnalyticsConstants.Category.COMPRESS);
            mGAParams.putString("action", AnalyticsConstants.Action.COMPRESS_DETAILS);

            mGAParams.putString("label", AnalyticsConstants.Label.APPS_SELECTED);
            mGAParams.putLong("value", (long) toCompressAppList.size());
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

            mGAParams.putString("label", AnalyticsConstants.Label.APPS_FROM_PRE_SELECTED);
            mGAParams.putLong("value", (long) (selected_apps - recom_apps_unselected));
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

            mGAParams.putString("label", AnalyticsConstants.Label.APPS_FROM_SUGGESTED);
            mGAParams.putLong("value", (long) suggested_apps_selected);
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());


            mGAParams.putString("label", AnalyticsConstants.Label.REVIEW_UNSELECT);
            mGAParams.putLong("value", (long) (confirmation_compressAppList.size() - toCompressAppList.size()));
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

            mGAParams.putString("action", AnalyticsConstants.Action.COMPRESS_TAP);
            mGAParams.putString("label", AnalyticsConstants.Label.QUICK_COM_FLAG);
            mGAParams.putLong("value", 1);


            mGAParams.putInt("cd", AnalyticsConstants.CustomDim.COMPRESSION_TYPE);
            mGAParams.putString("cd_value", AnalyticsConstants.CustomDimValue.QUICK);

            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());


            //Analytics stop
            Intent intent = new Intent(UninstallerActivity.this, AccessibilityCommunicator_ShortCut.class);
            intent.putStringArrayListExtra("compressList", toCompressAppList);
            intent.putStringArrayListExtra("compressName", toCompressAppName);
            intent.putExtra("state", "shortcut_app");
            startService(intent);
        }
    }

    void uninstallNew() {
        if (optimizeAppsList.size() + ignoredAppsList.size() == 0) {
            Toast.makeText(this, "No apps available to compress! Please check back later!", Toast.LENGTH_SHORT).show();
            return;
        }

        //Preference to  store first time compression ever for this user

        long savedSpace = 0;
        if (mRamCleanSelected) {
            savedSpace = selected_apps_size + mRamCleanSize * 1024 * 1024;

        } else {

            savedSpace = selected_apps_size;

        }
        if(PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.TOTAL_SPACE_SAVED) == 0){
            PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.TOTAL_SPACE_SAVED, savedSpace);
        }else{
            long totalAppsize = PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.TOTAL_SPACE_SAVED);
            PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.TOTAL_SPACE_SAVED, Integer.parseInt(String.valueOf(totalAppsize) + savedSpace));
        }
        PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.TOTAL_SPACE_SAVED, savedSpace);
        Log.d("uninstallNew: ",String.valueOf(PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.TOTAL_SPACE_SAVED)));
        PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.TOTAL_APP_SPACE_SAVED, (Long.parseLong(AppDBHelper.getInstance(getApplicationContext()).totalAppsSizes()) / (optimizeAppsList.size() + ignoredAppsList.size())));
        PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.TOTAL_CACHE_SAVED, (Long.parseLong(AppDBHelper.getInstance(getApplicationContext()).totalCache()) / (optimizeAppsList.size() + ignoredAppsList.size())));
        PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.TOTAL_RAM_SAVED, mRamCleanSize * 1024 * 1024);

        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.REVIEW,
                AnalyticsConstants.Action.DROP_AFTER_PROCEED,
                AnalyticsConstants.Label.CLICKED,
                null,
                false,
                null);

        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.REVIEW,
                AnalyticsConstants.Action.NORMAL_DROP,
                AnalyticsConstants.Label.CLICKED,
                null,
                false,
                null);
        PrefManager.getInstance(getApplicationContext()).setisCompresssingFirst(true);
        Log.d("UNINSTALLER", "setisCompresssingFirst" + true);
        if (!UtilityMethods.isAccessibilityServiceEnabled(getApplicationContext(), AccessibilityAutomation.class)) {
            showCustomAlert();
            Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
            AccessibilityAutomation.getSharedInstance().allowSpecialBack();

            startActivity(intent);

        } else {

            //Analytics start
            //counter variable

            int recom_apps_unselected = 0;
            int suggested_apps_selected = 0;
            long selectedAppSize = 0;
            int index = 0, checkCount = 0;
            for (AppInfo appInfo : optimizeAppsList) {
                if (index < selected_apps && !appInfo.isChecked()) {
                    recom_apps_unselected++;
                }
                if (appInfo.isChecked()) {
                    suggested_apps_selected++;
                    selectedAppSize += appInfo.getSize() + appInfo.getCacheSize() + appInfo.getCodeSize() + appInfo.getApkSize();
                    if (checkCount == 0) {
                        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.REVIEW,
                                AnalyticsConstants.Action.APP_POSITION,
                                AnalyticsConstants.Label.LONE,
                                String.valueOf(index),
                                false,
                                null);
                    }
                    if (checkCount == 1) {
                        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.REVIEW,
                                AnalyticsConstants.Action.APP_POSITION,
                                AnalyticsConstants.Label.LTWO,
                                String.valueOf(index),
                                false,
                                null);
                    }
                    if (checkCount == 2) {
                        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.REVIEW,
                                AnalyticsConstants.Action.APP_POSITION,
                                AnalyticsConstants.Label.LTHREE,
                                String.valueOf(index),
                                false,
                                null);
                    }
                    if (checkCount == 3) {
                        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.REVIEW,
                                AnalyticsConstants.Action.APP_POSITION,
                                AnalyticsConstants.Label.LFOUR,
                                String.valueOf(index),
                                false,
                                null);
                    }
                    if (checkCount == 4) {
                        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.REVIEW,
                                AnalyticsConstants.Action.APP_POSITION,
                                AnalyticsConstants.Label.LFIVE,
                                String.valueOf(index),
                                false,
                                null);
                    }

                    checkCount++;

                }
                index++;

            } //Analytics
            // Start TIme to calculate execution time of compression
            new AnalyticsHandler().startTimeStamp();
            Bundle paramBundle = new Bundle();
            paramBundle.putString(AnalyticsConstants.Params.APPS_SELECTED, "" + toCompressAppList.size());
            paramBundle.putString(AnalyticsConstants.Params.RECOM_APPS_UNSELECTED, "" + recom_apps_unselected);
            paramBundle.putString(AnalyticsConstants.Params.SUGGESTED_APPS_SELECTED, "" + suggested_apps_selected);
            new AnalyticsHandler().logEvent(AnalyticsConstants.Event.COMPRESSING, paramBundle);

            mGAParams = new Bundle();
            mGAParams.putString("category", AnalyticsConstants.Category.COMPRESS);
            mGAParams.putString("action", AnalyticsConstants.Action.COMPRESS_DETAILS);

            mGAParams.putString("label", AnalyticsConstants.Label.APPS_SELECTED);
            mGAParams.putLong("value", (long) toCompressAppList.size());
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
            if (PrefManager.getInstance(getApplicationContext()).getLong(PrefManager.FIRST_TIME_COMPRESS_TIME) == 0) {
                PrefManager.getInstance(getApplicationContext()).putInt(PrefManager.APP_COMPRESSED, toCompressAppList.size());
                PrefManager.getInstance(getApplicationContext()).putLong(PrefManager.FIRST_TIME_COMPRESS_TIME, System.currentTimeMillis());
            }

            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.COMPRESS,
                    AnalyticsConstants.Action.TOTAL_COMPRESSED,
                    null,
                    String.valueOf(toCompressAppList.size()),
                    false,
                    null);

            mFinalSelectedAppafterPopup = toCompressAppList.size();
            int unSelectedApp = mPreviousSelectedAppBeforePopup - mFinalSelectedAppafterPopup;
            if (unSelectedApp != 0) {
                new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.REVIEW,
                        AnalyticsConstants.Action.APPS_DESELECTED,
                        null,
                        String.valueOf(unSelectedApp),
                        false,
                        null);
            }
            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.REVIEW,
                    AnalyticsConstants.Action.APPS_COMPRESSED,
                    null,
                    String.valueOf(toCompressAppList.size()),
                    false,
                    null);


            long sizeInMB = selectedAppSize / (1000000);

            new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.COMPRESS,
                    AnalyticsConstants.Action.SPACE_SAVED,
                    null,
                    String.valueOf(sizeInMB),
                    false,
                    null);


            mGAParams.putString("label", AnalyticsConstants.Label.APPS_FROM_PRE_SELECTED);
            mGAParams.putLong("value", (long) (selected_apps - recom_apps_unselected));
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

            mGAParams.putString("label", AnalyticsConstants.Label.APPS_FROM_SUGGESTED);
            mGAParams.putLong("value", (long) suggested_apps_selected);
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

            mGAParams.putString("label", AnalyticsConstants.Label.REVIEW_UNSELECT);
            mGAParams.putLong("value", (long) (confirmation_compressAppList.size() - toCompressAppList.size()));
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

            mGAParams.putString("action", AnalyticsConstants.Action.COMPRESS_TAP);
            mGAParams.putString("label", AnalyticsConstants.Label.ADV_COM_FLAG);
            mGAParams.putLong("value", 1);

            mGAParams.putInt("cd", AnalyticsConstants.CustomDim.COMPRESSION_TYPE);
            mGAParams.putString("cd_value", AnalyticsConstants.CustomDimValue.ADV);
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
            //Analytics stop

            //Advcance Started
            Intent intent = new Intent(UninstallerActivity.this, AccessibilityCommunicator.class);
            intent.putStringArrayListExtra("compressList", toCompressAppList);
            intent.putStringArrayListExtra("compressName", toCompressAppName);
            intent.putExtra("state", "multiple_app");
            startService(intent);
        }
    }

    private void whiteListApps() {
        Set<String> apps = new LinkedHashSet<>();
        boolean otherAppSelected = false;
        for (int index = selected_apps; index < optimizeAppsList.size(); index++) {
            if (optimizeAppsList.get(index).isChecked()) {
                otherAppSelected = true;
                break;
            }
        }
        if (!otherAppSelected) {
            for (int index = 0; index < ignoredAppsList.size(); index++) {
                if (ignoredAppsList.get(index).isChecked()) {
                    otherAppSelected = true;
                    break;
                }
            }
        }
        if (otherAppSelected) {
            for (int index = 0; index < suggested_apps && index < optimizeAppsList.size(); index++) {
                if (!optimizeAppsList.get(index).isChecked()) {
                    apps.add(optimizeAppsList.get(index).getPkgName());
                }
            }
        } else {
            for (int index = 0; index < selected_apps && index < optimizeAppsList.size(); index++) {
                if (!optimizeAppsList.get(index).isChecked()) {
                    apps.add(optimizeAppsList.get(index).getPkgName());
                }
            }

        }
        if (!apps.isEmpty()) {
            if (Scanning.sFinanceApps != null && !Scanning.sFinanceApps.isEmpty()) {
                apps.addAll(Scanning.sFinanceApps);
            }
            if (ScanningOverlay.sFinanceApps != null && !ScanningOverlay.sFinanceApps.isEmpty()) {
                apps.addAll(ScanningOverlay.sFinanceApps);

            }
            File file = new File(Environment.getExternalStorageDirectory().getPath() + Constants.STASH + WHITE_LIST_FILE_PATH);
            if (file.exists()) {
                try {
                    FileOutputStream fos = new FileOutputStream(file, false);
                    for (String appName : apps) {
                        fos.write("\n".getBytes());
                        fos.write((appName).getBytes());
                        fos.write("\n".getBytes());
                    }
                    fos.close();
                } catch (IOException e) {
                    Log.d(TAG, e.getMessage(), e);
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1234) {
            if (data == null) {
                return;
            }
            String message = data.getStringExtra("MESSAGE");

            if ("SUCCESS".equals(message)) {
//                if (!getPackageManager().canRequestPackageInstalls()) {
//                    startActivity(new Intent(android.provider.Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES, Uri.parse("package:com.spaceup")));
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            starCompressionAfterAdvert();
//                        }
//                    },10000);
//                } else {
//            }
                    starCompressionAfterAdvert();
                }

            }
        switch (requestCode) {
            case 55:
                String label = AnalyticsConstants.Label.REVIEW_SCREEN;
                if (PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.SENT_FIRST_TIME_REVIEW_ACCESSIBILITY_EVENT)) {
                    label = AnalyticsConstants.Label.REVIEW_SCREEN_FIRST;
                    //value changes on onDestroy
                }
                new AnalyticsController(getApplicationContext()).sendAnalytics(
                        AnalyticsConstants.Category.PERMISSION,
                        AnalyticsConstants.Action.ACCESSIBILITY,
                        label,
                        String.valueOf(1),
                        false,
                        null);
                if (null != accRunnable) {
                    Log.d("backdebug", "stopping acc runnable");
                    accRunnable.stop();
                }
                Log.d("backdebug", "inside finish 55 ");

                try {
                    windowManager.removeView(overlayLayout);
                } catch (Exception e) {

                }
                if (methods.isAccessibilityServiceRunning(UninstallerActivity.this)) {
                    // Analytics Start
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
                    mGAParams.putString("action", AnalyticsConstants.Action.ACCESSIBILITY);

                    mGAParams.putString("label", AnalyticsConstants.Label.REVIEW_SCREEN);
                    mGAParams.putLong("value", 1);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                    // Analytics end

                    if (globalUnknown && methods.isHavingUnkownSourcePermission(UninstallerActivity.this)) {
                        uninstallNew();
                        //mAdvanceCompression = true;
                        //showPopup();
                    } else {
                        unknown_bottom_helper();
                        //mAdvanceCompression = false;
                        //showPopup();
                    }
                } else {
                    // Analytics Start
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
                    mGAParams.putString("action", AnalyticsConstants.Action.ACCESSIBILITY);

                    mGAParams.putString("label", AnalyticsConstants.Label.REVIEW_SCREEN);
                    mGAParams.putLong("value", 0);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                    // Analytics end

                }
                break;
            case 33:
                if (unknownRunnable != null) {
                    unknownRunnable.stop();
                }
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {

                }
                label = AnalyticsConstants.Label.REVIEW_SCREEN;
                if (!PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.SENT_FIRST_TIME_REVIEW_SCREEN)) {
                    label = AnalyticsConstants.Label.REVIEW_SCREEN_FIRST;
                    //changes on onDestroy
                }
                if (methods.isHavingUnkownSourcePermission(UninstallerActivity.this)) {
                    // Analytics Start
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
                    mGAParams.putString("action", AnalyticsConstants.Action.UNKNOWN_SOURCES);
                    mGAParams.putString("label", label);
                    mGAParams.putLong("value", 1);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                    // Analytics end
                } else {
                    // Analytics Start
                    mGAParams = new Bundle();
                    mGAParams.putString("category", AnalyticsConstants.Category.PERMISSION);
                    mGAParams.putString("action", AnalyticsConstants.Action.UNKNOWN_SOURCES);

                    mGAParams.putString("label", label);
                    mGAParams.putLong("value", 0);
                    new AnalyticsHandler().logGAEvent(mGAParams);
                    Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                    // Analytics end
                }
                if (methods.isAccessibilityServiceRunning(UninstallerActivity.this)) {
                    if (methods.isHavingUnkownSourcePermission(UninstallerActivity.this)) {
                        uninstallNew();
                        //mAdvanceCompression = true;
                        //showPopup();
                    } else {
                        unknown_bottom_helper();
                        //mAdvanceCompression = false;
                        //showPopup();
                    }
                }
                break;
            case OVERLAY_REQUEST_CODE:

                getPermissionDialog();

                break;

            default:

        }
    }

    @Override
    public void onBackPressed() {

        if (popup_background.getVisibility() == View.VISIBLE) {
            backpress = false;
            closeConfirmationPopup();
        } else {
            if (!AccessibilityAutomation.getAllowed() && backpress)
                super.onBackPressed();
        }

    }

    int globalVerticalOffset = -1;


    public void openAccessibilityHelper() {


        try {
            windowManager.removeView(overlayLayout);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String label = AnalyticsConstants.Label.REVIEW_SCREEN;
        if (PrefManager.getInstance(getApplicationContext()).getBoolean(PrefManager.SENT_FIRST_TIME_REVIEW_ACCESSIBILITY_EVENT)) {
            label = AnalyticsConstants.Label.REVIEW_SCREEN_FIRST;
            //value changes on onDestroy
        }
        new AnalyticsController(getApplicationContext()).sendAnalytics(
                AnalyticsConstants.Category.PERMISSION,
                AnalyticsConstants.Action.ACCESSIBILITY,
                label,
                String.valueOf(0),
                false,
                null);

        LayoutInflater accessibilityHelper = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        overlayLayout = accessibilityHelper.inflate(R.layout.accessibility_permission, null);
        WindowManager.LayoutParams params1;
        TextView span_text_view = (TextView) overlayLayout.findViewById(R.id.span_text_view);
        TextView permission_helper_text = (TextView) overlayLayout.findViewById(R.id.permission_helper_text);
        String first = "";
        String second = "\uD83C\uDF1F SpaceUp \uD83C\uDF1F";
        final String totalString = first + second;
        Spannable spanText = new SpannableString(totalString);
        //set color to white
        spanText.setSpan(new ForegroundColorSpan(getResources()
                .getColor(R.color.spaceup_black)), first.length(), first.length() + second.length(), 0);
        //set text to bradon medium
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon_medium.ttf"));
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spanText.setSpan(typefaceSpan, first.length(), first.length() + second.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        span_text_view.setText(spanText, TextView.BufferType.SPANNABLE);

        String f = "Give permission to ";
        String s = "save upto 1.2 GB";
        final String t = f + s;
        Spannable spannableText = new SpannableString(t);
        //set color to white
        spannableText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(UninstallerActivity.this, R.color.text_color)), f.length(), f.length() + s.length(), 0);
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spannableText.setSpan(typefaceSpan, f.length(), f.length() + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        permission_helper_text.setText(spannableText, TextView.BufferType.SPANNABLE);

        ImageView cancel = (ImageView) overlayLayout.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    windowManager.removeView(overlayLayout);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        params1 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);
        params1.gravity = Gravity.BOTTOM;


        windowManager = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        try {
            windowManager.addView(overlayLayout, params1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        accRunnable = new AccRunnable(windowManager, overlayLayout, this);
        accCheckThread = new Thread(accRunnable, "acc");
        accCheckThread.start();


        Intent accSettingsIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        startActivityForResult(accSettingsIntent, 55);
        AccessibilityAutomation.getSharedInstance().allowSpecialBack();


        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.PERMISSION_ACCESSIBILITY);
    }

    public void openUnknownHelper() {
        try {
            windowManager.removeView(overlayLayout);
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog_unknown = new Dialog(UninstallerActivity.this, R.style.Dialog1);
        dialog_unknown.setContentView(R.layout.unknown_source_popup);
        TextView later = (TextView) dialog_unknown.findViewById(R.id.textView28);
        later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("component", "tried opening intent for usage stats but unsuccessful");
                quickAdapter.notifyDataSetChanged();
                dialog_unknown.cancel();
            }
        });
        TextView got_it_btn = (TextView) dialog_unknown.findViewById(R.id.textView11);
        got_it_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity_code = 0;

                unknown_bottom_helper();


            }
        });
        dialog_unknown.setCanceledOnTouchOutside(false);
        dialog_unknown.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                quickAdapter.notifyDataSetChanged();
                if (!UtilityMethods.getInstance().isHavingUnkownSourcePermission(getApplicationContext())) {
                    dismissedOnce = 1;
                }

            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog_unknown.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog_unknown.show();


    }

    private void unknown_bottom_helper() {
        Intent unknownSource = new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS);
        startActivityForResult(unknownSource, 33);
        LayoutInflater accessibilityHelper = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        oView1 = accessibilityHelper.inflate(R.layout.unknown_permission, null);
        WindowManager.LayoutParams params1;

        TextView permission_helper_text = (TextView) oView1.findViewById(R.id.permission_helper_text);
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon_medium.ttf"));
        CalligraphyTypefaceSpan calligraphyRegular = new CalligraphyTypefaceSpan(TypefaceUtils.load(getAssets(), "fonts/brandon.ttf"));
        String f = "Give permission for ";
        String s = "upto 90% compression";
        final String t = f + s;
        Spannable spannableText = new SpannableString(t);
        //set color to white
        spannableText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(UninstallerActivity.this, R.color.text_color)), f.length(), f.length() + s.length(), 0);
        // Apply typeface to the Spannable 0 - 6 "Hello!" This can of course by dynamic.
        spannableText.setSpan(calligraphyRegular, 0, f.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableText.setSpan(typefaceSpan, f.length(), f.length() + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        permission_helper_text.setText(spannableText, TextView.BufferType.SPANNABLE);


        ImageView cancel = (ImageView) oView1.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    wm1.removeView(oView1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        params1 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);

        params1.gravity = Gravity.TOP;
        params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;


        try {
            wm1.addView(oView1, params1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        unknownRunnable = new UnknownRunnable();
        unknownThread = new Thread(unknownRunnable, "unknown");
        unknownThread.start();
        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.PERMISSION_UNKNOWN_SOURCE);
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    CardView card_toolbar;
    int prevVerticalOffset = -1;

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (prevVerticalOffset != verticalOffset) {
            if (verticalOffset == 0) {
                card_toolbar.setCardElevation(0f);
            } else {
                card_toolbar.setCardElevation(6f);

            }
            prevVerticalOffset = verticalOffset;
        }
    }

    class UnknownRunnable implements Runnable {
        int count = 0;
        private volatile boolean exit = false;

        @Override
        public void run() {


            do {
                try {
                    Thread.sleep(1000);
                    //show dialog for 8 seconds only
                    if (count > 8) {
                        try {
                            windowManager.removeView(overlayLayout);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    count++;
                } catch (Exception e) {

                }
            } while (!methods.isHavingUnkownSourcePermission(UninstallerActivity.this) && !exit);


            if (methods.isHavingUnkownSourcePermission(UninstallerActivity.this)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //inform onActivityResult if called not to start scanning
                        //start_scanning = true;

                        try {
                            windowManager.removeView(overlayLayout);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        finishActivity(33);
                    }
                });
            }
        }

        public void stop() {
            Log.d(TAG, "stop: thread");
            exit = true;
        }

    }

    private int mPreviousSelectedAppBeforePopup = 0, mFinalSelectedAppafterPopup = 0;
    private boolean needTOWatch = false;

    private void showPopup() {


        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.REVIEW,
                AnalyticsConstants.Action.DROP_AFTER_PROCEED,
                AnalyticsConstants.Label.FIRED,
                null,
                false,
                null);

        mGAParams = new Bundle();
        mGAParams.putString("category", AnalyticsConstants.Category.COMPRESS);
        mGAParams.putString("action", AnalyticsConstants.Action.PROCEED);
        new AnalyticsHandler().logGAEvent(mGAParams);
        Log.d(AnalyticsConstants.GATAG, mGAParams.toString());

        // Inflate the popup_layout.xml
        viewGroup = (LinearLayout) findViewById(R.id.popup_window);
        popup_background.setVisibility(View.VISIBLE);

        View dialogLayout = getLayoutInflater().inflate(R.layout.compression_confirmation_popup, viewGroup);
        confirmation_optimize_btn = dialogLayout.findViewById(R.id.confirmation_optimize_btn);
        warning_text1 = (TextView) dialogLayout.findViewById(R.id.warning_text1);
        disabledLayout = (LinearLayout) dialogLayout.findViewById(R.id.disabled_layout);
        //warning_text2 = (TextView) dialogLayout.findViewById(R.id.warning_text2);

        popupMessage = new PopupWindow(dialogLayout, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        popupMessage.setContentView(dialogLayout);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = (RecyclerView) dialogLayout.findViewById(R.id.compress_app);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        confirmation_compressAppList = (ArrayList<AppInfo>) compressAppList.clone();

        ConfirmationPopupAdapter confirmationPopupAdapter = new ConfirmationPopupAdapter(this, confirmation_compressAppList, this);
        recyclerView.setAdapter(confirmationPopupAdapter);


        //apps selected before pressing proceed
        mPreviousSelectedAppBeforePopup = compressAppList.size();
        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.REVIEW,
                AnalyticsConstants.Action.APPS_SELECTED,
                null,
                String.valueOf(compressAppList.size()),
                false,
                null);
        Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);

        try {
            viewGroup.startAnimation(slide_up);
        } catch (Exception e) {
            e.printStackTrace();
        }

        slide_up.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                confirmationPopupCountDownTimer.start();
                warning_text1.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        // Getting a reference to Close button, and close the popup when clicked.
        ImageView close = (ImageView) dialogLayout.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                closeConfirmationPopup();
            }
        });


        textView = (TextView) dialogLayout.findViewById(R.id.confirmation_optimize_btn);
        textView.setOnClickListener(this);
        needTOWatch = false;
        //Show advert only if 2 or more apps are selected
        if ( compressAppList.size() >= 0 && RemoteConfig.getBoolean(R.string.allow_reward_video) &&  UtilityMethods.getInstance().isNewUser(getApplicationContext())) {
            confirmation_optimize_btn.setText("WATCH AD TO UNINSTALL");
            needTOWatch = true;
        }
    }



    void closeConfirmationPopup() {
        popup_background.setVisibility(View.GONE);
        Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);

        try {
            viewGroup.startAnimation(slide_down);
        } catch (Exception e) {
            e.printStackTrace();
        }

        slide_down.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                backpress = true;
                popupMessage.dismiss();
                viewGroup.removeAllViews();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }


}
