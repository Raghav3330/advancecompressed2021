package com.spaceup.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsController;
import com.spaceup.R;
import com.spaceup.RemoteConfig;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;

import org.w3c.dom.Text;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class StashFeedPopup extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_stash_feed_popup);

        new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.STASH_REF,
                UtilityMethods.getInstance().getActionRefferalPopup(),
                AnalyticsConstants.Label.FIRED,
                null,
                false,
                null);


        final Dialog dialog = new Dialog(this, R.style.Dialog1);
        dialog.setContentView(R.layout.junk_photos_found_popup);


        final TextView cancelButton = (TextView) dialog.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.STASH_REF,
                        UtilityMethods.getInstance().getActionRefferalPopup(),
                        AnalyticsConstants.Label.CLOSED,
                        null,
                        false,
                        null);

                int installCount = PrefManager.getInstance(getApplicationContext()).getInt(PrefManager.PRESSED_STASH_INSTALL);
                installCount++;
                PrefManager.getInstance(getApplicationContext()).putInt(PrefManager.PRESSED_STASH_INSTALL, installCount);
                if (installCount > 4) {
                    PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.GLOBAL_TURNOFF_BUBBLE_PERMANENT, true);
                }
                dialog.dismiss();
            }
        });

        final TextView selectButton = (TextView) dialog.findViewById(R.id.select_button);
        TextView parentText = dialog.findViewById(R.id.parent_text);
        TextView bottomText = dialog.findViewById(R.id.bottom_text);
        String experimentVal = RemoteConfig.getString(R.string.stash_insall_test);

        if( experimentVal.equals("test1")){
            parentText.setTextColor(Color.parseColor("#ff4949"));
            parentText.setText("10+ Junk photos found!");
            bottomText.setVisibility(View.GONE);
        }
        else{
            bottomText.setVisibility(View.VISIBLE);
            parentText.setTextColor(Color.parseColor("#000000"));
            parentText.setText("Irritated with Junk Photos also ?");
        }
        GradientDrawable drawable = (GradientDrawable) selectButton.getBackground();
        drawable.setColor(Color.parseColor("#23c795"));
        selectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AnalyticsController(getApplicationContext()).sendAnalytics(AnalyticsConstants.Category.STASH_REF,
                        UtilityMethods.getInstance().getActionRefferalPopup(),
                        AnalyticsConstants.Label.CLICKED,
                        null,
                        false,
                        null);


                int installCount = PrefManager.getInstance(getApplicationContext()).getInt(PrefManager.PRESSED_STASH_INSTALL);
                installCount++;
                PrefManager.getInstance(getApplicationContext()).putInt(PrefManager.PRESSED_STASH_INSTALL, installCount);
                if (installCount > 4) {
                    PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.GLOBAL_TURNOFF_BUBBLE_PERMANENT, true);
                }

                Uri uri = Uri.parse(getString(R.string.market_app_url_stash));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Log.d("AppUpgradeActivity", "play store redirection failed ");
                }

                dialog.dismiss();

            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

}
