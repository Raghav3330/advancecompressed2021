package com.spaceup.notifications;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.spaceup.R;
import com.spaceup.Utility.PrefManager;
import com.spaceup.accessibility.AccessibilityAutomation;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.services.RecommendationService;
import com.spaceup.uninstall.activities.AppInfo;

import java.util.Calendar;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

/**
 * Created by Dhruv Kaushal on 16-12-2016.
 */

public class NotificationService extends IntentService {
    BroadcastReceiver accessibilityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("com.times.DbUpdated")) {

                try {
                    if (intent.getExtras().containsKey("status") && intent.getExtras().getString("status").equals("true")) {
                        LocalBroadcastManager.getInstance(context).unregisterReceiver(accessibilityReceiver);
                        Log.d("ALARM_RECEIVER", "RecommendationService Broadcast RECEIVED");

                        // Notification to be shown day wise if .stash app are not found in db
                        PrefManager prefManager = PrefManager.getInstance(getApplicationContext());
                        List<AppInfo> appListDbShortCut = AppDBHelper.getInstance(getApplicationContext()).getShorcutApps();

                        // Listing Installed Dummy APK


                        if (!prefManager.get_is_FIRSTNOTIFICATION("NotificationService")) {

                            long time = PrefManager.getInstance(getApplicationContext()).getLastDayTrigger();
                            AppDBHelper db = AppDBHelper.getInstance(getApplicationContext());
                            if (!AccessibilityAutomation.getAllowed() && db.noAppsCompressed() && appListDbShortCut.size() == 0) {
                                if (System.currentTimeMillis() - time > 86400000) {
                                    PrefManager.getInstance(getApplicationContext()).setLastDayTrigger();

                                    Log.d("ALARM_RECEIVER", "DIFFERENCE IS MORE");
                                    Log.d("ALARM_RECEIVER", "set_is_FIRSTNOTIFICATION false");

                                    if (!isRunningInForeground() && !PrefManager.getInstance(getApplicationContext()).isCompresssingFirst()) {
                                     /*   Intent intent1 = new Intent(NotificationService.this, popup_bottom_notification.class);
                                        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent1);*/
                                        Log.d("ALARM_RECEIVER", "NO COMPRESSED ALARM_NOT RUNNING IN BG");

                                    } else {
                                        Log.d("ALARM_RECEIVER", "NO COMPRESSED RUNNING IN BG");

                                    }


                                }
                            } else {
                                // Stop Temporarily
                                Calendar cur_cal = Calendar.getInstance();
                                cur_cal.setTimeInMillis(System.currentTimeMillis());
                                Intent intent1 = new Intent(getApplicationContext(), NotificationService.class);
                                PendingIntent pintent = PendingIntent.getService(getApplicationContext(), 0, intent1, 0);
                                AlarmManager alarm = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                alarm.setRepeating(AlarmManager.RTC_WAKEUP, cur_cal.getTimeInMillis(), 5 * 60 * 1000, pintent);
                                alarm.cancel(pintent);
                                Log.d("ALARM_RECEIVER", "ALARM_CANCELLED");

                            }
                        } else {

                            prefManager.set_is_FIRSTNOTIFICATION(false, "NotificationService");
                            Log.d("ALARM_RECEIVER", "set_is_FIRSTNOTIFICATION true");
                        }
                    } else if (intent.getExtras().containsKey("status") && intent.getExtras().getString("status").equals(Constants.REMOVE_UNCOMPRESS_NOTIFICATION)) {
                        LocalBroadcastManager.getInstance(context).unregisterReceiver(accessibilityReceiver);
                        Log.d("canceluncompress", "received broadcast REMOVE_UNCOMPRESS_NOTIFICATION");
                        if (null != PrefManager.getInstance(getApplicationContext()).getINSTALLED_APP_TRACKER()) {
                            PrefManager.getInstance(getApplicationContext()).setINSTALLED_APP_TRACKER(null);
                            try {
                                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                                notificationManager.cancel(69);
                            } catch (Exception e) {
                                Log.d("canceluncompress", Log.getStackTraceString(e));
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.d(NotificationService.class.getName(), Log.getStackTraceString(e));

                } finally {
                    LocalBroadcastManager.getInstance(context).unregisterReceiver(accessibilityReceiver);
                }
            }
        }

    };


    public NotificationService() {

        super(NotificationService.class.getName());

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("ALARM_RECEIVER", "STARTED_ALARM_ONCREATE");

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("ALARM_RECEIVER", "STARTED_ALARM_RECEIVER");


        IntentFilter accessibilityReceiverIntentFilter = new IntentFilter();
        accessibilityReceiverIntentFilter.addAction("com.times.DbUpdated");
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(accessibilityReceiver,
                accessibilityReceiverIntentFilter);
        if (intent.getExtras() == null  && intent.getExtras().getString(Constants.NOTIFICATION_TYPE) != null) {
            stopSelf();
            return;
        }
        String type = intent.getExtras().getString(Constants.NOTIFICATION_TYPE);

        Log.d(getClass().getName(), "Service" + type);
        try {
            if (type != null && type.equalsIgnoreCase(Constants.NOT_COMPRESS_WITHIN_FIVE_NOTIFICATION)) {

                UpdateRecommendationDb();
            } else if (type != null && type.equalsIgnoreCase(Constants.REMOVE_UNCOMPRESS_NOTIFICATION)) {
                Log.d("canceluncompress", "inside service sending broadcast");
                Intent removeNotification = new Intent("com.times.DbUpdated");
                removeNotification.putExtra("status", Constants.REMOVE_UNCOMPRESS_NOTIFICATION);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(removeNotification);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    public void UpdateRecommendationDb() {
        Log.d("ALARM_RECEIVER", "Starting RecommendationService");
        Intent intent = new Intent(getApplicationContext(), RecommendationService.class);
        startService(intent);

    }

    protected boolean isRunningInForeground() {

        try {
            ActivityManager am = (ActivityManager) NotificationService.this.getSystemService(ACTIVITY_SERVICE);
            // The first in the list of RunningTasks is always the foreground task.
            ActivityManager.RunningTaskInfo foregroundTaskInfo = am.getRunningTasks(1).get(0);
            String foregroundTaskPackageName = foregroundTaskInfo.topActivity.getPackageName();
            PackageManager pm = NotificationService.this.getPackageManager();
            ApplicationInfo foregroundAppPackageInfo = pm.getApplicationInfo(foregroundTaskPackageName, 0);
            String foregroundTaskAppName = foregroundAppPackageInfo.loadLabel(pm).toString();
            if (foregroundTaskAppName.equals(getResources().getString(R.string.app_name))) {

                return true;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}