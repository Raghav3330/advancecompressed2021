package com.spaceup.Animations;

/**
 * Created by Dhruv Kaushal on 23/03/17.
 */

import android.animation.ValueAnimator;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spaceup.Activities.Scanning;
import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.App;
import com.spaceup.R;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityAutomation;
import com.spaceup.apkgenerator.constants.Constants;
import com.spaceup.data.version_one.AppDBHelper;
import com.spaceup.models.DataModel;
import com.spaceup.uninstall.activities.AppInfo;
import com.spaceup.uninstall.activities.UninstallerActivity;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static android.content.Context.WINDOW_SERVICE;
import static com.spaceup.apkgenerator.constants.Constants.WHITE_LIST_FILE_ASSET_NAME;
import static com.spaceup.apkgenerator.constants.Constants.WHITE_LIST_FILE_PATH;


public class ScanningOverlay {
    public static List<AppInfo> userInstalled;
    public static int width, height, top;
    private AccessibilityAutomation sSharedInstance;

    private SurfaceHolder mActiveSurface;
    private MediaPlayer mMediaPlayer;

    private SurfaceHolder mFirstSurface;
    private Uri mVideoUri;
    public static int bottom, view_height, top_margin, indicator_height;
    public static Set<String> sFinanceApps;
    private final int FINISH = 101, INVALIDATE = 102, START = 100, ASYNC_FINISH = 108;
    private ImageView scanning;
    private Handler handle;
    private TextView scanning_text;
    //checks number of iteration by the glowing bar
    private int x = 0;
    private boolean mMakeVisible = true;
    //checks whether async task is finished or not. this status is updated in msg handler
    private boolean mAsyncStatus = false;
    private RelativeLayout.LayoutParams lp;
    private int mTheme;
    private RelativeLayout upper_layout;
    private Bundle mGAParams;
    private JSONObject mApsalarTracking;
    ValueAnimator downAnimator, upAnimator;
    Intent intent;
    Context context;
    private WindowManager mWindowManagerScanning;
    public static int mFinanceAppCount = 0;
    private String mUriScanning;
    private View scanView;
    private CountDownTimer countDownTimerTicker;

    public ScanningOverlay(Context context, Intent intent, AccessibilityAutomation sSharedInstance) {
        this.context = context;
        this.intent = intent;
        this.sSharedInstance = sSharedInstance;
    }

    public void overlayScanning() {
        mTheme = intent.getIntExtra("theme", PrefManager.getInstance(context).getTheme());
        //reset theme if started from notification intent
        PrefManager.getInstance(context).setTheme(mTheme);
        if (mWindowManagerScanning == null) {
            startTimer();
            startScanningAnimation();
            WindowManager.LayoutParams params;

            try {
                params = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                        PixelFormat.TRANSLUCENT);
                params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                mWindowManagerScanning = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
                try {
                    mWindowManagerScanning.addView(scanView, params);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception j) {
                j.printStackTrace();
                params = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.TYPE_PHONE,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                        PixelFormat.TRANSLUCENT);
                params.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

                mWindowManagerScanning = (WindowManager) sSharedInstance.getSystemService(WINDOW_SERVICE);
                try{mWindowManagerScanning.addView(scanView, params);
                }
                catch (Exception e){
                    e.printStackTrace();
                }


            }

            Display display = mWindowManagerScanning.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            width = size.x;
            height = size.y;
            view_height = height;
        }

    }

    private void startTimer() {
        Log.d("ScanningOverlay", "Timer START");
        countDownTimerTicker = null;
        countDownTimerTicker = new CountDownTimer(15000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {

                AccessibilityAutomation.getSharedInstance().removeScanningOverLay();
                mGAParams = new Bundle();
                //from manage apps icon
                mGAParams.putString("category", AnalyticsConstants.Category.APPUSAGE_FAIL);
                mGAParams.putString("action", Build.MANUFACTURER);
                new AnalyticsHandler().logGAEvent(mGAParams);
                Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
                Log.d("AppUsageFail", "Timer FINISH");


                // Above method will also open UninstallerActivity
            }
        }.start();

    }

    private void startScanningAnimation() {
        LayoutInflater inflater = (LayoutInflater) sSharedInstance.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        scanView = inflater.inflate(R.layout.scanning, null);

        mTheme = intent.getIntExtra("theme", PrefManager.getInstance(context).getTheme());

        //reset theme if started from notification intent
        PrefManager.getInstance(context).setTheme(mTheme);


        userInstalled = new ArrayList<>();
        upper_layout = (RelativeLayout) scanView.findViewById(R.id.upper_layout);

        scanning = (ImageView) scanView.findViewById(R.id.scanning_indicator);
        scanning_text = (TextView) scanView.findViewById(R.id.scanning_text);

        lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        if (mTheme == 0) {
            //blue
            mUriScanning = "android.resource://" + context.getPackageName() + "/" + R.raw.scanning;

            Resources res = context.getResources(); //resource handle
            Drawable drawable = res.getDrawable(R.drawable.blue_background); //new Image that was added to the res folder
            upper_layout.setBackground(drawable);

        } else {
            //red
            mUriScanning = "android.resource://" + context.getPackageName() + "/" + R.raw.scanning;

            Resources res = context.getResources(); //resource handle

        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scanning_text.setText("Scanning Background Processes");
            }
        }, 1000);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scanning_text.setText("Scanning Apps");
            }
        }, 3000);
        SurfaceView first = (SurfaceView) scanView.findViewById(R.id.firstSurface);
        first.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {

                mFirstSurface = surfaceHolder;
                String uri = mUriScanning;
                mVideoUri = Uri.parse(uri);

                if (mVideoUri != null) {
                    mMediaPlayer = MediaPlayer.create(context,
                            mVideoUri, mFirstSurface);
                    mMediaPlayer.setLooping(true);

                    mActiveSurface = mFirstSurface;
                    mMediaPlayer.start();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            }
        });
        new AnalyticsHandler().logGAScreen(AnalyticsConstants.Screens.SCANNING);
    }

    public void get_Apps(final Context ctx) {
        class load_Apps extends AsyncTask<Context, Void, List<AppInfo>> {
            AppDBHelper db = AppDBHelper.getInstance(context);
            String TAG = "RecommendAsync";

            @Override
            protected void onPostExecute(List<AppInfo> recommendedApps) {
                userInstalled = recommendedApps;
                // Make 1 wait of 3 seconds till Uninstall Activity Loads
                try {
                    mMediaPlayer.stop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Intent scan = new Intent(context, UninstallerActivity.class);
                scan.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                scan.putParcelableArrayListExtra("userAppList", (ArrayList<? extends Parcelable>) userInstalled);
                context.startActivity(scan);

                // Delay of 3 second till UninstallAtivity Appears on Background
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            mMediaPlayer.setLooping(true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            if (mWindowManagerScanning != null) {
                                mWindowManagerScanning.removeView(scanView);
                            }
                            scanView = null;
                            mWindowManagerScanning = null;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 3000);


            }

            // returns list of Finance apps from the financeApps
            private Set<String> getFinanceAppsList() {
                sFinanceApps = new LinkedHashSet<>();
                String line;
                try {
                    InputStream is;
                    File file = new File(Environment.getExternalStorageDirectory().getPath() + Constants.STASH + WHITE_LIST_FILE_PATH);
                    if (file.exists()) {
                        is = new FileInputStream(file);
                    } else {
                        is = App.getInstance().getAssets().open(WHITE_LIST_FILE_ASSET_NAME);
                    }
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    line = reader.readLine();

                    while (line != null) {
                        sFinanceApps.add(line);
                        line = reader.readLine();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return sFinanceApps;
            }


            @Override
            protected List<AppInfo> doInBackground(Context... ctx) {
                List<AppInfo> recommendedList = null;


                if (Build.VERSION.SDK_INT >= 21 && UtilityMethods.getInstance().isHavingUsageStatsPermission(ctx[0])) {
                    recommendedList = foregroundAndDataBasedHeuristics();

                } else {
                    recommendedList = listByCache();
                }


                //Write to 5 Apps to Top_FIVE_DB
                try {
                    AppDBHelper.getInstance(context).cleanDb();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < recommendedList.size(); i++) {
                    if (recommendedList.get(i).getCompressed_status() == -1 && !recommendedList.get(i).getPkgName().contains(".stash") && !recommendedList.get(i).getPkgName().contains("sample.example.dhruv.sample_apk")) {

                        try {
                            AppDBHelper.getInstance(context).addTop5Application(recommendedList.get(i));
                            Log.d(TAG, "LISTING TOP 5 APPS " + recommendedList.get(i).getPkgName());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.d(TAG, "LISTING TOP 5 OTHER " + recommendedList.get(i).getPkgName());
                    }

                }


                return recommendedList;
            }


            private List<AppInfo> listByCache() {

                List<AppInfo> userInstalled = new ArrayList<AppInfo>();

                Set<String> financeApps = getFinanceAppsList();
                List<AppInfo> appListDb = db.getAppDetails();
                /*
                Dividing List into three parts in accordance with the Priority
                Keeping Priority of Apps as
                1) Apps below threshold size
                2) Apps above Threshold Size
                3) Finanace Apps
                */
                List<AppInfo> financeAppsInDb = new ArrayList<>(); //Apps on phone falling under Finance category
                List<AppInfo> appsAbvSize = new ArrayList<>(); //Apps list above threshold size of data
                List<AppInfo> appsBelowSize = new ArrayList<>(); //Apps list below threshold size of data

                for (int i = 0; i < appListDb.size(); i++) {
                    if (financeApps.contains(appListDb.get(i).getPkgName())) {
                        financeAppsInDb.add(appListDb.get(i));
                        Log.d("financeApp", appListDb.get(i).getAppName() + " " + appListDb.get(i).getPkgName());
                    } else if (appListDb.get(i).getSize() > 10 * 1024 * 1024)
                        appsAbvSize.add(appListDb.get(i));
                    else
                        appsBelowSize.add(appListDb.get(i));
                }

                Collections.sort(appsAbvSize, new Comparator<AppInfo>() {
                    long crntTime = (System.currentTimeMillis());

                    @Override
                    public int compare(AppInfo o1, AppInfo o2) {


                        long timeFactor1 = crntTime - o1.getLastModifiedDate();
                        long timeFactor2 = crntTime - o2.getLastModifiedDate();

                        // deprioritizing apps which are older than 2 months
                        if (timeFactor1 > 2 * 30 * 24 * 60 * 60 * 1000) {
                            timeFactor1 = 24 * 60 * 60 * 1000;
                        }
                        if (timeFactor2 > 2 * 30 * 24 * 60 * 60 * 1000) {
                            timeFactor2 = 24 * 60 * 60 * 1000;
                        }

                        long cacheFactor1 = o1.getSize();
                        // long cacheFactor2 = o2.getSize();
                        //
                        Log.d("mychck ", "" + (crntTime - o1.getLastModifiedDate()) + "---" + cacheFactor1 + "---" + o1.getAppName());

                        if (timeFactor1 > timeFactor2)
                            return -1;
                        else if (timeFactor1 < timeFactor2)
                            return 1;
                        else
                            return 0;
                    }
                });

                Collections.sort(appsBelowSize, new Comparator<AppInfo>() {
                    long crntTime = (System.currentTimeMillis());

                    @Override
                    public int compare(AppInfo o1, AppInfo o2) {
                        long timeFactor1 = crntTime - o1.getLastModifiedDate();
                        long timeFactor2 = crntTime - o2.getLastModifiedDate();

                        // deprioritizing apps which are older than 2 months
                        if (timeFactor1 > 2 * 30 * 24 * 60 * 60 * 1000) {
                            timeFactor1 = 24 * 60 * 60 * 1000;
                        }
                        if (timeFactor2 > 2 * 30 * 24 * 60 * 60 * 1000) {
                            timeFactor2 = 24 * 60 * 60 * 1000;
                        }

                        long cacheFactor1 = o1.getSize();
                        // long cacheFactor2 = o2.getSize();

                        Log.d("mychck ", "" + (crntTime - o1.getLastModifiedDate()) + "---" + cacheFactor1 + "---" + o1.getAppName());

                        if (timeFactor1 > timeFactor2)
                            return -1;
                        else if (timeFactor1 < timeFactor2)
                            return 1;
                        else
                            return 0;
                    }
                });


                userInstalled.addAll(appsBelowSize);
                userInstalled.addAll(appsAbvSize);
                userInstalled.addAll(financeAppsInDb);


                return userInstalled;
            }

            private List<AppInfo> foregroundAndDataBasedHeuristics() {
                Log.d("Scanning", "I am here2");
                //list recommended by app usage
                List<AppInfo> recommendedList = new ArrayList<>();
                //app list from db
                List<AppInfo> listFromDb = db.getAppDetails();

                Calendar beginCal, endCal;
                beginCal = Calendar.getInstance();
                beginCal.add(Calendar.MONTH, -1);
                List<UsageStats> usageStatsList;
                Log.d(TAG, "Making list of apps on weekly basis for a month");
                endCal = Calendar.getInstance();

                usageStatsList =
                        getUsageStatistics(UsageStatsManager.INTERVAL_WEEKLY, beginCal, endCal);

                // list with pkg name and app name
                //List<AppInfo> userApps = getUserInstalledApplication();
                List<AppInfo> userApps = new ArrayList<>();
                for (AppInfo app : listFromDb) {

                    if (!app.getPkgName().contains(".stash") && app.getCompressed_status() != -2) {
                        //app does not contains .stash and not a system app
                        ApplicationInfo applicationInfo = null;
                        try {
                            applicationInfo = context.getPackageManager().getApplicationInfo(app.getPkgName(), 0);
                            if ((applicationInfo.flags & ApplicationInfo.FLAG_IS_GAME) != ApplicationInfo.FLAG_IS_GAME) {

                                userApps.add(app);
                                Log.d("GAMES NOT", ApplicationInfo.FLAG_IS_GAME + " package Name " + app.getPkgName());

                            } else {
                                Log.d("GAMES ", ApplicationInfo.FLAG_IS_GAME + " package Name " + app.getPkgName());
                            }
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }


                    }
                }

                //weekly list total size and total foreground time
                List<Mem> weekylylist = new ArrayList<>();
                //apps with zero foreground time
                List<AppInfo> remainingApps = new ArrayList<>();

                //monthly list total size and total foreground time
                List<Mem> monthlylist = new ArrayList<>();

                //list sorted by application size (REDUNDANT BUT REQUIRED FOR NOW)
                List<Mem> remaininglist = new ArrayList<>();
                HashMap<String, Node> weekly = new HashMap<>();
                for (UsageStats stats : usageStatsList) {
                    if (weekly.containsKey(stats.getPackageName())) {
                        Node n = weekly.get(stats.getPackageName());
                        n.count += 1;
                        n.lastTimeUsed.add(stats.getLastTimeStamp());
                        n.foregroundTime.add(stats.getTotalTimeInForeground());
                        weekly.put(stats.getPackageName(), n);
                    } else {
                        Node n = new Node();
                        n.count = 1;
                        n.lastTimeUsed = new ArrayList<>();
                        n.lastTimeUsed.add(stats.getLastTimeStamp());
                        n.foregroundTime = new ArrayList<>();
                        n.foregroundTime.add(stats.getTotalTimeInForeground());
                        weekly.put(stats.getPackageName(), n);
                    }
                }


                //calc total foreground time of user apps
                for (AppInfo app : userApps) {

                    if (weekly.containsKey(app.getPkgName())) {
                        Mem obj = new Mem();
                        Node n = weekly.get(app.getPkgName());
                        long totalTimeInForeground = 0;
                        for (Long timeInstance : n.foregroundTime) {
                            totalTimeInForeground = totalTimeInForeground + timeInstance;
                        }

                        app.setTotalTimeInForeground(totalTimeInForeground);
                        obj.packageName = app.getPkgName();
                        obj.time = app.getTotalTimeInForeground();
                        weekylylist.add(obj);
                        //update foreground time and last time used in db
                        db.dataUpdateForegroundTime(app.getPkgName(), app.getTotalTimeInForeground(), n.lastTimeUsed.get(n.lastTimeUsed.size() - 1));
                    }
                }


                //Log.d(TAG, userApps.size() + " " + weekylylist.size());
                //calculating foreground time and last time stamp for those apps which are not found in weekly data query
                if (userApps.size() - weekylylist.size() > 0) {
                    //calculate data on monthly basis
                    Log.d(TAG, "6 months data on monthly basis");
                    beginCal = Calendar.getInstance();
                    beginCal.add(Calendar.MONTH, -6);
                    endCal = Calendar.getInstance();
                    usageStatsList = getUsageStatistics(UsageStatsManager.INTERVAL_YEARLY, beginCal, endCal);

                    HashMap<String, Node> monthly = new HashMap<>();
                    for (UsageStats stats : usageStatsList) {
                        if (monthly.containsKey(stats.getPackageName())) {
                            Node n = monthly.get(stats.getPackageName());
                            n.count += 1;
                            n.lastTimeUsed.add(stats.getLastTimeStamp());
                            n.foregroundTime.add(stats.getTotalTimeInForeground());
                            monthly.put(stats.getPackageName(), n);
                        } else {
                            Node n = new Node();
                            n.count = 1;
                            n.lastTimeUsed = new ArrayList<>();
                            n.lastTimeUsed.add(stats.getLastTimeStamp());
                            n.foregroundTime = new ArrayList<>();
                            n.foregroundTime.add(stats.getTotalTimeInForeground());
                            monthly.put(stats.getPackageName(), n);
                        }
                    }
                    //apps not in weekly list calc their total foreground time of user apps and last time used
                    for (AppInfo app : userApps) {
                        if (monthly.containsKey(app.getPkgName()) && !weekly.containsKey(app.getPkgName())) {
                            Mem obj = new Mem();
                            Node n = monthly.get(app.getPkgName());
                            long totalTimeInForeground = 0;
                            for (Long timeInstance : n.foregroundTime) {
                                totalTimeInForeground = totalTimeInForeground + timeInstance;
                            }
                            app.setTotalTimeInForeground(totalTimeInForeground);
                            obj.packageName = app.getPkgName();
                            obj.time = app.getTotalTimeInForeground();
                            monthlylist.add(obj);
                            db.dataUpdateForegroundTime(app.getPkgName(), app.getTotalTimeInForeground(), n.lastTimeUsed.get(n.lastTimeUsed.size() - 1));
                        }
                    }

                    //apps not found in weekly list and monthly list, adding them to remaning list
                    if (userApps.size() - monthlylist.size() > 0) {
                        for (AppInfo app : userApps) {
                            if (!monthly.containsKey(app.getPkgName())) {
                                remainingApps.add(app);
                            }
                        }
                    }
                }

                //fetching data size of remaning apps
                AppDBHelper db = AppDBHelper.getInstance(context);
                for (AppInfo app : remainingApps) {
                    //object for recommended list
                    AppInfo ele = new AppInfo();
                    Mem obj = new Mem();
                    obj.packageName = app.getPkgName();
                    DataModel data = db.getDataSize(app.getPkgName());
                    obj.size = data.getDataSize() + data.getApkSize() + data.getCacheSize() + data.getCodeSize();
                    remaininglist.add(obj);
                }


                //get updated list from db
                listFromDb = db.getAppDetails();
                //getting updated data from db in a hash map
                HashMap<String, AppInfo> updatedDb = new HashMap<>();
                for (AppInfo app : listFromDb) {
                    if (!updatedDb.containsKey(app.getPkgName())) {
                        updatedDb.put(app.getPkgName(), app);
                    }
                }

                Log.d("Scanning", "I am here jadoo" + updatedDb.size());


                Collections.sort(remaininglist, new Comparator<Mem>() {
                    @Override
                    public int compare(Mem o1, Mem o2) {
                        return Long.compare(o1.size, o2.size);
                    }
                });


                Log.d(TAG, "first");
                Set<String> finance = getFinanceAppsList();
                List<AppInfo> financeAppsInDb = new ArrayList<>();

                UtilityMethods methods = UtilityMethods.getInstance();
                for (Mem app : remaininglist) {
                    if (!finance.contains(app.packageName)) {
                        Log.d(TAG, "Package name " + app.packageName);
                        Log.d(TAG, "size " + methods.getSizeinMB(app.size));
                        if (updatedDb.containsKey(app.packageName)) {
                            recommendedList.add(updatedDb.get(app.packageName));
                        }
                    } else {
                        mFinanceAppCount++;

                        if (updatedDb.containsKey(app.packageName)) {
                            financeAppsInDb.add(updatedDb.get(app.packageName));
                        }
                    }
                }


                Collections.sort(monthlylist, new Comparator<Mem>() {
                    @Override
                    public int compare(Mem o1, Mem o2) {
                        return Long.compare(o1.time, o2.time);
                    }
                });


                Log.d(TAG, "second");
                //to be show in middle
                for (Mem app : monthlylist) {

                    if (!finance.contains(app.packageName)) {


                        Log.d(TAG, "Package name " + app.packageName);

                        String t = String.format("%d min, %d sec",
                                TimeUnit.MILLISECONDS.toMinutes(app.time),
                                TimeUnit.MILLISECONDS.toSeconds(app.time) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(app.time))
                        );

                        if (updatedDb.containsKey(app.packageName)) {
                            recommendedList.add(updatedDb.get(app.packageName));
                        }

                        Log.d(TAG, "time in foreground " + t);
                    } else {
                        mFinanceAppCount++;

                        if (updatedDb.containsKey(app.packageName)) {
                            financeAppsInDb.add(updatedDb.get(app.packageName));
                        }
                    }
                }


                Collections.sort(weekylylist, new Comparator<Mem>() {
                    @Override
                    public int compare(Mem o1, Mem o2) {
                        return Long.compare(o1.time, o2.time);
                    }
                });


                //to be shown down
                Log.d(TAG, "third");

                for (Mem app : weekylylist) {
                    if (!finance.contains(app.packageName)) {

                        Log.d(TAG, "Package name " + app.packageName);

                        String t = String.format("%d min, %d sec",
                                TimeUnit.MILLISECONDS.toMinutes(app.time),
                                TimeUnit.MILLISECONDS.toSeconds(app.time) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(app.time))
                        );

                        if (updatedDb.containsKey(app.packageName)) {
                            recommendedList.add(updatedDb.get(app.packageName));
                        }

                        Log.d(TAG, "time in foreground " + t);
                    } else {
                        mFinanceAppCount++;

                        if (updatedDb.containsKey(app.packageName)) {
                            financeAppsInDb.add(updatedDb.get(app.packageName));
                        }
                    }
                }

                Log.d(TAG, "Total number of apps " + userApps.size());
                Log.d(TAG, "weekly apps list " + weekylylist.size());
                Log.d(TAG, "monthly apps list " + monthlylist.size());
                Log.d(TAG, "remaining apps list " + remainingApps.size());

                recommendedList.addAll(financeAppsInDb);

                return recommendedList;
            }

            private List<UsageStats> getUsageStatistics(int intervalType, Calendar beginCal, Calendar endCal) {

                UsageStatsManager mUsageStatsManager = (UsageStatsManager) context
                        .getSystemService(Context.USAGE_STATS_SERVICE); //Context.USAGE_STATS_SERVICE

                List<UsageStats> queryUsageStats = mUsageStatsManager
                        .queryUsageStats(intervalType, beginCal.getTimeInMillis(), endCal.getTimeInMillis());

                if (queryUsageStats.size() == 0) {
                    Log.d(TAG, "The user may not allow the access to apps usage.");
//                    Toast.makeText(getApplicationContext(),getString(R.string.explanation_access_to_appusage_is_not_enabled),Toast.LENGTH_LONG).show();

                    // startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));

                }
                return queryUsageStats;
            }

            class Mem {
                long size;
                long time;
                String packageName;
            }

            class Node {
                int count;
                List<Long> lastTimeUsed;
                List<Long> foregroundTime;
            }
        }

        load_Apps a = new load_Apps();
        Context[] myTaskParams = {ctx};
        a.execute(myTaskParams);

    }


    public void removeScanningOverLay() {
        countDownTimerTicker.cancel();

        get_Apps(context);


    }
}


