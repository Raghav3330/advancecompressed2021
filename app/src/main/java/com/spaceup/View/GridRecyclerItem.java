package com.spaceup.View;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Created by shashank.tiwari on 27/03/17.
 */

public class GridRecyclerItem extends RelativeLayout {

    public GridRecyclerItem(Context context) {
        super(context);
    }

    public GridRecyclerItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GridRecyclerItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec); // This is the key that will make the height equivalent to its width
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
    }
}