package com.spaceup.uninstall.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import com.spaceup.R;
import com.spaceup.Utility.PrefManager;
import com.spaceup.Utility.UtilityMethods;
import com.spaceup.uninstall.activities.AppInfo;
import com.spaceup.uninstall.activities.DataTransferInterface;

import java.util.List;

public class QuickListAdapter extends RecyclerView.Adapter<QuickListAdapter.ViewHolder> {

    private static final int TYPE_HEAD = 0;
    private static final int TYPE_LIST = 1;
    private static final int TYPE_QUICK_TIP = 2;
    Context context;
    List<AppInfo> optimizeList;
    DataTransferInterface dtInterface;
    private String mCompressionType;
    private String mFirstCompressionType;

    public QuickListAdapter(Context context, List<AppInfo> optimizeList, DataTransferInterface dtInterface, String pCompressionType, String pFirstCompressionType) {
        this.context = context;
        this.optimizeList = optimizeList;
        this.dtInterface = dtInterface;
        this.mCompressionType = pCompressionType;
        this.mFirstCompressionType = pFirstCompressionType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.enable_advance_compression, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dtInterface.setUnknownTracker(b);
            }
        });
        //default value
        holder.checkBox.setChecked(false);
        if (PrefManager.getInstance(context).onReviewScreenFirst()) {
            PrefManager.getInstance(context).setReviewScreenFirst();
            if (mFirstCompressionType.equalsIgnoreCase("advance")) {
                holder.checkBox.setChecked(true);
                dtInterface.setUnknownTracker(true);
            } else if (mFirstCompressionType.equalsIgnoreCase("quick")) {
                holder.checkBox.setChecked(false);
                dtInterface.setUnknownTracker(false);
            } else if (mFirstCompressionType.equalsIgnoreCase("default")) {
                //no changes from remote config
                if (UtilityMethods.getInstance().isHavingUnkownSourcePermission(context)) {
                    //go on do full compression
                    holder.checkBox.setChecked(true);
                    dtInterface.setUnknownTracker(true);
                }
            }

        } else {
            if (mCompressionType.equalsIgnoreCase("advance")) {
                //default values will be advance compression
                holder.checkBox.setChecked(true);
                dtInterface.setUnknownTracker(true);
            } else if (mCompressionType.equalsIgnoreCase("quick")) {
                holder.checkBox.setChecked(false);
                dtInterface.setUnknownTracker(false);
            } else if (mCompressionType.equalsIgnoreCase("default")) {
                //no changes from remote config
                if (UtilityMethods.getInstance().isHavingUnkownSourcePermission(context)) {
                    //go on do full compression
                    holder.checkBox.setChecked(true);
                    dtInterface.setUnknownTracker(true);
                }
            }
        }

        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.checkBox.toggle();
            }
        });
    }

    public List<AppInfo> getList() {
        return optimizeList;
    }

    @Override
    public int getItemCount() {
        //adding one for header
        return 1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        //variable for list
        CheckBox checkBox;
        RelativeLayout row;

        public ViewHolder(View view) {
            super(view);
            checkBox = (CheckBox) view.findViewById(R.id.check_box_advance);
            row = (RelativeLayout) view.findViewById(R.id.row);
        }
    }
}


