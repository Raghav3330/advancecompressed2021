package com.spaceup.AppShortcut;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Created by dhurv on 22-02-2017.
 */

public class CommonItemSpaceDecoration extends RecyclerView.ItemDecoration {
    private int mSpace = 5;
    private boolean mVerticalOrientation = true;

    public CommonItemSpaceDecoration(int space) {
        this.mSpace = space;
    }

    public CommonItemSpaceDecoration(int space, boolean verticalOrientation) {
        this.mSpace = space;
        this.mVerticalOrientation = verticalOrientation;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (mVerticalOrientation) {
            if (parent.getChildAdapterPosition(view) == 1) {
                outRect.set(0, 0,0, 0);

            }

            else if(parent.getChildAdapterPosition(view) % 5 == 0  ){
                outRect.set(40, 0, 40, 0);
            }
        }
    }
}