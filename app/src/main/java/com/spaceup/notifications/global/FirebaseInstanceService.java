package com.spaceup.notifications.global;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;

/**
 * Created by Sajal Jain on 12-01-2017.
 */

public class FirebaseInstanceService extends FirebaseMessagingService {
    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }

//    @Override
//    public void onTokenRefresh() {
//        String token = FirebaseInstanceId.getInstance().getToken();
//        Log.d("global", token);
//
//    }
}
