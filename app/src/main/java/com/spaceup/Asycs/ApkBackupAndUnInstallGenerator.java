package com.spaceup.Asycs;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.spaceup.Utility.UtilityMethods;
import com.spaceup.accessibility.AccessibilityAutomation;
import com.spaceup.apkgenerator.constants.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import static com.spaceup.AppShortcut.ShortCutFolder.MOVE_TO_MENU;

/**
 * Created by shashank.tiwari on 10/12/16.
 */

public class ApkBackupAndUnInstallGenerator extends Thread {

    Context context;
    ArrayList<String> packageList;
    String dummyApkPath;
    final String TAG = getClass().getName();
    String packageName;


    public ApkBackupAndUnInstallGenerator(Context context, ArrayList<String> packageList, String dummyApkPath) {
        this.context = context;
        this.packageList = packageList;
        this.dummyApkPath = dummyApkPath;
    }

    @Override
    public void run() {
        super.run();
        load_apk();
        if (AccessibilityAutomation.getAllowed()) {
            for (String packagename : packageList) {
                callUninstallIntent(packagename);
                packageName = packagename;
            }
           /* if (packageList.size() > 0)
                apkBackupAndUnInstallEventDone(packageName);*/
        }

    }

    public void load_apk() {
        for (String packagename : packageList)
            extractapk(packagename);
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    public void extractapk(String packagename) {
        Log.d(TAG, "APK BACKUP STARTED FOR APP" + packagename);
        PackageManager packageManager = context.getPackageManager();
        try {
            File file = new File(packageManager.getApplicationInfo(packagename, PackageManager.GET_META_DATA).publicSourceDir);
            File output = new File(Environment.getExternalStorageDirectory().getPath() + Constants.STASH + "/db/" + packagename + ".stash");
            if (output.exists()) {
                output.delete();
                output = new File(Environment.getExternalStorageDirectory().getPath() + Constants.STASH + "/db/" + packagename + ".stash");
            }
            try {
                output.createNewFile();
                FileOutputStream fos;
                try {

                    InputStream assetFile = new FileInputStream(file);
                    fos = new FileOutputStream(output);
                    copyFile(assetFile, fos);
                    fos.close();
                    assetFile.close();
                    //Log.d(TAG, "DELETE INTENT thrown for" + packagename);
                    //BACKED_APK_COUNT++;


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void callUninstallIntent(String packagename) {

        try {
            if (packagename != null && UtilityMethods.getInstance().isPackageExisted(packagename, context)) {
                Uri packageUri = Uri.parse("package:" + packagename);
                Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageUri);
                uninstallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(uninstallIntent);

                if (AccessibilityAutomation.getSharedInstance() != null)
                    AccessibilityAutomation.getSharedInstance().setCurrentInstallation(packagename);

                apkBackupAndUnInstallEventDone(packageName);

            } else {
                if( MOVE_TO_MENU == 1 ){

                    Intent babyStashInstallationEvent = new Intent("com.times.convertToAdvance");
                    babyStashInstallationEvent.putExtra("get_packagename", packageName);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(babyStashInstallationEvent);
                    Log.d(TAG, "CONVERT_TO_ADVANCE" + packagename);
                    UtilityMethods.getInstance().throwInstallEvent(context,packagename);


                }else {
                    Log.d("Compression", "package do not exists for uninstallation, starting next compression");
                    UtilityMethods.getInstance().startNextCompression(context);
                }
            }

        } catch (Exception e) {
            if (AccessibilityAutomation.getSharedInstance() != null) {
                AccessibilityAutomation.getSharedInstance().deactivateUninstallFlag();
                AccessibilityAutomation.getSharedInstance().getActionFailHandler().refreshUninstallCounter();
                AccessibilityAutomation.getSharedInstance().getActionFailHandler().setUninstall_handler(false, null, false);
            }
            UtilityMethods.getInstance().startNextCompression(context);
            if (packagename != null)
                Log.d("ApkBackupAndUnInstallGenerator", "package not found " + packagename);
            e.printStackTrace();
        }


    }

    void apkBackupAndUnInstallEventDone(String packageName) {
        Intent intent = new Intent("com.times.accessibilityReceiver");
        intent.putExtra("install_fail_event_timer", true);
        intent.putExtra("package", packageName);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);


    }


}
