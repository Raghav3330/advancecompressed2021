package com.spaceup.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.spaceup.Analytics.AnalyticsConstants;
import com.spaceup.Analytics.AnalyticsHandler;
import com.spaceup.accessibility.AccessibilityAutomation;

import java.util.Date;

/**
 * Created by shashanktiwari on 05/01/17.
 */

public class CallReceiver extends BroadcastReceiver {

    //The receiver will be recreated whenever android feels like it.  We need a static variable to remember data between instantiations

    private static int lastState = TelephonyManager.CALL_STATE_IDLE;
    private static Date callStartTime;
    private static boolean isIncoming;
    private static String savedNumber;  //because the passed incoming is only valid in ringing


    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null || intent.getExtras() == null) {
            return;
        }
        //We listen to two intents.  The new outgoing call only tells us of an outgoing call.  We use it to get the number.
        if ("android.intent.action.NEW_OUTGOING_CALL".equals(intent.getAction())) {
            savedNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");
        } else {
            String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            int state = 0;
            if (TelephonyManager.EXTRA_STATE_IDLE.equals(stateStr)) {
                state = TelephonyManager.CALL_STATE_IDLE;
            } else if (TelephonyManager.EXTRA_STATE_OFFHOOK.equals(stateStr)) {
                state = TelephonyManager.CALL_STATE_OFFHOOK;
            } else if (TelephonyManager.EXTRA_STATE_RINGING.equals(stateStr)) {
                state = TelephonyManager.CALL_STATE_RINGING;
            }


            onCallStateChanged(context, state, number);
        }
    }

    //Derived classes should override these to respond to specific events of interest
    protected void onIncomingCallStarted(Context ctx, String number, Date start) {
        Log.d("AccessibilityY", "Incoming Call");

        if (AccessibilityAutomation.getAllowed()) {
            Bundle mGAParams = new Bundle();
            mGAParams.putString("category", AnalyticsConstants.Category.COMPRESS);
            mGAParams.putString("action", AnalyticsConstants.Action.ANIMATION_STATS);
            mGAParams.putString("label", AnalyticsConstants.Label.CALL);
            new AnalyticsHandler().logGAEvent(mGAParams);
            Log.d(AnalyticsConstants.GATAG, mGAParams.toString());
        }

  
    }

    protected void onOutgoingCallStarted(Context ctx, String number, Date start) {
        Log.d("AccessibilityY", "OUtGoing Call");
    }

    protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end) {
        Log.d("AccessibilityY", "Incoming Call Ended");

    }

    protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end) {
        Log.d("AccessibilityY", "OutGoing Call Ended");

    }

    protected void onMissedCall(Context ctx, String number, Date start) {
        Log.d("AccessibilityY", "Missed Call");

    }

    public void onCallStateChanged(Context context, int state, String number) {
        if (lastState == state) {
            //No change, debounce extras
            return;
        }
        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
                isIncoming = true;
                callStartTime = new Date();
                savedNumber = number;
                onIncomingCallStarted(context, number, callStartTime);
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                //Transition of ringing->offhook are pickups of incoming calls.  Nothing done on them
                if (lastState != TelephonyManager.CALL_STATE_RINGING) {
                    isIncoming = false;
                    callStartTime = new Date();
                    onOutgoingCallStarted(context, savedNumber, callStartTime);
                }
                break;
            case TelephonyManager.CALL_STATE_IDLE:
                //Went to idle-  this is the end of a call.  What type depends on previous state(s)
                if (lastState == TelephonyManager.CALL_STATE_RINGING) {
                    //Ring but no pickup-  a miss
                    onMissedCall(context, savedNumber, callStartTime);
                } else if (isIncoming) {
                    onIncomingCallEnded(context, savedNumber, callStartTime, new Date());
                } else {
                    onOutgoingCallEnded(context, savedNumber, callStartTime, new Date());
                }
                break;
        }
        lastState = state;
    }
}
