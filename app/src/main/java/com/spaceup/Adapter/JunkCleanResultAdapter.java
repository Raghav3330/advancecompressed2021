package com.spaceup.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.spaceup.App;
import com.spaceup.R;
import com.spaceup.Utility.UtilityMethods;

/**
 * Created by Ananda.Kumar on 24-03-2017.
 */

public class JunkCleanResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_TRASH_HINT = 0;
    private final int VIEW_TYPE_FEEDBACK = 1;
    private final int VIEW_TYPE_FEEDBACK_POSITIVE = 2;
    private final int VIEW_TYPE_FEEDBACK_NEGATIVE = 3;
    private final int VIEW_TYPE_FEEDBACK_THANK_YOU = 4;
    private final int VIEW_TYPE_COMPRESS_APPS = 5;
    private final int VIEW_TYPE_SHARE_APP = 6;

    private int mItemCount;
    private JunkCleanResultCallBack mCallBack;

    private int mSelectedTextColor;
    private int mBlueColor;
    private int mStrokePxText;
    private int mStrokeColor;

    private enum FeedBackCardState {
        STATE_INITIAL,
        SATE_POSITIVE,
        STATE_NEGATIVE,
        STATE_THANK_YOU,
        STATE_HIDDEN
    }

    private FeedBackCardState mFeedBackCardState;

    public JunkCleanResultAdapter(@NonNull JunkCleanResultCallBack pCallBack) {
        mItemCount = 4;
        mFeedBackCardState = FeedBackCardState.STATE_INITIAL;
        mStrokePxText = (int) UtilityMethods.getInstance().convertDpToPixel(1, App.getInstance());
        mSelectedTextColor = ContextCompat.getColor(App.getInstance(), R.color.app_text_color);
        mBlueColor = ContextCompat.getColor(App.getInstance(), R.color.blue);
        mStrokeColor = ContextCompat.getColor(App.getInstance(), R.color.statusbar);
        mCallBack = pCallBack;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {

            case VIEW_TYPE_TRASH_HINT:
                return new OtherCardHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.junk_clean_hint_card, parent, false));

            case VIEW_TYPE_COMPRESS_APPS:

            case VIEW_TYPE_SHARE_APP:
                return new OtherCardHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.junk_clean_result_card, parent, false));

            case VIEW_TYPE_FEEDBACK:
                View finalScreenRateCard = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.final_screen_rate_card, parent, false);

                return new FeedBackHolder(finalScreenRateCard);

            case VIEW_TYPE_FEEDBACK_POSITIVE:
                View finalScreenPositiveRateCard = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.positive_feedback_card, parent, false);
                return new PositiveFeedBackHolder(finalScreenPositiveRateCard);

            case VIEW_TYPE_FEEDBACK_NEGATIVE:
                View finalScreenNegativeRateCard = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.negative_feedback_layout, parent, false);
                return new NegativeFeedBackHolder(finalScreenNegativeRateCard);

            case VIEW_TYPE_FEEDBACK_THANK_YOU:
                View feedBackThankYouCard = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.feedback_thank_you_card, parent, false);
                return new FeedBackThankYouHolder(feedBackThankYouCard);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case VIEW_TYPE_TRASH_HINT:
                OtherCardHolder otherCardHolder = (OtherCardHolder) holder;
                otherCardHolder.title.setText(App.getInstance().getString(R.string.review_trash_title));
                otherCardHolder.subTitle.setText(App.getInstance().getString(R.string.recover_images_message));
                otherCardHolder.actionButton.setText(App.getInstance().getString(R.string.review_trash_action));
                otherCardHolder.actionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mCallBack.reviewTrash();
                    }
                });
                otherCardHolder.icon.setImageResource(R.drawable.review_trash_illustration);
                break;
            case VIEW_TYPE_COMPRESS_APPS:
                otherCardHolder = (OtherCardHolder) holder;
                otherCardHolder.title.setText(App.getInstance().getString(R.string.compress_apps_title));
                otherCardHolder.subTitle.setText(App.getInstance().getString(R.string.compress_apps_message));
                otherCardHolder.actionButton.setText(App.getInstance().getString(R.string.compress_apps_action));
                otherCardHolder.actionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCallBack.compressApps();
                    }
                });
                otherCardHolder.icon.setImageResource(R.drawable.compress_illustration);
                break;
            case VIEW_TYPE_SHARE_APP:
                otherCardHolder = (OtherCardHolder) holder;
                otherCardHolder.title.setText(App.getInstance().getString(R.string.share_app_title));
                otherCardHolder.subTitle.setText(App.getInstance().getString(R.string.share_app_message));
                otherCardHolder.actionButton.setText(App.getInstance().getString(R.string.share_app_action));
                otherCardHolder.actionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCallBack.shareApp();
                    }
                });
                otherCardHolder.icon.setImageResource(R.drawable.share_illustration);
                break;
            case VIEW_TYPE_FEEDBACK:
                final FeedBackHolder feedBackHolder = (FeedBackHolder) holder;
                feedBackHolder.negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mFeedBackCardState = FeedBackCardState.STATE_NEGATIVE;
                        notifyDataSetChanged();
                    }
                });
                feedBackHolder.positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mFeedBackCardState = FeedBackCardState.SATE_POSITIVE;
                        notifyDataSetChanged();
                    }
                });
                break;
            case VIEW_TYPE_FEEDBACK_POSITIVE:
                final PositiveFeedBackHolder positiveFeedBackHolder = (PositiveFeedBackHolder) holder;
                positiveFeedBackHolder.maybe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mFeedBackCardState = FeedBackCardState.STATE_HIDDEN;
                        mItemCount--;
                        notifyDataSetChanged();
                    }
                });
                positiveFeedBackHolder.sure.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCallBack.openPlayStore();
                    }
                });
                break;
            case VIEW_TYPE_FEEDBACK_NEGATIVE:
                final NegativeFeedBackHolder negativeFeedBackHolder = (NegativeFeedBackHolder) holder;
                negativeFeedBackHolder.difficult.setOnClickListener(mNegativeCauseClickListener);
                negativeFeedBackHolder.appCrashed.setOnClickListener(mNegativeCauseClickListener);
                negativeFeedBackHolder.dataLost.setOnClickListener(mNegativeCauseClickListener);
                negativeFeedBackHolder.slowProcess.setOnClickListener(mNegativeCauseClickListener);
                negativeFeedBackHolder.cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        InputMethodManager inputManager =
                                (InputMethodManager) negativeFeedBackHolder.negativeFeedbackEditText.getContext().
                                        getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(
                                negativeFeedBackHolder.negativeFeedbackEditText.getWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);

                        mFeedBackCardState = FeedBackCardState.STATE_HIDDEN;
                        mItemCount--;
                        notifyDataSetChanged();
                    }
                });
                negativeFeedBackHolder.send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        InputMethodManager inputManager =
                                (InputMethodManager) negativeFeedBackHolder.negativeFeedbackEditText.getContext().
                                        getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(
                                negativeFeedBackHolder.negativeFeedbackEditText.getWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);

                        mCallBack.sendNegativeFeedback(negativeFeedBackHolder.difficult.isSelected(),
                                negativeFeedBackHolder.appCrashed.isSelected(), negativeFeedBackHolder.dataLost.isSelected(),
                                negativeFeedBackHolder.slowProcess.isSelected(),
                                negativeFeedBackHolder.negativeFeedbackEditText.getText().toString());
                        mFeedBackCardState = FeedBackCardState.STATE_THANK_YOU;
                        notifyDataSetChanged();
                    }
                });
                break;
        }
    }

    private final View.OnClickListener mNegativeCauseClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.slow_process:
                case R.id.difficult:
                case R.id.app_crashed:
                case R.id.data_lost:
                    if (v.isSelected()) {
                        GradientDrawable drawable = (GradientDrawable) v.getBackground();
                        drawable.setColor(Color.TRANSPARENT);
                        drawable.setStroke(mStrokePxText, mStrokeColor);
                        ((TextView) v).setTextColor(mSelectedTextColor);
                        v.setSelected(false);
                    } else {
                        GradientDrawable drawable = (GradientDrawable) v.getBackground();
                        drawable.setColor(mBlueColor);
                        drawable.setStroke(0, Color.TRANSPARENT);
                        ((TextView) v).setTextColor(Color.WHITE);
                        v.setSelected(true);
                    }
                    break;
            }
        }
    };

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return VIEW_TYPE_TRASH_HINT;
            case 1:
                switch (mFeedBackCardState) {
                    case STATE_INITIAL:
                        return VIEW_TYPE_FEEDBACK;
                    case SATE_POSITIVE:
                        return VIEW_TYPE_FEEDBACK_POSITIVE;
                    case STATE_NEGATIVE:
                        return VIEW_TYPE_FEEDBACK_NEGATIVE;
                    case STATE_THANK_YOU:
                        return VIEW_TYPE_FEEDBACK_THANK_YOU;
                    case STATE_HIDDEN:
                        return VIEW_TYPE_COMPRESS_APPS;
                }
                break;
            case 2:
                switch (mFeedBackCardState) {
                    case STATE_INITIAL:
                    case SATE_POSITIVE:
                    case STATE_NEGATIVE:
                    case STATE_THANK_YOU:
                        return VIEW_TYPE_COMPRESS_APPS;
                    case STATE_HIDDEN:
                        return VIEW_TYPE_SHARE_APP;
                }
                break;
            case 3:
                switch (mFeedBackCardState) {
                    case STATE_INITIAL:
                    case SATE_POSITIVE:
                    case STATE_NEGATIVE:
                    case STATE_THANK_YOU:
                        return VIEW_TYPE_SHARE_APP;
                    case STATE_HIDDEN:
                        break;
                }
                break;
        }
        return position;
    }

    @Override
    public int getItemCount() {
        return mItemCount;
    }

    public interface JunkCleanResultCallBack {

        void reviewTrash();

        void compressApps();

        void shareApp();

        void openPlayStore();

        void sendNegativeFeedback(boolean difficultSelected, boolean appCrashedSelected, boolean dataLostSelected,
                                  boolean slowProcessSelected, String reason);
    }

    private static class OtherCardHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView subTitle;
        TextView actionButton;
        ImageView icon;

        OtherCardHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            subTitle = (TextView) itemView.findViewById(R.id.subtitle);
            actionButton = (TextView) itemView.findViewById(R.id.action);
            icon = (ImageView) itemView.findViewById(R.id.icon);
        }
    }

    private static class FeedBackHolder extends RecyclerView.ViewHolder {
        TextView negative;
        TextView positive;

        FeedBackHolder(View itemView) {
            super(itemView);
            negative = (TextView) itemView.findViewById(R.id.negative);
            positive = (TextView) itemView.findViewById(R.id.positive);
        }
    }

    private static class NegativeFeedBackHolder extends RecyclerView.ViewHolder {
        TextView send;
        TextView cancel;
        TextView difficult;
        TextView appCrashed;
        TextView dataLost;
        TextView slowProcess;
        EditText negativeFeedbackEditText;

        NegativeFeedBackHolder(View itemView) {
            super(itemView);
            send = (TextView) itemView.findViewById(R.id.send);
            cancel = (TextView) itemView.findViewById(R.id.cancel);
            difficult = (TextView) itemView.findViewById(R.id.difficult);
            appCrashed = (TextView) itemView.findViewById(R.id.app_crashed);
            dataLost = (TextView) itemView.findViewById(R.id.data_lost);
            slowProcess = (TextView) itemView.findViewById(R.id.slow_process);
            negativeFeedbackEditText = (EditText) itemView.findViewById(R.id.negative_feedback_editText);
        }
    }

    private static class PositiveFeedBackHolder extends RecyclerView.ViewHolder {
        TextView sure;
        TextView maybe;

        PositiveFeedBackHolder(View itemView) {
            super(itemView);
            sure = (TextView) itemView.findViewById(R.id.sure);
            maybe = (TextView) itemView.findViewById(R.id.maybe);
        }
    }

    private static class FeedBackThankYouHolder extends RecyclerView.ViewHolder {
        FeedBackThankYouHolder(View itemView) {
            super(itemView);
        }
    }
}
