package com.spaceup.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.spaceup.R;
import com.spaceup.Utility.PrefManager;

public class FolderDiscoveryPopup extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PrefManager.getInstance(getApplicationContext()).putBoolean(PrefManager.SEEN_MANAGE_APPS_TUTORIAL, true);
        setContentView(R.layout.activity_folder_disovery_popup);
        TextView open_button = (TextView) findViewById(R.id.open_button);
        open_button.setOnClickListener(this);
        ImageView image_shortcut = (ImageView) findViewById(R.id.image_shortcut);
        image_shortcut.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        finish();
        startActivity(new Intent(FolderDiscoveryPopup.this, ManageApps.class));

    }
}
